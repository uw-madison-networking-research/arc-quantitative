#!/bin/bash

SCRIPTPATH=`readlink -f $0`
BASEDIR=`dirname $SCRIPTPATH`

if [[ $# -lt 2 ]]; then
    echo "Usage: $0 <topology-smt2> <policy-smt2> ..."
    exit 1
fi

TOPOLOGY=$1
shift
POLICIES=$@

Z3="z3"
Z3_ARGS="-in"


COMMON="$BASEDIR/common.smt2"
CLASSES="$BASEDIR/reachable.smt2 $BASEDIR/waypoint.smt2 $BASEDIR/blocked.smt2"
SOLVE="$BASEDIR/solve.smt2"

cat $COMMON $CLASSES $TOPOLOGY $POLICIES $SOLVE | $Z3 $Z3_ARGS
