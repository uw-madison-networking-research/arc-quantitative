;REACHABLE
;Entities
(define-sort N () (_ BitVec 4)) ;Edge/path numbers
(declare-fun edgenum (V V) N)
(declare-fun pathnum (V V) N)

;Numbered edges must be used
(assert (forall ((v1 V) (v2 V))
         (=> (not (= (edgenum v1 v2) #x0))
          (edge v1 v2)
          )))

;Numbered path
(assert (forall ((v1 V) (v3 V))
         (=> (not (= (pathnum v1 v3) #x0))
          (exists ((v2 V))
           (or (and (= (edgenum v1 v2) (pathnum v1 v3))
                    (= (edgenum v2 v3) (pathnum v1 v3)))
               (and (= (edgenum v1 v2) (pathnum v1 v3))
                    (= (pathnum v2 v3) (pathnum v1 v3))) ))
           )))

;No sharing of edges between paths -- assumes paths don't contain a vertex twice
(assert (forall ((v1 V))
         (not (exists ((v2 V) (v3 V))
               (and (distinct v2 v3)
               (and (not (= (edgenum v2 v1) #x0))
                    (= (edgenum v3 v1) (edgenum v2 v1))))
              ))))

