;COMMON
;Use multiples of 4 for bitvectors to allow hex constants

;Entities
(define-sort V () (_ BitVec 4)) ;Vertex
(declare-fun edge (V V) Bool) ;Is there an edge between two vertices?
(declare-fun posedge (V V) Bool) ;Can there be an edge between two vertices?
(declare-const src V) ;Src vertex
(declare-const dst V) ;Dst vertex

;Only allow possible edges
(assert (forall ((v1 V) (v2 V))
         (=> (edge v1 v2) 
          (posedge v1 v2)
          )))

