;TOPOLOGY: TESTING
;Possible edges
(assert (forall ((v1 V) (v2 V))
         (=> (posedge v1 v2)
          (or (and (= v1 #x1) (= v2 #x2))
          (or (and (= v1 #x1) (= v2 #x7))
          (or (and (= v1 #x2) (= v2 #x3))
          (or (and (= v1 #x3) (= v2 #x4))
          (or (and (= v1 #x3) (= v2 #x5))
          (or (and (= v1 #x4) (= v2 #x6))
          (or (and (= v1 #x5) (= v2 #x6))
              (and (= v1 #x7) (= v2 #x5))
              )))))))
         )))

;Src & dst vertices
(assert (= src #x1))
(assert (= dst #x6))

