;WAYPOINT
;Entities
(declare-fun nwedge (V V) Bool)
(declare-fun nwpath (V V) Bool)
(declare-fun wedge (V V) Bool)
(declare-fun wpath (V V) Bool)

;Used edges must be labeled
(assert (forall ((v1 V) (v2 V))
         (=> (edge v1 v2)
          (or (nwedge v1 v2) (wedge v1 v2))
         )))

;Labeled edges must be used
(assert (forall ((v1 V) (v2 V))
         (=> (nwedge v1 v2)
          (edge v1 v2)
         )))
(assert (forall ((v1 V) (v2 V))
         (=> (wedge v1 v2)
          (edge v1 v2)
         )))

;Edge can only be non-waypoint or waypoint
(assert (forall ((v1 V) (v2 V))
         (and (=> (nwedge v1 v2) (not (wedge v1 v2)))
              (=> (wedge v1 v2) (not (nwedge v1 v2))) ;redundant?
          )))

;Non-waypoint path
(assert (forall ((v1 V) (v3 V))
         (=> (nwpath v1 v3) 
          (exists ((v2 V)) 
           (or (and (nwedge v1 v2) 
                    (nwedge v2 v3)) 
               (and (nwedge v1 v2) 
                    (nwpath v2 v3)))) 
          )))

;Waypoint path -- FIXME: what about adding another wedge to a wpath?
(assert (forall ((v1 V) (v3 V))
         (=> (wpath v1 v3) 
          (or (and (wedge v1 v3) 
                   (not (nwpath v1 v3)))
              (exists ((v2 V)) 
               (and
                (or (and (nwedge v1 v2)
                         (wpath v2 v3))
                    (and (wpath v1 v2)  
                         (nwedge v2 v3))) 
                (and (not (nwpath v1 v3))
                     (not (nwedge v1 v3))))))
          )))

