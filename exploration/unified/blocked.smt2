;BLOCKED
;Entities
(declare-fun path (V V) Bool)

;Path base case
(assert (forall ((v1 V) (v2 V))
        (=> (edge v1 v2) 
         (path v1 v2))))

;Path inductive case
(assert (forall ((v1 V) (v2 V) (v3 V))
         (=> (and (edge v1 v2) (path v2 v3))
          (path v1 v3))))

