#/bin/bash

if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`"
CONFIGDIR="$BASEDIR/../../configs/snapshots"

ls --color=none $CONFIGDIR | while read NETWORK; do
    SNAPSHOT=`ls --color=none $CONFIGDIR/$NETWORK | head -n 1`
    ls --color=none $CONFIGDIR/$NETWORK/$SNAPSHOT | while read CONFIG; do
        grep "set community" $CONFIGDIR/$NETWORK/$SNAPSHOT/$CONFIG > /dev/null
        if [ $? -eq 0 ]; then
            echo $NETWORK $SNAPSHOT $CONFIG
        fi
    done
done
