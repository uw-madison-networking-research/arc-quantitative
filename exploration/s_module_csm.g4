//https://www.cisco.com/c/en/us/td/docs/interfaces_modules/services_modules/csm/4-2-x/configuration/guide/config/getstart.html

s_module_csm
:
   MODULE CONTENTSWITCHINGMODULE slot = DEC NEWLINE
   (
      s_module_csm_null
   )*
;

s_module_csm_null
:
   (
      FT
      | NATPOOL
      | POLICY
      | PROBE
      | SCRIPT
      | SERVERFARM
      | STICKY
      | VLAN
      | VSERVER
      | XML_CONFIG
   ) ~NEWLINE NEWLINE
   (
      NO?
      (
         EXPECT
         | FAILED
         | GATEWAY
         | INSERVICE
         | INTERVAL
         | IP
         | NAT
         | OPEN
         | PORT
         | PREEMPT
         | PRIORITY
         | REAL
         | RECEIVE
         | REQUEST
         | RETRIES
         | ROUTE
         | SCRIPT
      ) ~NEWLINE* NEWLINE
   )*
;
