(declare-datatypes () ((V A B C)))
(declare-fun edge (V V) Bool)
(declare-fun path (V V) Bool)

;No circular edges
(assert (forall ((v1 V)) (not (edge v1 v1))))

(assert (exists ((v1 V) (v2 V) (v3 V)) 
         (=> (and (edge v1 v2) (edge v2 v3)) (path v1 v3))))
;         (=> (path v1 v3) (and (edge v1 v2) (edge v2 v3)))))


(assert (not (edge B A)))
(assert (not (edge C A)))
(assert (not (edge C B)))
(assert (not (edge A C)))

(assert (path A C))
(check-sat)
(get-model)
