(define-sort V () (_ BitVec 4))
(define-sort N () (_ BitVec 2))
(declare-fun posedge (V V) Bool)
(declare-fun edgenum (V V) N)
(declare-fun pathnum (V V) N)

;Possible edges
(assert (forall ((v1 V) (v2 V))
         (=> (posedge v1 v2)
          (or (and (= v1 #x1) (= v2 #x2))
          (or (and (= v1 #x1) (= v2 #x7))
          (or (and (= v1 #x2) (= v2 #x3))
          (or (and (= v1 #x3) (= v2 #x4))
          (or (and (= v1 #x3) (= v2 #x5))
          (or (and (= v1 #x5) (= v2 #x6))
              (and (= v1 #x7) (= v2 #x5))
              ))))))
         )))

;Only allow possible edges
(assert (forall ((v1 V) (v2 V))
         (=> (not (= (edgenum v1 v2) #x0))
          (posedge v1 v2))))

;Paths must have a valid number
;(assert (forall ((v1 V) (v2 V))
;         (=> (path v1 v2) 
;          (not (= (pathnum v1 v2) #x0))
;          )))

;Numbered path base case
;(assert (forall ((v1 V) (v2 V))
;         (= (edgenum v1 v2) (pathnum v1 v2))))

;Numbered path
;(assert (forall ((v1 V) (v3 V))
;         (=> (not (= (pathnum v1 v3) #x0))
;          (exists ((v2 V))
;           (and (not (= (edgenum v1 v2) #x0))
;                (= (edgenum v1 v2) (edgenum v2 v3)))))))

;           (or (and (not (= (edgenum v1 v2) #x0))
;                    (= (edgenum v1 v2) (edgenum v2 v3)))
;               (and (not (= (edgenum v1 v2) #x0))
;                    (= (edgenum v1 v2) (pathnum v2 v3)))))
;           )))

;(assert (posedge #x2 #x3))
;(assert (posedge #x3 #x4))
(assert (= (edgenum #x2 #x3) #x1))
(assert (= (edgenum #x3 #x4) #x1))
;(assert (= (pathnum #x2 #x4) #x1))
(check-sat)
(get-model)
