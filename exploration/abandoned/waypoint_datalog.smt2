(set-option :fixedpoint.engine datalog)
(define-sort V () (_ BitVec 4))
(declare-rel nwedge (V V))
(declare-rel nwpath (V V))
(declare-rel wedge (V V))
(declare-rel wpath (V V))
(declare-var v1 V)
(declare-var v2 V)
(declare-var v3 V)

; Non-waypoint path base rule
(rule (=>
       (and (nwedge v1 v2) (nwedge v2 v3) )
       (nwpath v1 v3) ))

; Non-waypoint path inductive rule
(rule (=>
       (and (nwedge v1 v2) (nwpath v2 v3) )
       (nwpath v1 v3) ))

; Waypoint path base rule
(rule (=>
       (and (wedge v1 v2) (not (nwpath v1 v2)) )
       (wpath v1 v2) ))

; Waypoint path inductive rules
(rule (=>
       (and (and (and (nwedge v1 v2) (wpath v2 v3) ) 
             (not (nwpath v1 v3)) ) (not (nwedge v1 v3)) )
       (wpath v1 v3) ))
(rule (=>
       (and (and (and (wpath v1 v2) (nwedge v2 v3) ) 
             (not (nwpath v1 v3)) ) (not (nwedge v1 v3)) )
       (wpath v1 v3) ))


(rule (nwedge #x1 #x2))
(rule (nwedge #x1 #x7))
(rule (nwedge #x2 #x3))
(rule (nwedge #x3 #x4))
(rule (wedge #x3 #x5))
(rule (nwedge #x5 #x6))
(rule (nwedge #x7 #x5))

(query (nwpath #x1 #x4))
(query (wpath #x2 #x6))
(query (wpath #x1 #x6))
