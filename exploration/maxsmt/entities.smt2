;ENTITIES
;Use multiples of 4 for bitvectors to allow hex constants
(define-sort V () (_ BitVec 4)) ;Vertex
(declare-fun edge (V V) Bool) ;Is there an edge between two vertices?

