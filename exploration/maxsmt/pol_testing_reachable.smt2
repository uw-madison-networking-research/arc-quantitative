;Reachability policy
(assert (exists ((v1 V) (v2 V))
         (and (distinct v1 v2)
         (and (distinct (pathnum v1 dst) (pathnum v2 dst) #x0)
         (and (= (edgenum src v1) (pathnum v1 dst))
              (= (edgenum src v2) (pathnum v2 dst)))))
        ))
