;Waypoint policy
(assert (not (nwpath src dst)))

(assert-soft (nwedge #x1 #x2))
(assert-soft (nwedge #x1 #x7))
(assert-soft (nwedge #x2 #x3))
(assert-soft (nwedge #x3 #x4))
(assert-soft (nwedge #x5 #x6))
(assert-soft (nwedge #x7 #x5))
(assert-soft (wedge #x3 #x5))

