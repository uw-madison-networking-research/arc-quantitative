#!/bin/bash

SCRIPTPATH=`readlink -f $0`
BASEDIR=`dirname $SCRIPTPATH`

if [[ $# -lt 1 ]]; then
    echo "Usage: $0 <topology-smt2> [<policy-smt2>] ..."
    exit 1
fi

TOPOLOGY=$1
shift
POLICIES=$@

Z3="z3"
Z3_ARGS="-in"


ENTITIES="$BASEDIR/entities.smt2"
CLASSES="$BASEDIR/common.smt2 $BASEDIR/reachable.smt2 $BASEDIR/waypoint.smt2 $BASEDIR/blocked.smt2"
SOLVE="$BASEDIR/solve.smt2"
TMP="/tmp/repair.smt2"

cat $ENTITIES $TOPOLOGY $CLASSES $POLICIES $SOLVE > $TMP
cat $TMP | $Z3 $Z3_ARGS
