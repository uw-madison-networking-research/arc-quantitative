;TOPOLOGY: TESTING
;Possible edges
(define-fun posedge ((v1 V) (v2 V)) Bool
    (or (and (= v1 #x1) (= v2 #x2))
    (or (and (= v1 #x1) (= v2 #x7))
    (or (and (= v1 #x2) (= v2 #x3))
    (or (and (= v1 #x3) (= v2 #x4))
    (or (and (= v1 #x3) (= v2 #x5))
    (or (and (= v1 #x4) (= v2 #x6))
    (or (and (= v1 #x5) (= v2 #x6))
        (and (= v1 #x7) (= v2 #x5))
        )))))))
    )

;Src & dst vertices
(define-fun src () V #x1)
(define-fun dst () V #x6)

;Existing edges
;(assert-soft (edge #x1 #x2))
;(assert-soft (edge #x1 #x7))
;(assert-soft (edge #x2 #x3))
;(assert-soft (edge #x3 #x4))
;(assert-soft (edge #x3 #x5))
;(assert-soft (edge #x5 #x6))
;(assert-soft (edge #x7 #x5))
;(assert-soft (not (edge #x4 #x6)))

