;COMMON
;Only allow possible edges
(assert (forall ((v1 V) (v2 V))
         (=> (edge v1 v2) 
          (posedge v1 v2)
          )))

