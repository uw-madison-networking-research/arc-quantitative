(define-sort V () (_ BitVec 4))
(declare-fun posedge (V V) Bool)
(declare-fun nwedge (V V) Bool)
(declare-fun nwpath (V V) Bool)
(declare-fun wedge (V V) Bool)
(declare-fun wpath (V V) Bool)

;Possible edges
(assert (forall ((v1 V) (v2 V))
         (=> (posedge v1 v2)
          (or (and (= v1 #x1) (= v2 #x2))
          (or (and (= v1 #x1) (= v2 #x7))
          (or (and (= v1 #x2) (= v2 #x3))
          (or (and (= v1 #x3) (= v2 #x4))
          (or (and (= v1 #x3) (= v2 #x5))
          (or (and (= v1 #x5) (= v2 #x6))
              (and (= v1 #x7) (= v2 #x5))
              ))))))
         )))

;No circular edges -- unnecessary when providing list of possible edges
;(assert (forall ((v1 V)) 
;         (not (nwedge v1 v1)) ))
;(assert (forall ((v1 V)) 
;         (not (wedge v1 v1)) ))

;Only allow possible edges
(assert (forall ((v1 V) (v2 V))
         (=> (nwedge v1 v2) (posedge v1 v2)) ))
(assert (forall ((v1 V) (v2 V))
         (=> (wedge v1 v2) (posedge v1 v2)) ))

;Edge can only be non-waypoint or waypoint
(assert (forall ((v1 V) (v2 V))
         (and (=> (nwedge v1 v2) (not (wedge v1 v2)))
              (=> (wedge v1 v2) (not (nwedge v1 v2))) ;redundant?
             )))

;Non-waypoint path
(assert (forall ((v1 V) (v3 V))
         (=> (nwpath v1 v3) 
          (exists ((v2 V)) 
           (or (and (nwedge v1 v2) 
                    (nwedge v2 v3)) 
               (and (nwedge v1 v2) 
                    (nwpath v2 v3)))) 
         )))

;Waypoint path
(assert (forall ((v1 V) (v3 V))
         (=> (wpath v1 v3) 
          (or (and (wedge v1 v3) 
                   (not (nwpath v1 v3)))
              (exists ((v2 V)) 
               (and
                (or (and (nwedge v1 v2)
                         (wpath v2 v3))
                    (and (wpath v1 v2)
                         (nwedge v2 v3)))
                (and (not (nwpath v1 v3))
                     (not (nwedge v1 v3))))))
              )))



(assert (wpath #x1 #x6))
(check-sat)
(get-model)
