;COMMON
;Use multiples of 4 for bitvectors to allow hex constants

;Entities
(define-sort V () (_ BitVec 4)) ;Vertex
(declare-fun edge (V V) Bool) ;Is there an edge between two vertices?
(declare-fun posedge (V V) Bool) ;Can there be an edge between two vertices?
(declare-const src V) ;Src vertex
(declare-const dst V) ;Dst vertex

;Only allow possible edges
(assert (forall ((v1 V) (v2 V))
         (=> (edge v1 v2) 
          (posedge v1 v2)
          )))

;BLOCKED
;Entities
(declare-fun path (V V) Bool)

;Path base case
(assert (forall ((v1 V) (v2 V))
        (=> (edge v1 v2) 
         (path v1 v2))))

;Path inductive case
(assert (forall ((v1 V) (v2 V) (v3 V))
         (=> (and (edge v1 v2) (path v2 v3))
          (path v1 v3))))

;TOPOLOGY: TESTING
;Possible edges
(assert (forall ((v1 V) (v2 V))
         (=> (posedge v1 v2)
          (or (and (= v1 #x1) (= v2 #x2))
          (or (and (= v1 #x1) (= v2 #x7))
          (or (and (= v1 #x2) (= v2 #x3))
          (or (and (= v1 #x3) (= v2 #x4))
          (or (and (= v1 #x3) (= v2 #x5))
          (or (and (= v1 #x4) (= v2 #x6))
          (or (and (= v1 #x5) (= v2 #x6))
              (and (= v1 #x7) (= v2 #x5))
              )))))))
         )))

;Src & dst vertices
(assert (= src #x1))
(assert (= dst #x6))

;Required edges
(assert (edge #x1 #x2))
(assert (edge #x1 #x7))
(assert (edge #x2 #x3))
(assert (edge #x3 #x4))
(assert (edge #x5 #x6))
(assert (edge #x7 #x5))

;Blocked policy
(assert (not (path src dst)))

;SOLVE
;Find repair
(check-sat)
(get-model)
