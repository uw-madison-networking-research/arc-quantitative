# Set directories
if (!exists("basedir")) {
    basedir <- dirname(normalizePath(sys.frame(1)$ofile))
}
datadir <- paste(basedir,'../../../output/bulk_logs',logpath,sep='/')
plotsdir <- paste(basedir,'../../../output/plots',logpath,sep='/')

# Load data
modtimesfile <- paste(datadir,'mod_times.csv',sep='/')
modtimes <- read.table(modtimesfile, sep=",", header=TRUE)

# Adjust labels
#rownames(modtimes) <- modtimes$Snapshot
modtimes$Snapshot <- as.character(modtimes$Snapshot)
modtimes$Network <- sapply(strsplit(modtimes$Snapshot, "/"), "[", 1)
modtimes$Timestamp <- sapply(strsplit(modtimes$Snapshot, "/"), "[", 2)

# Define plot colors
safecolorsfive <- c('#d7191c','#fdae61','#ffffbf','#abdda4','#2b83ba')
safecolorsfour <- safecolorsfive[c(1,2,4,5)]
safecolorsthree <- c('#fc8d59','#ffffbf','#99d594')
safecolorsthree <- safecolorsfive[c(1,2,5)]
