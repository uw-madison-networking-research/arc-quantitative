#!/usr/bin/perl

# PURPOSE: Extract ETG comparison results from a log file.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Cwd 'abs_path';
use File::Basename;

# Get directories and files
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

my $outputdir = "$basedir/../../output";
my $logsdir = "$outputdir/changes";

# Get arguments
my %lognames = (per_tc => 'compare_per-tc.log', all_tcs => 'compare_all-tcs.log', per_dst => 'compare_per-dst.log', per_src => 'compare_per-src.log', isolated => 'compare_isolated.log', per_pair => 'compare_per-pair.log', manual => 'compare-etgs.log');

# Get list of networks
opendir(D, $logsdir) or die("Could not open $logsdir");
my @networks = sort(readdir(D));
shift @networks;
shift @networks;

# Process all networks
foreach my $network (@networks) {

    # Get list of snapshots
    my $networkdir = "$logsdir/$network";
    opendir(D, $networkdir) or die("Could not open $networkdir");
    my @snapshots = sort(readdir(D));
    shift @snapshots;
    shift @snapshots;

    # Process all snapshots
	foreach my $currsnap (@snapshots) {
        print "$network/$currsnap\n";

        # Extract comparisons
        my $etgCount = {};
        my $cetgCount = {};
        my $aetgCount = {};
        my $retgCount = {};
        my $diffs = {};
        my $globaldiffs = {};
        foreach my $modifier (sort (keys %lognames)) {
            my $logname = $lognames{$modifier};

            # Make sure log file exists
            my $logfile = "$networkdir/$currsnap/$logname";
            if (not -e $logfile) {
                next;
            }
            # Find start and end of comparisons output
            my $startline = `grep -n "*** Compare ETGs" $logfile | cut -f1 -d':' | tr -d '\n'`;
            if ($startline eq "") {
                next;
            }
            $startline += 2;
            my $endline = `wc -l $logfile | cut -f1 -d' ' | tr -d '\n'`;
            my $length = $endline - $startline + 1;
            print "$modifier\tstart:$startline end:$endline\n";
            
            # Do nothing if comparisons weren't found
            if ($length < 0) {
                next;
            }

            # Extract relevant lines of output
            my $comparisons = `tail -n +$startline $logfile | head -n $length`;

            # Get number of ETGs
            my $etgs = `grep -E "Need to generate" $logfile`;
            $etgs =~ s/[^0-9]//g;
            $etgCount->{$modifier} = $etgs;

            # Get number of comparison ETGs
            my $cetgs = `grep -E "COUNT: comparisonETGs" $logfile`;
            $cetgs =~ s/[^0-9]//g;
            $cetgCount->{$modifier} = $cetgs;


            # Store the comparisons
            print "$comparisons";
            my @lines = split("\n",$comparisons);
            shift @lines;
            my $tc = "";
            $diffs->{$modifier} = {};
            $aetgCount->{$modifier} = 0;
            $retgCount->{$modifier} = 0;
            foreach my $line (@lines) {
                $line =~ s/^\s+|\s+$//g;
                if ($line =~ /^ADD ETG/) {
                    $aetgCount->{$modifier} += 1;
                    next;
                }
                if ($line =~ /^REMOVE ETG/) {
                    $retgCount->{$modifier} += 1;
                    next;
                }
                if ($line =~ /^((ADD)|(REMOVE))/) {
                    if (!exists($diffs->{$modifier}->{$tc})) {
                        $diffs->{$modifier}->{$tc} = {};
                    }
                    my $flag = 1;
                    if ($line =~ /REMOVE/) {
                        $flag = -1;
                    }
                    $diffs->{$modifier}->{$tc}->{$line} = $flag;
                } else {
                    $tc = $line;
                }
            }

            # Compute the global comparisons
            my $numComp = ($etgs < $cetgs ? $etgs : $cetgs);
            my $numDiffs = scalar(keys(%{$diffs->{$modifier}}));
            print "\t$etgs $cetgs $numComp $numDiffs\n";
            $globaldiffs->{$modifier} = {};
            if ($numDiffs == $numComp) {
                print "Global\n";
                my $anytc = (keys %{$diffs->{$modifier}})[0];
                foreach my $diff (sort (keys %{$diffs->{$modifier}->{$anytc}})){
                    my $isglobal = $diffs->{$modifier}->{$anytc}->{$diff};
                    foreach my $tc (sort (keys %{$diffs->{$modifier}})) {
                        if (!exists($diffs->{$modifier}->{$tc}->{$diff})) {
                            $isglobal = 0;
                            last;
                        }
                    }
                    if ($isglobal) {
                        $globaldiffs->{$modifier}->{$diff} = $isglobal;
                        print "\t$diff\n";
                        foreach my $tc (sort (keys %{$diffs->{$modifier}})) {
                            $diffs->{$modifier}->{$tc}->{$diff} = 0;
                        }
                    }
                }
            }
        }

        # Ignore if we don't have output from any modifiers
        if (scalar(keys %{$diffs}) < 1) {
            next;
        }

        # Count the number of ETGs generated
        print "\tNum ETGs: ";
        foreach my $modifier (sort (keys %{$etgCount})) {
            print "$modifier=$etgCount->{$modifier} ";
        }
        print "\n";

        # Count the number of diff ETGs
        print "\tDiff ETGs: ";
        foreach my $modifier (sort (keys %{$diffs})) {
            my $count = scalar(keys %{$diffs->{$modifier}});
            print "$modifier=$count ";
        }
        print "\n";

        # Count the number of added ETGs
        print "\tAdded ETGs: ";
        foreach my $modifier (sort (keys %{$aetgCount})) {
            print "$modifier=$aetgCount->{$modifier} ";
        }
        print "\n";

        # Count the number of removed ETGs
        print "\tRemoved ETGs: ";
        foreach my $modifier (sort (keys %{$retgCount})) {
            print "$modifier=$retgCount->{$modifier} ";
        }
        print "\n";

        # Count the number of diffs across all ETGs
        print "\tDiff Edges: ";
        foreach my $modifier (sort (keys %{$diffs})) {
            my $count = 0;
            foreach my $tc (sort (keys %{$diffs->{$modifier}})) {
                foreach my $diff (sort (keys %{$diffs->{$modifier}->{$tc}})) {
                    if ($diffs->{$modifier}->{$tc}->{$diff} != 0) {
                        $count += 1;
                    }
                }
            }
            print "$modifier=$count ";
        }
        print "\n";

        # Count the number of global diffs
        print "\tDiff Edges Global: ";
        foreach my $modifier (sort (keys %{$globaldiffs})) {
            my $count = scalar(keys %{$globaldiffs->{$modifier}});
            print "$modifier=$count ";
        }
        print "\n";


    }
}
