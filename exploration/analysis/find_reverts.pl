#!/usr/bin/perl

# PURPOSE: Look for a change in a policy for a traffic class which is reverted
#          in the subsequent snapshot where policies are changed.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;

# Get directories and files
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

my $outputdir = "$basedir/../../output";
my $logsdir = "$outputdir/logs";

my %policies = (blocked => 'Always Blocked', reachable => 'Always Reachable');

# Get arguments
my $snapshotsfile = "";
GetOptions ("snapshots=s" => \$snapshotsfile);
print "Analyzing snapshots listed in $snapshotsfile\n";

# Handle each snapshot in the list
open sfh, "$snapshotsfile" or die("ERROR: Could not open $snapshotsfile");
my $prevnetwork;
my $prevsnapshot;
my $prevpolicies;
<sfh>; #HACK
while(<sfh>) {
    # Parse line
    chomp $_;
    my @parts = split("/", $_);
    if (scalar(@parts) != 2) {
        die("Invalid line of data: $_");
    }
    my $network = $parts[0];
    my $snapshot = $parts[1];
    print "$network $snapshot\n";

    my $currpolicies = {};

    # Load and compare policies if this is not the first stapshot for a network
    if ($prevnetwork eq $network) {
        # Extract policies
        foreach my $policy (sort (keys %policies)) {
#            print "$policy\n";
            $currpolicies->{$policy} = {};
            my $policylog = "$logsdir/$network/$snapshot/$policy.diff";
            open pfh, "$policylog" or next;
            while (<pfh>) {
                chomp $_;
                # Always consider current policy state
                if ($_ =~ /^>/) {
                    my @cols = split("\t", $_);
                    my $applies = $cols[1];
                    my $tc = $cols[2];
#                    print "\t$applies $tc\n";
                    $currpolicies->{$policy}->{$tc} = $applies;
                }
                # Consider prev policy state if prev policies were not loaded
                if (0 == scalar(keys %{$prevpolicies}) and $_ =~ /^</) {
                    my @cols = split("\t", $_);
                    my $applies = $cols[1];
                    my $tc = $cols[2];
#                    print "\t*$applies $tc\n";
                    $prevpolicies->{$policy}->{$tc} = $applies;
                }
            }
            close pfh;
        }

        # Check for policy reversion
        foreach my $policy (sort (keys %policies)) {
            my $currpolicyset = $currpolicies->{$policy};
            my $prevpolicyset = $prevpolicies->{$policy};
            foreach my $tc (sort (keys %{$currpolicyset})) {
                if (exists $prevpolicyset->{$tc}
                        and $prevpolicyset->{$tc} ne $currpolicyset->{$tc}) {
                    print "\tREVERT-$policy $prevpolicyset->{$tc}"."->"
                         ."$currpolicyset->{$tc} $tc\n";
                }
            }
        }
    }

    # Store previous network, snapshot, and policies
    $prevnetwork = $network;
    $prevsnapshot = $snapshot;
    $prevpolicies = $currpolicies;
}
close sfh;
