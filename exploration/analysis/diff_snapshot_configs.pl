#!/usr/bin/perl

# PURPOSE: Diff configurations from consecutive logical snapshots of the 
#          configurations for all devices in a network.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;

# Get directories and files
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

my $snapshotsdir = "$basedir/../../configs";
$snapshotsdir = "/shared/configs/uwmadison/snapshots_6hr";
my $outputdir = "$basedir/../../output";
my $logsdir = "$outputdir/logs";
$logsdir = "../logs";
my $changesdir = "$outputdir/changes";
$changesdir = ".";

# Get arguments
my $snapshotsfile = "";
GetOptions ("snapshots=s" => \$snapshotsfile);
print "Diffing snapshots listed in $snapshotsfile\n";

# Handle each snapshot in the list
open sfh, "$snapshotsfile" or die("ERROR: Could not open $snapshotsfile");
my $prevnetwork;
my $prevsnapshot;
#<sfh>; #HACK
while(<sfh>) {
    # Parse line
    chomp $_;
    my @parts = split("/", $_);
    if (scalar(@parts) != 2) {
        die("Invalid line of data: $_");
    }
    my $network = $parts[0];
    my $snapshot = $parts[1];
#    print "$network $snapshot\n";

    # If the same network as previous snapshot, then diff with previous snapshot
    if ($prevnetwork eq $network) {
        print "Diff $network $prevsnapshot $snapshot\n";
        my $prevsnapshotdir = "$snapshotsdir/$network/$prevsnapshot";
        my $currsnapshotdir = "$snapshotsdir/$network/$snapshot";

        # Get list of devices
        opendir(D, $prevsnapshotdir) or die("Could not open $prevsnapshotdir");
        my @devices = sort(readdir(D));
        close(D);
        shift @devices;
        shift @devices;

        # Diff all devices
        my $hasdiff = 0;
        foreach my $device (@devices) {
            print "\t$device\n";
            my $difffile = "$logsdir/$network/$snapshot/$device.diff";
            `diff -u $prevsnapshotdir/$device $currsnapshotdir/$device > $difffile\n`;
            my $diffsize = `wc -l $difffile | cut -f1 -d' ' | tr -d '\n'`;
            if ($diffsize > 0) {
                $hasdiff = 1;
            }
        }
        
        # If configs differ, then link into changes directory
        if ($hasdiff) {
            `mkdir -p "$changesdir/$network"`;
            `ln -s "../$logsdir/$network/$snapshot" "$changesdir/$network/$snapshot"`;
            `ln -s "../$snapshot/policies.obj" "$logsdir/$network/$prevsnapshot/nextpolicies.obj"`;
        }
    }

    # Store previous network and snapshot
    $prevnetwork = $network;
    $prevsnapshot = $snapshot;
}
close sfh;
