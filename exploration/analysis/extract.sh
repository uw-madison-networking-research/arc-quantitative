#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`"

# Set parameters
OUTPUTDIR="$BASEDIR/../../output"
LOGDIR="$OUTPUTDIR/logs"
BULKLOGDIR="$OUTPUTDIR/bulk_logs"
if [ "$#" -lt 1 ]; then
    echo "Provide relative log path"
    exit 1
fi
LOGPATH=$1
LOGDIR="$LOGDIR/$LOGPATH"
if [ ! -d $LOGDIR ]; then
    echo "Log directory $LOGDIR does not exist"
    exit 1
fi
BULKLOGDIR="$BULKLOGDIR/$LOGPATH"
mkdir -p $BULKLOGDIR

mkdir -p $OUTPUTDIR/plots/$LOGPATH

echo "Extracting checkpolicies..."
$BASEDIR/extract_checkpolicies.pl $LOGPATH > $BULKLOGDIR/checkpolicies.csv
echo "Extracting mods..."
$BASEDIR/extract_mods.pl $LOGPATH > $BULKLOGDIR/mods.log
$BASEDIR/summarize_mods.pl -mods $BULKLOGDIR/mods.log > $BULKLOGDIR/mods.csv
echo "Extracting mod times..."
$BASEDIR/extract_mod_times.pl $LOGPATH > $BULKLOGDIR/mod_times.csv
