#!/bin/bash

SCRIPTPATH=`readlink -f $0`
BASEDIR="`dirname $SCRIPTPATH`"
SNAPSHOTSDIR="$BASEDIR/../../output/changes"
CONFIGDIR="/mnt/internal1/configs/microsoft/snapshots_ratul"
COPYDIR="/mnt/internal1/configs/microsoft/cpr_data"

# Process each network
ls $SNAPSHOTSDIR | while read NETWORK; do
    echo $NETWORK
    mkdir -p $COPYDIR/$NETWORK

    # Process each snapshot
    ls $SNAPSHOTSDIR/$NETWORK | while read TIMESTAMP; do
        cp -L -r $CONFIGDIR/$NETWORK/$TIMESTAMP $COPYDIR/$NETWORK/
    done
done
