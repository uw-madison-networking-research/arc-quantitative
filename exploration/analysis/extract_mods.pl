#!/usr/bin/perl

# PURPOSE: Extract computed graph modifications from a log file.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Cwd 'abs_path';
use File::Basename;

# Determine script path
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

# Constants
my $basetc = "* -> *";

# Set paramaters
my $outputdir = "$basedir/../../output";
if ($#ARGV < 0) {
    die("Must provide relative path to logs directory");
}
my $logsdir = "$outputdir/logs/$ARGV[0]";
if (! -d $logsdir) {
    die("Log directory $logsdir does not exist");
}
my $logprefix = "repair_";
#$logprefix = "repair_filtered_"; # Uncomment if repair logs are filtered
my $logsuffix = ".log";
my @repairtypes = ('all-tcs','per-dst');

# Get list of networks
opendir(D, $logsdir) or die("Could not open $logsdir");
my @networks = sort(readdir(D));
shift @networks;
shift @networks;

# Process all networks
foreach my $network (@networks) {
    # Get list of snapshots
    my $networkdir = "$logsdir/$network";
    opendir(D, $networkdir) or die("Could not open $networkdir");
    my @snapshots = sort(readdir(D));
    shift @snapshots;
    shift @snapshots;

    # Process all snapshots
	foreach my $currsnap (@snapshots) {
        print "$network/$currsnap\n";

        # Extract modifications
        my $pgCount = {};
        my $brokenCount = {};
        my $mods = {};
        my $basemods = {};
        my $dstmods = {};
        my @seentypes = ();
        foreach my $modifier (sort @repairtypes) {
            # Make sure log file exists
            my $logname = $logprefix.$modifier.$logsuffix;
            my $logfile = "$networkdir/$currsnap/$logname";
            if (not -e $logfile) {
                next;
            }

            # Find start and end of modifications output
            my $startline = `grep -n "Modifications:" $logfile | cut -f1 -d':' | tr -d '\n'`;
            if ($startline eq "") {
                next;
            }
            $startline;
            my $endline = `tail -n +$startline $logfile | grep -n "TIME: graphModification" | head -n 1 | cut -f1 -d':' | tr -d '\n'`;
            if ($endline eq "") {
                $endline = `wc -l $logfile | cut -f1 -d' ' | tr -d '\n'`;
            }
            else {
                $endline += $startline - 2;
            }
            my $length = $endline - $startline + 1;
            print "\t$modifier\tstart:$startline end:$endline\n";
            
            # Do nothing if modifications weren't found
            if ($length < 1) {
                next;
            }
            push(@seentypes, $modifier); 

            # Extract relevant lines of output
            my $modifications = `tail -n +$startline $logfile | head -n $length | grep -v CHANGE`;

            # Get number of policy groups
            my $pgs = `grep -E "COUNT: separatePolicyGroups" $logfile`;
            $pgs =~ s/[^0-9]//g;
            $pgCount->{$modifier} = $pgs;

            # Get number of broken flows
            my $broken = `grep -E "COUNT: brokenFlows" $logfile`;
            $broken =~ s/[^0-9]//g;
            $brokenCount->{$modifier} = $broken;

            print "$modifier $modifications";
            my @lines = split("\n",$modifications);
            shift @lines;
            my $tc = "";
            my $dst = 0;
            my $base = 0;
            $mods->{$modifier} = {};
            if ($modifier eq "all_tcs" or $modifier eq "per_dst") {
                $dstmods->{$modifier} = {};
                $basemods->{$modifier} = {};
            }
            foreach my $line (@lines) {
                $line =~ s/^\s+|\s+$//g;
                if ($line =~ /^(CHANGE)/) {
                } elsif ($line =~ /^((ADD)|(REMOVE))/) {
                    if ($base) {
                        # Static route removals for base don't count, because
                        # base should never contain static routes in the first
                        # place, but it does as a hack
                        if ($line =~ /static/) {
                            next;
                        }
                        $basemods->{$modifier}->{$tc}->{$line} = 1;
                    } elsif ($dst) {
                        $dstmods->{$modifier}->{$tc}->{$line} = 1;
                    } else {
                        my $flag = 1;
                        if ($line =~ /(Base)|(Dst)/) {
                            $flag = 0;
                        }
                        $mods->{$modifier}->{$tc}->{$line} = $flag;
                    }
                } elsif ($line =~ /^\* -> \*/) {
                    $tc = $line;
                    $base = 1;
                    $dst = 0;
                    $basemods->{$modifier}->{$tc} = {};
                } elsif ($line =~ /^\* -> /) {
                    $tc = $line;
                    $base = 0;
                    $dst = 1;
                    $dstmods->{$modifier}->{$tc} = {};
                } else {
                    $tc = $line;
                    $base = 0;
                    $dst = 0;
                    $mods->{$modifier}->{$tc} = {};
                }
            }

            # Remove basetc if all changes were static route removals
            if (scalar(keys %{$basemods->{$modifier}->{$basetc}}) == 0) {
                delete($basemods->{$modifier}->{$basetc});
            }
        }


        # Ignore if we don't have output from any modifiers
        if (scalar(@seentypes) < 1) {
            next;
        }

        # Identify base mods
        foreach my $modifier (sort (keys %{$dstmods})) {
            # Java code computes base mods for all_tcs
            if ($modifier eq "all_tcs") {
                next;
            }

            $basemods->{$modifier}->{$basetc} = {};
            # All dsts must be modified
            if (scalar($dstmods->{$modifier}) == $pgCount->{$modifier}) {
                # Every dst must have the mod for it to be a base mod, so pick  
                # a random dst and check all of its mods
                my $anydst = (keys %{$dstmods->{$modifier}})[0];
                foreach my $mod (sort (keys %{$dstmods->{$modifier}->{$anydst}})){
                    # Check if every dst has the same mod
                    my $isbase = 1;
                    foreach my $dst (sort (keys %{$dstmods->{$modifier}})) {
                        if (!exists($dstmods->{$modifier}->{$dst}->{$mod})) {
                            $isbase = 0;
                            last;
                        }
                    }
                    # Set as dst mod, if every dst has the same mod
                    if ($isbase) {
                        $basemods->{$modifier}->{$basetc}->{$mod} = 1;
                        print "\t\t$mod\n";
                        foreach my $dst (sort (keys %{$dstmods->{$modifier}})) {
                            $dstmods->{$modifier}->{$dst}->{$mod} = 0;
                        }
                    }
                }
            }
        }

        # Output counts
        print "\tPgCount: ";
        foreach my $modifier (sort (keys %{$pgCount})) {
            my $count = $pgCount->{$modifier};
            print "$modifier=$count ";
        }
        print "\n";
        print "\tBrokenCount: ";
        foreach my $modifier (sort (keys %{$brokenCount})) {
            my $count = $brokenCount->{$modifier};
            print "$modifier=$count ";
        }
        print "\n";

        # Output whether base was changed
        my $baseChanges = {};
        print "\tBaseEtgs:";
        foreach my $modifier (sort (keys %{$basemods})) {
            my @tcs = keys %{$basemods->{$modifier}};
            my $count = scalar(@tcs);    
            print " $modifier=$count";
        }
        print "\n";

        # Output base change counts
        print "\tBaseMods $basetc:";
        foreach my $modifier (@seentypes) {
            my $totalMods = scalar(keys %{$basemods->{$modifier}->{$basetc}});
            print " $modifier=$totalMods";
        }
        printf "\n";

        # Output the number of dst ETGs changed
        my $dstChanges = {};
        print "\tDstEtgs:";
        foreach my $modifier (@seentypes) {
            my @tcs = keys %{$dstmods->{$modifier}};
            my $count = scalar(@tcs);    
            print " $modifier=$count";
            foreach my $tc (@tcs) {
                $dstChanges->{$tc} = 1;
            }
        }
        print "\n";

        # Output dst change counts
        foreach my $dst (sort (keys %{$dstChanges})) {
            print "\tDstMods $dst:";
            foreach my $modifier (sort (keys %{$dstmods})) {
                # Count the number of mods made to dst that aren't actually
                # mods made to base
                my $uniqueMods = 0;
                foreach my $mod (sort (keys %{$dstmods->{$modifier}->{$dst}})) {
                    $uniqueMods += $dstmods->{$modifier}->{$dst}->{$mod};
                }
                my $totalMods = scalar(keys %{$dstmods->{$modifier}->{$dst}});
                print " $modifier=$totalMods $uniqueMods";
            }
            printf "\n";
        }

        # Output the number of flow ETGs changed
        my $etgChanges = {};
        print "\tFlowEtgs:";
        foreach my $modifier (sort (keys %{$mods})) {
            my @tcs = keys %{$mods->{$modifier}};
            my $count = scalar(@tcs);    
            print " $modifier=$count";
            foreach my $tc (@tcs) {
                $etgChanges->{$tc} = 1;
            }
        }
        print "\n";

        # Output the number of changes to each flow ETG
        foreach my $tc (sort (keys %{$etgChanges})) {
            print "\tFlowMods $tc:";
            foreach my $modifier (sort (keys %{$mods})) {
                # Count the number of mods made to flow that aren't actually
                # mods made to all flows with the same dst
                my $uniqueMods = 0;
                foreach my $mod (sort (keys %{$mods->{$modifier}->{$tc}})) {
                    $uniqueMods += $mods->{$modifier}->{$tc}->{$mod};
                }
                my $totalMods = scalar(keys %{$mods->{$modifier}->{$tc}});
                print " $modifier=$totalMods $uniqueMods";
            }
            print "\n";
        }
    }
}
