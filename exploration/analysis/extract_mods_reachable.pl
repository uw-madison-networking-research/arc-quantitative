#!/usr/bin/perl

# PURPOSE: Extract computed graph modifications ONLY FOR REACHABLE POLICIES 
# from a log file.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Cwd 'abs_path';
use File::Basename;

# Get directories and files
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

my $outputdir = "$basedir/../../output";
my $logsdir = "$outputdir/changes";

# Get arguments
my %lognames = (per_tc => 'repair_per-tc_reachable.log', isolated => 'repair_isolated_reachable.log');

# Get list of networks
opendir(D, $logsdir) or die("Could not open $logsdir");
my @networks = sort(readdir(D));
shift @networks;
shift @networks;

# Process all networks
foreach my $network (@networks) {

    # Get list of snapshots
    my $networkdir = "$logsdir/$network";
    opendir(D, $networkdir) or die("Could not open $networkdir");
    my @snapshots = sort(readdir(D));
    shift @snapshots;
    shift @snapshots;

    # Process all snapshots
	foreach my $currsnap (@snapshots) {
        print "$network/$currsnap\n";

        # Extract modifications
        my $mods = {};
        my $globalmods = {};
        foreach my $modifier (sort (keys %lognames)) {
            my $logname = $lognames{$modifier};

            # Make sure log file exists
            my $logfile = "$networkdir/$currsnap/$logname";
            if (not -e $logfile) {
                next;
            }
            # Find start and end of modifications output
            my $startline = `grep -n "Modifications:" $logfile | cut -f1 -d':' | tr -d '\n'`;
            if ($startline eq "") {
                next;
            }
            $startline;
            my $endline = `tail -n +$startline $logfile | grep -n "TIME:" | cut -f1 -d':' | tr -d '\n'`;
            if ($endline eq "") {
                $endline = `wc -l $logfile | cut -f1 -d' ' | tr -d '\n'`;
            }
            else {
                $endline += $startline - 2;
            }
            my $length = $endline - $startline + 1;
            print "\t$modifier\tstart:$startline end:$endline\n";
            
            # Do nothing if modifications weren't found
            if ($length < 1) {
                next;
            }

            # Extract relevant lines of output
            my $modifications = `tail -n +$startline $logfile | head -n $length`;
            print "$modifier $modifications";
            my @lines = split("\n",$modifications);
            shift @lines;
            my $tc = "";
            my $global = 0;
            $mods->{$modifier} = {};
            if ($modifier eq "all_tcs" or $modifier eq "per_dst") {
                $globalmods->{$modifier} = {};
            }
            foreach my $line (@lines) {
                $line =~ s/^\s+|\s+$//g;
                if ($line =~ /^((ADD)|(REMOVE))/) {
                    if ($global) {
                        $globalmods->{$modifier}->{$tc}->{$line} = 1;
                    } else {
                        my $flag = 1;
                        if ($line =~ /Global/) {
                            $flag = 0;
                        }
                        $mods->{$modifier}->{$tc}->{$line} = $flag;
                    }
                } elsif ($line =~ /^Global/) {
                    $tc = $line;
                    $global = 1;
                    $globalmods->{$modifier}->{$tc} = {};
                } else {
                    $tc = $line;
                    $global = 0;
                    $mods->{$modifier}->{$tc} = {};
                }
            }
        }

        # Don't compare if we only have output from one modifier
        if (scalar(keys %{$mods}) <= 1) {
            next;
        }

        # Compute the global mods for isolated
        if (exists($mods->{isolated})) {
            $globalmods->{isolated}->{Global} = {};
            my $anytc = (keys %{$mods->{isolated}})[0];
            foreach my $mod (sort (keys %{$mods->{isolated}->{$anytc}})) {
                my $isglobal = 1;
                foreach my $tc (sort (keys %{$mods->{isolated}})) {
                    if (!exists($mods->{isolated}->{$tc}->{$mod})) {
                        $isglobal = 0;
                        last;
                    }
                }
                if ($isglobal) {
                    $globalmods->{isolated}->{Global}->{$mod} = 1;
                    print "\t\t$mod\n";
                    foreach my $tc (sort (keys %{$mods->{isolated}})) {
                        $mods->{isolated}->{$tc}->{$mod} = 0;
                    }
                }
            }
        }


        # Compute the global mods for per_tc
        if (exists($mods->{per_tc})) {
            $globalmods->{per_tc}->{Global} = {};
            my $anytc = (keys %{$mods->{per_tc}})[0];
            foreach my $mod (sort (keys %{$mods->{per_tc}->{$anytc}})) {
                my $isglobal = 1;
                foreach my $tc (sort (keys %{$mods->{per_tc}})) {
                    if (!exists($mods->{per_tc}->{$tc}->{$mod})) {
                        $isglobal = 0;
                        last;
                    }
                }
                if ($isglobal) {
                    $globalmods->{per_tc}->{Global}->{$mod} = 1;
                    print "\t\t$mod\n";
                    foreach my $tc (sort (keys %{$mods->{per_tc}})) {
                        $mods->{per_tc}->{$tc}->{$mod} = 0;
                    }
                }
            }
        }


        # Compute the global mods for per_dst
        if (exists($globalmods->{per_dst})) {
            $globalmods->{per_dst}->{Global} = {};
            my $anytc = (keys %{$globalmods->{per_dst}})[0];
            foreach my $mod (sort (keys %{$globalmods->{per_dst}->{$anytc}})) {
                my $isglobal = 1;
                foreach my $tc (sort (keys %{$globalmods->{per_dst}})) {
                    if ($tc eq 'Global') {
                        next;
                    }
                    if (!exists($globalmods->{per_dst}->{$tc}->{$mod})) {
                        $isglobal = 0;
                        last;
                    }
                }
                if ($isglobal) {
                    $globalmods->{per_dst}->{Global}->{$mod} = 1;
                    print "\t\t$mod\n";
                    foreach my $tc (sort (keys %{$globalmods->{per_dst}})) {
                        if ($tc eq 'Global') {
                            next;
                        }
                        $globalmods->{per_dst}->{$tc}->{$mod} = 0;
                    }
                }
            }
        }

        # Compute the global mods for per_src
        if (exists($globalmods->{per_src})) {
            $globalmods->{per_src}->{Global} = {};
            my $anytc = (keys %{$globalmods->{per_src}})[0];
            foreach my $mod (sort (keys %{$globalmods->{per_src}->{$anytc}})) {
                my $isglobal = 1;
                foreach my $tc (sort (keys %{$globalmods->{per_src}})) {
                    if ($tc eq 'Global') {
                        next;
                    }
                    if (!exists($globalmods->{per_src}->{$tc}->{$mod})) {
                        $isglobal = 0;
                        last;
                    }
                }
                if ($isglobal) {
                    $globalmods->{per_src}->{Global}->{$mod} = 1;
                    print "\t\t$mod\n";
                    foreach my $tc (sort (keys %{$globalmods->{per_src}})) {
                        if ($tc eq 'Global') {
                            next;
                        }
                        $globalmods->{per_src}->{$tc}->{$mod} = 0;
                    }
                }
            }
        }

        # Compare the number of global
        my $globalChanges = {};
        print "\tGlobals: ";
        foreach my $modifier (sort (keys %{$globalmods})) {
            my @tcs = keys %{$globalmods->{$modifier}};
            my $count = scalar(@tcs);    
            print "$modifier=$count ";
            foreach my $tc (@tcs) {
                $globalChanges->{$tc} = 1;
            }
        }
        print "\n";

        # Output global change counts
        foreach my $tc (sort (keys %{$globalChanges})) {
            print "\t$tc";
            if (exists($globalmods->{isolated})) {
                my $totalIso = scalar(keys %{$globalmods->{isolated}->{$tc}});
                print " isolated=$totalIso";
            }
            if (exists($globalmods->{per_tc})) {
                my $totalTc = scalar(keys %{$globalmods->{per_tc}->{$tc}});
                print " per_tc=$totalTc";
            }
            if (exists($globalmods->{per_dst})) {
                my $localDst = 0;
                foreach my $mod (sort (keys %{$globalmods->{per_dst}->{$tc}})) {
                    $localDst += $globalmods->{per_dst}->{$tc}->{$mod};
                }
                my $totalDst = scalar(keys %{$globalmods->{per_dst}->{$tc}});
                print " per_dst=$totalDst $localDst";
            }
            if (exists($globalmods->{per_src})) {
                my $localSrc = 0;
                foreach my $mod (sort (keys %{$globalmods->{per_src}->{$tc}})) {
                    $localSrc += $globalmods->{per_src}->{$tc}->{$mod};
                }
                my $totalSrc = scalar(keys %{$globalmods->{per_src}->{$tc}});
                print " per_src=$totalSrc $localSrc";
            }
            if (exists($globalmods->{all_tcs})) {
                my $totalAll = scalar(keys %{$globalmods->{all_tcs}->{$tc}});
                print " all_tcs=$totalAll";
            }
            printf "\n";
        }

        # Compare the number of ETGs changed
        my $etgChanges = {};
        print "\tETGs Changed: ";
        foreach my $modifier (sort (keys %{$mods})) {
            my @tcs = keys %{$mods->{$modifier}};
            my $count = scalar(@tcs);    
            print "$modifier=$count ";
            foreach my $tc (@tcs) {
                $etgChanges->{$tc} = 1;
            }
        }
        print "\n";

        # Compare the changes to each ETG
        foreach my $tc (sort (keys %{$etgChanges})) {
            print "\tETG:";
            if (exists($mods->{per_tc}) and exists($mods->{all_tcs})) {
                my $sameTcAll = 0;
                my $sameTcIso = 0;
                foreach my $mod (sort (keys %{$mods->{per_tc}->{$tc}})) {
                    if (exists($mods->{all_tcs}->{$tc}->{$mod})) {
                        $sameTcAll += 1;
                    }
                }
                print " same_tc_all=$sameTcAll";
            }
            if (exists($mods->{per_tc}) and exists($mods->{isolated})) {
                my $sameTcIso = 0;
                foreach my $mod (sort (keys %{$mods->{per_tc}->{$tc}})) {
                    if (exists($mods->{isolated}->{$tc}->{$mod})) {
                        $sameTcIso += 1;
                    }
                }
                print "same_tc_iso=$sameTcIso";
            }
            if (exists($mods->{per_dst}) and exists($mods->{all_tcs})) {
                my $sameDstAll = 0;
                foreach my $mod (sort (keys %{$mods->{per_dst}->{$tc}})) {
                    if (exists($mods->{all_tcs}->{$tc}->{$mod})) {
                        $sameDstAll += 1;
                    }
                }
                print " same_dst_all=$sameDstAll";
            }
            if (exists($mods->{per_dst}) and exists($mods->{isolated})) {
                my $sameDstIso = 0;
                foreach my $mod (sort (keys %{$mods->{per_dst}->{$tc}})) {
                    if (exists($mods->{isolated}->{$tc}->{$mod})) {
                        $sameDstIso += 1;
                    }
                }
                print " same_dst_iso=$sameDstIso";
            }

            if (exists($mods->{isolated})) {
                my $localIso = 0;
                foreach my $mod (sort (keys %{$mods->{isolated}->{$tc}})) {
                    if (!exists($globalmods->{isolated}->{Global}->{$mod})) {
                        $localIso += $mods->{isolated}->{$tc}->{$mod};
                    }
                }
                my $totalIsolated = scalar(keys %{$mods->{isolated}->{$tc}});
                print " isolated=$totalIsolated $localIso";
            }
            if (exists($mods->{per_tc})) {
                my $localTc = 0;
                foreach my $mod (sort (keys %{$mods->{per_tc}->{$tc}})) {
                    if (!exists($globalmods->{per_tc}->{Global}->{$mod})) {
                        $localTc += $mods->{per_tc}->{$tc}->{$mod};
                    }
                }
                my $totalPerTc = scalar(keys %{$mods->{per_tc}->{$tc}});
                print " per_tc=$totalPerTc $localTc";
            }
            if (exists($mods->{per_dst})) {
                my $localDst = 0;
                foreach my $mod (sort (keys %{$mods->{per_dst}->{$tc}})) {
                    if (!exists($globalmods->{per_dst}->{Global}->{$mod})) {
                        $localDst += $mods->{per_dst}->{$tc}->{$mod};
                    }
                }
                my $totalPerDst = scalar(keys %{$mods->{per_dst}->{$tc}});
                print " per_dst=$totalPerDst $localDst";
            }
            if (exists($mods->{per_src})) {
                my $localSrc = 0;
                foreach my $mod (sort (keys %{$mods->{per_src}->{$tc}})) {
                    if (!exists($globalmods->{per_src}->{Global}->{$mod})) {
                        $localSrc += $mods->{per_src}->{$tc}->{$mod};
                    }
                }
                my $totalPerSrc = scalar(keys %{$mods->{per_src}->{$tc}});
                print " per_src=$totalPerSrc $localSrc";
            }
            if (exists($mods->{all_tcs})) {
                my $localAll = 0;
                foreach my $mod (sort (keys %{$mods->{all_tcs}->{$tc}})) {
                    $localAll += $mods->{all_tcs}->{$tc}->{$mod};
                }
                my $totalAllTcs = scalar(keys %{$mods->{all_tcs}->{$tc}});
                print " all_tcs=$totalAllTcs $localAll\n";
            }
            print "\n";
        }
    }

}
