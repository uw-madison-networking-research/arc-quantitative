#! /bin/bash

RAWDIR=$1
UNCOMPDIR=$2
echo $RAWDIR $UNCOMPDIR

ls --color=none $RAWDIR | while read RCSFILE; do
    # Determine device
    DEVICE=`echo $RCSFILE | sed -e 's/.conf,v//'`
    echo $DEVICE

    # Create directory for device's configs
    mkdir $UNCOMPDIR/$DEVICE

    # Obtain versions of device's config
    VERSIONS=`rlog $RAWDIR/$RCSFILE | grep -e "^revision [1-9]\." | \
        sed -e 's/revision //'`
    echo "$VERSIONS" | while read VERSION; do
        TIMESTAMP=`rlog $RAWDIR/$RCSFILE | \
            grep -e "^revision $VERSION$" -A 1 | grep -e "^date: " | \
            cut -f1 -d';' | sed -e 's/date: //' |  sed -e 's#[/: ]#-#g'`
        echo $VERSION $TIMESTAMP
        co -p$VERSION $RAWDIR/$RCSFILE > $UNCOMPDIR/$DEVICE/$TIMESTAMP.cfg \
            2>/dev/null
    done
done
