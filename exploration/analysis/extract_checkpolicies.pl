#!/usr/bin/perl

# PURPOSE: Extract policy violation information.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Cwd 'abs_path';
use File::Basename;
use List::Util qw(sum max);

# Determine script path
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

# Set parameters
my $outputdir = "$basedir/../../output";
if ($#ARGV < 0) {
    die("Must provide relative path to logs directory");
}
my $logsdir = "$outputdir/logs/$ARGV[0]";
if (! -d $logsdir) {
    die("Log directory $logsdir does not exist");
}
my $logname = "checkpolicies.log";
my %countNames = (Dsts => 'policyGroups', Tcs => 'flows', 
    Policies => 'policies', TcsWithPolicies => 'flowsWithPolicies', 
    BlockedPolicies => 'policiesALWAYS_BLOCKED', 
    ReachablePolicies => 'policiesALWAYS_REACHABLE',
    DstsWithViolations => 'destinationsWithViolations',
    Violated => 'policiesViolated',
    BlockedViolated => 'violatedALWAYS_BLOCKED', 
    ReachableViolated => 'violatedALWAYS_REACHABLE',
    Devices => 'devices');

# Print file header
print "Snapshot";
foreach my $name (sort (keys %countNames)) {
    print ",$name"
}
print "\n";

# Get list of networks
opendir(D, $logsdir) or die("Could not open $logsdir");
my @networks = sort(readdir(D));
shift @networks;
shift @networks;

# Process all networks
foreach my $network (@networks) {
    # Get list of snapshots
    my $networkdir = "$logsdir/$network";
    opendir(D, $networkdir) or die("Could not open $networkdir");
    my @snapshots = sort(readdir(D));
    shift @snapshots;
    shift @snapshots;

    # Process all snapshots
	foreach my $currsnap (@snapshots) {
        # Make sure log file exists
        my $logfile = "$networkdir/$currsnap/$logname";
        if (not -e $logfile) {
            next;
        }

        # Extract relevant lines of output
        print "$network/$currsnap";
        foreach my $name (sort (keys %countNames)) {
            my $keyword = $countNames{$name};
            my $countRaw = `grep "COUNT: $keyword " $logfile`;
            chomp $countRaw;
            $countRaw =~ m/$keyword (\d+)/;
            print ",$1";
        }

        print "\n";
    }

}
