#!/usr/bin/perl

# PURPOSE: Summarize ETG comparisons.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;

# Get directories and files
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

my $outputdir = "$basedir/../../output";
my $logsdir = "$outputdir/changes";

# Get arguments
my $diffsfile = "";
GetOptions ("diffs=s" => \$diffsfile);
#print "Summarizing mods in $diffsfile\n";

my %modifiers = (all_tcs => 'All', per_dst => 'Dst', per_src => 'Src', per_pair => 'Pair', per_tc => 'Tc', isolated => 'Iso', manual => 'Man');

# Print column meanings
print "Snapshot,EtgCount,AdjustedEtgCount";
foreach my $modifier (sort (keys %modifiers)) {
    print ",EtgsDiff$modifiers{$modifier}";
} 
foreach my $modifier (sort (keys %modifiers)) {
    print ",EdgesDiff$modifiers{$modifier}";
} 
foreach my $modifier (sort (keys %modifiers)) {
    print ",GlobalEdgesDiff$modifiers{$modifier}";
} 
print "\n";

# Handle each line in the mods file 
open mfh, "$diffsfile" or die("ERROR: Could not open $diffsfile");
my $currsnapshot;
my $havedata = 0;
my $numEtgs = '';
my $addedEtgs = 0;
my $etg = {};
my $edge = {};
my $edgeglobal = {};
for my $modifier (keys %modifiers) {
    $etg->{$modifier} = '';
    $edge->{$modifier} = '';
    $edgeglobal->{$modifier} = '';
}

while(<mfh>) {
    chomp $_;

    # Check for snapshot
    if ($_ =~ m/\//) {
        # Switching snapshots
        # Output stats for prior snapshot
        if ($havedata) {
            my $adjustedNumEtgs = $numEtgs - $addedEtgs;
            print "$currsnapshot,$numEtgs,$adjustedNumEtgs";
            foreach my $modifier (sort (keys %modifiers)) {
                print ",$etg->{$modifier}";
            }
            foreach my $modifier (sort (keys %modifiers)) {
                print ",$edge->{$modifier}";
            }
            foreach my $modifier (sort (keys %modifiers)) {
                print ",$edgeglobal->{$modifier}";
            }
            print "\n";
        }

        $currsnapshot = $_;
        $havedata = 0;
        $numEtgs = '';
        $addedEtgs = 0;
        foreach my $modifier (keys %modifiers) {
            $etg->{$modifier} = '';
            $edge->{$modifier} = '';
            $edgeglobal->{$modifier} = '';
        }
    }

    # Check for ETGs counts
    if ($_ =~ m/Num ETGs:/) {
        foreach my $modifier (keys %modifiers) {
            if ($_ =~ m/$modifier=(\d+)/) {
                $numEtgs = $1;
                last;
            }
        }
    }

    # Get ETG count adjustment
    if ($_ =~ m/Added ETGs:/) {
        foreach my $modifier (keys %modifiers) {
            if ($modifier eq "manual") {
                next;
            }
            if ($_ =~ m/$modifier=(\d+)/) {
                $addedEtgs = $1;
                last;
            }
        }
    }

    # Check for diff ETGs counts
    if ($_ =~ m/Diff ETGs:/) {
        foreach my $modifier (keys %modifiers) {
            if ($_ =~ m/$modifier=(\d+)/) {
                $etg->{$modifier} = $1;
            }
        }
        $havedata = 1;
    }
    
    # Check for diff edges counts
    if ($_ =~ m/Diff Edges:/) {
        foreach my $modifier (keys %modifiers) {
            if ($_ =~ m/$modifier=(\d+)/) {
                $edge->{$modifier} = $1;
            }
        }
    }

    # Check for diff edges global counts
    if ($_ =~ m/Diff Edges Global:/) {
        foreach my $modifier (keys %modifiers) {
            if ($_ =~ m/$modifier=(\d+)/) {
                $edgeglobal->{$modifier} = $1;
            }
        }
    }
}
close mfh;
