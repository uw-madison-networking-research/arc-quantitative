#!/usr/bin/perl

# PURPOSE: Diff policies dervied from consecutive logical snapshots of the 
#          configurations for all devices in a network.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Cwd 'abs_path';
use File::Basename;

# Get directories and files
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

my $outputdir = "$basedir/../../output";
$outputdir = "/shared/output";
my $logsdir = "$outputdir/logs";

# Get arguments
my $logname = "blocked-reachable.log";
#$logname = "policies.log";
my %policies = (groups => 'Extract Policy Groups', 
    policy_blocked => 'AlwaysBlocked', policy_reachable => 'AlwaysReachable');

# Get list of networks
opendir(D, $logsdir) or die("Could not open $logsdir");
my @networks = sort(readdir(D));
shift @networks;
shift @networks;
@networks = ('dist');

# Process all networks
foreach my $network (@networks) {
#    print "$network\n";

    # Get list of snapshots
    my $networkdir = "$logsdir/$network";
    opendir(D, $networkdir) or die("Could not open $networkdir");
    my @snapshots = sort(readdir(D));
    shift @snapshots;
    shift @snapshots;

    # Process all snapshots
    my $prevsnap = undef;
	foreach my $currsnap (@snapshots) {
#        print "\t$currsnap\n";

        # Extract verification output for all policies
        my $logfile = "$networkdir/$currsnap/$logname";
        my $hastcs = 0;
        my %diffsize = {};
        my $groupexc = "cat";
        foreach my $policy (sort (keys %policies)) {
            my $policytitle = $policies{$policy};

            # Find start and end of verification output for policy
            my $startline = `grep -n -F "*** $policytitle " $logfile | head -n 1 | cut -f1 -d':' | tr -d '\n'`;
            if ($startline eq "") {
                next;
            }
            $startline += 1;
            my $endline = `tail --lines=+$startline $logfile | grep -n -F "TIME:" | head -n 1 | cut -f1 -d':' | tr -d '\n'`;
            if ($endline eq "") {
                $endline = `tail --lines=+$startline $logfile | grep -n -F "*** " | head -n 1 | cut -f1 -d':' | tr -d '\n'`;
                if ($endline eq "") {
                    #$endline = `wc -l $logfile | cut -f1 -d' ' | tr -d '\n'`;
                    $endline = 0;
                }
            }
            $endline += $startline-2;

            my $length = $endline - $startline + 1;
#            print "\t\t$policy start:$startline end:$endline\n";
            
            my $policyfile = "$networkdir/$currsnap/$policy.log";

            # Do nothing if verification output was empty or unexpected
            if ($length <= 1) {
                `touch $policyfile`;
                next;
            }

            # Extract relevant lines of output
            if ($policy ne "groups") {
                $hastcs = 1;
            }
            `tail -n +$startline $logfile | head -n $length | grep -v 'Parsing' > $policyfile`;

            # Compare with previous snapshot
            if (defined $prevsnap) {
                # Diff with previous verification output
                my $prevpolicyfile = "$networkdir/$prevsnap/$policy.log";
                my $difffile = "$networkdir/$currsnap/$policy.diff";
                `diff $prevpolicyfile $policyfile | egrep "<|>" | $groupexc > $difffile`;

                $diffsize{$policy} = `wc -l $difffile | cut -f1 -d' ' | tr -d '\n'`;

                # Determine which TCs changed, so they can be excluded from the
                # list of policy differences
                if ($policy eq "groups" and $diffsize{groups} > 0) {
                    $groupexc = `egrep "<|>" $difffile | cut -f2 | cut -f1 -d' ' | paste -sd "|" - | tr -d '\n' | sed -e 's/|/)|(/g'`;
#                    print "Diff TCs $network/$currsnap $groupexc\n"; 
                    $groupexc = "egrep -v \"($groupexc)\"";
#                    print "$groupexc\n";
                }
            } else {
                $diffsize{$policy} = -1;
            }
        }

        if ($diffsize{policy_blocked} > 0 or $diffsize{policy_reachable} > 0) {
            print "$network/$currsnap\n";
        }

        if (not defined $prevsnap) {
            # Ignore the network if its first snapshot lacks traffic classes
            if (not $hastcs) {
                last;
            }
            # Otherwise, the first snapshot is always of interest
            print "$network/$currsnap\n";
        }
        $prevsnap = $currsnap;
    }
}
