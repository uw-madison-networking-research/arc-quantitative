#!/usr/bin/perl

# PURPOSE: Summarize differences between computed graph modifications.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;

# Determine script path
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

# Set paramaters
my %repairtypes = (All => 'all-tcs', Dst => 'per-dst');

# Get arguments
my $modsfile = "";
GetOptions ("mods=s" => \$modsfile);
#print "Summarizing mods in $modsfile\n";

# Print headings
print "Snapshot";
for my $modifier (sort (keys %repairtypes)) {
    print ",BaseEtgs$modifier,BaseEdges$modifier,DstEtgs$modifier"
        .",DstEdges$modifier,FlowEtgs$modifier,FlowEdges$modifier";
}
print "\n";

# Handle each line in the mods file 
open mfh, "$modsfile" or die("ERROR: Could not open $modsfile");
my $currsnapshot;
my $havedata = 0;
my $baseEtgs = {};
my $baseEdges = {};
my $dstEtgs = {};
my $dstEdges = {};
my $flowEtgs = {};
my $flowEdges = {};
my $brokenCount = 0;
while(<mfh>) {
    chomp $_;

    # Check for snapshot
    if ($_ =~ m/\//) {
        # Switching snapshots
        # Output stats for prior snapshot
        if ($havedata) {
            print "$currsnapshot";
            for my $modifier (sort (keys %repairtypes)) {
                print ",$baseEtgs->{$modifier},$baseEdges->{$modifier}"
                    .",$dstEtgs->{$modifier},$dstEdges->{$modifier}"
                    .",$flowEtgs->{$modifier},$flowEdges->{$modifier}";
                delete $baseEtgs->{$modifier};
                delete $baseEdges->{$modifier};
                delete $dstEtgs->{$modifier};
                delete $dstEdges->{$modifier};
                delete $flowEtgs->{$modifier};
                delete $flowEdges->{$modifier};
            }
            print "\n";
        }

        $currsnapshot = $_;
        $havedata = 0;
    }

    # Check for Broken counts
    if ($_ =~ m/BrokenCount:/) {
        $havedata = 1;
        for my $modifier (sort (keys %repairtypes)) {
            if ($_ =~ m/$repairtypes{$modifier}=(\d+)/) {
                $brokenCount = $1;
            }
        }
    }

    # Check for Base ETG counts
    if ($_ =~ m/BaseEtgs:/) {
        for my $modifier (sort (keys %repairtypes)) {
            if ($_ =~ m/$repairtypes{$modifier}=(\d+)/) {
                $baseEtgs->{$modifier} = $1;
                $baseEdges->{$modifier} = 0;
            }
        }
    }

    # Check for per-BaseETG analysis
    if ($_ =~ m/BaseMods/) {
        for my $modifier (sort (keys %repairtypes)) {
            if ($_ =~ m/$repairtypes{$modifier}=(\d+)/) {
                $baseEdges->{$modifier} += $1;
            }
        }
    }

    # Check for Dst ETG counts
    if ($_ =~ m/DstEtgs:/) {
        for my $modifier (sort (keys %repairtypes)) {
            if ($_ =~ m/$repairtypes{$modifier}=(\d+)/) {
                $dstEtgs->{$modifier} = $1;
                $dstEdges->{$modifier} = 0;
            }
        }
    }

    # Check for per-DstETG analysis
    if ($_ =~ m/DstMods/) {
        for my $modifier (sort (keys %repairtypes)) {
            if ($_ =~ m/$repairtypes{$modifier}=(\d+) (\d+)/) {
                $dstEdges->{$modifier} += $2;
            }
        }
    }

    # Check for Flow ETG counts
    if ($_ =~ m/FlowEtgs:/) {
        for my $modifier (sort (keys %repairtypes)) {
            if ($_ =~ m/$repairtypes{$modifier}=(\d+)/) {
                $flowEtgs->{$modifier} = $1;
                $flowEdges->{$modifier} = 0;
            }
        }
    }

    # Check for per-FlowETG analysis
    if ($_ =~ m/FlowMods/) {
        for my $modifier (sort (keys %repairtypes)) {
            if ($_ =~ m/$repairtypes{$modifier}=(\d+) (\d+)/) {
                $flowEdges->{$modifier} += $2;
            }
        }
    }
}

# Output stats for last snapshot
if ($havedata) {
    print "$currsnapshot";
    for my $modifier (sort (keys %repairtypes)) {
        print ",$baseEtgs->{$modifier},$baseEdges->{$modifier}"
            .",$dstEtgs->{$modifier},$dstEdges->{$modifier}"
            .",$flowEtgs->{$modifier},$flowEdges->{$modifier}";
        delete $baseEtgs->{$modifier};
        delete $baseEdges->{$modifier};
        delete $dstEtgs->{$modifier};
        delete $dstEdges->{$modifier};
        delete $flowEtgs->{$modifier};
        delete $flowEdges->{$modifier};
    }
    print "\n";
}

close mfh;
