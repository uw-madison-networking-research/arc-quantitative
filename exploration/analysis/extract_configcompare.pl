#!/usr/bin/perl

# PURPOSE: Extract configuration comparison results from a log file.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Cwd 'abs_path';
use File::Basename;

# Get directories and files
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

my $outputdir = "$basedir/../../output";
my $logsdir = "$outputdir/changes";

# Get arguments
my $logname = 'compare-configs.log';

# Get list of networks
opendir(D, $logsdir) or die("Could not open $logsdir");
my @networks = sort(readdir(D));
shift @networks;
shift @networks;

print "Snapshot,Etgs,Devices,DevicesCh,LinesAdd,LinesRem,LinesCh,"
        ."GlobalLinesAdd,GlobalLinesRem,GlobalLinesCh\n";

# Process all networks
foreach my $network (@networks) {

    # Get list of snapshots
    my $networkdir = "$logsdir/$network";
    opendir(D, $networkdir) or die("Could not open $networkdir");
    my @snapshots = sort(readdir(D));
    shift @snapshots;
    shift @snapshots;

    # Process all snapshots
	foreach my $currsnap (@snapshots) {
#        print "$network/$currsnap\n";

        # Extract comparisons
        my $etgCount;
        my $deviceCount;
        my $clinesCount = 0;
        my $alinesCount = 0;
        my $rlinesCount = 0;
        my $diffs = {};

        # Make sure log file exists
        my $logfile = "$networkdir/$currsnap/$logname";
        if (not -e $logfile) {
            next;
        }
        # Find start and end of comparisons output
        my $startline = `grep -n "*** Compare Configs" $logfile | cut -f1 -d':' | tr -d '\n'`;
        if ($startline eq "") {
            next;
        }
        $startline += 2;
        my $endline = `wc -l $logfile | cut -f1 -d' ' | tr -d '\n'`;
        my $length = $endline - $startline + 1;
#        print "start:$startline end:$endline\n";
        
        # Do nothing if comparisons weren't found
        if ($length < 0) {
            next;
        }

        # Extract relevant lines of output
        my $comparisons = `tail -n +$startline $logfile | head -n $length`;

        # Get number of ETGs
        my $etgs = `grep -E "Need to generate" $logfile`;
        $etgs =~ s/[^0-9]//g;
        $etgCount = $etgs;

        # Get number of devices
        my $devices = `grep -E "COUNT: comparisonDevices" $logfile`;
        $devices =~ s/[^0-9]//g;
        $deviceCount = $devices;

        # Store the comparisons
#        print "$comparisons";
        my @lines = split("\n",$comparisons);
        shift @lines;
        my $device = "";
        foreach my $line (@lines) {
            $line =~ s/^\s+|\s+$//g;
            if ($line =~ /^ADD/) {
                $alinesCount += 1;
                if (!exists($diffs->{$device})) {
                    $diffs->{$device} = {};
                }
                $diffs->{$device}->{$line} = 'A';
            }
            elsif ($line =~ /^REMOVE/) {
                $rlinesCount += 1;
                if (!exists($diffs->{$device})) {
                    $diffs->{$device} = {};
                }
                $diffs->{$device}->{$line} = 'R';
            }
            elsif ($line =~ /^CHANGE/) {
                $clinesCount += 1;
                if (!exists($diffs->{$device})) {
                    $diffs->{$device} = {};
                }
                $diffs->{$device}->{$line} = 'C';
            }
            else {
                $device = $line;
            }
        }

        # Compute the global comparisons
        my $cdevicesCount = scalar(keys(%{$diffs}));
        my $globaldiffs = {};
        my $agloballinesCount = 0;
        my $rgloballinesCount = 0;
        my $cgloballinesCount = 0;
        if ($deviceCount == $cdevicesCount) {
#            print "Global\n";
            my $anydevice = (keys %{$diffs})[0];
            foreach my $diff (sort (keys %{$diffs->{$anydevice}})){
                my $isglobal = $diffs->{$anydevice}->{$diff};
                foreach my $device (sort (keys %{$diffs})) {
                    if (!exists($diffs->{$device}->{$diff})) {
                        $isglobal = 0;
                        last;
                    }
                }
                if ($isglobal) {
                    $globaldiffs->{$diff} = $isglobal;
                    if ($isglobal eq 'A') {
                        $agloballinesCount += 1;
                    }
                    elsif ($isglobal eq 'R') {
                        $rgloballinesCount += 1;
                    }
                    elsif ($isglobal eq 'C') {
                        $cgloballinesCount += 1;
                    }
#                    print "\t$isglobal $diff\n";
                }
            }
        }

        # Output statistics
        print "$network/$currsnap,$etgCount,$deviceCount,$cdevicesCount,"
            ."$alinesCount,$rlinesCount,$clinesCount,"
            ."$agloballinesCount,$rgloballinesCount,$cgloballinesCount\n";
    }
}
