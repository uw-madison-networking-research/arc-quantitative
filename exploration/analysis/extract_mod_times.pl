#!/usr/bin/perl

# PURPOSE: Extract time(s) to compute graph modifications from a log file.
# AUTHOR: Aaron Gember-Jacobson (agemberjacobson@cs.colgate.edu)

use strict;
use Cwd 'abs_path';
use File::Basename;
use List::Util qw(sum max);

# Determine script path
my $scriptpath = abs_path($0);
my $basedir = dirname($scriptpath);

# Set parameters
my $outputdir = "$basedir/../../output";
if ($#ARGV < 0) {
    die("Must provide relative path to logs directory");
}
my $logsdir = "$outputdir/logs/$ARGV[0]";
if (! -d $logsdir) {
    die("Log directory $logsdir does not exist");
}
my $logprefix = "repair_";
#$logprefix = "repair_filtered_"; # Uncomment if repair logs are filtered
my $logsuffix = ".log";
my %repairtypes = (All => 'all-tcs', Dst => 'per-dst');

# Print file header
print "Snapshot,DevCount,EtgCount,PolicyCount,BrokenCount";
foreach my $modifier (sort (keys %repairtypes)) {
    print ",Total$modifier"
}
foreach my $modifier (sort (keys %repairtypes)) {
    if ($modifier eq "All") {
        next;
    }
    print ",Median$modifier,Mean$modifier,Max$modifier";
}
print "\n";

# Get list of networks
opendir(D, $logsdir) or die("Could not open $logsdir");
my @networks = sort(readdir(D));
shift @networks;
shift @networks;

# Process all networks
foreach my $network (@networks) {
    # Get list of snapshots
    my $networkdir = "$logsdir/$network";
    opendir(D, $networkdir) or die("Could not open $networkdir");
    my @snapshots = sort(readdir(D));
    shift @snapshots;
    shift @snapshots;

    # Process all snapshots
	foreach my $currsnap (@snapshots) {
        #print "$network/$currsnap\n";

        # Extract modifications
        my $devCount = '';
        my $policyCount = '';
        my $etgCount = '';
        my $brokenCount = '';
        my $totalTimes = {};
        my $indivTimes = {};
        foreach my $modifier (sort (keys %repairtypes)) {
            # Make sure log file exists
            my $logname = $logprefix.$repairtypes{$modifier}.$logsuffix;
            my $logfile = "$networkdir/$currsnap/$logname";
            if (not -e $logfile) {
                next;
            }

            # Made sure we succesfully found modifications
            my $mods = `grep "Modifications:" $logfile`;
            if ($mods eq "") {
                next;
            }
           
            # Extract relevant lines of output
            my $totalTimeRaw = `grep "TIME: graphModification" $logfile`;
            my $indivTimesRaw = `grep "TIMEONE:" $logfile`;
            my $devCountRaw = `grep "COUNT: devices" $logfile`;
            my $policyCountRaw = `grep "COUNT: policies" $logfile`;
            my $etgCountRaw = `grep "Need to generate" $logfile`;
            my $brokenCountRaw = `grep "COUNT: brokenFlows" $logfile`;
            
            # Do nothing if times weren't found
            if ($totalTimeRaw eq "") {
                next;
            }
            
#            print "$modifier\n$totalTimeRaw$indivTimesRaw";

            chomp $devCountRaw;
            $devCountRaw =~ m/devices (\d+)/;
            $devCount = $1;

            chomp $etgCountRaw;
            $etgCountRaw =~ m/(\d+) ETGs/;
            $etgCount = $1;
            
            chomp $policyCountRaw;
            $policyCountRaw =~ m/policies (\d+)/;
            $policyCount = $1;

            chomp $brokenCountRaw;
            $brokenCountRaw =~ m/brokenFlows (\d+)/;
            $brokenCount = $1;

            chomp $totalTimeRaw;
            $totalTimeRaw =~ m/(\d+) ms/;
            my $totalTime = $1;
            $totalTimes->{$modifier} = $totalTime;
#            print "$modifier\ntotal=$totalTime\n";

            my @lines = split("\n",$indivTimesRaw);
            $indivTimes->{$modifier} = {};
            foreach my $line (@lines) {
                $line =~ m/(\d+) ms (.+)/;
                my $indivTime = $1;
                my $flow = $2;
#                print "$indivTime $flow\n";
                $indivTimes->{$modifier}->{$flow} = $indivTime;
            }
        }

        # Don't output if we don't have output from any modifiers
        if (0 == scalar(keys %{$totalTimes})) {
            next;
        }

        print "$network/$currsnap,$devCount,$etgCount,$policyCount,$brokenCount";

        # Output total times for each modifier
        foreach my $modifier (sort (keys %repairtypes)) {
            if (exists($totalTimes->{$modifier})) {
                print ",$totalTimes->{$modifier}";
            } else {
                print ",";
            }
        }

        # Output stats for multi-problem modifiers
        foreach my $modifier (sort (keys %repairtypes)) {
            if ($modifier eq "All") {
                next;
            }
            if (exists($indivTimes->{$modifier})) {
                my @times = (sort (values %{$indivTimes->{$modifier}}));
                my $numFlows = scalar(@times);
                if (0 == $numFlows) {
                    print ",,,";
                    next;
                }
                my $median = @times[$numFlows/2];
                if (0 == $numFlows % 2) {
                    $median = (@times[$numFlows/2] + @times[($numFlows/2)-1])/2;
                }
                my $sum = sum(@times);
                my $mean = $sum/$numFlows;
                my $max = max(@times);
                print ",$median,$mean,$max";
            } else {
                print ",,,";
            }
        }

        print "\n";
    }

}
