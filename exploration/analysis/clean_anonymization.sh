#!/bin/bash
# Clean-up anonymization performed on configs so they are parseable

SED=`which gsed`
if [ -z "$SED" ]; then
    SED=`which sed`
    if [ -z "$SED" ]; then
        echo "Cannot locate (g)sed"
        exit 1
    fi
fi

DIR=$1
ls $DIR | while read FILE; do
    $SED -i  's/^access-list \(.*\) xxxxxxxxx/access-list \1 any/' $DIR/$FILE
    $SED -i  's/permit ip \(.*\) xxxxxxxxx/permit ip \1 any/' $DIR/$FILE
    grep -v -E "^banner motd(\r|\n)" $DIR/$FILE > $DIR/$FILE.tmp
    mv $DIR/$FILE.tmp $DIR/$FILE
done
