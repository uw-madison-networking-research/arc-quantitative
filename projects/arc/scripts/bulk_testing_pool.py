#!/usr/bin/python

import os
from pool import Pool
import subprocess
import sys

scriptpath = subprocess.check_output(["readlink", "-f", sys.argv[0]])
basedir = os.path.dirname(scriptpath)+"/.."
outputdir = basedir+"/../../output/testing"

if (len(sys.argv) < 3):
    print "Usage: %s <testcases-file> <script> [<args>]" % (sys.argv[0])
    sys.exit(1)

testcasesfile = sys.argv[1]
script = basedir+"/scripts/"+sys.argv[2]
args = sys.argv[3:]

pool = Pool(4, 30, 60*60*3)

# Process each test case
with open(testcasesfile) as tcf: 
    for testcase in tcf:
        testcase = testcase.rstrip()
#        if (os.path.isfile(outputdir+"/"+snapshot+"/repair_"+args+".log")):
#            break # Skip completed testcases
        run = [script, "-t", testcase]
        run.extend(args)
        pool.submit(run, name=testcase)

# Wait for everything to finish
pool.join()
