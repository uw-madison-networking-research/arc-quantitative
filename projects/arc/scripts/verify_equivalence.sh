#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
source $BASEDIR/scripts/common_preamble.sh

# Additional settings
OBJSDIR=$OUTPUTDIR/objs/
# Gurobi settings
export CODE_DIR="$BASEDIR/../../"
source $CODE_DIR/set_gurobi_envs.sh

java $ARC_ARGS -veq $OBJSDIR/${1}.obj 2>&1 | tee $LOGDIR/veq.log

JAVAEXIT=${PIPESTATUS[0]}
if [[ $JAVAEXIT -ne 0 ]]; then
    exit $JAVAEXIT
fi

source $BASEDIR/scripts/common_postamble.sh
