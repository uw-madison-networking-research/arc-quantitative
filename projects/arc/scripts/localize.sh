#!/bin/bash

if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
source $BASEDIR/scripts/common_preamble.sh

POLICYFILE="nextpolicies.obj"
POLICYFILEPATH="$LOGDIR/$POLICYFILE"
echo $POLICYFILEPATH

if [ ! -f $POLICYFILEPATH ]; then
	echo "FILE NOT FOUND"
	exit 1
fi

java $ARC_ARGS -checkpolicies $POLICYFILEPATH -localize 2>&1 | \
    tee $LOGDIR/localize.log

source $BASEDIR/scripts/common_postamble.sh
