#!/usr/bin/python

import os
from pool import Pool
import subprocess
import sys

scriptpath = subprocess.check_output(["readlink", "-f", sys.argv[0]])
basedir = os.path.dirname(scriptpath)+"/.."
snapshotsdir = basedir+"/../../output/changes"

if (len(sys.argv) < 2):
    print "Usage: %s <script> [<args>]" % (sys.argv[0])
    sys.exit(1)

script = basedir+"/scripts/"+sys.argv[1]
args = sys.argv[2:]

#pool = Pool(15, 30, 60*60*3)
pool = Pool(2, 5, 60*60*1)

# Process each network
for network in sorted(os.listdir(snapshotsdir)):
#    if (not network.startswith('a')):
#        continue
#
    # Process each snapshot
    snapshots = sorted(os.listdir(snapshotsdir+"/"+network))
#    snapshots.pop(0)
    snapshots.pop()
    for timestamp in snapshots:
        snapshot = network+"/"+timestamp
#        if (not os.path.isfile(snapshotsdir+"/"+snapshot+"/prev_repairedetgs_isolated.obj")):
#            break # Only do first snapshot per network
#        if (os.path.isfile(snapshotsdir+"/"+snapshot+"/compare_isolated.log")):
#            break # Only do first snapshot per network
        run = [script, snapshot]
        run.extend(args)
        pool.submit(run, name=snapshot)
        break # Only do first snapshot per network

# Wait for everything to finish
pool.join()
