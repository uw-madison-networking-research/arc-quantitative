#!/bin/bash

SCRIPTPATH=`readlink -f $0`
BASEDIR="`dirname $SCRIPTPATH`/.."
SNAPSHOTSDIR=$BASEDIR/../../configs/snapshots

RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)
COL=50

SCRIPT=$1

# Process each network
ls $SNAPSHOTSDIR | while read NETWORK; do
    echo $NETWORK

    # Process each snapshot
    ls $SNAPSHOTSDIR/$NETWORK | while read TIMESTAMP; do
        printf "%-40s" "    $TIMESTAMP"
        $BASEDIR/scripts/$SCRIPT "$NETWORK/$TIMESTAMP" > /dev/null
        if [ $? -eq 0 ]; then
            echo -e "$GREEN[OK]$NORMAL"
        else
            echo -e "$RED[FAIL]$NORMAL"
            break
        fi
    done
done
