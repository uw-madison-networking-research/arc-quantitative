#!/usr/bin/python

import subprocess
import sys
import os

print 'Number of arguments: ', len(sys.argv)
print 'Argument List:', str(sys.argv)
#subprocess.call(['./pipeLogInfo.sh'])
if len(sys.argv)!=2:
	print 'USAGE : ./evaluate_localize.sh <PATH TO BYDATE DIR>'
	sys.exit()
thispath = os.path.dirname(os.path.realpath(sys.argv[0]))
policyFilePath = thispath.split('/')
policyFilePath = policyFilePath[:len(policyFilePath)-3]
policyFilePath = '/'.join(policyFilePath) + '/' + "policies" + '/'
byDateDir = sys.argv[1]
listDir = os.listdir(byDateDir)
listDir.sort()
#cfgs = set()
for dirnamesTuple in zip(listDir, listDir[1:]):
	'''
	print dirname, "Exploring this date"
	datePath = os.path.abspath(os.path.join(byDateDir,dirname))
	#run pipeloginfo.sh and diffs across pairs of snapshots ... ??
	for filename in os.listdir(datePath):
		if (filename.endswith('.cfg')):
			#cfgs.add(filename)
			print filename
	'''

	# print dirnamesTuple
	#consider the newer version ideal.
	#run arc on the newer version to save policies
	#run localize on older version to figure out LINE RANGES (pipeLogInfo.sh)
	configDirPath = dirnamesTuple[1]
	faultyDirPath = dirnamesTuple[0]
	shellArgs = "./run.sh -c madison/%s -m 255 -a \"-vall -savepolicies %s\"" % (configDirPath, policyFilePath+configDirPath)
	print shellArgs
	subprocess.call(shellArgs, shell=True)
	shellArgs = "./run.sh -c madison/%s -m 255 -a \"-checkpolicies %s -localize\"" %(faultyDirPath, policyFilePath+configDirPath)
	subprocess.call(shellArgs, shell=True)
