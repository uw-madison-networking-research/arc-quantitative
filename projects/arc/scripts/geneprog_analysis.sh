# grep -rnw "Unable to" $LOGDIR/geneprog_results.log
numLines=$(wc -l < $LOGDIR/geneprog_results.log) 
ANALYSISFILE=$LOGDIR/"analysis.log"
analysis=$(grep -A $numLines "correct" $LOGDIR/geneprog_results.log)
if [ -z $analysis ]; then
 	echo "No correct builds found..."
 	# analysis=$(grep -A $numLines "Unable to" $LOGDIR/geneprog_results.log)
else
	echo "$analysis" > $ANALYSISFILE
	cat $ANALYSISFILE
fi