#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
source $BASEDIR/scripts/common_preamble.sh

# Additional settings
CHANGESDIR=$OUTPUTDIR/changes

ls --color=none $CHANGESDIR | while read NETNAME; do
    FIRSTSNAP=`ls --color=none $CHANGESDIR/$NETNAME | head -n 1`
    SECONDSNAP=`ls --color=none $CHANGESDIR/$NETNAME | head -n 2 | tail -n 1`
    if [ $FIRSTSNAP == $SECONDSNAP ]; then
        continue
    fi

    echo $NETNAME $FIRSTSNAP $SECONDSNAP
    FIRSTDIR=$CHANGESDIR/$NETNAME/$FIRSTSNAP
    SECONDDIR=$CHANGESDIR/$NETNAME/$SECONDSNAP

    # Link repaired ETGs
    rm $SECONDDIR/prev_*etgs*.obj 2> /dev/null
    ls --color=none $FIRSTDIR/*etgs*.obj | while read OBJFILE; do 
        OBJFILE=`basename $OBJFILE`
        ln -s ../$FIRSTSNAP/$OBJFILE $SECONDDIR/prev_$OBJFILE
    done

    # Link configs
    rm $SECONDDIR/prev_configs.obj 2> /dev/null
    ln -s ../$FIRSTSNAP/configs.obj $SECONDDIR/prev_configs.obj
done
