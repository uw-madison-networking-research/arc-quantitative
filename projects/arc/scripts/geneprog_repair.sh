#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
source $BASEDIR/scripts/common_preamble.sh
POLICYDIR=$LOGDIR/"nextpolicies.obj"

java $ARC_ARGS -repair $POLICYDIR -genrepair $LOGDIR/policies.obj 2>&1 | \
    tee $LOGDIR/geneprog_results.log

JAVAEXIT=${PIPESTATUS[0]}
if [[ $JAVAEXIT -ne 0 ]]; then
    exit $JAVAEXIT
fi

source $BASEDIR/scripts/geneprog_analysis.sh