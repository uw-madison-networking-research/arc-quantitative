#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
source $BASEDIR/scripts/common_preamble.sh

# Additional settings
POLICIES=$LOGDIR/nextpolicies.obj
if [ ! -f "$POLICIES" ]; then
    if [ -f $LOGDIR/../fixed/policies.obj ]; then
        echo "Symlinking policies file: $POLICIES -> $LOGDIR/../fixed/policies.obj"
        ln -s "../fixed/policies.obj" $POLICIES
    elif [ -f $LOGDIR/../1-unbroken/policies.obj ]; then
        echo "Symlinking policies file: $POLICIES -> $LOGDIR/../1-unbroken/policies.obj"
        ln -s "../1-unbroken/policies.obj" $POLICIES
    elif [ `\ls $LOGDIR/../ | wc -l` -eq 2 ]; then
        PREV=`\ls $LOGDIR/../ | tail -n 1`
        echo "Symlinking policies file: $POLICIES -> $LOGDIR/../$PREV/policies.obj"
        ln -s "../$PREV/policies.obj" $POLICIES
    else
        echo "Policies file ($POLICIES) does not exist"
        exit 1
    fi
fi

java $ARC_ARGS -checkpolicies $POLICIES -dstetgs 2>&1 | tee "$LOGDIR/checkpolicies.log"

JAVAEXIT=${PIPESTATUS[0]}
if [[ $JAVAEXIT -ne 0 ]]; then
    exit $JAVAEXIT
fi

source $BASEDIR/scripts/common_postamble.sh
