#!/bin/bash

trap "kill -$$" SIGTERM

ARC_JVM_ARGS="-DbatfishBatfishPropertiesPath=$BASEDIR/batfish.properties -Djava.library.path=.:/usr/lib:/usr/local/lib"
ARC_CLASSPATH="$BASEDIR/target/*:$BASEDIR/target/lib/*"
ARC_MAIN_CLASS="edu.wisc.cs.arc.Driver"
ARC_COMMON_ARGS="$ARC_JVM_ARGS -cp $ARC_CLASSPATH $ARC_MAIN_CLASS"

CONFIGDIR="$BASEDIR/../../configs"
OUTPUTDIR="$BASEDIR/../../output"
LOGDIR="$OUTPUTDIR/logs"

# Options
CONFIGPATH=""
GRAPHVIZ=""
EXTRA_ARGS=""
DESCRIPTIONS=""
INTERNAL_ONLY="-internalonly"
REPAIR_MOD=""
MIN_HOSTS=4
K=2
while getopts c:ga:der:m:k:h OPT; do
    case $OPT in
        c) # Relative configs path
            CONFIGPATH=$OPTARG
            ;;
        g) # Generate graphviz
            GRAPHVIZ="-graphviz"
            ;;
        a) # Extra arguments
            EXTRA_ARGS=$OPTARG
            ;;
        d) # Use descriptions
            DESCRIPTIONS="-descriptions"
            ;;
        e) # Include external
            INTERNAL_ONLY=""
            ;;
        r) # Repair modifier
            REPAIR_MOD=$OPTARG
            ;;
        m) # Min hosts
            MIN_HOSTS=$OPTARG
            ;;
        k) # Tolerate less than K link failures
            K=$OPTARG
            ;;
        h | \?)
    echo "Usage: `basename $0` -c CONFIGS_DIR [-g] [-a EXTRA_ARGS] [-d] [-e]"
    echo "                     [-r REPAIR_MOD] [-m MIN_HOSTS (default=4)]"
    echo "                     [-k FAILURE_BOUND (default=2)]"
            exit 1
            ;;
    esac
done

NETNAME=`echo $CONFIGPATH | sed -e 's#^[^/]*/##'`

# Confirm configs exist
CONFIGDIR=$CONFIGDIR/$CONFIGPATH
if [ ! -d "$CONFIGDIR" ]; then
    echo "Configs directory \"$CONFIGDIR\" does not exist"
    exit 1
fi
# Check if waypoints exist
WAYPOINTS=""
if [ -f "$CONFIGDIR/waypoints.csv" ]; then
   WAYPOINTS="-waypoints $CONFIGDIR/waypoints.csv" 
fi
ARC_ARGS="$ARC_COMMON_ARGS -configs $CONFIGDIR $DESCRIPTIONS $INTERNAL_ONLY -minhosts $MIN_HOSTS $WAYPOINTS $EXTRA_ARGS"

# Create log directory
LOGDIR=$LOGDIR/$CONFIGPATH
mkdir -p $LOGDIR

# Create graphviz directory
if [ ! -z "$GRAPHVIZ" ]; then
    mkdir -p $LOGDIR/graphs
    ARC_ARGS="$ARC_ARGS $GRAPHVIZ $LOGDIR/graphs"
fi
