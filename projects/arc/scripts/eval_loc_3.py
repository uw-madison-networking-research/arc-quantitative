#!/usr/bin/python

import subprocess
import sys
import os

scripts = os.path.dirname(os.path.realpath(sys.argv[0]))
# print scripts
path = scripts.split('/')[:-3]
pathDir = '/'.join(path)
configDir = '/'.join(path) + '/configs/madison/'
changesDir = '/shared/output/changes/uwmadison/'

listFiles = os.listdir(changesDir)
# print listFiles
listFiles.sort()
for changeFileTuple in zip(listFiles, listFiles[1:]):
    print changeFileTuple
    #Run localize on $0 with policies of $1
    configPath = 'madison/'+changeFileTuple[0]
    policyFile = changesDir + changeFileTuple[1] +'/policies.obj'
    print configPath, policyFile
    eval_str = './scripts/run.sh -c '+configPath + ' -m 255 -a \"-checkpolicies ' + policyFile + ' -localize simple\" | grep \'^HOST: \' >'+ pathDir+ '/output/new/'+changeFileTuple[0]+'.txt'
    print eval_str
    subprocess.call(eval_str, shell=True)
