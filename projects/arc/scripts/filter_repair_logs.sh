#!/bin/bash

# Filters out parts of repair logs that are unnecessary for analysis

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
source $BASEDIR/scripts/common_preamble.sh

if [ -z "$REPAIR_MODE" ]; then
    echo "Specify repair mode"
    exit 1
fi

cd $CHANGESDIR
ls */*/repair_$REPAIR_MODE.log | while read FILE; do 
    START=`grep -n -m 1 "Modifications:" $FILE | cut -f1 -d':' | tr -d '\n'`
    MODFILE=`echo $FILE | sed -e 's/repair_/repair_filtered_/'`
    echo $START $MODFILE
    tail --lines=+$START $FILE > $MODFILE

    grep -E -m 1 "Need to generate" $FILE >> $MODFILE;
    grep -E "(TIME)|(TIMEONE)|(COUNT)" $FILE >> $MODFILE;
done
