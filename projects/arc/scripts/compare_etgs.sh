#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
source $BASEDIR/scripts/common_preamble.sh

# Additional settings
SERIALIZEDETGS="prev_repairedetgs_${REPAIR_MODE}.obj"
LOGFILE="compare-etgs_${REPAIR_MODE}.log"
if [ -z "$REPAIR_MODE" ]; then
    SERIALIZEDETGS="prev_etgs.obj"
    LOGFILE="compare-etgs.log"
fi

java $ARC_ARGS -compareetgs $LOGDIR/$SERIALIZEDETGS 2>&1 | \
    tee $LOGDIR/$LOGFILE

JAVAEXIT=${PIPESTATUS[0]}
if [[ $JAVAEXIT -ne 0 ]]; then
    exit $JAVAEXIT
fi

source $BASEDIR/scripts/common_postamble.sh
