#!/usr/bin/python
#Analyzing logs
import sys, os, subprocess
if (subprocess.check_output(["uname"]).rstrip() == "Darwin"):
    scriptpath = subprocess.check_output(["greadlink", "-f", sys.argv[0]])
else:
    scriptpath = subprocess.check_output(["readlink", "-f", sys.argv[0]])

basedir = os.path.dirname(scriptpath)+"/../../../output/logs/"
postfix = "/run.log"
dirname = basedir + sys.argv[1]
START_STMNT = "Analysing"
NEWLINE = "\n"
PERIOD = "."

stats = {}
count = 0

for subDir in sorted(os.listdir(dirname)):
	if PERIOD in subDir:
		continue
	filename = dirname +"/"+ subDir + postfix
	with open(filename) as f:
		log = f.read().split(NEWLINE)
		for i in xrange(len(log)):
			# print log[i]
			if START_STMNT in log[i]:
				hostname = log[i].split()[2]
				if hostname not in stats:
					stats[hostname] = {"Tokens":0.0, "Statements":0.0}
				line = log[i+1].split()
				stats[hostname][line[0]]+= float(line[3])
				line = log[i+2].split()
				stats[hostname][line[0]]+= float(line[3])
	count += 1

output = dirname + "/stats.csv"
header = "Host,Token Ratio,Statement Ratio\n"
with open(output, "w") as f:
	f.write(header)
	for host in sorted(stats):
		line = host
		for _type in stats[host]:
			line += ","+ str((stats[host][_type]/count))
		line += NEWLINE
		f.write(line)