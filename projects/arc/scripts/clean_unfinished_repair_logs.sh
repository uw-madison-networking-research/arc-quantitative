#!/bin/bash

# Removes log files from repair attempts that timed out

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."

# Additional settings
OUTPUTDIR=$BASEDIR/../../output
CHANGESDIR=$OUTPUTDIR/changes

if [ $# -lt 1 ]; then
    echo "Specify modifier"
    exit 1
fi
MODIFIER=$1

cd $CHANGESDIR
ls */*/repair_$MODIFIER.log | while read FILE; do 
    grep "Exception in thread" $FILE > /dev/null
    if [ $? -eq 0 ]; then 
        echo Debug $FILE
        continue
    fi
    grep "Modifications:" $FILE > /dev/null
    if [ $? -eq 1 ]; then 
#        echo $FILE
#        grep "COUNT: separatePolicyGroups" $FILE
#        SOLVED=`grep "Solver result:" $FILE | wc -l`
#        echo $SOLVED
#        if [ $SOLVED -ge 5 ]; then
            echo Retry $FILE
            rm $FILE
#            rm -v $FILE
#        fi
    fi
    echo Good $FILE
done
