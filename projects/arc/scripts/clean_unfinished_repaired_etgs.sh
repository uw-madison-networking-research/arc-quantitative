#!/bin/bash

# Removes log files from repair attempts that timed out

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."

# Additional settings
OUTPUTDIR=$BASEDIR/../../output
CHANGESDIR=$OUTPUTDIR/changes

if [ $# -lt 1 ]; then
    echo "Specify modifier"
    exit 1
fi
MODIFIER=$1

cd $CHANGESDIR
ls --color=none | while read NETWORK; do 
    ls --color=none $NETWORK | while read SNAPSHOT; do
        ls $NETWORK/$SNAPSHOT/repair_$MODIFIER.log &> /dev/null
        if [ $? -ne 0 ]; then 
            rm -v $NETWORK/$SNAPSHOT/repairedetgs_$MODIFIER.obj 2> /dev/null
        fi
    done
done
