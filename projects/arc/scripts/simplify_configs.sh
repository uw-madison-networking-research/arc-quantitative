#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
source $BASEDIR/scripts/common_preamble.sh

# Additional settings
SIMPLIFIED_DIR="$LOGDIR/simplified"
mkdir -p $SIMPLIFIED_DIR

java $ARC_ARGS -simplify -savesimplify $SIMPLIFIED_DIR 2>&1 | tee "$LOGDIR/simplify_configs.log"

JAVAEXIT=${PIPESTATUS[0]}
if [[ $JAVAEXIT -ne 0 ]]; then
    exit $JAVAEXIT
fi

source $BASEDIR/scripts/common_postamble.sh
