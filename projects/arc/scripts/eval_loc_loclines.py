#!/usr/bin/python

import os
import subprocess
import sys
import re

scripts = os.path.dirname(os.path.realpath(sys.argv[0]))
path  = scripts.split('/')[:-3]
#output dir contains locline files:
outputDir= '/'.join(path) +'/output/new/'
changes = ['+','-']
changesDir = '/shared/output/changes/uwmadison/'
snapshots = os.listdir(changesDir)
snapshots.sort()
unsuccessfulTotal= 0
successfulTotal = 0
totalTotal =0
for snapshot,nextSnapShot in zip(snapshots, snapshots[1:]):
    print snapshot, ' <- OLD | NEW ->', nextSnapShot
    snapshotDirPath = changesDir+nextSnapShot+'/'
    filesInSnapShot = os.listdir(snapshotDirPath)
    #GET only diff files
    listDiffs = [i for i in filesInSnapShot if i[0]=='r' and i[-4:]=='diff']
    i=0
    diffMap={}
    for diffFile in listDiffs:
        i+=1
        #get (hostname, lines modified) tuples here
        printlineNum=0
        hostname = diffFile.split('.')[0]
        # print '\t', hostname
        with open(snapshotDirPath+diffFile, 'r') as diffContent:
            relativeLineNum =0
            for line in diffContent:
                if (line[0:2]=='@@'):
                    startLineNum = line.split(' ')[1].split(',')[0][1:]
                    relativeLineNum =0
                if (line[0] in changes and line[1] not in changes):
                    #DONT CHECK THIS FIXME: include redundant linenums
                    # if printlineNum != int(startLineNum)+relativeLineNum:
                    #     print '\t\t',printlineNum
                    printlineNum = int(startLineNum)+relativeLineNum
                    if hostname in diffMap:
                        diffMap[hostname].append(printlineNum)
                    else:
                        diffMap[hostname] =[printlineNum]
                else:
                    relativeLineNum +=1
    # raw_input()
    localizationFilePath = outputDir + snapshot+'.txt'
    localizationMap = {}
    with open(localizationFilePath) as loclinesFile:
        for line in loclinesFile:
            splitLine = line.split(' ')
            # print splitLine[1],splitLine[-1]
            locRange = (int(splitLine[-1].split('-')[0]),int(splitLine[-1].split('-')[1]))
            key = splitLine[1]
            if key in localizationMap:
                localizationMap[key].append(locRange)
            else:
                localizationMap[key] = [locRange]
    # print localizationMap.keys(),'\n\n', diffMap.keys()
    unsuccessful=0
    successful=0
    total=0
    for key in diffMap.keys():
        if key in localizationMap:
            for diff in diffMap[key]:
                for loclineRange in localizationMap[key]:
                    if loclineRange[0]<=diff<=loclineRange[1]:
                        successful+=1
                        break
                    else:
                        unsuccessful+=1
                        break
            #see how many diffs in loc range
        # else:
        #     unsuccessful=unsuccessful
            # unsuccessful += len(diffMap[key])
        total +=len(diffMap[key])
    # print 'unsuccessful', unsuccessful, ' in total changes count --> ', total , ' successful-> ',successful
    unsuccessfulTotal +=unsuccessful
    successfulTotal +=successful
    totalTotal +=total
    # raw_input()
print '\n', unsuccessfulTotal,'<- unsuccessfulTotal successfulTotal ->',successfulTotal , '\nTOTAL ->', totalTotal
