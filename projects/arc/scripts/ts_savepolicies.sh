#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
NEXT="Feb"/policies.obj
source $BASEDIR/scripts/common_preamble.sh

java $ARC_ARGS -vab -var $K -vcb -savepolicies $LOGDIR/policies.obj 2>&1 | \
    tee $LOGDIR/policies.log

JAVAEXIT=${PIPESTATUS[0]}
if [[ $JAVAEXIT -ne 0 ]]; then
    exit $JAVAEXIT
fi

NEXTDIR=$LOGDIR/../$NEXT
ln -s $NEXTDIR $LOGDIR/nextpolicies.obj