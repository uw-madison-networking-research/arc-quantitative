#!/usr/bin/python

import os
import helpers
import sys
import json

path = 'obj/'
files =  os.listdir(path)
line_number_files= sorted([f for f in files if f.endswith('d.pkl')]) #actual changes that I consider ideal
localizedFiles=sorted([f for f in files if f.endswith('o.pkl')]) #localization results
with open('analysis/bFullMap.json') as jsonFile:
	numViolationsByCfg = json.load(jsonFile)
print numViolationsByCfg.keys()
#print numViolationsByCfg
print localizedFiles
for i in range(len(localizedFiles)-1):
	print '\n'
	line_number_file = helpers.load_obj(line_number_files[i])
	
	localized_file = helpers.load_obj(localizedFiles[i])
	actualChanges = line_number_file.keys()
	localizedResult =  localized_file.keys()



	print 'OKAY'
	print 'keys:'
	for idx in numViolationsByCfg.keys():
		print idx
	#getting number of violations caused by each routerconfig file for a given snapshot. 
	
	#initialize a dictionary that stores
	#for every router, how many violations it causes,
	#how many diffs were matched by localized line_ranges
	#and how many bad localizations occured. 
	info =dict()
	for router_file in line_number_file:
		#get numviolations from the bFullMap.json in analysis.(using highest frequency)  
		tempKey = localizedFiles[i+1].split('.')[0]
		if tempKey not in numViolationsByCfg:
			numViolationsByFile= 0
		else:
			numViolationsByFile = numViolationsByCfg[tempKey][router_file[:-5]]
		info[router_file[:-9]] = {'num_violations_caused':numViolationsByFile, 'num_diffs_localized':0,'num_localization_bad':0, 'num_diffs':len(line_number_file[router_file])}
	print info
	
	for localizedrouter in localized_file:
		actual_results = line_number_file[localizedrouter+'.cfg.diff']
		lineranges_localized= localized_file[localizedrouter]
		missing = len(actual_results)
		
		
		accurate_locs = 0
		bad_locs =0
		
		
		for linerange_localized in lineranges_localized: 
			start, end =  linerange_localized.split('-')
			b =int(start)
			e =int(end)
			#print b, e, b-e
			#print linerange_localized
			accurate = 0
			extra = 0
			for ii,jj in actual_results:
				if ii>b and jj<e:
					info[localizedrouter]['num_diffs_localized']+=1
					accurate +=1
					missing -=1
					#actual_results.remove((i,j))
			if accurate ==0:
				#badlocalization
				info[localizedrouter]['num_localization_bad']+=1

				bad_locs +=1
			else:
				accurate_locs +=accurate
		print 'snapshot:', line_number_files[i], 'localizedrouter : ' ,localizedrouter , ' missed diffs ', missing, ' accurate ' , accurate_locs , ' bad_locs ', bad_locs
		
	with open('analysis/%d_%s.json'%(i,localizedFiles[i].split('.')[0]), 'w') as jd:
		json.dump(info, jd)
	
	print info				
					
