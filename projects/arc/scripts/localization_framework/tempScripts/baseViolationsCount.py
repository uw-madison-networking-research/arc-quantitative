#!/usr/bin/python

import os
import sys
import subprocess

changesPath= '/shared/output/changes/simplified_dist/'
snapshots= sorted(os.listdir(changesPath))
for past, present in zip(snapshots,snapshots[1:]):
	print past, present

	#bash command to be executed. 

	command = "../../run.sh -c madison/%s -m 255 -a \"-checkpolicies %s%s/policies.obj\" > violations_in_%s.txt"%(past,changesPath, present, past)
	subprocess.call(command, shell=True)
	print command
