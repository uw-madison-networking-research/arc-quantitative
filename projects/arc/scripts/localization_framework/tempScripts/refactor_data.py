#!/usr/bin/python

import json
import os
import sys
import pickle

with open('lineranges.json') as data:
	loaded = json.load(data)


def save_obj(obj, name):
	with open('obj/'+name +'.pkl','wb') as wf:
		pickle.dump(obj, wf, pickle.HIGHEST_PROTOCOL)
				
def load_obj(name):
	with open('obj/' + name+'.pkl', 'rb') as rf:
		return pickle.load(rf)

keys = loaded.keys()
for key in keys:
	dict_one_snapshot =  dict()

	one_snapshot= loaded[key]
	#each one_snapshot object is a list of tuples containing router name to line ranges. 
	for i, j in one_snapshot:
		if i not in dict_one_snapshot.keys():
			dict_one_snapshot[i] = []
		dict_one_snapshot[i].append(j) 
	

	save_obj(dict_one_snapshot, key+'_localizedTo')



