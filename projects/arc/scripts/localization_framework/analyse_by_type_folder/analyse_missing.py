#!/usr/bin/python

import os
import helpers
import sys
import json

path ='../obj/'
files =  os.listdir(path)
line_number_files= sorted([f for f in files if f.endswith('d.pkl')]) #actual changes that I consider ideal
localizedFiles=sorted([f for f in files if f.endswith('o.pkl')]) #localization results
with open('../analysis/bFullMap.json') as jsonFile:
	numViolationsByCfg = json.load(jsonFile)
print numViolationsByCfg.keys()
#print numViolationsByCfg
print localizedFiles
for i in range(len(localizedFiles)-1):
	print '\n'
	line_number_file = helpers.load_obj(line_number_files[i])
	
	localized_file = helpers.load_obj(localizedFiles[i])
	actualChanges = line_number_file.keys()
	localizedResult =  localized_file.keys()
	missing_lineranges = dict()
	for localizedrouter in localized_file:
		actual_results = line_number_file[localizedrouter+'.cfg.diff']
		lineranges_localized= localized_file[localizedrouter]
		missing = len(actual_results)
		
		missing_lineranges[localizedrouter] = []
		for ii, jj in actual_results:
			diff_in_localization = False
			for linerange_localized in lineranges_localized:
				start, end = linerange_localized.split('-')
				b=int(start)
				e=int(end)
				if ii>=b and jj<=e:
					diff_in_localization = True
					break
			if diff_in_localization==False:
				missing_lineranges[localizedrouter].append((ii,jj))

	snapshot_name = localizedFiles[i].split('.')[0]
	#with open('analysis/%d_%s.json'%(i,snapshot_name), 'w') as jd:
	#	json.dump(info, jd)
	with open('../analysis/missing_%s.json'%(snapshot_name),'w') as jd:
		json.dump(missing_lineranges,jd)
				
print 'Completed'
