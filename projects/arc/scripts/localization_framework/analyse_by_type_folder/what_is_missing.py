#!/usr/bin/python

import os
import sys
import helpers
import json


simplified_configs_root_dir = '/shared/configs/uwmadison/snapshots_6hr/simplified_dist/'
analysis_dir_path = '../analysis/'
missing_logs = os.listdir('../analysis')
missing_analysis_filename = analysis_dir_path+'missing_localization.analysis'
missing_logs = [i for i in missing_logs if i.startswith('missing') and i.endswith('json')]
missing_fp = open(missing_analysis_filename,'w')

for log in missing_logs:
	curr_snapshot = log.split('.')[0].split('_')[1]+'/'
	missing_fp.write(log+'\n\n')
	print analysis_dir_path+log
	with open(analysis_dir_path+log,'r') as jsonfile:
		json_log_dict = json.load(jsonfile)
		for key in json_log_dict.keys():
			curr_cfg_filename = key + '.cfg'
			missing_fp.write(key+'\n')

			list_of_ranges_missing_in_key = json_log_dict[key]
			#open file router key in the relevant snapshot. 
			for each_range  in list_of_ranges_missing_in_key:
				path_to_cfg_file = simplified_configs_root_dir+curr_snapshot+curr_cfg_filename
				#print path_to_cfg_file
				#print curr_snapshot, ' -> ', curr_cfg_filename, ' --> ' ,each_range[0]
				fp = open(path_to_cfg_file,'r')
				for i, line in enumerate(fp):
					if i+1==int(each_range[0]):
						#print line
						missing_fp.write(str(each_range[0])+' : ' + line + '\n')
				fp.close()
missing_fp.close()
