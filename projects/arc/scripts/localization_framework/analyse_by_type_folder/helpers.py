import pickle

def save_obj(obj, name):
	with open('../obj/'+name +'.pkl','wb') as wf:
		pickle.dump(obj, wf, pickle.HIGHEST_PROTOCOL)
	
def load_obj(name):
	with open('../obj/' + name, 'rb') as rf:
		return pickle.load(rf)
