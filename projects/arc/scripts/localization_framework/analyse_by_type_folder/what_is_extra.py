#!/usr/bin/python

import os
import sys
import helpers
import json


simplified_configs_root_dir = '/shared/configs/uwmadison/snapshots_6hr/simplified_dist/'
analysis_dir_path = '../analysis/'
extra_logs = os.listdir('../analysis')
extra_analysis_filename = analysis_dir_path+'extra_localization.analysis'
extra_logs = [i for i in extra_logs if i.startswith('extra')]
extra_fp = open(extra_analysis_filename,'w')

for log in extra_logs:
	curr_snapshot = log.split('.')[0].split('_')[1]+'/'
	extra_fp.write(log+'\n\n')
	with open(analysis_dir_path+log,'r') as jsonfile:
		json_log_dict = json.load(jsonfile)
		for key in json_log_dict.keys():
			curr_cfg_filename = key + '.cfg'
			extra_fp.write(key+'\n')

			list_of_ranges_extra_in_key = json_log_dict[key]
			#open file router key in the relevant snapshot. 
			for each_range  in list_of_ranges_extra_in_key:
				path_to_cfg_file = simplified_configs_root_dir+curr_snapshot+curr_cfg_filename
				#print path_to_cfg_file
				#print curr_snapshot, ' -> ', curr_cfg_filename, ' --> ' ,each_range[0]
				fp = open(path_to_cfg_file,'r')
				for i, line in enumerate(fp):
					if i+1==int(each_range[0]):
						#print line
						extra_fp.write(each_range[0]+' : ' + line + '\n')
				fp.close()
extra_fp.close()
