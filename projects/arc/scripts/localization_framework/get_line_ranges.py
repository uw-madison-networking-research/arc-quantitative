#!/usr/bin/python
import os
import sys
import helpers

path = '/shared/output/changes/simplified_dist/'

list_snapshot_dirs=  os.listdir(path)
for snapshot_dir in list_snapshot_dirs:
	#get all diff files for each router. 
	list_diffs = [i for i in os.listdir(path+snapshot_dir) if i.endswith('cfg.diff')]
	line_ranges_dict = dict()
	for diff in list_diffs:
		#get a list of line-number ranges
		with open(path+snapshot_dir+'/'+diff, 'r') as df:
			temp_diff_list=  []
			for line in df: # for each line in the diff file:
				if line.startswith('@@'):
					line_range = line.split(' ')[1]
					start_line_num = int(line_range.split(',')[0][1:])
					count = int(line_range.split(',')[1])
					temp_diff_list.append((start_line_num, start_line_num+count))
			line_ranges_dict[diff] = temp_diff_list
			print temp_diff_list
	helpers.save_obj(line_ranges_dict, snapshot_dir+'_lineranges_changed')
	
			
