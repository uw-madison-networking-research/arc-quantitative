#!/bin/bash

if [ ! -z "$GRAPHVIZ" ]; then
    $BASEDIR/scripts/render_graphs.sh $LOGDIR/graphs
fi
