#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
source $BASEDIR/scripts/common_preamble.sh

# Additional Settings
if [ -z "$REPAIR_MOD" ]; then
    echo "Must specify modifier: all-tcs, per-dst, per-src, per-tc, per-pair, isolated"
    exit 1
fi
SERIALIZED_MODS=$LOGDIR/mods_$REPAIR_MOD.obj
if [ !-f "$SERIALIZED_MODS" ]; then
    echo "Modifications file ($SERIALIZED_MODS) doe snot exist"
    exit 1
fi
EXPORT_DIR=$LOGDIR/repaired_configs_$REPAIR_MOD/
mkdir -p $EXPORT_DIR

java $ARC_ARGS -exportdir $EXPORT_DIR -savemods $SERIALIZED_MODS \
	 2>&1 | tee $LOGDIR/translate.log

JAVAEXIT=${PIPESTATUS[0]}
if [[ $JAVAEXIT -ne 0 ]]; then
    exit $JAVAEXIT
fi

source $BASEDIR/scripts/common_postamble.sh
