#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."
source $BASEDIR/scripts/common_preamble.sh

# Additional settings
TESTDIR="$BASEDIR/../../testGVs/$NETNAME"
if [ ! -d "$TESTDIR" ]; then
    echo "Test files directory \"$TESTDIR\" does not exist"
    exit 1
fi

java $ARC_ARGS -testgraphs $TESTDIR 2>&1 | tee "$LOGDIR/test.log"

JAVAEXIT=${PIPESTATUS[0]}
if [[ $JAVAEXIT -ne 0 ]]; then
    exit $JAVAEXIT
fi

source $BASEDIR/scripts/common_postamble.sh
