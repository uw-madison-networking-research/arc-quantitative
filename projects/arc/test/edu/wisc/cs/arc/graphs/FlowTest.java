package edu.wisc.cs.arc.graphs;

import org.batfish.datamodel.IpAccessList;
import org.batfish.datamodel.IpWildcard;
import org.batfish.datamodel.LineAction;
import org.batfish.datamodel.Prefix;
import org.batfish.representation.cisco.CiscoConfiguration;
import org.batfish.representation.cisco.StandardAccessList;
import org.batfish.representation.cisco.StandardAccessListLine;
import org.junit.Assert;
import org.junit.Test;

public class FlowTest {

	@Test
	public void testStandard() {
		Flow flow = new Flow(new PolicyGroup(new Prefix("10.0.1.0/24")),
				new PolicyGroup(new Prefix("10.0.2.0/24")));
		StandardAccessList stdAcl = new StandardAccessList("blocked", 1);
		stdAcl.addLine(new StandardAccessListLine("1", LineAction.REJECT, 
				new IpWildcard("10.0.1.0/24"), null, null));
		stdAcl.addLine(new StandardAccessListLine("2", LineAction.ACCEPT, 
				IpWildcard.ANY, null, null));
		CiscoConfiguration config = new CiscoConfiguration(null);
		config.getStandardAcls().put(stdAcl.getName(), stdAcl);
		IpAccessList acl = config.toVendorIndependentConfiguration().
		    getIpAccessLists().firstEntry().getValue();
		Assert.assertTrue(flow.isBlocked(acl));
		
		stdAcl = new StandardAccessList("allowed", 2);
		stdAcl.addLine(new StandardAccessListLine("1", LineAction.ACCEPT, 
		        IpWildcard.ANY, null, null));
		stdAcl.addLine(new StandardAccessListLine("2",LineAction.REJECT, 
				new IpWildcard("10.0.1.0/24"), null, null));
		config = new CiscoConfiguration(null);
        config.getStandardAcls().put(stdAcl.getName(), stdAcl);
        acl = config.toVendorIndependentConfiguration().
            getIpAccessLists().firstEntry().getValue();
		Assert.assertFalse(flow.isBlocked(acl));
	}
}
