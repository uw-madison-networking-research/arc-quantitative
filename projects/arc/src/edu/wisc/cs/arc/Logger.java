package edu.wisc.cs.arc;

/**
 * Interface for writing error/debug messages to the console.
 * @author Aaron Gember-Jacobson
 */
public class Logger {
    public enum Level
    { FATAL, ERROR, WARN, INFO, DEBUG }
    
    private Level levelEnabled;

    private static Logger loggerInstance;
    
    /**
     * Create a logger.
     * @param level lowest level of messages to log
     */
    private Logger(Level level) {
    	this.levelEnabled = level;
    }

    /**
     * Get or create a logger.
     * @param level lowest level of messages to log.
     * @return Logger
     */
    public static Logger getInstance(Level level) {
        if (!instanceExists()){
            loggerInstance = new Logger(level);
        }
        loggerInstance.levelEnabled = level;
        return loggerInstance;
    }

    /**
     * Get the program logger.
     * @return Logger
     */
    public static Logger getInstance() {
        if (!instanceExists()) {
            System.err.println("Logger not created properly. Use Logger.getInstance(Level level)");
            loggerInstance = new Logger(Level.INFO);
        }
        return loggerInstance;
    }

    private static boolean instanceExists() {
        return (null != loggerInstance);
    }
    
    /**
     * Log a fatal message.
     * @param msg message to log
     */
    public void fatal(String msg) {
		this.write("FATAL ERROR: "+msg, Level.FATAL);
	}
    
    /**
     * Log an error message.
     * @param msg message to log
     */
    public void error(String msg) {
		this.write(msg, Level.ERROR);
	}
    
    /**
     * Log a warning message.
     * @param msg message to log
     */
    public void warn(String msg) {
		this.write(msg, Level.WARN);
	}

    /**
     * Log an informational message.
     * @param msg message to log
     */
	public void info(String msg) {
		this.write(msg, Level.INFO);
	}
	
    /**
     * Log an informational message.
     * @param msg message to log
     */
	public void infoNoNL(String msg) {
		this.writeNoNL(msg, Level.INFO);
	}
	
	/**
     * Log a debug message.
     * @param msg message to log
     */
    public void debug(String msg) {
		this.write(msg, Level.DEBUG);
	}
	
    /**
     * Output a message to the log if the level is higher than or equal to
     * the currently enabled logging level.
     * @param msg message to log
     * @param level level of message to log
     */
	private void write(String msg, Level level) {
		this.writeNoNL(msg + "\n", level);
	}
	
    /**
     * Output a message to the log without a newline if the level is higher 
     * than or equal to the currently enabled logging level.
     * @param msg message to log
     * @param level level of message to log
     */
	private void writeNoNL(String msg, Level level) {
		if (this.levelEnabled.compareTo(level) >= 0) {
			System.out.print(msg); 
		}
	}
	
	/**
	 * Determine whether a particular level is logged.
	 * @param level the level to check
	 * @return true if the level is logged, otherwise false
	 */
	public boolean willLog(Level level) {
		return (this.levelEnabled.compareTo(level) >= 0);
	}
}
