package edu.wisc.cs.arc.repair.graph;

import edu.wisc.cs.arc.graphs.Vertex;
import edu.wisc.cs.arc.repair.VirtualDirectedEdge;

import java.io.Serializable;

/**
 * Represents a modification to an extended topology graph. 
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class GraphModification<V extends Vertex> implements Serializable {
	private static final long serialVersionUID = 987458412546684313L;

	/** Type of modification */
	public enum Action {
		ADD,
		REMOVE,
		CHANGE
	}
	
	/** Type of modification */
	private Action action;
	
	/** Edge impacted */
	private VirtualDirectedEdge<V> edge;
	
	/**
	 * Create an edge modification.
	 * @param action type of modification
	 * @param edge edge modified
	 */
	public GraphModification(Action action, 
			VirtualDirectedEdge<V> edge) {
		this.action = action;
		this.edge = edge;
	}
	
	
	/**
	 * Get the type of modification.
	 * @return the type of modification
	 */
	public Action getAction() {
		return this.action;
	}
	
	/**
	 * Get the edge being modified.
	 * @return the edge being modified; null the modification is not to an edge
	 */
	public VirtualDirectedEdge<V> getEdge() {
		return this.edge;
	}
	
	@Override
	public String toString() {
		return this.action.toString() + " " + this.edge.toString() + " " 
		        + this.edge.getWeight();
	}
	
	/**
	 * Determine if two graph modifications apply the same action to the same
	 * edge.
	 * @param other modification to compare against
	 */
	public boolean equals(Object other) {
		if (other instanceof GraphModification) {
			GraphModification<V> otherMod  = (GraphModification<V>)other;
			return (otherMod.getAction().equals(this.getAction())
					&& (otherMod.getEdge().equals(
							this.getEdge())));
		}
		return false;
	}
}
