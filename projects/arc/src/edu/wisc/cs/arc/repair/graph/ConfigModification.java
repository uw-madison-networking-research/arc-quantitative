package edu.wisc.cs.arc.repair.graph;

import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.repair.VirtualDirectedEdge;

import java.io.Serializable;

/**
 * Represents a modification to a configuration. 
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class ConfigModification implements Serializable {
    private static final long serialVersionUID = 5282494433212388455L;

    /** Type of modification */
    public enum Action {
        ADD,
        REMOVE,
        CHANGE
    }
    
    /** Stanza modified */
    public enum Stanza {
        INTERFACE,
        ROUTER,
        STANDARD_ACL,
        EXTENDED_ACL
    }
    
    /** Substanza modified */
    public enum Substanza {
        // Interace
        PREFIX,
        ACTIVE,
        OSPF_COST,
        INCOMING_FILTER,
        OUTGOING_FILTER,
        // Routing process
        ADMINISTRATIVE_DISTANCE,
        INTERFACE,
        BGP_NEIGHBOR,
        BGP_REDISTRIBUTION,
        OSPF_REDISTRIBUTION,
        // ACL
        LINE,
        // Static Route
        STATIC_ROUTE
    }
    
    /** Type of modification*/
    private Action action;
    
    /** Stanza modified */
    private Stanza stanza;
    
    /** Stanza identifier */
    private String stanzaIdentifier;
    
    /** Substanza modified */
    private Substanza substanza;
    
    /** Substanza identifier */
    private String substanzaIdentifier;

    /** Edge */
    private VirtualDirectedEdge<ProcessVertex> edge;

    public ConfigModification(Action action, Stanza stanza,
                              String stanzaIdentifier, Substanza substanza,
                              String substanzaIdentifier,
                              VirtualDirectedEdge<ProcessVertex> edge) {
        this(action, stanza, stanzaIdentifier, substanza, substanzaIdentifier);
        this.edge = edge;

    }
    
    public ConfigModification(Action action, Stanza stanza,
            String stanzaIdentifier, Substanza substanza,
            String substanzaIdentifier) {
        this.action = action;
        this.stanza = stanza;
        this.stanzaIdentifier = stanzaIdentifier;
        this.substanza = substanza;
        this.substanzaIdentifier = substanzaIdentifier;
    }
    
    public ConfigModification(Action action, Stanza stanza, 
            String stanzaIdentifier) {
        this(action, stanza, stanzaIdentifier, null, null);
    }
    
    public ConfigModification(Stanza stanza, String stanzaIdentifier, 
            Substanza substanza, String substanzaIdentifier) {
        this(Action.CHANGE, stanza, stanzaIdentifier, null, null);
    }

    public Action getAction() {
        return action;
    }

    public Stanza getStanza() {
        return stanza;
    }

    public Substanza getSubstanza() {
        return substanza;
    }

    @Override
    public String toString() {
        String result = this.action.toString() + " " + this.stanza.toString() 
                + " " + this.stanzaIdentifier;
        if (this.substanza != null) {
            result += " " + this.substanza.toString() + " " 
                    + this.substanzaIdentifier;
        }
        return result;
    }
}
