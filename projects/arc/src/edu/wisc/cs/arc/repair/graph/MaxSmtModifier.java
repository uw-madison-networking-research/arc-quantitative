package edu.wisc.cs.arc.repair.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.DeviceGraph;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.PolicyGroup;
import edu.wisc.cs.arc.graphs.ProcessGraph;
import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.policies.Policy;
import edu.wisc.cs.arc.repair.RepairException;
import edu.wisc.cs.arc.repair.VirtualDirectedEdge;

/**
 * Formulates and solves a MaxSMT problem to determine how a set of extended
 * topology graphs should be modified to conform to a set of policies.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class MaxSmtModifier extends GraphModifier {

	public MaxSmtModifier(Map<Flow, List<Policy>> policies, 
			DeviceGraph deviceEtg, ProcessGraph baseEtg,
			Map<PolicyGroup, ProcessGraph> dstEtgs, 
			Map<Flow, ProcessGraph> flowEtgs, Settings settings) {
		super(policies, deviceEtg, baseEtg, dstEtgs, flowEtgs, settings);
	}

	@Override
	public ConfigModificationGroup getModifications(){
		// Results of MaxSMT solving
		Map<Flow, List<VirtualDirectedEdge<ProcessVertex>>> flowEdges = null;
		Map<PolicyGroup, List<VirtualDirectedEdge<ProcessVertex>>> dstEdges = 
				null;
		List<VirtualDirectedEdge<ProcessVertex>> baseEdges = null;
		
		// Check whether to assume all policies are broken or only repair for
		// explicitly broken policies
		Set<Flow> brokenFlows = this.policies.keySet();
		if (settings.shouldOnlyRepairBrokenETGs()) {
			brokenFlows = this.getBrokenFlows();
			logger.debug("COUNT: brokenFlows "+brokenFlows.size());
		}
		
		switch (this.algorithm) {
		// Formulate and solve a MaxSMT problem for each traffic class in 
		// isolation
		case MAXSMT_PER_TC:
		{
			flowEdges = new LinkedHashMap<Flow, 
					List<VirtualDirectedEdge<ProcessVertex>>>();
			for (Flow flow : this.etgs.keySet()) {
				// Only consider flows that have broken policies
				if (!brokenFlows.contains(flow)) {
					continue;
				}
				
				long startTime = System.currentTimeMillis();
				
				// Formulate and solve problem
				MaxSmtFormulation formulation = new MaxSmtFormulation(this, 
						Arrays.asList(new Flow[] {flow}));
				Map<Flow, List<VirtualDirectedEdge<ProcessVertex>>> 
					flowSolution = formulation.getEdges();
				if (null == flowSolution) {
					return null;
				}
				
				flowEdges.put(flow, flowSolution.get(flow));
                formulation.getDisjointEdges();
				
				long endTime = System.currentTimeMillis();
				System.out.println("TIMEONE: " 
            			+ this.settings.getModifierAlgorithm()
            			+ " " + (endTime - startTime) + " ms "
            			+ flow.toString());
			}
			break;
		}
		// Formulate and solve a MaxSMT problem for each traffic class in 
		// isolation
		case MAXSMT_PER_PAIR:
		{
			flowEdges = new LinkedHashMap<Flow, 
					List<VirtualDirectedEdge<ProcessVertex>>>();
			
			// Group flows by source and destination
			Map<Flow,List<Flow>> flowsByPair =
					new LinkedHashMap<Flow,List<Flow>>();
			for (Flow flow : this.etgs.keySet()) {
				Flow key = new Flow(flow.getSource(), flow.getDestination());
				if (flow.getDestination().getStartIp().asLong() 
						< flow.getSource().getStartIp().asLong()) {
					key = new Flow(flow.getDestination(), flow.getSource());
				}
				if (!flowsByPair.containsKey(key)) {
					flowsByPair.put(key, new ArrayList<Flow>());
				}
				List<Flow> flowsForPair = flowsByPair.get(key);
				flowsForPair.add(flow);
			}
			
			for (List<Flow> flows : flowsByPair.values()) {
				// Only consider pairs with flows that have broken policies
				boolean hasBroken = false;
				for (Flow flow : flows) {
					if (brokenFlows.contains(flow)) {
						hasBroken = true;
						break;
					}
				}
				if (!hasBroken) {
					continue;
				}
				
				long startTime = System.currentTimeMillis();
				
				// Formulate and solve problem
				MaxSmtFormulation formulation = new MaxSmtFormulation(this,
						flows);
				Map<Flow, List<VirtualDirectedEdge<ProcessVertex>>> 
						flowsSolution = formulation.getEdges();
				if (null == flowsSolution) {
					return null;
				}
				
				flowEdges.putAll(flowsSolution);
				
				long endTime = System.currentTimeMillis();
				System.out.println("TIMEONE: " 
            			+ this.settings.getModifierAlgorithm()
            			+ " " + (endTime - startTime) + " ms "
            			+ flows.get(0).getSource().toString() + "<->"
            			+ flows.get(0).getDestination().toString());
			}
			break;
		}
		// Formulate and solve a MaxSMT problem for each set of traffic classes 
		// with the same destination
		case MAXSMT_PER_DST:
		{
			flowEdges = new LinkedHashMap<Flow, 
					List<VirtualDirectedEdge<ProcessVertex>>>();
			if (GraphModifierObj.NUM_LINES_CHANGED == this.objective) {
				dstEdges = new LinkedHashMap<PolicyGroup, 
						List<VirtualDirectedEdge<ProcessVertex>>>();
			}
			
			// Group flows by destination
			Map<PolicyGroup,List<Flow>> flowsByDst =
					new LinkedHashMap<PolicyGroup,List<Flow>>();
			for (Flow flow : this.etgs.keySet()) {
				if (!flowsByDst.containsKey(flow.getDestination())) {
					flowsByDst.put(flow.getDestination(),new ArrayList<Flow>());
				}
				List<Flow> flowsForDst = flowsByDst.get(flow.getDestination());
				flowsForDst.add(flow);
			}
			
			for (List<Flow> flows : flowsByDst.values()) {
				// Only consider groups with flows that have broken policies
				boolean hasBroken = false;
				for (Flow flow : flows) {
					if (brokenFlows.contains(flow)) {
						hasBroken = true;
						break;
					}
				}
				if (!hasBroken) {
					continue;
				}
				
				long startTime = System.currentTimeMillis();
				
				// Formulate and solve problem
				MaxSmtFormulation formulation = new MaxSmtFormulation(this,
						flows);
				Map<Flow, List<VirtualDirectedEdge<ProcessVertex>>> 
						flowsSolution = formulation.getEdges();
				if (null == flowsSolution) {
					return null;
				}
				if (GraphModifierObj.NUM_LINES_CHANGED == this.objective) {
					PolicyGroup destination = flows.get(0).getDestination();
					dstEdges.put(destination, 
							formulation.getDestinationEdges().get(destination));
				}
                formulation.getDisjointEdges();
				
				flowEdges.putAll(flowsSolution);
				
				long endTime = System.currentTimeMillis();
				System.out.println("TIMEONE: " 
            			+ this.settings.getModifierAlgorithm()
            			+ " " + (endTime - startTime) + " ms "
            			+ flows.get(0).getDestination().toString());
			}
			break;
		}
		// Formulate and solve a MaxSMT problem for each set of traffic classes 
		// with the same source
		case MAXSMT_PER_SRC:
		{
			flowEdges = new LinkedHashMap<Flow, 
					List<VirtualDirectedEdge<ProcessVertex>>>();
			
			// Group flows by source
			Map<PolicyGroup,List<Flow>> flowsBySrc =
					new LinkedHashMap<PolicyGroup,List<Flow>>();
			for (Flow flow : this.etgs.keySet()) {
				if (!flowsBySrc.containsKey(flow.getSource())) {
					flowsBySrc.put(flow.getSource(),new ArrayList<Flow>());
				}
				List<Flow> flowsForSrc = flowsBySrc.get(flow.getSource());
				flowsForSrc.add(flow);
			}
			
			for (List<Flow> flows : flowsBySrc.values()) {
				// Only consider groups with flows that have broken policies
				boolean hasBroken = false;
				for (Flow flow : flows) {
					if (brokenFlows.contains(flow)) {
						hasBroken = true;
						break;
					}
				}
				if (!hasBroken) {
					continue;
				}
				
				long startTime = System.currentTimeMillis();
				
				// Formulate and solve problem
				MaxSmtFormulation formulation = new MaxSmtFormulation(this, 
						flows);
				Map<Flow, List<VirtualDirectedEdge<ProcessVertex>>> 
						flowsSolution = formulation.getEdges();
				if (null == flowsSolution) {
					return null;
				}
				
				flowEdges.putAll(flowsSolution);
				
				long endTime = System.currentTimeMillis();
				System.out.println("TIMEONE: " 
            			+ this.settings.getModifierAlgorithm()
            			+ " " + (endTime - startTime) + " ms "
            			+ flows.get(0).getSource().toString());
			}
			break;
		}
		// Formulate and solve a single MaxSMT problem encompassing all traffic
		// classes
		case MAXSMT_ALL_TCS:
		{			
			if (brokenFlows.size() == 0) {
				flowEdges = new LinkedHashMap<Flow, 
						List<VirtualDirectedEdge<ProcessVertex>>>();
				break;
			}
			
			List<Flow> flows = new ArrayList<Flow>();
			flows.addAll(this.etgs.keySet());
			
			// Formulate and solve problem
			MaxSmtFormulation formulation = new MaxSmtFormulation(this, 
					flows);
			flowEdges = formulation.getEdges();
			if (GraphModifierObj.NUM_LINES_CHANGED == this.objective) {
				dstEdges = formulation.getDestinationEdges();
				baseEdges = formulation.getBaseEdges();
                formulation.getDisjointEdges();
			}
			if (null == flowEdges) {
				return null;
			}
			break;
		}
		default:
			throw new RepairException(this.algorithm.toString() 
					+ " repair algorithm not supported by "
					+ this.getClass().getSimpleName());
		}
		
		logger.debug("Solution:");
		for (Flow flow : flowEdges.keySet()) {
			logger.debug(flow.toString());
			for (VirtualDirectedEdge<ProcessVertex> edge : 
					flowEdges.get(flow)) {
				logger.debug("\t" + edge.toString() + " (" + edge.getWeight() 
				    + ")");
			}
		}
		
		// Convert solution to set of modifications
		return this.diffEdges(flowEdges, dstEdges, baseEdges);
	}
	
	/**
	 * Diff the edges in existing ETGs with the edges in the solution to 
	 * produce a set of modifications that must be made to the ETGs to match 
	 * the solution.
	 * @param desiredEdges edges in the solution
	 * @param dstEdges destination edges in the solution
	 * @param baseEdges base edges in the solution
	 * @return
	 */
	private ConfigModificationGroup diffEdges(
			Map<Flow, List<VirtualDirectedEdge<ProcessVertex>>> flowEdges,
			Map<PolicyGroup, List<VirtualDirectedEdge<ProcessVertex>>> dstEdges, 
			List<VirtualDirectedEdge<ProcessVertex>> baseEdges) {
		logger.info("Modifications:");
		
		// Compute base modifications
		List<GraphModification<ProcessVertex>> baseMods = null;
		if (baseEdges != null) {
			baseMods = this.diffEdges(this.baseEtg, baseEdges);
			
			// Output modifications
			if (baseMods.size() > 0) {
				logger.info("* -> *");
				for (GraphModification<ProcessVertex> mod : baseMods) {
					logger.info("\t" + mod.toString());
				}
			}
		}
				
		// Compute for destination ETGs
		Map<PolicyGroup, List<GraphModification<ProcessVertex>>> dstMods = null;
		if (dstEdges != null) {
			dstMods = new LinkedHashMap<PolicyGroup,List<GraphModification<ProcessVertex>>>();
			for (PolicyGroup destination : dstEdges.keySet()) {
				// Compute modifications
				List<VirtualDirectedEdge<ProcessVertex>> edges =
						dstEdges.get(destination);
				ProcessGraph dstEtg = this.dstEtgs.get(destination);
				List<GraphModification<ProcessVertex>> mods = 
						this.diffEdges(dstEtg, edges);
				dstMods.put(destination, mods);
				
				// Output modifications
				if (mods.size() > 0) {
					logger.info("* -> " + destination.toString());
					for (GraphModification<ProcessVertex> mod : mods) {
						String parent = "";
						if (baseMods != null && baseMods.contains(mod)) {
							parent = "(Base)";
						}
						logger.info("\t" + mod.toString() + " " + parent);
					}
				}
			}
		}
		
		Map<Flow, List<GraphModification<ProcessVertex>>> flowMods = 
				new LinkedHashMap<Flow,List<GraphModification<ProcessVertex>>>();
		for (Flow flow : flowEdges.keySet()) {
			// Compute modifications
			List<VirtualDirectedEdge<ProcessVertex>> edges = flowEdges.get(flow);
			ProcessGraph flowEtg = this.etgs.get(flow);
			List<GraphModification<ProcessVertex>> mods =
					this.diffEdges(flowEtg, edges);

			// Output modifications
			if (mods.size() > 0) {
				logger.info(flow.toString());
				for (GraphModification<ProcessVertex> mod : mods) {
					String parent = "";
					if (baseMods != null && baseMods.contains(mod)) {
						parent = "(Base)";
					} else if (dstMods != null
							&& dstMods.get(flow.getDestination()) != null
							&& dstMods.get(flow.getDestination()).contains(mod)) {
						parent = "(Dst)";
					}
					logger.info("\t" + mod.toString() + " " + parent);
				}
			}
			flowMods.put(flow, mods);
		}
		return new ConfigModificationGroup(flowMods, dstMods, baseMods);
	}
	
	/**
	 * Diff the edges in an existing ETG with the edges in a solution and
	 * produce a set of modifications that must be made to the ETG to match
	 * the solution.
	 * @param etg extended topology graph
	 * @param desiredEdges edges in the solution
	 * @return the set of modifications to make
	 */
	private List<GraphModification<ProcessVertex>> diffEdges(ProcessGraph etg, 
				List<VirtualDirectedEdge<ProcessVertex>> desiredEdges) {
		List<GraphModification<ProcessVertex>> modifications = 
				new ArrayList<GraphModification<ProcessVertex>>();
		
		// Identify edge additions and changes
		for (VirtualDirectedEdge<ProcessVertex> edge : desiredEdges) {
			if (!etg.containsEdgeExcludeBlocked(edge)) {
				modifications.add(new GraphModification<ProcessVertex>(
						GraphModification.Action.ADD, edge));
			} else {
			    DirectedEdge originalEdge = etg.getEdge(edge.getSource(), 
			            edge.getDestination());
			    if (originalEdge.getWeight() != edge.getWeight()) {
			        modifications.add(new GraphModification<ProcessVertex>(
	                        GraphModification.Action.CHANGE, edge));
			    }
			}
		}
		
		// Identify edge removals
		Iterator<DirectedEdge<ProcessVertex>> iterator = etg.getEdgesIterator();
		while (iterator.hasNext()) {
			DirectedEdge<ProcessVertex> edge = iterator.next();
			if (edge.isBlocked()) {
				continue;
			}
			if (!desiredEdges.contains(edge)) {
				modifications.add(new GraphModification<ProcessVertex>(
						GraphModification.Action.REMOVE,
						new VirtualDirectedEdge<ProcessVertex>(edge)));
			}
		}
		
		return modifications;
	}
}
