package edu.wisc.cs.arc.repair.graph.configwriters;

/**
 * Details the contents and location of configuration snippets.
 * @author Franklin van Nes (fvannes@colgate.edu)
 */
public class ConfigInsert {

    public enum Method {
        INSERT,
        OVERWRITE,  // overwrite existing info.
        DELETE,
    }

    private Method insertMethod = null;
    private String snippet = "";

    private int insertIndex = -1;
    private int exitIndex = -1;

    public ConfigInsert(String snippet, Method insertMethod, int insertIndex, int exitIndex) {
        this.insertMethod = insertMethod;
        this.insertIndex = insertIndex;
        this.exitIndex = exitIndex;  // if method is insert then exitIndex doesn't matter.
        this.snippet = snippet;
    }

    public Method getMethod() {
        return insertMethod;
    }

    public String getSnippet() {
        return snippet;
    }

    public int getInsertIndex() {
        return insertIndex;
    }

    public int getExitIndex() {
        return exitIndex;
    }

    public String toString() {
        return String.format("idx:%d to %d  %s ---> %s", insertIndex, exitIndex, insertMethod, snippet);
    }

    public String toString(String configName) {
        return String.format("%s idx:%d to %d  %s ---> %s", configName, insertIndex, exitIndex, insertMethod, snippet);
    }

}