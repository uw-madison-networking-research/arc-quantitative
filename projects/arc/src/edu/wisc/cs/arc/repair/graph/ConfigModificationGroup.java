package edu.wisc.cs.arc.repair.graph;

import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.PolicyGroup;
import edu.wisc.cs.arc.graphs.ProcessGraph;
import edu.wisc.cs.arc.graphs.ProcessVertex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Groups the different HARC classes of modifications (aETG, dETG, and tcETG modifications).
 *
 * @author Franklin van Nes (fvannes@colgate.edu)
 */
public class ConfigModificationGroup {

    /** Flow/Traffic Class modifications **/
    private Map tcMods;
    /** Destination modifications **/
    private Map dMods;
    /** Base modifications **/
    private List aMod;

    /** Flow/Traffic Class ETG **/
    private Map<Flow, edu.wisc.cs.arc.graphs.ProcessGraph> tcEtgs;
    /** Destination ETGs **/
    private Map<PolicyGroup, ProcessGraph> dEtgs;
    /** Base ETG **/
    private ProcessGraph aEtg;

    ConfigModificationGroup(Map<Flow, List<GraphModification<ProcessVertex>>> tcMods,
                            Map<PolicyGroup, List<GraphModification<ProcessVertex>>> dMods,
                            List<GraphModification<ProcessVertex>> aMod) {
        this.tcMods = tcMods;
        if (null == tcMods) {
            this.tcMods = new HashMap(0);
        }
        this.dMods = dMods;
        if (null == dMods) {
            this.dMods = new HashMap(0);
        }
        this.aMod = aMod;
        if (null == aMod) {
            this.aMod = new ArrayList(0);
        }
    }

    public ConfigModificationGroup(Map tcMods, Map dMods, List aMod,
                                   Map<Flow, ProcessGraph> tcEtgs,
                                   Map<PolicyGroup, ProcessGraph> dEtgs,
                                   ProcessGraph aEtg) {
        this.tcMods = tcMods;
        if (null == tcMods) {
            this.tcMods = new HashMap(0);
        }
        this.dMods = dMods;
        if (null == dMods) {
            this.dMods = new HashMap(0);
        }
        this.aMod = aMod;
        if (null == aMod) {
            this.aMod = new ArrayList(0);
        }
        this.tcEtgs = tcEtgs;
        if (null == tcEtgs) {
            this.tcEtgs = new HashMap<>(0);
        }
        this.dEtgs = dEtgs;
        if (null == dEtgs) {
            this.dEtgs = new HashMap<>(0);
        }
        this.aEtg = aEtg;
    }

    public Map<Flow, ProcessGraph> getTcEtgs() {
        return tcEtgs;
    }

    public Map<PolicyGroup, ProcessGraph> getDestEtgs() {
        return dEtgs;
    }

    public ProcessGraph getBaseEtg() {
        return aEtg;
    }

    public void setEtgs(Map<Flow, ProcessGraph> tcEtgs,
                        Map<PolicyGroup, ProcessGraph> dEtgs,
                        ProcessGraph aEtg) {
        if (null != tcEtgs) {
            this.tcEtgs = tcEtgs;
        }
        if (null != dEtgs) {
            this.dEtgs = dEtgs;
        }
        if (null != aEtg) {
            this.aEtg = aEtg;
        }
    }

    public Map<Flow, List<GraphModification<ProcessVertex>>> getTcMods() {
        return tcMods;
    }

    public Map<PolicyGroup, List<GraphModification<ProcessVertex>>> getDestMods() {
        return dMods;
    }

    public List<GraphModification<ProcessVertex>> getBaseMod() {
        return aMod;
    }

}
