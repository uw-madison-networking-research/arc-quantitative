package edu.wisc.cs.arc.repair;

import edu.wisc.cs.arc.configs.Config;

/**
 * Created by smaug on 7/6/17.
 */
public class GeneticProgrammingResult implements Comparable<GeneticProgrammingResult>{
    private int score;
    private float selectionProbability;
    private Config mutant;
    private Mutation mutation;

    public GeneticProgrammingResult(Config mutant) {
        this.score = Integer.MAX_VALUE;
        this.mutant = mutant;
        this.mutation = new Mutation();
    }
    public GeneticProgrammingResult(Config mutant, Mutation mutation) {
        this.score = Integer.MAX_VALUE;
        this.mutant = mutant;
        this.mutation = mutation;
    }

    public GeneticProgrammingResult(int score, Config mutant, Mutation mutation) {
        this.score = score;
        this.mutant = mutant;
        this.mutation = mutation;
    }

    public void setSelectionProbability(float selectionProbability) {
        this.selectionProbability = selectionProbability;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setMutant(Config mutant) {
        this.mutant = mutant;
    }

    public void setMutation(Mutation mutation) {
        this.mutation = mutation;
    }

    public int getScore() {
        return score;
    }

    public Config getMutant() {
        return mutant;
    }

    public Mutation getMutation() {
        return mutation;
    }

    public float getSelectionProbability() {
        return selectionProbability;
    }

	@Override
	public int compareTo(GeneticProgrammingResult other) {
		return Integer.compare(this.score, other.score);
	}
}
