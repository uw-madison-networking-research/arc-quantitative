package edu.wisc.cs.arc.repair.graph;

import edu.wisc.cs.arc.repair.RepairException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Saves and loads serializable ModificationPairs.
 * @author Franklin van Nes (fvannes@colgate.edu)
 */
public class ModificationsFile {

    /**
     * Serializes modifications to a file.
     * @param modifications the modifications to serialize
     * @param filepath the file to save the modifications to
     */
    public static void saveModifications(List<ModificationPair> modifications, String filepath) {
        // Save modifications
        try {
            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
            for (ModificationPair mod : modifications) {
                objOut.writeObject(mod);
            }
            objOut.close();
            fileOut.close();
        } catch(IOException e) {
            e.printStackTrace();
            throw new RepairException("Could not save modifications", e);
        }
    }

    /**
     * Deserializes modifications from a file.
     * @param filepath the file to load the modifications from
     * @return the ModificationPairs in the file
     */
    public static List<ModificationPair> loadModifications(String filepath) {
        List<ModificationPair> mods = new ArrayList<>();
        try {
            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream objIn = new ObjectInputStream(fileIn);
            try {
                while (true) {
                    Object obj = objIn.readObject();
                    if (obj instanceof ModificationPair) {
                        ModificationPair mod = (ModificationPair) obj;
                        if (!mods.contains(mod)) {
                            mods.add(mod);
                        }
                    } else {
                        break;
                    }
                }
            }
            catch (EOFException e) {

            }
            objIn.close();
            fileIn.close();
        } catch(IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RepairException("Could not load saved Modifications", e);
        }
        return mods;
    }
}
