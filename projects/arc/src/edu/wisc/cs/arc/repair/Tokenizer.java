package edu.wisc.cs.arc.repair;

import org.antlr.v4.runtime.Token;

import java.io.File;
import java.util.*;
import java.util.Map.Entry;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.configs.Config;
import edu.wisc.cs.arc.configs.ConfigurationTasks;

import org.batfish.datamodel.Configuration;

/**
 * Parses config files and breaks into tokens
 * Created by amahmood on 5/26/17.
 */
public class Tokenizer {

    private final static String IGNORE = "^([,\n \t!]+)";
    private final static String WHITESPACE = "\\s";
    private final static String NEWLINE = "\n";
    private Logger logger;

    private Map<String, Configuration> genericConfigurations;
    private Map<String, List<? extends Token>> tokenMap;
    private Map<String, Config> configs;
    private Settings settings;

    public Tokenizer(Settings settings){
        this.settings = settings;
        this.logger = this.settings.getLogger();
        
        this.tokenMap = new HashMap<String, List<? extends Token>>();
        this.genericConfigurations = new HashMap<String, Configuration>();
    }

    public void build() {
        configs = ConfigurationTasks.loadConfigurations(settings);

        for (Entry<String, Config> entry : configs.entrySet()){
            String hostname = entry.getKey();
            Config config = entry.getValue();

            logger.info("Tokenizing " + hostname);
            List<? extends Token> tokens = config.getTokens();
            this.tokenMap.put(config.getHostname(), tokens);
//            this.genericConfigurations.put(hostname,
//                    config.getGenericConfiguration());
        }
    }

    /**
     * Compares config tokens and returns a similarity metric
     */
    public void compareConfigs() {
        Set<String> files = tokenMap.keySet();
        Set<String> seenHosts = new HashSet<>();

        for (String cfgFileA : files) {

            logger.info("*** Analysing " + cfgFileA + " ***");

            //Save all tokens
            Set<String> tokenSet_A = new HashSet<>();

            String tokText;
            List<String> tokens_A = new ArrayList<>();
            for(Token _token : tokenMap.get(cfgFileA)){
                tokText = _token.getText();
                if (!tokText.matches("^([,\n \t!]+)")) {
                    tokens_A.add(tokText);
                    tokenSet_A.add(tokText);
                }
            }

            String rawText = configs.get(cfgFileA).getText();
            List<String> statementList_A = new LinkedList<String>();
            for (String statement : rawText.split(NEWLINE)){
                if (!statement.matches(IGNORE)){
                    statementList_A.add(statement);
                }
            }

            Set<String> tokenSet_Rest = new HashSet<>();

            for (String cfgFileB : files) {

                if (cfgFileA.equals(cfgFileB)){
                    continue;
                }

//                logger.info("Comparing against " + cfgFileB);

                Set<String> tokenSet_B = new HashSet<>();
                for(Token _token : tokenMap.get(cfgFileB)){
                    tokText = _token.getText();
                    if (!tokText.matches(IGNORE)) {
                        tokenSet_B.add(tokText);
                        tokenSet_Rest.add(tokText);
                    }
                }

                String _rawText = configs.get(cfgFileB).getText();
                Set<String> statementSet_B = new HashSet<String>();
                for (String statement : _rawText.split(NEWLINE)){
                    if (!statement.matches(IGNORE)){
//                        System.out.println(statement);
                        statementSet_B.add(statement);
                        tokenSet_Rest.add(statement);
                    }
                }
            }

            System.out.print("Tokens shared:    ");
            findCommon(tokens_A, tokenSet_Rest);

            System.out.print("Statements shared:");
            findCommon(statementList_A, tokenSet_Rest);

        }
    }

    public void findCommon(List<String> listA, Set<String> setB){
        float rawCount = 0, rawRatio = 0;

        //Count tokens that exist in both list A and set B
        for (String _token : listA) {

            if (setB.contains(_token)) {
                rawCount++;
            } else{
//                            System.out.println(_token);
            }
        }
        rawRatio = rawCount / listA.size();
        System.out.format("\t%.0f\t%.3f\n", rawCount, rawRatio);
    }

}