package edu.wisc.cs.arc.repair;

import edu.wisc.cs.arc.configs.ConfigComparer;
import edu.wisc.cs.arc.graphs.Device;
import edu.wisc.cs.arc.repair.graph.ConfigModification;

import java.util.List;

/**
 * Checks similarity between configs across devices
 * Created by amahmood on 5/25/17.
 */
public class SimilarityChecker {

    /** Devices to compare */
    private Device compareDevice;
    private List<Device> devices;

    /**
     * Compare the configurations between first device and the rest.
     */
    public SimilarityChecker(List<Device> devices) {
        this.compareDevice = devices.remove(0);
        this.devices = devices;
    }

    public void getDifferences(){
        System.out.println("Comparing " + compareDevice.getName()+" with other devices...");
        for (Device device : devices) {
            ConfigComparer comparer = new ConfigComparer(compareDevice, device);
            List<ConfigModification> differences = comparer.getDifferences();
            if (differences.size() > 0) {
                System.out.println("Device "+device.getName());
                for (ConfigModification mod : differences) {
                    System.out.println("\t" + mod.toString());
                }
            }
        }
    }

}
