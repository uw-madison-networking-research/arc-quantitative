package edu.wisc.cs.arc.repair.graph;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.*;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.Process;
import edu.wisc.cs.arc.graphs.Process.ProcessType;
import edu.wisc.cs.arc.policies.Policy;
import edu.wisc.cs.arc.repair.RepairException;
import edu.wisc.cs.arc.repair.VirtualDirectedEdge;
import edu.wisc.cs.arc.verifiers.*;

import java.util.*;

/**
 * Determines how to modify a set of extended topology graph to conform to a
 * set of policies.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public abstract class GraphModifier {
	
	/** Policies to conform to */
	protected Map<Flow, List<Policy>> policies;
	
	/** Physical network topology */
	protected DeviceGraph deviceEtg;
	
	/** Base extended topology graphs */
	protected ProcessGraph baseEtg;
	
	/** Destination extended topology graphs */
	protected Map<PolicyGroup, ProcessGraph> dstEtgs;
	
	/** Extended topology graphs */
	protected Map<Flow, ProcessGraph> etgs;
	
	/** Settings */
	protected Settings settings;
	
	/** Logger */
	protected Logger logger;
	
	/** Algorithm */
	protected GraphModifierAlgo algorithm;
	
	/** Objective */
	protected GraphModifierObj objective;
	
	/** Algorithms */
	public enum GraphModifierAlgo {
		MAXSMT_ALL_TCS,
		MAXSMT_PER_TC,
		MAXSMT_PER_DST,
		MAXSMT_PER_SRC,
		MAXSMT_PER_PAIR,
		ISOLATED,
	}
	
	/** Objective */
	public enum GraphModifierObj {
		NUM_LINES_CHANGED,
		NUM_DEVICES_CHANGED
	}
	
	/**
	 * Create a modifier.
	 * @param policies the policies to conform to
	 * @param deviceEtg the physical network topology
	 * @param baseEtg the base extended topology graphs
	 * @param dstEtgs the destination extended topology graphs
	 * @param flowEtgs the flow extended topology graphs
	 * @param settings
	 */
	public GraphModifier(Map<Flow, List<Policy>> policies,
			DeviceGraph deviceEtg, ProcessGraph baseEtg,
			Map<PolicyGroup, ProcessGraph> dstEtgs,
			Map<Flow, ProcessGraph> flowEtgs, Settings settings) {
		this.policies = policies;
		this.deviceEtg = deviceEtg;
		this.baseEtg = baseEtg;
		this.dstEtgs = dstEtgs;
		this.etgs = flowEtgs;
		this.settings = settings;
		this.logger = settings.getLogger();
		this.algorithm = settings.getModifierAlgorithm();
		this.objective = settings.getModifierObjective();
	}
	
	/**
	 * Repair the network to satisfy the provided policies.
	 * @return the set of graph modifications
	 */
	public abstract ConfigModificationGroup
			getModifications();
	
	/**
	 * Get all possible edges that could exist based on the physical topology
	 * and existing processes.
	 * @return the set of possible edges
	 */
	protected List<VirtualDirectedEdge<ProcessVertex>> getPossibleBaseEdges() {
		List<VirtualDirectedEdge<ProcessVertex>> possibleEdges = 
				new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
		
		// Iterate over all source devices and processes
        for (Device device : this.deviceEtg.getDevices().values()) {
        	for (Process srcProcess : device.getRoutingProcesses()) {
        		// Don't include static routes as common possibilities
        		if (srcProcess.isStaticProcess()) {
					continue;
				}
        		
        		// Get all possible intra-device edges
        		for (Process dstProcess : device.getRoutingProcesses()) {
        			// Don't include static routes as common possibilities
            		if (dstProcess.isStaticProcess()) {
    					continue;
    				}
            		possibleEdges.add(
            		        new VirtualDirectedEdge<ProcessVertex>(
                            srcProcess.getInVertex(), 
                            dstProcess.getOutVertex(), EdgeType.INTRA_DEVICE));
        		}
        		        		
        		// Get all possible inter-device edges
        		// Device graph does not include bidirectional edges, so we
        		// must consider bidirectional here
        		// Edges to other devices
        		Set<DirectedEdge<DeviceVertex>> interDeviceEdges = 
        				this.deviceEtg.getOutgoingEdges(
    					this.deviceEtg.getVertex(device.getName()));
        		for (DirectedEdge<DeviceVertex> edge : interDeviceEdges) {
        			Device dstDevice = edge.getDestination().getDevice();
        			//logger.debug("Inter-device edge:" + device.toString() 
        			//		+ " -> " + dstDevice.toString());
        			for (Process dstProcess : dstDevice.getRoutingProcesses()) {
        				if (srcProcess.getType() != dstProcess.getType()) {
        					continue;
        				}
        				possibleEdges.add(
        				        new VirtualDirectedEdge<ProcessVertex>(
        						srcProcess.getOutVertex(), 
        						dstProcess.getInVertex(), 
        						EdgeType.INTER_DEVICE));
        			}
        		}
        		
        		// Edges from other devices
        		interDeviceEdges = 
        				this.deviceEtg.getIncomingEdges(
    					this.deviceEtg.getVertex(device.getName()));
        		for (DirectedEdge<DeviceVertex> edge : interDeviceEdges) {
        			Device dstDevice = edge.getSource().getDevice();
        			//logger.debug("Inter-device edge:" + device.toString() 
        			//		+ " -> " + dstDevice.toString());
        			for (Process dstProcess : dstDevice.getRoutingProcesses()) {
        				if (srcProcess.getType() != dstProcess.getType()) {
        					continue;
        				}
        				possibleEdges.add(
        				        new VirtualDirectedEdge<ProcessVertex>(
        						srcProcess.getOutVertex(), 
        						dstProcess.getInVertex(), 
        						EdgeType.INTER_DEVICE));
        			}
        		}
        	}
        }
        
        // Set weights and waypoints
        for (VirtualDirectedEdge<ProcessVertex> possibleEdge : possibleEdges) {
            if (this.baseEtg.containsEdge(possibleEdge)) {
                DirectedEdge<ProcessVertex> actualEdge =
                        this.baseEtg.getEdge(possibleEdge);
                possibleEdge.setWeight(actualEdge.getWeight());
                if (actualEdge.hasWaypoint()) {
                    possibleEdge.markWaypoint();
                }
            }
        }
        return possibleEdges;
	}
	
	/**
	 * Get all required edges that must exist based on the existing processes.
	 * @return the set of possible edges
	 */
	protected List<VirtualDirectedEdge<ProcessVertex>> getRequiredEdges() {
		List<VirtualDirectedEdge<ProcessVertex>> requiredEdges = 
				new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
		
		// Iterate over all devices and processes
        for (Device device : this.deviceEtg.getDevices().values()) {
        	for (Process process : device.getRoutingProcesses()) {
        		if (process.isStaticProcess()) {
        			continue;
        		}
        		requiredEdges.add(new VirtualDirectedEdge<ProcessVertex>(
        				process.getInVertex(), 
        				process.getOutVertex(), EdgeType.INTRA_DEVICE, 0));
        	}
        }
        return requiredEdges;
	}
	
	/**
	 * Get all possible static route edges that could exist based on the 
	 * physical topology and existing processes.
	 * @param destination
	 * @return the set of possible static route edges
	 */
	protected List<VirtualDirectedEdge<ProcessVertex>> 
			getPossibleStaticRouteEdges(PolicyGroup destination) {
		List<VirtualDirectedEdge<ProcessVertex>> possibleEdges = 
				new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
		
		// Iterate over all source devices and processes
        for (Device device : this.deviceEtg.getDevices().values()) {
        	for (Process staticProcess : device.getRoutingProcesses()) {
        		// Only include static routes as possibilities
        		if (!staticProcess.isStaticProcess()) {
					continue;
				}
        		
        		if (!destination.intersects(
        				staticProcess.getStaticRouteConfig().getNetwork())) {
        			continue;
        		}
        		
        		// Include in->out intra-device edge for static routes
        		/*possibleEdges.add(new VirtualDirectedEdge<ProcessVertex>(
						srcProcess.getInVertex(), 
						srcProcess.getOutVertex(), 
						EdgeType.INTRA_DEVICE));*/
        		
        		// Include intra-device edges for static routes
        		for (Process srcProcess : device.getRoutingProcesses()) {
        			// No intra-device edges between static routes
        			if (srcProcess.isStaticProcess() 
        					&& srcProcess != staticProcess) {
        				continue;
        			}
        			possibleEdges.add(new VirtualDirectedEdge<ProcessVertex>(
        					srcProcess.getInVertex(), 
    						staticProcess.getOutVertex(), 
    						EdgeType.INTRA_DEVICE));
        		}
        		
        		for (Interface localInterface : staticProcess.getInterfaces()) {	
        			// Get the remote interface that is physically connected
        			Interface remoteInterface = 
        					this.deviceEtg.getConnectedInterface(
        							localInterface);
        			if (null == remoteInterface) {
        				logger.warn("No interface connected to " 
        						+ localInterface.getDevice() + ":" 
        						+ localInterface
        						+ " which is used by process " + staticProcess);
        				continue;
        			}
        			
        			// Add connection to every remote process
        			for (Process remoteProcess : 
        					remoteInterface.getDevice().getRoutingProcesses()) {	
        				// Create an edge
        				possibleEdges.add(
        						new VirtualDirectedEdge<ProcessVertex>(
        						staticProcess.getOutVertex(), 
        						remoteProcess.getInVertex(), 
        						EdgeType.INTER_DEVICE));
        			}
        		}
        	}
        }
        
        // Set weights and waypoints
        ProcessGraph dstEtg = this.dstEtgs.get(destination);
        for (VirtualDirectedEdge<ProcessVertex> possibleEdge : possibleEdges) {
            if (dstEtg.containsEdge(possibleEdge)) {
                DirectedEdge<ProcessVertex> actualEdge = 
                        dstEtg.getEdge(possibleEdge);
                possibleEdge.setWeight(actualEdge.getWeight());
                if (actualEdge.hasWaypoint()) {
                    possibleEdge.markWaypoint();
                }
            }
        }
        
        return possibleEdges;
	}
	
	/**
	 * Get all possible endpoint-edges that could exist for a traffic class
	 * based on the physical topology and existing processes.
	 * @param flow the traffic class
	 * @return the set of possible edges
	 */
	protected List<VirtualDirectedEdge<ProcessVertex>> getPossibleEndpointEdges(
			Flow flow, ProcessVertex sourceVertex, ProcessVertex destinationVertex) {
		//logger.debug("Possible endpoint edges for: "+flow.toString());
		List<VirtualDirectedEdge<ProcessVertex>> possibleEdges = 
				new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
		
		// Iterate over all source devices and interfaces
        for (Device device : this.deviceEtg.getDevices().values()) {
        	for (Interface iface : device.getInterfaces()) {
        		// Ignore interfaces without prefixes
        		if (!iface.hasPrefix()) {
        			continue;
        		}
        		
        		// Check if source is connected to this device
        		if (flow.getSource().contains(iface.getAddress())) {
        			// Possible edge from source to all outgoing vertices of 
        			// processes on device
        			for (Process dstProcess : device.getRoutingProcesses()){
        				VirtualDirectedEdge<ProcessVertex> edge = 
        						new VirtualDirectedEdge<ProcessVertex>(
        								sourceVertex, 
        								dstProcess.getOutVertex(),
        								EdgeType.ENDPOINT, 0);
        				possibleEdges.add(edge);
        				//logger.debug("\t"+edge.toString());
        			}
        			
        			// See if edge from source to destination is possible
        			for (Interface deviceIface : device.getInterfaces()) {
    					if (deviceIface.hasPrefix() 
    							&& flow.getDestination().contains(
    									deviceIface.getAddress())) {
    						VirtualDirectedEdge<ProcessVertex> edge = 
            						new VirtualDirectedEdge<ProcessVertex>(
            								sourceVertex, 
            								destinationVertex,
            								EdgeType.ENDPOINT, 0);
            				possibleEdges.add(edge);
    						break;
    					}
    				}
        		}
        		
        		// Check if destination is connected to this device
        		if (flow.getDestination().contains(iface.getAddress())) {
        			// Possible edge from source to all outgoing vertices of 
        			// processes on device
        			for (Process srcProcess : device.getRoutingProcesses()){
        				VirtualDirectedEdge<ProcessVertex> edge =
        						new VirtualDirectedEdge<ProcessVertex>(
        								srcProcess.getInVertex(), 
        								destinationVertex,
        								EdgeType.ENDPOINT, 0);
        				possibleEdges.add(edge);
        				//logger.debug("\t"+edge.toString());
        			}
        		}
        	}
   
        	/*// Also add inter-device edges for static routes
        	for (Process srcProcess : device.getRoutingProcesses()) {
        		// Only care about static route sources
        		if (ProcessType.STATIC != srcProcess.getType()) {
					continue;
				}
        		
        		// Only care if destination is within static route's prefix
        		if (!flow.getDestination().within(
        				srcProcess.getStaticRouteConfig().getNetwork())) {
        			continue;
        		}
        		
        		// Get all possible inter-device edges
        		// Device graph does not include bidirectional edges, so we
        		// must consider bidirectional here
        		// Edges to other devices
        		Set<DirectedEdge<DeviceVertex>> interDeviceEdges = 
        				this.deviceEtg.getOutgoingEdges(
        						this.deviceEtg.getVertex(device.getName()));
        		for (DirectedEdge<DeviceVertex> edge : interDeviceEdges) {
        			Device dstDevice = edge.getDestination().getDevice();
        			for (Process dstProcess : dstDevice.getRoutingProcesses()) {
        				possibleEdges.add(
        						new VirtualDirectedEdge<ProcessVertex>(
        						srcProcess.getOutVertex(), 
        						dstProcess.getInVertex(), 
        						EdgeType.INTER_DEVICE));
        				// FIXME: set weight
        			}
        		}
        		
        		// Edges from other devices
        		interDeviceEdges = 
        				this.deviceEtg.getIncomingEdges(
        						this.deviceEtg.getVertex(device.getName()));
        		for (DirectedEdge<DeviceVertex> edge : interDeviceEdges) {
        			Device dstDevice = edge.getSource().getDevice();
        			for (Process dstProcess : dstDevice.getRoutingProcesses()) {
        				possibleEdges.add(
        						new VirtualDirectedEdge<ProcessVertex>(
        						srcProcess.getOutVertex(), 
        						dstProcess.getInVertex(), 
        						EdgeType.INTER_DEVICE));
        				// FIXME: set weight
        			}
        		}
        	}*/
        }
        return possibleEdges;
	}
	
	/**
	 * Get the subset of flows whose policies are currently unsatisfied.
	 * @return flows with at least one unsatisfied policy
	 */
	protected Set<Flow> getBrokenFlows() {
		Set<Flow> brokenFlows = new LinkedHashSet<Flow>();
		
		// Assume all unknown flows are broken
		for (Flow flow : this.policies.keySet()) {
			if (!this.etgs.containsKey(flow)) {
				brokenFlows.add(flow);
			}
		}
		
		// Verify policies
		Map<Policy, List<DirectedEdge>> violations = 
		        VerificationTasks.verifyPolicies(this.settings, this.policies, 
		        this.etgs);
		for (Policy policy : violations.keySet()) {
		    brokenFlows.add(policy.getTrafficClass());
		}
		
		return brokenFlows;
	}
}
