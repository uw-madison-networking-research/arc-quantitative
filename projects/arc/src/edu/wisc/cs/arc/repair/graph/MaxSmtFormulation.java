package edu.wisc.cs.arc.repair.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.EnumSort;
import com.microsoft.z3.Expr;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.IntExpr;
import com.microsoft.z3.IntNum;
import com.microsoft.z3.Model;
import com.microsoft.z3.Optimize;
import com.microsoft.z3.Optimize.Handle;
import com.microsoft.z3.Sort;
import com.microsoft.z3.Status;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.Logger.Level;
import edu.wisc.cs.arc.graphs.Device;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.PolicyGroup;
import edu.wisc.cs.arc.graphs.Process;
import edu.wisc.cs.arc.graphs.ProcessGraph;
import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.policies.Policy;
import edu.wisc.cs.arc.repair.VirtualDirectedEdge;
import edu.wisc.cs.arc.repair.graph.GraphModifier.GraphModifierObj;

/**
 * Formulates and solves a MaxSMT problem to determine how a set of extended
 * topology graphs should be modified to conform to a set of policies.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class MaxSmtFormulation {
    
    private static final boolean ALLOW_WAYPOINT_CHANGES = false;
    
	private Context ctx;
	
	private EnumSort typeVertex;
	private EnumSort typeDevice;
	private EnumSort typeFlow;
	private EnumSort typeDestination;
	
	/** Base ETG edge variables */
	private Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr> constsBaseedge;
	
	/** Destination ETG edge variables */
    private Map<PolicyGroup, Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr>>
            constsDstedge;
    
    /** Flow ETG edge variables */
    private Map<Flow, Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr>>
            constsFlowedge;

	/** Required edges by flow */
	private List<VirtualDirectedEdge<ProcessVertex>> requiredEdgesCommon;
		
	/** Device changed function */
	private FuncDecl funcDevchanged;
	
	/** Path function */
	private FuncDecl funcPath;
			
	/** Non-waypoint path function */
	private FuncDecl funcNwpath;
	
    /** Disjoint edge variables */
    private Map<Flow, Map<Integer, Map<VirtualDirectedEdge<ProcessVertex>, 
            BoolExpr>>> constsDisjointedge;
    
    /** Waypoint edge variables */
    private Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr> constsWaypointEdge;
    
    /** Edge cost variables */
    private Map<VirtualDirectedEdge<ProcessVertex>, IntExpr> constsEdgeCost;
	
	/** Source vertex */
	private ProcessVertex sourceVertex;
	
	/** Source vertex */
	private ProcessVertex destinationVertex;
	
	/** Mapping of vertices to constant expressions */
	private Map<ProcessVertex,Expr> verticesToConsts;
	
	/** Mapping of names to vertices */
	private Map<String,ProcessVertex> namesToVertices;
	
	/** Mapping of devices to constant expressions */
	private Map<Device,Expr> devicesToConsts;
	
	/** Mapping of flows to constant expressions */
	private Map<Flow,Expr> flowsToConsts;
	
	/** Mapping of constant expressions to flows */
	private Map<String,Flow> constsToFlows;
	
	/** Mapping of destinations to constant expressions */
	private Map<PolicyGroup,Expr> destinationsToConsts;
	
	/** Mapping of integers to destinations */
	private Map<String,PolicyGroup> namesToDestinations;
		
	/** Store the current flows when solving for a subset of traffic classes */
	private List<Flow> currentFlows;
	
	/** Store the current dsts when solving for a subset of traffic classes */
	private List<PolicyGroup> currentDestinations;
	
	/** Modifier */
	private MaxSmtModifier modifier;

	/** Logger */
	private Logger logger;
	
	/** Indicate whether problem has been solved */
	private boolean solved;
	
	/** Problem formulation */
	private Optimize formulation;
	
	/** Problem solution */
	private Model solution;
	
	/** Soft-assertion handles */
	private Handle softAssertions;

	public MaxSmtFormulation(MaxSmtModifier modifier, List<Flow> flows) {
		this.modifier = modifier;
		this.logger = modifier.logger;
		this.currentFlows = flows;
		this.currentDestinations = new ArrayList<PolicyGroup>();
		for (Flow flow : flows) {
			if (!currentDestinations.contains(flow.getDestination())) {
				currentDestinations.add(flow.getDestination());
			}
		}
		this.solved = false;
		
		// Formulate problem
		this.formulation = this.formulate();
		if (logger.willLog(Level.DEBUG)) {
			logger.debug("Problem:\n" + this.formulation);
		}
	}

	private Optimize formulate() {
		// Enable model generation
		Map<String, String> z3Config = new LinkedHashMap<String, String>();
        z3Config.put("model", "true");
        
		this.ctx = new Context(z3Config);
		Optimize solver = ctx.mkOptimize();
	    
		// Declare constants and functions
		this.declareEdgeConstants();
		this.declareFunctions();
		
		// Topology constraints
		this.addTopologyConstraints(solver);
		
		// Soft constraints
		this.addSoftConstraints(solver);

		//Policy constraints
		this.addPolicyConstraints(solver);
		
		return solver;
	}
		
	/**
	 * Creates an expression representing the application of a boolean function
	 * representing the existence of a specific type of edge or path between 
	 * two vertices for a specific traffic class.
	 * @param func function corresponding to the edge or path
	 * @param v1 source vertex
	 * @param v2 destination vertex
	 * @param tc flow identifier
	 * @return the expression
	 */
	private BoolExpr applyFunc(FuncDecl func, Expr v1, Expr v2, Expr tc) {
		return (BoolExpr)this.ctx.mkApp(func, v1, v2, tc);
	}
	
	/**
	 * Creates an expression representing the application of a boolean function
	 * representing the existence of a specific type of edge or path between 
	 * two vertices for a specific traffic class.
	 * @param func function corresponding to the edge or path
	 * @param v1 source vertex
	 * @param v2 destination vertex
	 * @param tc flow identifier
	 * @return the expression
	 */
	private BoolExpr applyFunc(FuncDecl func, ProcessVertex v1, 
			ProcessVertex v2, Flow flow) {
		return this.applyFunc(func, this.getVertexExpr(v1), 
				this.getVertexExpr(v2), this.getTrafficClassExpr(flow));
	}
		
	/**
	 * Gets an expression corresponding to a vertex.
	 * @param vertex the vertex of interest
	 */
	private Expr getVertexExpr(ProcessVertex vertex) {
		return this.verticesToConsts.get(vertex);
	}
	
	/**
	 * Gets an expression corresponding to a device.
	 * @param vertex the vertex of interest
	 */
	private Expr getDeviceExpr(ProcessVertex vertex) {
		Device device = vertex.getProcess().getDevice();
		return this.devicesToConsts.get(device);
	}
	
	/**
	 * Gets an expression corresponding to a traffic class.
	 * @param flow the traffic class of interest
	 */
	private Expr getTrafficClassExpr(Flow flow) {
		return this.flowsToConsts.get(flow);
	}	
	
	/**
     * Get an expression corresponding to the cost of a particular edge.
     * @param edge the edge
     * @return an expression corresponding to the cost of the edge
     */
    private IntExpr getEdgeCostExpr(VirtualDirectedEdge<ProcessVertex> edge) {
        return this.constsEdgeCost.get(edge);
    }
	
	/**
	 * Get an expression corresponding to a particular edge.
	 * @param flow the flow to which the edge applies
	 * @param edge the edge
	 * @return an expression corresponding to the existence/absence of the edge
	 */
	private BoolExpr getFlowedgeExpr(Flow flow, 
			VirtualDirectedEdge<ProcessVertex> edge) {
	    return this.constsFlowedge.get(flow).get(edge);
	}
	
	/**
	 * Get an expression corresponding to a particular edge.
	 * @param destination the destination to which the edge applies
	 * @param edge the edge
	 * @return an expression corresponding to the existence/absence of the edge
	 */
	private BoolExpr getDstedgeExpr(PolicyGroup destination, 
			VirtualDirectedEdge<ProcessVertex> edge) {
		return this.constsDstedge.get(destination).get(edge);
	}
	
	/**
	 * Get an expression corresponding to a particular edge.
	 * @param edge the edge
	 * @return an expression corresponding to the existence/absence of the edge
	 */
	private BoolExpr getBaseedgeExpr(VirtualDirectedEdge<ProcessVertex> edge) {
		return this.constsBaseedge.get(edge);
	}
	
	/**
	 * Declare functions used in the problem.
	 */
	private void declareFunctions() {
        // Vertex type     
        this.namesToVertices = this.getPossibleVertices();
        this.verticesToConsts = new LinkedHashMap<ProcessVertex,Expr>();
		this.typeVertex = this.ctx.mkEnumSort("Vertex", 
				this.namesToVertices.keySet().toArray(new String[]{}));
		int i = 0;
		Map<String,Device> namesToDevices = new LinkedHashMap<String,Device>();
		for (ProcessVertex vertex : this.namesToVertices.values()) {
        	this.verticesToConsts.put(vertex, this.typeVertex.getConst(i++));
        	if (vertex.getProcess() != null) {
        		Device device = vertex.getProcess().getDevice();
        		namesToDevices.put(device.getName(), device);
        	}
        }
		
		// Device type
		this.typeDevice = this.ctx.mkEnumSort("Device", 
				namesToDevices.keySet().toArray(new String[]{}));
		this.devicesToConsts = new LinkedHashMap<Device,Expr>();
		i = 0;
		for (Device device : namesToDevices.values()) {
			this.devicesToConsts.put(device, this.typeDevice.getConst(i++));
		}
		
        // Flow type
        String flowNames[] = new String[this.currentFlows.size()];
	    for (i = 0; i < flowNames.length; i++) {
	    	flowNames[i] =  "Flow" + i;
        }
	    this.typeFlow = this.ctx.mkEnumSort("Flow", flowNames);
	    i = 0;
        logger.debug("Flow mappings");
        this.flowsToConsts = new LinkedHashMap<Flow,Expr>();
        this.constsToFlows = new LinkedHashMap<String,Flow>();
	    for (Flow flow : this.currentFlows) {
	    	Expr tc = this.typeFlow.getConst(i);
	    	this.flowsToConsts.put(flow, tc);
	    	this.constsToFlows.put(flowNames[i], flow);
	    	logger.debug("\t" + tc.toString() + " " + flow.toString());
	    	i++;
	    }
	    
	    // Destination type
	    String destinationNames[] = new String[this.currentDestinations.size()];
	    for (i = 0; i < destinationNames.length; i++) {
	    	destinationNames[i] =  "Dst" + i;
        }
	    this.typeDestination = this.ctx.mkEnumSort("Dst", destinationNames);
	    i = 0;
        logger.debug("Destination mappings");
        this.destinationsToConsts = new LinkedHashMap<PolicyGroup,Expr>();
        this.namesToDestinations = new LinkedHashMap<String,PolicyGroup>();
	    for (PolicyGroup destination : this.currentDestinations) {
	    	Expr dst = this.typeDestination.getConst(i);
	    	this.destinationsToConsts.put(destination, dst);
	    	this.namesToDestinations.put(destinationNames[i], destination);
	    	logger.debug("\t" + dst.toString() + " " + destination.toString());
	    	i++;
	    }
	    		
		// Function arguments
		Sort[] flowedgeArgs = new Sort[3];
		flowedgeArgs[0] = flowedgeArgs[1] = this.typeVertex;
		flowedgeArgs[2] = this.typeFlow;
		
		// Edge functions
		Sort[] dstedgeArgs = new Sort[3];
		dstedgeArgs[0] = dstedgeArgs[1] = this.typeVertex;
		dstedgeArgs[2] = this.typeDestination;
		Sort[] baseedgeArgs = new Sort[2];
		baseedgeArgs[0] = this.typeVertex;
		baseedgeArgs[1] = this.typeVertex;
		
		// Soft-constraint functions
		if (GraphModifierObj.NUM_DEVICES_CHANGED == modifier.objective) {
			this.funcDevchanged = this.ctx.mkFuncDecl("devchanged", 
					new Sort[] { this.typeDevice }, 
					this.ctx.getBoolSort());
		}
		
		// Path functions
		Sort[] flowpathArgs = new Sort[3];
		flowpathArgs[0] = flowpathArgs[1] = this.typeVertex;
		flowpathArgs[2] = this.typeFlow;
		this.funcPath = this.ctx.mkFuncDecl("path", flowpathArgs, 
				this.ctx.getBoolSort());
        
        // Waypoint functions
		this.funcNwpath = this.ctx.mkFuncDecl("nwpath", flowpathArgs, 
				this.ctx.getBoolSort());
	}
	
	/**
	 * Add topology constraints to the problem.
	 * @param solver MaxSMT solver
	 */
	private void addTopologyConstraints(Optimize solver) {             
		// Add required edge constraints
		this.addRequiredEdgeConstraints(solver);
		
		// Edges in flow ETG must exist in destination ETG
		for (Flow flow : this.currentFlows) {
		    Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr> flowEdges =
		            this.constsFlowedge.get(flow);
		    Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr> dstEdges =
                    this.constsDstedge.get(flow.getDestination());
		    
		    for (VirtualDirectedEdge<ProcessVertex> edge : flowEdges.keySet()) {
		        if (dstEdges.containsKey(edge)) {
		            BoolExpr flowEdge = flowEdges.get(edge);
		            BoolExpr dstEdge = dstEdges.get(edge);
		            BoolExpr implies = this.ctx.mkImplies(flowEdge, dstEdge);
		            solver.Assert(implies);
		        }
		    }
		}
		
		// Non-static edges in destination ETG must exist in base ETG
		for (Flow flow : this.currentFlows) {
		    Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr> dstEdges =
                    this.constsDstedge.get(flow.getDestination());
		    Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr> baseEdges =
		            this.constsBaseedge;
		    
		    for (VirtualDirectedEdge<ProcessVertex> edge : dstEdges.keySet()) {
		    	// FIXME: Also check destination?
		    	if (edge.getSource().getProcess().isStaticProcess()) {
		    		continue;
		    	}
		        if (baseEdges.containsKey(edge)) {
		            BoolExpr dstEdge = dstEdges.get(edge);
		            BoolExpr baseEdge = baseEdges.get(edge);
		            BoolExpr implies = this.ctx.mkImplies(dstEdge, baseEdge);
		            solver.Assert(implies);
		        }
		    }
		}
		
		// Waypoint edges must remain unchanged
        if (!ALLOW_WAYPOINT_CHANGES) {
            for (Entry<VirtualDirectedEdge<ProcessVertex>, BoolExpr> entry : 
                    this.constsWaypointEdge.entrySet()) {
                if (entry.getKey().hasWaypoint()) {
                    solver.Assert(entry.getValue());
                } else {
                    solver.Assert(this.ctx.mkNot(entry.getValue()));
                }
            }
        }
	}
	
	/**
	 * Declare boolean and cost constants for possible edges.
	 */
	private void declareEdgeConstants() {
		this.constsWaypointEdge = new LinkedHashMap<
				VirtualDirectedEdge<ProcessVertex>, BoolExpr>();
	    this.constsEdgeCost = new LinkedHashMap<
	    		VirtualDirectedEdge<ProcessVertex>, IntExpr>();
	    this.constsDisjointedge = new LinkedHashMap<Flow, Map<Integer, 
	            Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr>>>();
	    
		// Define possible base edges
		this.constsBaseedge = 
		        new HashMap<VirtualDirectedEdge<ProcessVertex>, BoolExpr>();
        for (VirtualDirectedEdge<ProcessVertex> edge : 
                modifier.getPossibleBaseEdges()) {
            BoolExpr e = this.ctx.mkBoolConst("base:" + edge.toString());
            this.constsBaseedge.put(edge, e);
           
            IntExpr edgeCost = this.ctx.mkIntConst("cost:" + edge.toString());
            this.constsEdgeCost.put(edge, edgeCost);
            
            BoolExpr waypointEdge = this.ctx.mkBoolConst("waypoint:" 
            		+ edge.toString());
            this.constsWaypointEdge.put(edge, waypointEdge);
        }
        
		// Define possible destination edges
        this.constsDstedge = new LinkedHashMap<PolicyGroup, 
                Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr>>();
        for (PolicyGroup dst : this.currentDestinations) {
        	// Get possible static route edges for destination
        	List<VirtualDirectedEdge<ProcessVertex>> possibleStaticEdges = 
	        		this.modifier.getPossibleStaticRouteEdges(dst);
        	
        	// Create cost and waypoint edge costs for static route edges
        	for (VirtualDirectedEdge<ProcessVertex> edge : possibleStaticEdges){
	        	if (!this.constsEdgeCost.containsKey(edge)) {
	        	    IntExpr edgeCost = this.ctx.mkIntConst("cost:" 
	        	            + edge.toString());
	        	    this.constsEdgeCost.put(edge, edgeCost);
	
	        	    BoolExpr waypointEdge = this.ctx.mkBoolConst("waypoint:" 
	                		+ edge.toString());
	                this.constsWaypointEdge.put(edge, waypointEdge);
	        	}
        	}
        	
        	// Include possible base edges
        	List<VirtualDirectedEdge<ProcessVertex>> possibleEdgesDst
        		= new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
        	possibleEdgesDst.addAll(possibleStaticEdges);
        	possibleEdgesDst.addAll(this.constsBaseedge.keySet());
        	
        	// Create edge constants
        	Map<VirtualDirectedEdge<ProcessVertex>, BoolExpr> constsForDst 
        	    = new HashMap<VirtualDirectedEdge<ProcessVertex>, BoolExpr>();
	        for (VirtualDirectedEdge<ProcessVertex> edge : possibleEdgesDst) {
	            BoolExpr e = this.ctx.mkBoolConst(
	                    "dst:" + dst.toString() + ":" + edge.toString());
	        	constsForDst.put(edge, e);
	        }
	        
	        this.constsDstedge.put(dst, constsForDst);
        }
        
        // Define possible flow edges
        this.constsFlowedge = new LinkedHashMap<Flow,
                Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr>>();
        for (Flow flow : this.currentFlows) {
        	// Get possible endpoint edges for flow
            ProcessGraph flowEtg = this.modifier.etgs.get(flow);
        	List<VirtualDirectedEdge<ProcessVertex>> possibleEndpointEdges = 
	        		this.modifier.getPossibleEndpointEdges(flow,
	        		        flowEtg.getFlowSourceVertex(), 
	        		        flowEtg.getFlowDestinationVertex());
        	
        	// Create cost and waypoint edge costs for endpoint edges
        	for (VirtualDirectedEdge<ProcessVertex> edge : 
        			possibleEndpointEdges) {
	        	if (!this.constsEdgeCost.containsKey(edge)) {
	        	    IntExpr edgeCost = this.ctx.mkIntConst("cost:" 
	        	            + edge.toString());
	        	    this.constsEdgeCost.put(edge, edgeCost);
	
	        	    BoolExpr waypointEdge = this.ctx.mkBoolConst("waypoint:" 
	                		+ edge.toString());
	                this.constsWaypointEdge.put(edge, waypointEdge);
	        	}
        	}
        	
        	// Include possible destination edges
        	List<VirtualDirectedEdge<ProcessVertex>> possibleEdgesFlow =
        			new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
        	possibleEdgesFlow.addAll(possibleEndpointEdges);
        	possibleEdgesFlow.addAll(
        			this.constsDstedge.get(flow.getDestination()).keySet());
        	
        	// Create edge constants
            Map<VirtualDirectedEdge<ProcessVertex>, BoolExpr> constsForFlow 
                = new HashMap<VirtualDirectedEdge<ProcessVertex>, BoolExpr>();
            for (VirtualDirectedEdge<ProcessVertex> edge : possibleEdgesFlow) {
                BoolExpr e = this.ctx.mkBoolConst(
                        "flow:" + flow.toString() + ":" + edge.toString());
                constsForFlow.put(edge, e);
                
                if (!this.constsEdgeCost.containsKey(edge)) {
                    IntExpr edgeCost = this.ctx.mkIntConst("cost:" 
                            + edge.toString());
                    this.constsEdgeCost.put(edge, edgeCost);
                }
            }
            
            this.constsFlowedge.put(flow, constsForFlow);
        }
	}
	
	/**
	 * Add required edge constraints.
	 * @param solver MaxSMT solver
	 */
	private void addRequiredEdgeConstraints(Optimize solver) {
        // Get required intra-device edges
        this.requiredEdgesCommon = this.modifier.getRequiredEdges();
        
        // Add required intra-device edges for base
        for (VirtualDirectedEdge<ProcessVertex> edge : 
	    		this.requiredEdgesCommon) {
	    	solver.Assert(this.getBaseedgeExpr(edge));
	    }
        
        // Add required intra-device edges for each destination
        for (PolicyGroup destination : this.destinationsToConsts.keySet()) {	        
            // Required edges must exist
            for (VirtualDirectedEdge<ProcessVertex> edge : 
            		this.requiredEdgesCommon) {
            	solver.Assert(this.getDstedgeExpr(destination, edge));
            }
        }
        
        // Add required intra-device edges for each flow
        for (Flow flow : this.flowsToConsts.keySet()) {	        
            // Required edges must exist
            for (VirtualDirectedEdge<ProcessVertex> edge : 
            		this.requiredEdgesCommon) {
            	solver.Assert(this.getFlowedgeExpr(flow, edge));
            }
        }
	}
    
    /**
	 * Add soft constraints to the problem.
	 * @param solver MaxSMT solver
	 */
	private void addSoftConstraints(Optimize solver) {
		switch(this.modifier.objective) {
		case NUM_LINES_CHANGED:
			this.addNumLinesConstraints(solver);
			break;
		case NUM_DEVICES_CHANGED:
			// All devices should not change
			for (Expr deviceExpr : this.devicesToConsts.values()) {
	        	BoolExpr deviceUnchanged = this.ctx.mkNot(
	        			(BoolExpr)this.ctx.mkApp(this.funcDevchanged, 
	        			deviceExpr));
        		this.softAssertions = solver.AssertSoft(deviceUnchanged, 
        				1, "DeviceUnchanged");
        	}
			
			// FIXME
			break;
		}
	}
	
	/**
	 * Add soft constraints for the number of lines of configuration changed.
	 * @param solver MaxSMT solver
	 */
	private void addNumLinesConstraints(Optimize solver) {
		// Add soft constraints for base ETG
		// Adding (or removing) a base edge, requires adding (or removing)
		// a routing adjacency, so we want to avoid adding (or removing)
		// base edges
		for (VirtualDirectedEdge<ProcessVertex> edge : 
				this.constsBaseedge.keySet()) {
			// FIXME: Prefer to keep edgecosts unchanged
        	/*IntExpr edgecostExpr = this.getEdgecostExpr(flow, edge);
            IntExpr currentWeight = this.ctx.mkInt((int)etg.getEdge(
                    edge.getSource(), edge.getDestination()).getWeight());
        	assertion = this.ctx.mkEq(edgecostExpr, currentWeight);
        	this.softAssertions = solver.AssertSoft(assertion, 1, 
        	        "DesiredEdges");*/
        	
			// Ignore required edges
			if (this.requiredEdgesCommon.contains(edge)) {
				continue;
			}
			
			BoolExpr assertion = this.getBaseedgeExpr(edge);
			if (!this.modifier.baseEtg.containsEdgeExcludeBlocked(edge)) {
				assertion = this.ctx.mkNot(assertion);
			}
			this.softAssertions = solver.AssertSoft(assertion, 1, 
			        "DesiredEdges");
		}
		
		// Add soft constraints for destination ETGs
		for (PolicyGroup destination : this.destinationsToConsts.keySet()) {
			ProcessGraph destinationEtg = 
					this.modifier.dstEtgs.get(destination);
			
			for (VirtualDirectedEdge<ProcessVertex> edge : 
					this.constsDstedge.get(destination).keySet()) {
				// Ignore required edges
				if (this.requiredEdgesCommon.contains(edge)) {
					continue;
				}
				
				BoolExpr dstEdge = this.getDstedgeExpr(destination, edge);
				BoolExpr baseEdge = this.getBaseedgeExpr(edge);
				
				boolean inDestination = 
						destinationEtg.containsEdgeExcludeBlocked(edge);
				boolean inBase = 
						this.modifier.baseEtg.containsEdgeExcludeBlocked(edge);
				
				BoolExpr assertion = null;
				if (this.constsBaseedge.containsKey(edge)) {
					// Making the destination different than the base requires
					// using a static route or a route filter, so we want to 
					// avoid making the base and destination different
					if (inDestination == inBase) {
						assertion = this.ctx.mkEq(dstEdge, baseEdge);
					}
					// Adding (or removing) a destination edge, if the base edge
					// doesn't (or does) exist requires adding (or removing) a
					// static route or removing (or adding) a route filter, so 
					// we want to avoid adding (or removing) destination edges 
					// if the base and destination are currently different
					else {
						assertion = dstEdge;
						if (!inDestination) {
							assertion = this.ctx.mkNot(assertion);
						}
					}
				}
				// If the base edge cannot exist, adding (or removing) a 
				// destination edge requires adding (or removing) a static route 
				// so we want to avoid adding (or removing) destination edges
				else {
					assertion = dstEdge;
					if (!inDestination) {
						assertion = this.ctx.mkNot(assertion);
					}
				}
				this.softAssertions = solver.AssertSoft(assertion, 1, 
				        "DesiredEdges");
			}
		}
		
		// Add soft constraints for flow ETGs
		for (Flow flow : this.flowsToConsts.keySet()) {
			ProcessGraph flowEtg = this.modifier.etgs.get(flow);
			ProcessGraph destinationEtg = 
					this.modifier.dstEtgs.get(flow.getDestination());
			Set<VirtualDirectedEdge<ProcessVertex>> possibleEdgesDestination =
					this.constsDstedge.get(flow.getDestination()).keySet();
			
			for (VirtualDirectedEdge<ProcessVertex> edge : 
					this.constsFlowedge.get(flow).keySet()) {
				// Ignore required edges
				if (this.requiredEdgesCommon.contains(edge)) {
					continue;
				}
				
				BoolExpr flowEdge = this.getFlowedgeExpr(flow, edge);
				BoolExpr dstEdge = this.getDstedgeExpr(flow.getDestination(), 
				        edge);
				
				boolean inFlow = flowEtg.containsEdgeExcludeBlocked(edge);
				boolean inDestination = destinationEtg.containsEdgeExcludeBlocked(edge);
				
				BoolExpr assertion = null;
				if (possibleEdgesDestination.contains(edge)) {
					// Making the flow different than the destination requires
					// using an ACL, so we want to avoid making the flow and 
					// destination different
					if (inFlow == inDestination) {
						assertion = this.ctx.mkEq(flowEdge, dstEdge);
					}
					// Adding a flow edge, if the destination edge exists
					// requires removing an ACL, so adding flow edges if the
					// flow and destination are currently different
					else {
						assertion = flowEdge;
						if (!inFlow) {
							assertion = this.ctx.mkNot(assertion);
						}
					}
				}
				// If the destination edge cannot exist, adding (or removing) a 
				// flow edge requires adding (or removing) an ACL so we want to
				// avoid adding (or removing) flow edges
				else {
					assertion = flowEdge;
					if (!inFlow) {
						assertion = this.ctx.mkNot(assertion);
					}
				}
				this.softAssertions = solver.AssertSoft(assertion, 1, 
				        "DesiredEdges");
			}
		}
		
		// Add soft contraints for costs
		for (Entry<VirtualDirectedEdge<ProcessVertex>, IntExpr> entry :
		        this.constsEdgeCost.entrySet()) {
		    IntNum target = this.ctx.mkInt((int)entry.getKey().getWeight());
		    BoolExpr assertion = this.ctx.mkEq(entry.getValue(), target);
		    this.softAssertions = solver.AssertSoft(assertion, 1, 
                    "DesiredEdges");
		}
	}	

	/** 
	 * Add soft topology constraints for a specific flow and set of edges to 
	 * minimize the number of devices changed.
	 * @param solver MaxSMT solver
	 * @param etg extended topology graph from which to derive constraints
	 * @param possibleEdges possible edges
	 */
	private void addSoftConstraintsNumDevices(Optimize solver, ProcessGraph etg, 
			Flow flow, List<VirtualDirectedEdge<ProcessVertex>> possibleEdges) {	
		for (VirtualDirectedEdge<ProcessVertex> edge : possibleEdges) {
			BoolExpr edgeExpr = this.getFlowedgeExpr(flow, edge);
			boolean etgEdge = etg.containsEdgeExcludeBlocked(edge);
			if (etgEdge) {
				edgeExpr = this.ctx.mkNot(edgeExpr);
			}
			
			List<BoolExpr> devicesChanged = new ArrayList<BoolExpr>();
			if (!edge.getSource().equals(this.sourceVertex)) {
				devicesChanged.add((BoolExpr)this.ctx.mkApp(this.funcDevchanged, 
						this.getDeviceExpr(edge.getSource())));
			}
			if (!edge.getDestination().equals(this.destinationVertex)) {
				devicesChanged.add((BoolExpr)this.ctx.mkApp(this.funcDevchanged, 
						this.getDeviceExpr(edge.getDestination())));
			}
			BoolExpr implies = this.ctx.mkImplies(edgeExpr, 
					this.ctx.mkAnd(devicesChanged.toArray(new BoolExpr[]{})));
			solver.Assert(implies);
		}
	}
	
	/**
     * Add path constraints to the problem.
     * @param solver MaxSMT solver
     */
    private void addPathConstraints(Optimize solver, Flow flow) {
        Expr tc = this.getTrafficClassExpr(flow);
        
        // Path base case
        // Path always exists for required edges    
        for (VirtualDirectedEdge<ProcessVertex> edge : 
                this.requiredEdgesCommon) {
            BoolExpr pathExists = this.applyFunc(this.funcPath, 
                    this.getVertexExpr(edge.getSource()),
                    this.getVertexExpr(edge.getDestination()), tc);
            solver.Assert(pathExists);
        }
        
        // Path may exist for optional edges
        List<VirtualDirectedEdge<ProcessVertex>> optionalEdgesFlow =
        		new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
        optionalEdgesFlow.addAll(this.constsFlowedge.get(flow).keySet());
        optionalEdgesFlow.removeAll(this.requiredEdgesCommon);
        for (VirtualDirectedEdge<ProcessVertex> edge : optionalEdgesFlow) {
            BoolExpr antecedent = this.getFlowedgeExpr(flow, edge);
            BoolExpr consequent = this.applyFunc(this.funcPath, 
                    this.getVertexExpr(edge.getSource()),
                    this.getVertexExpr(edge.getDestination()), tc);
            BoolExpr implies = this.ctx.mkImplies(antecedent,
                    consequent);
            solver.Assert(implies);
        }
        
        // Path inductive case
        // Path may be expanded by possible edges
        for (VirtualDirectedEdge<ProcessVertex> edge : 
                this.constsFlowedge.get(flow).keySet()) {
            Expr v3 = this.ctx.mkConst("v3", this.typeVertex);
            BoolExpr edgeV1V2 = this.getFlowedgeExpr(flow, edge);
            BoolExpr pathV2V3 = this.applyFunc(this.funcPath, 
                    this.getVertexExpr(edge.getDestination()), v3, tc);
            BoolExpr antecedent = this.ctx.mkAnd(edgeV1V2, pathV2V3);
            BoolExpr consequent = this.applyFunc(this.funcPath, 
                    this.getVertexExpr(edge.getSource()), v3, tc);
            BoolExpr implies = this.ctx.mkImplies(antecedent,
                    consequent);
            BoolExpr exprPathInductive = this.ctx.mkForall(
                    new Expr[] {v3}, implies, 
                    1, null, null, null,null);
            solver.Assert(exprPathInductive);
        }
    }
	
	/**
	 * Add blocked constraints to the problem.
	 * @param solver MaxSMT solver
	 */
	private void addBlockedConstraints(Optimize solver, Flow flow) {
		Expr tc = this.getTrafficClassExpr(flow);
		
		// No path from src to dst
		Expr src = this.getVertexExpr(this.sourceVertex);
		Expr dst = this.getVertexExpr(this.destinationVertex);
		BoolExpr exprNoPath = this.ctx.mkNot(
				this.applyFunc(this.funcPath, src, dst, tc));
		solver.Assert(exprNoPath);
	}
	
	/**
	 * Add waypoint constraints to the problem.
	 * @param solver MaxSMT solver
	 */
	// FIXME
	private void addWaypointConstraints(Optimize solver, Flow flow) {
		/*Expr tc = this.getTrafficClassExpr(flow);
		
		// Used edges must be labeled
		Expr v1 = this.ctx.mkConst("v1", this.typeVertex);
		Expr v2 = this.ctx.mkConst("v2", this.typeVertex);
		BoolExpr antecedent = this.applyFunc(this.funcFlowedge, v1, v2, tc);
		BoolExpr nwedgeV1V2 = this.applyFunc(this.funcNwedge, v1, v2, tc);
		BoolExpr wedgeV1V2 = this.applyFunc(funcWedge, v1, v2, tc);
		BoolExpr consequent = this.ctx.mkXor(nwedgeV1V2, wedgeV1V2);
		Expr implies = this.ctx.mkImplies(antecedent, consequent);
		BoolExpr exprUsedEdgesLabeled = this.ctx.mkForall(
				new Expr[] {v1, v2}, implies, 
				1, null, null, null, null);
		solver.Assert(exprUsedEdgesLabeled);
		
		// Labeled edges must be used
		v1 = this.ctx.mkConst("v1", this.typeVertex);
		v2 = this.ctx.mkConst("v2", this.typeVertex);
		antecedent = this.ctx.mkOr(this.applyFunc(this.funcNwedge, v1, v2, tc),
				this.applyFunc(funcWedge, v1, v2, tc));
		consequent = this.applyFunc(this.funcFlowedge, v1, v2, tc);
		implies = this.ctx.mkImplies(antecedent, consequent);
		BoolExpr exprNwedgeUsed = this.ctx.mkForall(
				new Expr[] {v1, v2}, implies, 
				1, null, null, null, null);
		solver.Assert(exprNwedgeUsed);*/
		
		// Non-waypoint path base case
	    for (Entry<VirtualDirectedEdge<ProcessVertex>, BoolExpr> entry : 
	    		this.constsFlowedge.get(flow).entrySet()) {
	    	VirtualDirectedEdge<ProcessVertex> edge = entry.getKey();
	    	BoolExpr edgeV1V2 = entry.getValue();
	    	BoolExpr wedgeV1V2 = this.constsWaypointEdge.get(edge);
	    	BoolExpr antecedent = this.ctx.mkAnd(edgeV1V2, 
	    			this.ctx.mkNot(wedgeV1V2));
	    	BoolExpr consequent = this.applyFunc(funcNwpath, 
	    			edge.getSource(), edge.getDestination(), flow);
	    	BoolExpr nwpathBase = this.ctx.mkImplies(antecedent, consequent);
	    	solver.Assert(nwpathBase);
	    }
		/*v1 = this.ctx.mkConst("v1", this.typeVertex);
		v2 = this.ctx.mkConst("v2", this.typeVertex);
		antecedent = this.applyFunc(this.funcNwedge, v1, v2, tc);
		consequent = this.applyFunc(this.funcNwpath, v1, v2, tc);
		implies = this.ctx.mkImplies(antecedent, consequent);
		BoolExpr exprNwpathBase = this.ctx.mkForall(
				new Expr[] {v1, v2}, implies, 
				1, null, null, null, null);
		solver.Assert(exprNwpathBase);*/
		
		// Non-waypoint path inductive case
	    for (Entry<VirtualDirectedEdge<ProcessVertex>, BoolExpr> entry : 
    			this.constsFlowedge.get(flow).entrySet()) {
	    	VirtualDirectedEdge<ProcessVertex> edge = entry.getKey();
	    	BoolExpr edgeV1V2 = entry.getValue();
	    	BoolExpr wedgeV1V2 = this.constsWaypointEdge.get(edge);
	    	for (ProcessVertex vertex : this.getPossibleVertices().values()) {
	    		BoolExpr nwpathV2V3 = this.applyFunc(this.funcNwpath, 
	    				edge.getDestination(), vertex, flow);
	    		BoolExpr antecedent = this.ctx.mkAnd(edgeV1V2, 
		    			this.ctx.mkNot(wedgeV1V2), nwpathV2V3);
	    		BoolExpr consequent = this.applyFunc(funcNwpath, 
	    				edge.getSource(), vertex, flow);
	    		BoolExpr nwpathInductive = this.ctx.mkImplies(antecedent, 
	    				consequent);
		    	solver.Assert(nwpathInductive);
	    	}
	    }
		/*v1 = this.ctx.mkConst("v1", this.typeVertex);
		v2 = this.ctx.mkConst("v2", this.typeVertex);
		Expr v3 = this.ctx.mkConst("v3", this.typeVertex);
		tc = this.ctx.mkConst("tc", this.typeFlow);
		nwedgeV1V2 = this.applyFunc(this.funcNwedge, v1, v2, tc);
		BoolExpr nwpathV2V3 = this.applyFunc(this.funcNwpath, v2, v3, tc);
		antecedent = this.ctx.mkAnd(nwedgeV1V2, nwpathV2V3);
		consequent = this.applyFunc(this.funcNwpath, v1, v3, tc);
		implies = this.ctx.mkImplies(antecedent, consequent);
		BoolExpr exprNwpathInductive = this.ctx.mkForall(
				new Expr[] {v1, v2, v3}, implies, 
				1, null, null, null,null);
		solver.Assert(exprNwpathInductive);*/
		
		// No non-waypoint path from src to dst
	    BoolExpr exprNoNwpath = this.ctx.mkNot(this.applyFunc(
				this.funcNwpath, this.sourceVertex, this.destinationVertex, 
				flow));
		solver.Assert(exprNoNwpath);
		/*Expr src = this.getVertexExpr(this.sourceVertex);
		Expr dst = this.getVertexExpr(this.destinationVertex);
		BoolExpr exprNoNwpath = this.ctx.mkNot(this.applyFunc(
				this.funcNwpath, src, dst, tc));
		solver.Assert(exprNoNwpath);*/
	}
	
	/**
	 * Add reachable constraints to the problem.
	 * @param solver MaxSMT solver
	 */
	private void addReachableConstraints(Optimize solver, Flow flow, int k) {
	    Set<VirtualDirectedEdge<ProcessVertex>> flowEdges = 
	            this.constsFlowedge.get(flow).keySet();
	    ProcessGraph flowEtg = this.modifier.etgs.get(flow);
	    
	    // Categorize edges by source and destination vertex
	    Map<ProcessVertex,List<VirtualDirectedEdge<ProcessVertex>>> flowEdgesBySrc =
                new LinkedHashMap<ProcessVertex,
                    List<VirtualDirectedEdge<ProcessVertex>>>();
	    Map<ProcessVertex,List<VirtualDirectedEdge<ProcessVertex>>> flowEdgesByDst =
	            new LinkedHashMap<ProcessVertex,
	                List<VirtualDirectedEdge<ProcessVertex>>>();
	    for (VirtualDirectedEdge<ProcessVertex> edge : flowEdges) {
	        // Categorize edge based on source vertex
	        if (!flowEdgesBySrc.containsKey(edge.getSource())) {
	            flowEdgesBySrc.put(edge.getSource(), 
	                    new ArrayList<VirtualDirectedEdge<ProcessVertex>>());
	        }
	        flowEdgesBySrc.get(edge.getSource()).add(edge);
	        
	        // Categorize edge based on destination vertex
            if (!flowEdgesByDst.containsKey(edge.getDestination())) {
                flowEdgesByDst.put(edge.getDestination(), 
                        new ArrayList<VirtualDirectedEdge<ProcessVertex>>());
            }
            flowEdgesByDst.get(edge.getDestination()).add(edge);
	    }
	    
	    // Declare disjoint edge constants and constraints
        Map<Integer, Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr>> 
                constsDisjoint = new LinkedHashMap<Integer,
                    Map<VirtualDirectedEdge<ProcessVertex>,BoolExpr>>();
        for (int i = 0; i < k; i++) {
            Map<VirtualDirectedEdge<ProcessVertex>, BoolExpr> constsForI 
                = new HashMap<VirtualDirectedEdge<ProcessVertex>, BoolExpr>();
            
            List<BoolExpr> sourceEdges = new ArrayList<BoolExpr>();
            List<BoolExpr> destinationEdges = new ArrayList<BoolExpr>();
            
            for (VirtualDirectedEdge<ProcessVertex> edge : flowEdges) {
                // Ignore non-source edges without upstream edges
                if(!edge.getSource().equals(flowEtg.getFlowSourceVertex())
                		&& null == flowEdgesByDst.get(edge.getSource())) {
                	//logger.debug("Ignore edge " + edge + " with no upstream");
                	continue;
                }
                
                // Ignore non-destination edges without downstream edges
                if(!edge.getDestination().equals(
                		flowEtg.getFlowDestinationVertex())
                		&& null == flowEdgesBySrc.get(edge.getDestination())) {
                	//logger.debug("Ignore edge " + edge + " with no downstream");
                	continue;
                }
                
                // Create disjoint edge constant
                BoolExpr disjointEdge = this.ctx.mkBoolConst("disjoint" + i + ":" 
                        + flow.toString() + ":" + edge.toString());
                constsForI.put(edge, disjointEdge);
                
                // Store source edges
                if (edge.getSource().equals(flowEtg.getFlowSourceVertex())) {
                    sourceEdges.add(disjointEdge);
                }
                
                // Store destination edges
                if (edge.getDestination().equals(
                        flowEtg.getFlowDestinationVertex())) {
                    destinationEdges.add(disjointEdge);
                }
                   
                // Disjoint edge must be used
                BoolExpr flowEdge = this.getFlowedgeExpr(flow, edge);
                BoolExpr used = this.ctx.mkImplies(disjointEdge, flowEdge);
                solver.Assert(used);
            }
            constsDisjoint.put(i, constsForI);
            
            /*// Define function encoding paths derived from edges in disjoint set
            Sort[] disjointpathArgs = new Sort[2];
            disjointpathArgs[0] = disjointpathArgs[1] = this.typeVertex;
    		FuncDecl funcDisjointpath = this.ctx.mkFuncDecl("disjointpath" + i
    				+ ":" + flow.toString(), disjointpathArgs, 
    				this.ctx.getBoolSort());
    		
    		// Define path existence
            for (VirtualDirectedEdge<ProcessVertex> edge : flowEdges) {            	
            	// Path base case
                BoolExpr antecedent = constsForI.get(edge);
                BoolExpr consequent = this.applyFunc(funcDisjointpath, 
                        this.getVertexExpr(edge.getSource()),
                        this.getVertexExpr(edge.getDestination()));
                BoolExpr pathBase = this.ctx.mkImplies(antecedent,
                        consequent);
                solver.Assert(pathBase);
                
                // Path inductive case
                Expr v3 = this.ctx.mkConst("v3", this.typeVertex);
                BoolExpr edgeV1V2 = constsForI.get(edge);
                BoolExpr pathV2V3 = this.applyFunc(funcDisjointpath, 
                        this.getVertexExpr(edge.getDestination()), v3);
                antecedent = this.ctx.mkAnd(edgeV1V2, pathV2V3);
                consequent = this.applyFunc(funcDisjointpath, 
                        this.getVertexExpr(edge.getSource()), v3);
                BoolExpr implies = this.ctx.mkImplies(antecedent,
                        consequent);
                BoolExpr pathInductive = this.ctx.mkForall(
                        new Expr[] {v3}, implies, 
                        1, null, null, null,null);
                solver.Assert(pathInductive);
            }
    		
    		// Define path existence
    		for (ProcessVertex srcVertex : this.verticesToConsts.keySet()) {
    			for (ProcessVertex dstVertex : this.verticesToConsts.keySet()) {
    				// Skip same vertices
    				if (srcVertex.equals(dstVertex)) {
    					continue;
    				}
    				
    				// Skip if there are no edges from source vertex
    				List<VirtualDirectedEdge<ProcessVertex>> edgesFromSrc = 
    						flowEdgesBySrc.get(srcVertex);
    				if (null == edgesFromSrc) {
    					continue;
    				}
    				
    				List<VirtualDirectedEdge<ProcessVertex>> edgesToDst = 
    						flowEdgesByDst.get(dstVertex);
    				
    				// Add possible ways to have a path
    				List<BoolExpr> pathOptions = new ArrayList<BoolExpr>();
    				for (VirtualDirectedEdge<ProcessVertex> edge :edgesFromSrc){
    					BoolExpr edgeConst = constsForI.get(edge);
    					
    					// Check for direct edge
    					if (edgesToDst != null && edgesToDst.contains(edge)) {
    						pathOptions.add(edgeConst);
    						continue;
    					}
    					
    					// Expand existing path
    					BoolExpr pathConst = this.applyFunc(funcDisjointpath, 
    	                        this.getVertexExpr(edge.getDestination()), 
    	                        this.getVertexExpr(dstVertex));
    					pathOptions.add(this.ctx.mkAnd(edgeConst, pathConst));
    				}
    				
    				BoolExpr pathVSVD = this.applyFunc(funcDisjointpath, 
                            this.getVertexExpr(srcVertex),
                            this.getVertexExpr(dstVertex));
    				BoolExpr canHavePath = this.ctx.mkOr(
    						pathOptions.toArray(new BoolExpr[]{}));
    				BoolExpr pathExists = this.ctx.mkIff(pathVSVD,canHavePath);
    				solver.Assert(pathExists);
    			}
    		}
    		
    		// If no edge from vertex, then no path from vertex
    		for (ProcessVertex srcVertex : this.verticesToConsts.keySet()) {
    			BoolExpr antecedent = this.ctx.mkBool(true);
    			if (flowEdgesBySrc.containsKey(srcVertex)) {
		            // Obtain disjoint edge constants for possible edges from vertex
		            List<BoolExpr> possibleEdges = new ArrayList<BoolExpr>();
		            for (VirtualDirectedEdge<ProcessVertex> edge :
		            		flowEdgesBySrc.get(srcVertex)) {
		                if (constsForI.get(edge) != null) {
		                	possibleEdges.add(constsForI.get(edge));
		                }
		            }
		            
		            // No edge from vertex
		            BoolExpr hasPossibleEdge = this.ctx.mkOr(
		            		possibleEdges.toArray(new BoolExpr[]{}));
		            antecedent = this.ctx.mkNot(hasPossibleEdge);
    			}
	            
	            // Obtain path calls for possible paths from vertex
	            List<BoolExpr> possiblePaths = new ArrayList<BoolExpr>();
	            for (ProcessVertex dstVertex : this.verticesToConsts.keySet()) {
	            	if (dstVertex.equals(srcVertex)) {
	            		continue;
	            	}
	            	possiblePaths.add(this.applyFunc(funcDisjointpath, 
	                        this.getVertexExpr(srcVertex), 
	                        this.getVertexExpr(dstVertex)));
	            }
	            
	            // No path from vertex
	            BoolExpr hasPossiblePath = this.ctx.mkOr(
	            		possiblePaths.toArray(new BoolExpr[]{}));
	            BoolExpr consequent = this.ctx.mkNot(hasPossiblePath);
	           	
	            BoolExpr implies = this.ctx.mkImplies(antecedent,
	                    consequent);
	            solver.Assert(implies);
    		}
    		
    		// If no edge to vertex, then no path to vertex
    		for (ProcessVertex dstVertex : this.verticesToConsts.keySet()) {
    			BoolExpr antecedent = this.ctx.mkBool(true);
    			if (flowEdgesByDst.containsKey(dstVertex)) {
		            // Obtain disjoint edge constants for possible edges to vertex
		            List<BoolExpr> possibleEdges = new ArrayList<BoolExpr>();
		            for (VirtualDirectedEdge<ProcessVertex> edge :
		            		flowEdgesByDst.get(dstVertex)) {
		                if (constsForI.get(edge) != null) {
		                	possibleEdges.add(constsForI.get(edge));
		                }
		            }
		            
		            // No edge from vertex
		            BoolExpr hasPossibleEdge = this.ctx.mkOr(
		            		possibleEdges.toArray(new BoolExpr[]{}));
		            antecedent = this.ctx.mkNot(hasPossibleEdge);
    			}
	            
	            // Obtain path calls for possible paths to vertex
	            List<BoolExpr> possiblePaths = new ArrayList<BoolExpr>();
	            for (ProcessVertex srcVertex : this.verticesToConsts.keySet()) {
	            	if (srcVertex.equals(dstVertex)) {
	            		continue;
	            	}
	            	possiblePaths.add(this.applyFunc(funcDisjointpath, 
	                        this.getVertexExpr(srcVertex), 
	                        this.getVertexExpr(dstVertex)));
	            }
	            
	            // No path from vertex
	            BoolExpr hasPossiblePath = this.ctx.mkOr(
	            		possiblePaths.toArray(new BoolExpr[]{}));
	            BoolExpr consequent = this.ctx.mkNot(hasPossiblePath);
	           	
	            BoolExpr implies = this.ctx.mkImplies(antecedent,
	                    consequent);
	            solver.Assert(implies);
    		}
            
            // Path from src to dst must exist
            BoolExpr fullPathExists = this.applyFunc(funcDisjointpath,
            		this.getVertexExpr(sourceVertex), 
            		this.getVertexExpr(destinationVertex));
            solver.Assert(fullPathExists);*/
            
            // Set must contain an edge from the source
            BoolExpr hasSrcEdge = this.ctx.mkOr(
                    sourceEdges.toArray(new BoolExpr[]{}));
            solver.Assert(hasSrcEdge);
            
            // Set must contain an edge to the destination
            BoolExpr hasDstEdge = this.ctx.mkOr(
                    destinationEdges.toArray(new BoolExpr[]{}));
            solver.Assert(hasDstEdge);
            
            // Set must contain exactly one upstream edge for each edge
            // in the set (except edges connected to source)
            for (Entry<VirtualDirectedEdge<ProcessVertex>, BoolExpr> entry :
                    constsForI.entrySet()) {
                // Ignore source edge
                if (sourceEdges.contains(entry.getValue())) {
                    continue;
                }

                // Obtain disjoint edge constants for possible upstream edges
                List<BoolExpr> possibleUpstream = new ArrayList<BoolExpr>();
                for (VirtualDirectedEdge<ProcessVertex> edge :
                        flowEdgesByDst.get(entry.getKey().getSource())) {
                    if (constsForI.get(edge) != null) {
                    	possibleUpstream.add(constsForI.get(edge));
                    }
                }
                
                // Create items for cardinality constraint
                List<BoolExpr> upstreamOptions = new ArrayList<BoolExpr>();
                for (BoolExpr upstream : possibleUpstream) {
                	List<BoolExpr> notPossibleUpstream = 
                			new ArrayList<BoolExpr>(possibleUpstream);
                	notPossibleUpstream.remove(upstream);
                	BoolExpr notUpstream = this.ctx.mkNot(this.ctx.mkOr(
                			notPossibleUpstream.toArray(new BoolExpr[]{})));
                	BoolExpr upstreamOption = this.ctx.mkAnd(upstream, 
                			notUpstream);
                	upstreamOptions.add(upstreamOption);
                }
                
                // Exactly one upstream
                BoolExpr disjointEdge = entry.getValue();
                BoolExpr upstreamEdges = this.ctx.mkOr(
                		upstreamOptions.toArray(new BoolExpr[]{}));
                upstreamEdges = this.ctx.mkAtMost(
                		possibleUpstream.toArray(new BoolExpr[]{}),1);
                BoolExpr hasUpstream = this.ctx.mkImplies(disjointEdge, 
                        upstreamEdges);
                solver.Assert(hasUpstream);
            }
            
            // Set must contain at least one downstream edge for each edge
            // in the set (except edges connected to destination)
            for (Entry<VirtualDirectedEdge<ProcessVertex>, BoolExpr> entry :
                    constsForI.entrySet()) {
                // Ignore destination edge
                if (destinationEdges.contains(entry.getValue())) {
                    continue;
                }
                                
                // Obtain disjoint edge constants for possible downstream edges
                List<BoolExpr> possibleDownstream = new ArrayList<BoolExpr>();
                for (VirtualDirectedEdge<ProcessVertex> edge :
                        flowEdgesBySrc.get(entry.getKey().getDestination())) {
                    if (constsForI.get(edge) != null) {
                        possibleDownstream.add(constsForI.get(edge));
                    }
                }
                
                // At least one downstream
                BoolExpr disjointEdge = entry.getValue();
                BoolExpr downstreamEdges = this.ctx.mkOr(
                        possibleDownstream.toArray(new BoolExpr[]{}));
                BoolExpr hasDownstream = this.ctx.mkImplies(disjointEdge, 
                        downstreamEdges);
                solver.Assert(hasDownstream);
            }
        }
		
		// Inter-device edges can only be in one set
		for (VirtualDirectedEdge<ProcessVertex> edge : flowEdges) {
		    // Only consider inter-device edges
		    if (edge.getType() != EdgeType.INTER_DEVICE) {
		        continue;
		    }
		    
		    // Get disjoint edge from each set
		    List<BoolExpr> disjointEdges = new ArrayList<BoolExpr>();
		    for (int i = 0; i < k; i++) {
		        disjointEdges.add(constsDisjoint.get(i).get(edge));
		    }
		    
		    // Constraint for each disjoint edge
		    for (int i = 0; i < k; i++) {
		        List<BoolExpr> otherEdges = new ArrayList<BoolExpr>(disjointEdges);
		        BoolExpr disjointEdge = otherEdges.remove(i);
		        BoolExpr noOtherEdges = this.ctx.mkNot(this.ctx.mkOr(
		                otherEdges.toArray(new BoolExpr[]{})));
		        BoolExpr onlyInOneSet = this.ctx.mkImplies(disjointEdge, 
		                noOtherEdges);
		        solver.Assert(onlyInOneSet);
		    }
		}
		
		this.constsDisjointedge.put(flow, constsDisjoint);
	}
	
	/**
     * Add primary path constraints to the problem.
     * @param solver MaxSMT solver
     */
    private void addPrimaryPathConstraints(Optimize solver, Flow flow, 
            List<VirtualDirectedEdge<ProcessVertex>> primaryPath) {
        /*throw new RepairException(
            "Primary path constraints are not currently supported");*/
        
        Set<VirtualDirectedEdge<ProcessVertex>> flowEdges = 
	            this.constsFlowedge.get(flow).keySet();
        
        // Declare cost and predecessor constants
        Map<ProcessVertex, IntExpr> vertexCostConsts
                = new HashMap<ProcessVertex, IntExpr>();
        Map<ProcessVertex, Expr> vertexPredConsts
                = new HashMap<ProcessVertex, Expr>();
	    for (VirtualDirectedEdge<ProcessVertex> edge : flowEdges) {
	        // Cost and predecessor from vertex to SOURCE
	        ProcessVertex vertex = edge.getSource();
	        if (!vertexCostConsts.containsKey(vertex)) {
	            IntExpr vertexCost = this.ctx.mkIntConst("cost:" 
	                    + flow.toString() + ":" + vertex.toString());
	            vertexCostConsts.put(vertex, vertexCost);
	            Expr vertexPred = this.ctx.mkConst("pred:" + flow.toString() 
	                    + ":" + vertex.toString(), this.typeVertex);
	            vertexPredConsts.put(vertex, vertexPred);
	        }
            vertex = edge.getDestination();
            if (!vertexCostConsts.containsKey(vertex)) {
                IntExpr vertexCost = this.ctx.mkIntConst("cost:" 
                        + flow.toString() + ":" + vertex.toString());
                vertexCostConsts.put(vertex, vertexCost);
                Expr vertexPred = this.ctx.mkConst("pred:" + flow.toString() 
                    + ":" + vertex.toString(), this.typeVertex);
                vertexPredConsts.put(vertex, vertexPred);
            }
	    }
    
        //Expr tc = this.getTrafficClassExpr(flow);
        
        // Required edge costs must be zero
        for (VirtualDirectedEdge<ProcessVertex> edge : 
        		this.requiredEdgesCommon) {
	        IntExpr edgeV1V2Cost = this.getEdgeCostExpr(edge);
	        BoolExpr zeroCost = this.ctx.mkEq(edgeV1V2Cost, this.ctx.mkInt(0));
	        solver.Assert(zeroCost);
	    }
        /*for (VirtualDirectedEdge<ProcessVertex> edge : 
        		this.requiredEdgesCommon) {
	    	Expr v1 = this.getVertexExpr(edge.getSource());
	        Expr v2 = this.getVertexExpr(edge.getDestination());
	        IntExpr edgeV1V2Cost = this.applyIntFunc(this.funcEdgecost, 
	                v1, v2);
	        BoolExpr zeroCost = this.ctx.mkEq(edgeV1V2Cost, this.ctx.mkInt(0));
	        solver.Assert(zeroCost);
	    }*/
        
        // Optional edges that exist must have positive costs
        List<VirtualDirectedEdge<ProcessVertex>> optionalEdgesFlow =
        		new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
        optionalEdgesFlow.addAll(this.constsFlowedge.get(flow).keySet());
        optionalEdgesFlow.removeAll(this.requiredEdgesCommon);
        for (VirtualDirectedEdge<ProcessVertex> edge : optionalEdgesFlow) {
	        BoolExpr antecedent = this.getFlowedgeExpr(flow, edge);
	        IntExpr edgeV1V2Cost = this.getEdgeCostExpr(edge);
	        int lowerBound = 0;
	        int upperBound = 100; // FIXME: Calculate
	        if (EdgeType.INTER_DEVICE == edge.getType()) {
	        	lowerBound = 1;
	        }
	        BoolExpr lower = this.ctx.mkGe(edgeV1V2Cost,
	        		this.ctx.mkInt(lowerBound));
	        BoolExpr upper = this.ctx.mkLe(edgeV1V2Cost,
	        		this.ctx.mkInt(upperBound));
	        BoolExpr consequent = this.ctx.mkAnd(lower, upper);
	        BoolExpr implies = this.ctx.mkImplies(antecedent, consequent);
	        solver.Assert(implies);
	    }
        /*List<VirtualDirectedEdge<ProcessVertex>> optionalEdgesFlow =
        		new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
        optionalEdgesFlow.addAll(this.possibleEdgesByFlow.get(flow));
        optionalEdgesFlow.removeAll(this.requiredEdgesCommon);
        for (VirtualDirectedEdge<ProcessVertex> edge : optionalEdgesFlow) {
	    	Expr v1 = this.getVertexExpr(edge.getSource());
	        Expr v2 = this.getVertexExpr(edge.getDestination());
	        BoolExpr antecedent = this.applyFunc(this.funcFlowedge, v1, v2, tc);
	        IntExpr edgeV1V2Cost = this.applyIntFunc(this.funcEdgecost, v1, v2);
	        int lowerBound = 0;
	        if (EdgeType.INTER_DEVICE == edge.getType()) {
	        	lowerBound = 1;
	        }
	        BoolExpr consequent = this.ctx.mkGe(edgeV1V2Cost,
	        		this.ctx.mkInt(lowerBound));
	        BoolExpr implies = this.ctx.mkImplies(antecedent, consequent);
	        solver.Assert(implies);
	    }*/
        
        // Categorize edges by destination vertex
        Map<ProcessVertex,List<VirtualDirectedEdge<ProcessVertex>>> flowEdgesByDst =
                new LinkedHashMap<ProcessVertex,
                    List<VirtualDirectedEdge<ProcessVertex>>>();
        for (VirtualDirectedEdge<ProcessVertex> edge : flowEdges) {
            if (!flowEdgesByDst.containsKey(edge.getDestination())) {
                flowEdgesByDst.put(edge.getDestination(), 
                        new ArrayList<VirtualDirectedEdge<ProcessVertex>>());
            }
            flowEdgesByDst.get(edge.getDestination()).add(edge);
        }
        
        // Constraints to calculate cost from SOURCE to every vertex
        for (ProcessVertex v2 : flowEdgesByDst.keySet()) {
            List<VirtualDirectedEdge<ProcessVertex>> edgesToV2 = 
                    flowEdgesByDst.get(v2);
            
            IntExpr srcV2Cost = vertexCostConsts.get(v2);
            Expr srcV2Pred = vertexPredConsts.get(v2);
            
            // Consider all edges to reach vertex
            for (VirtualDirectedEdge<ProcessVertex> edgeV1V2 : edgesToV2) {
                ProcessVertex v1 = edgeV1V2.getSource();
                BoolExpr hasEdgeV1V2 = this.getFlowedgeExpr(flow, edgeV1V2);
                IntExpr srcV1Cost = vertexCostConsts.get(v1);
                IntExpr edgeV1V2Cost = this.getEdgeCostExpr(edgeV1V2);
                ArithExpr viaV1Cost = this.ctx.mkAdd(srcV1Cost, edgeV1V2Cost);
                
                // No alternative path to vertex should be cheaper
                List<BoolExpr> altPaths = new ArrayList<BoolExpr>();
                for (VirtualDirectedEdge<ProcessVertex> edgeV3V2 : edgesToV2) {
                    if (edgeV3V2.equals(edgeV1V2)) {
                        altPaths.add(this.ctx.mkBool(false));
                        continue;
                    }
                    
                    ProcessVertex v3 = edgeV3V2.getSource();
                    BoolExpr hasEdgeV3V2 = this.getFlowedgeExpr(flow, edgeV3V2);
                    IntExpr srcV3Cost = vertexCostConsts.get(v3);
                    IntExpr edgeV3V2Cost = this.getEdgeCostExpr(edgeV3V2);
                    ArithExpr viaV3Cost = this.ctx.mkAdd(srcV3Cost, 
                            edgeV3V2Cost);
                    BoolExpr altLowerCost = this.ctx.mkLt(viaV3Cost, viaV1Cost);
                    BoolExpr altPath = this.ctx.mkAnd(hasEdgeV3V2,altLowerCost);
                    altPaths.add(altPath);
                }
                BoolExpr hasAltPath = this.ctx.mkOr(
                        altPaths.toArray(new BoolExpr[]{}));
                BoolExpr antecedent = this.ctx.mkAnd(hasEdgeV1V2, 
                        this.ctx.mkNot(hasAltPath));
                
                
                ArithExpr toV2Cost = this.ctx.mkAdd(srcV1Cost, edgeV1V2Cost);
                BoolExpr assignCost = this.ctx.mkEq(srcV2Cost, toV2Cost);
                BoolExpr assignPred = this.ctx.mkEq(srcV2Pred, 
                        this.getVertexExpr(v1));
                BoolExpr consequent = this.ctx.mkAnd(assignCost, assignPred);
                BoolExpr exprFromSrcCosts = this.ctx.mkImplies(antecedent, 
                        consequent);
                solver.Assert(exprFromSrcCosts);
            }
        }
        /*Expr v1 = this.ctx.mkConst("v1", this.typeVertex);
        Expr v2 = this.ctx.mkConst("v2", this.typeVertex);
        BoolExpr edgeV1V2 = this.applyFunc(this.funcFlowedge, v1, v2, tc);        
        Expr v3 = this.ctx.mkConst("v3", this.typeVertex);
        BoolExpr edgeV3V2 = this.applyFunc(this.funcFlowedge, v3, v2, tc);
        IntExpr srcV1Cost = this.applyIntFunc(this.funcFromsrccost, v1, tc);
        IntExpr edgeV1V2Cost = this.applyIntFunc(this.funcEdgecost, v1, v2);
        IntExpr srcV3Cost = this.applyIntFunc(this.funcFromsrccost, v3, tc);
        IntExpr edgeV3V2Cost = this.applyIntFunc(this.funcEdgecost, v3, v2);
        ArithExpr viaV1Cost = this.ctx.mkAdd(srcV1Cost, edgeV1V2Cost);
        ArithExpr viaV3Cost = this.ctx.mkAdd(srcV3Cost, edgeV3V2Cost);
        BoolExpr altLowerCost = this.ctx.mkLt(viaV3Cost, viaV1Cost);
        BoolExpr altPath = this.ctx.mkAnd(edgeV3V2, altLowerCost);
        BoolExpr hasAltPath = this.ctx.mkExists(new Expr[] {v3}, altPath, 
				1, null, null, null, null);
        BoolExpr antecedent = this.ctx.mkAnd(edgeV1V2, 
        		this.ctx.mkNot(hasAltPath));
        IntExpr srcV2Cost = this.applyIntFunc(this.funcFromsrccost, v2, tc);
        ArithExpr toV2Cost = this.ctx.mkAdd(srcV1Cost, edgeV1V2Cost);
        BoolExpr assignCost = this.ctx.mkEq(srcV2Cost, toV2Cost);
        Expr srcV2Pred = this.ctx.mkApp(this.funcFromsrcpred, v2, tc);
        BoolExpr assignPred = this.ctx.mkEq(srcV2Pred, v1);
        BoolExpr consequent = this.ctx.mkAnd(assignCost, assignPred);
        BoolExpr implies = this.ctx.mkImplies(antecedent, consequent);
        BoolExpr exprFromSrcCosts = this.ctx.mkForall(
                new Expr[] {v1, v2}, implies, 
                1, null, null, null, null);
        solver.Assert(exprFromSrcCosts);*/
        
        // Cost from source to source is zero
        IntExpr srcSrcCost = vertexCostConsts.get(this.sourceVertex);
        BoolExpr zeroCost = this.ctx.mkEq(srcSrcCost, this.ctx.mkInt(0));
        solver.Assert(zeroCost);
        
        // Predecessor from source is source
        Expr srcSrcPred = vertexPredConsts.get(this.sourceVertex);
        BoolExpr selfPred = this.ctx.mkEq(srcSrcPred, 
                this.getVertexExpr(this.sourceVertex));
        solver.Assert(selfPred);
        
        // Edges in primary path must exist
        for (VirtualDirectedEdge<ProcessVertex> edge : primaryPath) {
            BoolExpr edgeV1V2 = this.getFlowedgeExpr(flow, edge);
            Expr predV2 = vertexPredConsts.get(edge.getDestination());
            BoolExpr edgesrcIsPred = this.ctx.mkEq(predV2, 
                    this.getVertexExpr(edge.getSource()));
            solver.Assert(this.ctx.mkAnd(edgeV1V2, edgesrcIsPred));
        }
        /*for (VirtualDirectedEdge<ProcessVertex> edge : primaryPath) {
            // Edges in primary path must exist
            v1 = this.getVertexExpr(edge.getSource());
            v2 = this.getVertexExpr(edge.getDestination());
            edgeV1V2 = this.applyFunc(this.funcFlowedge, v1, v2, tc);
            Expr predV2 = this.ctx.mkApp(this.funcFromsrcpred, v2, tc);
            BoolExpr edgesrcIsPred = this.ctx.mkEq(predV2, v1);
            solver.Assert(this.ctx.mkAnd(edgeV1V2, edgesrcIsPred));
        }*/
    }
	
	/**
	 * Add policy constraints for all flows to the problem.
	 * @param solver MaxSMT solver
	 */
	private void addPolicyConstraints(Optimize solver) {
		logger.debug("Policies:");
		for (Flow flow : modifier.policies.keySet()) {
			if (!this.flowsToConsts.containsKey(flow)) {
				continue;
			}
			this.addPolicyConstraints(solver, flow);
		}
	}
	
	/**
	 * Add policy constraints for a flow to the problem.
	 * @param solver MaxSMT solver
	 * @param flow the flow for which policy constraints should be added
	 */
	private void addPolicyConstraints(Optimize solver, Flow flow) {
		logger.debug(flow.toString());
		List<Policy> policies = modifier.policies.get(flow);
		
		// Ensure we know about this flow
		if (!this.flowsToConsts.containsKey(flow)) {
			logger.error("No number assigned to " + flow.toString());
			return;
		}
		
		boolean pathConstraints = false;
		
		// Add policy specific rules
		for (Policy policy : policies) {
		    logger.debug(policy.toString());
			switch (policy.getType()) {
			case ALWAYS_BLOCKED:
			{
			    if (!pathConstraints) {
			        this.addPathConstraints(solver, flow);
			        pathConstraints = true;
			    }
				this.addBlockedConstraints(solver, flow);
				break;
			}
			case ALWAYS_ISOLATED:
			{
				break;
			}
			case ALWAYS_REACHABLE:
			{
				// Get number of failures tolerated
				Integer k = (Integer)policy.getParameter();
				if (k <= 0) {
					continue;
				}
				
				this.addReachableConstraints(solver, flow, k);
				break;
			}
			case ALWAYS_WAYPOINT:
			{
				this.addWaypointConstraints(solver, flow);
				break;
			}
			case PRIMARY_PATH:
			{
			    if (!pathConstraints) {
                    this.addPathConstraints(solver, flow);
                    pathConstraints = true;
                }
			    List<VirtualDirectedEdge<ProcessVertex>> primaryPath =
			            (List<VirtualDirectedEdge<ProcessVertex>>)
			            policy.getParameter();
			    this.addPrimaryPathConstraints(solver, flow, primaryPath);
			    break;
			}
			}
		}
	}
	
	/**
	 * Get all possible vertices that could exist based on the physical topology
	 * and existing processes.
	 * @return the set of possible vertices
	 */
	private Map<String,ProcessVertex> getPossibleVertices() {
		Map<String,ProcessVertex> possibleVertices = 
				new LinkedHashMap<String,ProcessVertex>();
        
        // Iterate over all devices and processes
        for (Device device : modifier.deviceEtg.getDevices().values()) {
        	for (Process process : device.getRoutingProcesses()) {
        		possibleVertices.put(process.getInVertex().getName(), 
        				process.getInVertex());
        		possibleVertices.put(process.getOutVertex().getName(),
        				process.getOutVertex());
        	}
        }
        
        // Add endpoint vertices of src/dst vertices
    	ProcessGraph anyEtg = modifier.etgs.values().iterator().next();
    	this.sourceVertex = anyEtg.getFlowSourceVertex();
    	possibleVertices.put(this.sourceVertex.getName(), 
    			this.sourceVertex);
    	this.destinationVertex = anyEtg.getFlowDestinationVertex();
    	possibleVertices.put(this.destinationVertex.getName(), 
    			this.destinationVertex);
        
        return possibleVertices;
	}
	
	/**
	 * Solve the MaxSMT problem.
	 * @return true if all policies can be satisfied, otherwise false
	 */
	private boolean solve() {
		// Solve problem
		Status result = this.formulation.Check();
		this.solved = true;
		
		// Make sure problem is satisfiable
		logger.info("Solver result: " + result);
		if (result != Status.SATISFIABLE) {
			logger.info("Policies cannot be satisifed");
			return false;
		}
		
		// Get model
		this.solution = this.formulation.getModel();
		logger.debug(this.solution.toString());
		
		logger.debug("COUNT: unsatSoftAssertions " + this.softAssertions);
		
		return true;
	}

    /**
     * Get the edges that exist in the repaired ETG for each flow.
     * @return edges in the repaired ETG for each flow
     */    
	public Map<Flow,List<VirtualDirectedEdge<ProcessVertex>>> getEdges() {
		// Solve problem if not already solved
		if (!solved) {
			this.solve();
		}
		
		// If unsolvable, then return null
		if (null == this.solution) {
			return null;
		}
		
		// Get value of every flow edge constant
		Map<Flow,List<VirtualDirectedEdge<ProcessVertex>>> edges = 
	            new LinkedHashMap<Flow,List<VirtualDirectedEdge<ProcessVertex>>>();
		for (Flow flow : this.constsFlowedge.keySet()) {
		    List<VirtualDirectedEdge<ProcessVertex>> flowEdges = 
		            new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
		    for (Entry<VirtualDirectedEdge<ProcessVertex>,BoolExpr> entry : 
		            this.constsFlowedge.get(flow).entrySet()) {
		        VirtualDirectedEdge<ProcessVertex> edge = entry.getKey();
		        Expr edgeConst = this.solution.getConstInterp(entry.getValue());
		        
		        // Add edge to edge set if constant value is true
		        if(edgeConst.toString().equals("true")) {
		            flowEdges.add(edge);
		            
		            // Update edge cost
                    Expr edgeCost = this.solution.getConstInterp(
                            this.getEdgeCostExpr(edge));
                    if (edgeCost.isIntNum()) {
                        int cost = ((IntNum)edgeCost).getInt();
                        edge.setWeight(cost);
                    }
                    
                    
		        }
		    }
		    edges.put(flow, flowEdges);
		}
		return edges;
	}
	
	 /**
     * Get the edges that exist in the repaired ETG for each destination.
     * @return edges in the repaired ETG for each destination
     */  
	public Map<PolicyGroup,List<VirtualDirectedEdge<ProcessVertex>>> 
			getDestinationEdges() {		
		// Solve problem if not already solved
		if (!solved) {
			this.solve();
		}
		
		// If unsolvable, then return null
		if (null == this.solution) {
			return null;
		}
		
		// Get value of every destination edge constant
        Map<PolicyGroup,List<VirtualDirectedEdge<ProcessVertex>>> edges = 
                new LinkedHashMap<PolicyGroup,List<VirtualDirectedEdge<ProcessVertex>>>();
        for (PolicyGroup dst : this.constsDstedge.keySet()) {
            List<VirtualDirectedEdge<ProcessVertex>> dstEdges = 
                    new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
            for (Entry<VirtualDirectedEdge<ProcessVertex>,BoolExpr> entry : 
                    this.constsDstedge.get(dst).entrySet()) {
                VirtualDirectedEdge<ProcessVertex> edge = entry.getKey();
                Expr edgeConst = this.solution.getConstInterp(entry.getValue());
                
                // Add edge to edge set if constant value is true
                if(edgeConst.toString().equals("true")) {
                    dstEdges.add(edge);
                    
                    // Update edge cost
                    Expr edgeCost = this.solution.getConstInterp(
                            this.getEdgeCostExpr(edge));
                    if (edgeCost.isIntNum()) {
                        int cost = ((IntNum)edgeCost).getInt();
                        edge.setWeight(cost);  
                    }
                }
            }
            edges.put(dst, dstEdges);
        }
        return edges;
	}
	
	/**
     * Get the edges that exist in the repaired base ETG.
     * @return edges in the repaired base ETG
     */  
	public List<VirtualDirectedEdge<ProcessVertex>> getBaseEdges() {		
		// Solve problem if not already solved
		if (!solved) {
			this.solve();
		}
		
		// If unsolvable, then return null
		if (null == this.solution) {
			return null;
		}
		
		// Get value of every base edge constant
        List<VirtualDirectedEdge<ProcessVertex>> edges = 
                new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
        for (Entry<VirtualDirectedEdge<ProcessVertex>,BoolExpr> entry : 
                this.constsBaseedge.entrySet()) {
            VirtualDirectedEdge<ProcessVertex> edge = entry.getKey();
            Expr edgeConst = this.solution.getConstInterp(entry.getValue());
            
            // Add edge to edge set if constant value is true
            if(edgeConst.toString().equals("true")) {
                edges.add(edge);
                
                // Update edge cost
                Expr edgeCost = this.solution.getConstInterp(
                        this.getEdgeCostExpr(edge));
                if (edgeCost.isIntNum()) {
                    int cost = ((IntNum)edgeCost).getInt();
                    edge.setWeight(cost);  
                }
            }
        }
        return edges;
	}
	
	/**
     * Get the edges that exist in the disjoint edge sets for each flow.
     * @return edges in the repaired ETG for each flow
     */    
    public Map<Flow, Map<Integer, List<VirtualDirectedEdge<ProcessVertex>>>> 
            getDisjointEdges() {
        // Solve problem if not already solved
        if (!solved) {
            this.solve();
        }
        
        // If unsolvable, then return null
        if (null == this.solution) {
            return null;
        }
        
        // Get value of disjoint edge constants for every flow
        logger.info("Disjoint");
        Map<Flow,Map<Integer,List<VirtualDirectedEdge<ProcessVertex>>>> edges = 
                new LinkedHashMap<Flow,
                    Map<Integer,List<VirtualDirectedEdge<ProcessVertex>>>>();
        for (Flow flow : this.constsDisjointedge.keySet()) {
            Map<Integer,List<VirtualDirectedEdge<ProcessVertex>>> flowEdges = 
                    new LinkedHashMap<Integer,
                        List<VirtualDirectedEdge<ProcessVertex>>>();
            logger.info(flow.toString());
            // Get value of disjoint edge constants for every disjoint set
            for (Integer num : this.constsDisjointedge.get(flow).keySet()) {
                List<VirtualDirectedEdge<ProcessVertex>> disjointEdges = 
                        new ArrayList<VirtualDirectedEdge<ProcessVertex>>();
                logger.info("\t"+num);
            	List<ProcessVertex> srcVertices = new ArrayList<ProcessVertex>();
                // Get value of disjoint edge constants for every edge
                for (Entry<VirtualDirectedEdge<ProcessVertex>,BoolExpr> entry : 
                        this.constsDisjointedge.get(flow).get(num).entrySet()) {
                    Expr disjointConst = this.solution.getConstInterp(
                            entry.getValue());
                    if (null == disjointConst) {
                        logger.error("\t\tNULL " + entry.getValue());
                        continue;
                    }
                    
                    // Add edge to disjoint edge set if constant value is true
                    if(disjointConst.toString().equals("true")) {
                    	String warn = "";
                    	if (srcVertices.contains(entry.getKey().getSource())) {
                    		warn = " ***DUPLICATE SOURCE***";
                    	}
                        disjointEdges.add(entry.getKey());
                        srcVertices.add(entry.getKey().getSource());
                        logger.info("\t\t"+entry.getKey().toString()+warn);
                    }
                }
                flowEdges.put(num, disjointEdges);
            }
            edges.put(flow, flowEdges);
        }
        return edges;
    }
}
