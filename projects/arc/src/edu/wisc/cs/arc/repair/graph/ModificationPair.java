package edu.wisc.cs.arc.repair.graph;

import edu.wisc.cs.arc.graphs.PolicyGroup;

import java.io.Serializable;

/**
 * Contains all the necessary information for a modification to occur.
 * @author Franklin van Nes (fvannes@colgate.edu)
 */
public class ModificationPair implements Serializable {
    private static final long serialVersionUID = 5847758965232544412L;

    public enum Level {
        BASE,
        DESTINATION,
        TRAFFIC_CLASS  // FLOW
    }

    private GraphModification graphMod;
    private ConfigModification configMod;
    /** Policy Groups
     * This can have a max length of two, and can be empty.
     * The size determines the type of content.
     * policyGroups = []  Empty. Built from a base mod.
     * policyGroups = [Destination] 1 entry. Built from dMods
     * policyGroups = [Source, Destination] // 2 entries. Built from tcMods
     * */
    private PolicyGroup[] policyGroups;
    private Level level;

    ModificationPair(ConfigModification.Action action, ConfigModification.Stanza stanza,
                     ConfigModification.Substanza substanza,
                     GraphModification modification) {
        this.configMod = new ConfigModification(action, stanza, null, substanza, null);
        this.graphMod = modification;
        this.level = Level.BASE;
    }

    ModificationPair(ConfigModification.Action action, ConfigModification.Stanza stanza,
                     ConfigModification.Substanza substanza,
                     Level level,
                     PolicyGroup[] policyGroups,
                     GraphModification modification) throws IllegalArgumentException {
        this.configMod = new ConfigModification(action, stanza, null, substanza, null);
        this.graphMod = modification;
        this.policyGroups = policyGroups;
        this.level = level;
        if ((level == Level.TRAFFIC_CLASS && policyGroups.length != 2)
                || (level == Level.DESTINATION && policyGroups.length != 1)
                || (level == Level.BASE && policyGroups.length != 0)){
            throw new IllegalArgumentException("ModificationPair level does not match " +
                    "the number of policy groups.");
        }
    }

    public ConfigModification getConfigModification() {
        return configMod;
    }

    public GraphModification getGraphModification() {
        return graphMod;
    }

    public PolicyGroup getDestination() {
        if (level == Level.BASE){
            return null;
        }
        return policyGroups[policyGroups.length-1];
    }

    public PolicyGroup getSource() {
        if (level != Level.TRAFFIC_CLASS){
            return null;
        }
        return policyGroups[0];
    }

    public Level getLevel() {
        return this.level;
    }

    public String toString() {
        if (this.level == Level.TRAFFIC_CLASS) {
            return String.format("%s %s->%s : %s", level, getSource(), getDestination(), getConfigModification().getAction());
        } else if (this.level == Level.DESTINATION) {
            return String.format("%s any->%s : %s", level, getDestination(), getConfigModification().getAction());
        } else {
            return String.format("%s : %s", level, getConfigModification());
        }
    }
}
