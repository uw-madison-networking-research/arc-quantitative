package edu.wisc.cs.arc.repair;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.configs.Config;
import org.antlr.v4.runtime.Token;

/**
 * Inserts a randomly selected mutation from the gene pool into the individual
 * Created by amahmood on 6/12/17.
 */
public class Insertion extends MutationOperator {

    public Insertion(Settings settings){

        this.settings = settings;
        this.logger = settings.getLogger();
    }

    public GeneticProgrammingResult mutate(Config original){

        Mutation mutation = getMutation();
        mutation.setMutationType(Mutation.MUTATION_TYPE.INSERTION);
        Config mutant = insertMutation(original, mutation.getMutationText());

        return new GeneticProgrammingResult(mutant, mutation);
    }

    private Config insertMutation(Config original, String mutation) {
        StringBuilder rawMutantText = new StringBuilder();
        boolean inserted = false;

        for (Token tok : original.getTokens()) {
            //Insert mutation above statement
            if (!inserted && tok.getLine() == mutationPos) {
                rawMutantText.append(mutation);
                inserted = true;
            }
            //Append tokens in normal order
            rawMutantText.append(tok.getText());
        }

       return buildConfig(rawMutantText, original);
    }
}
