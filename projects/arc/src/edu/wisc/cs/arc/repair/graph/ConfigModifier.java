package edu.wisc.cs.arc.repair.graph;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.ProcessGraph;

import java.util.List;
import java.util.Map;

/**
 * Determines how to modify a set of configurations based on a set of 
 * modifications to the corresponding extended topology graphs.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public abstract class ConfigModifier {
	
	/** Original extended topology graphs */
	protected Map<Flow, ProcessGraph> etgs;
	
	/** Graph modifications */
	protected ConfigModificationGroup
			graphModifications;
	
	/** Settings */
	protected Settings settings;
	
	/** Logger */
	protected Logger logger;
	
	/** 
	 * Create a modifier.
	 * @param etgs the current extended topology graphs.
	 * @param graphModifications a graph modifications from aETGs, dETGs, and tcETGs.
	 * @param settings
	 */
	public ConfigModifier(Map<Flow, ProcessGraph> etgs, 
			ConfigModificationGroup graphModifications,
			Settings settings) {
		this.etgs = etgs;
		this.graphModifications = graphModifications;
		this.settings = settings;
		this.logger = settings.getLogger();
	}
	
	/**
	 * Repair the network to satisfy the provided policies.
	 * @return true if the network can be repaired
	 */
	public abstract List<ModificationPair> getModifications();
}
