package edu.wisc.cs.arc.repair;

/**
 * Stores useful information about a particular mutation
 * Created by amahmood on 6/30/17.
 */
public class Mutation {

    public enum MUTATION_TYPE{
        INSERTION,
        DELETION,
        REPLACEMENT
    }

    private static final String UNUSED = "N/A";
    private MUTATION_TYPE mutationType;
    private String hostUsed;
    private String stanzaType;
    private int hostLine;
    private int mutationLine;
    private String mutationText;

    public Mutation() {
        this.mutationType = MUTATION_TYPE.DELETION;
        this.mutationLine = -1;
        this.stanzaType = UNUSED;
        this.hostLine = -1;
        this.mutationLine= -1;
        this.mutationText = UNUSED;
    }

    public Mutation(MUTATION_TYPE mutationType) {
        this.mutationType = mutationType;
    }

    public Mutation(MUTATION_TYPE mutationType,
                    String hostUsed,
                    String stanzaType,
                    int hostLine,
                    int mutationLine,
                    String mutation) {
        this.mutationType = mutationType;
        this.hostUsed = hostUsed;
        this.stanzaType = stanzaType;
        this.hostLine = hostLine;
        this.mutationLine = mutationLine;
        this.mutationText = mutation;
    }

    public MUTATION_TYPE getMutationType() {
        return mutationType;
    }

    public void setMutationType(MUTATION_TYPE mutationType) {
        this.mutationType = mutationType;
    }

    public String getHostUsed() {
        return hostUsed;
    }

    public void setHostUsed(String hostUsed) {
        this.hostUsed = hostUsed;
    }

    public String getStanzaType() {
        return stanzaType;
    }

    public void setStanzaType(String stanzaType) {
        this.stanzaType = stanzaType;
    }

    public int getHostLine() {
        return hostLine;
    }

    public void setHostLine(int hostLine) {
        this.hostLine = hostLine;
    }

    public int getMutationLine() {
        return mutationLine;
    }

    public void setMutationLine(int mutationLine) {
        this.mutationLine = mutationLine;
    }

    public String getMutationText() {
        return mutationText;
    }

    public void setMutationText(String mutationText) {
        this.mutationText = mutationText;
    }

}
