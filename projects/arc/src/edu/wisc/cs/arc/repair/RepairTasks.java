package edu.wisc.cs.arc.repair;

import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.configs.Config;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.PolicyGroup;
import edu.wisc.cs.arc.policies.Policy;

/**
 * Perform tasks related to repair.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class RepairTasks {

    public static enum LocalizeMethod{
        SIMPLE,
        SMART
    };
    /**
     * Perform genetic repair on configurations.
     * @param settings
     * @param configs configurations to repair
     * @param policyGroups traffic classes in the network
     */
    public static void geneticRepair(Settings settings,
            Map<String, Config> configs, List<PolicyGroup> policyGroups) {
        Logger logger = settings.getLogger();
        logger.info("*** Genetic Repair ***");
        GeneProg geneTest = new GeneProg(settings, policyGroups, configs);
        geneTest.run();
    }

    public static void localizeFaults(Settings settings,
            Map<String, Config> configs, List<PolicyGroup> policyGroups,
            Map<Policy, List<DirectedEdge>> violations){
        Logger logger  = settings.getLogger();
        logger.info("*** Localizing faults in configs using " 
        		+ settings.getFaultLocalizeMethod() + " localization ***");
        GeneProg gp = new GeneProg(settings, policyGroups, configs);
        switch(settings.getFaultLocalizeMethod()){
            case SIMPLE:
                gp.localizeOnly(violations, false);
                break;
            case SMART:
                gp.localizeOnly(violations, true);
                break;
            default:
                break;
        }

    }
}
