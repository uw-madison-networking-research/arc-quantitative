package edu.wisc.cs.arc.repair;

/**
 * Runtime exception thrown during network repair.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class RepairException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public RepairException(String msg) {
		super(msg);
	}
	
	public RepairException(String msg, Exception ex) {
		super(msg, ex);
	}
}
