package edu.wisc.cs.arc.repair.graph.configwriters;

import edu.wisc.cs.arc.repair.graph.ModificationPair;

/**
 * Raised when a repair modification cannot be written to a file.
 * @author Franklin van Nes (fvannes@colgate.edu)
 */
public class ModificationNotImplementedException extends Exception {

    public ModificationPair modPair;

    public ModificationNotImplementedException(ModificationPair modPair, String message) {
        super(message);
        this.modPair = modPair;
    }

    public ModificationNotImplementedException(ModificationPair modPair, String message, Throwable cause) {
        super(message, cause);
        this.modPair = modPair;
    }

    @Override
    public String toString() {
        return "For modification: " + this.modPair.toString() + " :: " + super.toString();
    }
}
