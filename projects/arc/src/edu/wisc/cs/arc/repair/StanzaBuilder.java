package edu.wisc.cs.arc.repair;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.configs.Config;
import edu.wisc.cs.arc.graphs.PolicyGroup;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.batfish.datamodel.Ip;
import org.batfish.datamodel.IpWildcard;
import org.batfish.datamodel.Prefix;
import org.batfish.grammar.cisco.CiscoParser;
import org.batfish.grammar.cisco.CiscoParser.*;
import org.batfish.grammar.cisco.CiscoParserBaseListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class StanzaBuilder extends CiscoParserBaseListener{

    public static final String INTERFACE = "interface";
    public static final String ROUTER = "router";
    public static final String STATIC_ROUTE = "ip route";
    public static final String ACL = "access-list";
    public static final String OSPF = "OSPF";
    
	/* Sort all stanzas in a given configuration by type. 
	 * This map stores every stanza in the configuration keyed by its StanzaType. 
	 */
	Map<String, List<Stanza>> stanzaMap;
    List<? extends Token> tokenList;
    
	private String hostname;
    private Config config;
    private Logger logger = Logger.getInstance();

    /** Extract a map from stanza type to a list of stanzas
     * @param config : Config object from which the stanzas are to be extracted
     * @param hostname : Hostname of the device
     * @return Map of stanzatype(string) to list of stanzas
     */
    public Map<String, List<Stanza>> parseStanzas(Config config,String hostname) {
        stanzaMap = new HashMap<>();
        this.hostname = hostname;
        this.config = config;
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(this, config.getParseTree());
        return stanzaMap;
    }
	
	/** Helper function to ensure all stanzas are included in the stanza map. **/
    private Stanza makeAndAdd(ParserRuleContext stanza){
        String stanzaType = stanza.getStart().getText();

        List<? extends Token> tokenList = config.getTokens(
                stanza.getStart().getTokenIndex(),
                stanza.getStop().getTokenIndex());

        Stanza _stanza = new Stanza(
            stanza.getStart().getLine(),
            stanza.getStop().getLine(),
            stanzaType,
            this.hostname,
            tokenList
        );
        if (!this.stanzaMap.containsKey(stanzaType))
            this.stanzaMap.put(stanzaType, new ArrayList<Stanza>());
        this.stanzaMap.get(stanzaType).add(_stanza);
        return _stanza;
    }

    private Stanza makeAndAdd(ParserRuleContext stanza, String stanzaType){
        List<? extends Token> tokenList = config.getTokens(
                stanza.getStart().getTokenIndex(),
                stanza.getStop().getTokenIndex());

        Stanza _stanza = new Stanza(
                stanza.getStart().getLine(),
                stanza.getStop().getLine(),
                stanzaType,
                this.hostname,
                tokenList
        );
        if (!this.stanzaMap.containsKey(stanzaType))
            this.stanzaMap.put(stanzaType, new ArrayList<Stanza>());
        this.stanzaMap.get(stanzaType).add(_stanza);
        return _stanza;
    }


    @Override
    public void enterS_interface(S_interfaceContext ctx) {
        String stanzaType = ctx.getStart().getText();
        List<? extends Token> tokenList = config.getTokens(
                ctx.getStart().getTokenIndex(),
                ctx.getStop().getTokenIndex());

        Stanza _stanza = new Stanza(
            ctx.getStart().getLine(),
            ctx.getStop().getLine(),
            stanzaType, //this should be 'interface'
            this.hostname,
            tokenList
        );

        List<If_ip_access_groupContext> interfaceACLs = ctx.if_ip_access_group();
        ArrayList<String> aclNames = new ArrayList<>();
        for (If_ip_access_groupContext access_group:interfaceACLs){
            aclNames.add(access_group.name.getText());
        }
        _stanza.setInterface(ctx.iname.getText(), aclNames);

        if (!this.stanzaMap.containsKey(stanzaType))
            this.stanzaMap.put(stanzaType, new ArrayList<Stanza>());
        this.stanzaMap.get(stanzaType).add(_stanza);
        //TODO :change the above to the following:
        // _stanza.setInterface(ctx.iname);

    }

    @Override
    public void exitS_router_ospf(S_router_ospfContext ctx) {
        String stanzaType = ctx.getStart().getText();
        List<? extends Token> tokenList = config.getTokens(
                ctx.getStart().getTokenIndex(),
                ctx.getStop().getTokenIndex());

        Stanza _stanza = new Stanza(
            ctx.getStart().getLine(),
            ctx.getStop().getLine(),
            stanzaType, //this should be 'interface'
            this.hostname,
            tokenList
        );

        List<Ro_networkContext> listNetworkContexts = ctx.ro_network();
        String ip, wildcard;
        ArrayList<PolicyGroup> policyGroups = new ArrayList<>();
        for (Ro_networkContext ron : listNetworkContexts){
            ip = ron.ip.getText();
            Ip routerIp = new Ip(ip);

            if (ron.wildcard!=null){
                wildcard = ron.wildcard.getText();
            }else{
                //no wildcard . TODO: look for prefix and then assume everything before the consecutive zeros at end is mask
                logger.debug("No Wildcard found");
                return;
            }
            Ip routerWildCard = new Ip(wildcard);
            IpWildcard routerSubnetWildCard = new IpWildcard(routerIp, routerWildCard);
            Prefix routerSubnetPrefix = routerSubnetWildCard.toPrefix();
            policyGroups.add(new PolicyGroup(routerSubnetPrefix));

        }
        _stanza.setRouter(policyGroups);
        if (!this.stanzaMap.containsKey(stanzaType))
            this.stanzaMap.put(stanzaType, new ArrayList<Stanza>());
        this.stanzaMap.get(stanzaType).add(_stanza);

    }

    @Override
    public void exitRouter_bgp_stanza(Router_bgp_stanzaContext ctx) {
        makeAndAdd(ctx);
    }

    @Override
    public void exitStandard_access_list_stanza(Standard_access_list_stanzaContext ctx) {
        Stanza s = makeAndAdd(ctx);
        s.setStandardAcl(true);
    }

    @Override
    public void exitExtended_access_list_stanza(Extended_access_list_stanzaContext ctx) {
        Stanza s = makeAndAdd(ctx);
        s.setExtendedAcl(true);
    }

    @Override
    public void exitIf_description(If_descriptionContext ctx){
        makeAndAdd(ctx);
    }

    @Override
    public void exitIf_channel_group(If_channel_groupContext ctx){
        makeAndAdd(ctx);
    }

    @Override
    public void exitIf_ip_access_group(If_ip_access_groupContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_ip_address(If_ip_addressContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_ip_address_secondary(If_ip_address_secondaryContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_ip_address_virtual(If_ip_address_virtualContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_ip_ospf_area(If_ip_ospf_areaContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_ip_ospf_cost(If_ip_ospf_costContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_ip_ospf_passive_interface(If_ip_ospf_passive_interfaceContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_ip_policy(If_ip_policyContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_ip_router_ospf_area(If_ip_router_ospf_areaContext ctx){
        Stanza stanza = makeAndAdd(ctx);
        stanza.setRouterProtocol(OSPF);
    }
    @Override
    public void exitIf_no_ip_address(If_no_ip_addressContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_shutdown(If_shutdownContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_switchport(If_switchportContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_switchport_access(If_switchport_accessContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_switchport_mode(If_switchport_modeContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_switchport_trunk_allowed(If_switchport_trunk_allowedContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_vrf(If_vrfContext ctx){
        makeAndAdd(ctx);
    }
     @Override
    public void exitIf_vrf_forwarding(If_vrf_forwardingContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIf_vrf_member(If_vrf_memberContext ctx){
        makeAndAdd(ctx);
    }

    @Override
    public void exitS_policy_map(S_policy_mapContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitS_class_map(S_class_mapContext ctx){
        makeAndAdd(ctx);
    }


    @Override
    public void exitRoute_map_stanza(Route_map_stanzaContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIp_prefix_list_stanza(Ip_prefix_list_stanzaContext ctx){
        makeAndAdd(ctx);
    }

    @Override
    public void exitRouter_hsrp_if(Router_hsrp_ifContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitRoute_policy_stanza(Route_policy_stanzaContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitIp_route_stanza(Ip_route_stanzaContext ctx) {
        makeAndAdd(ctx, STATIC_ROUTE);
    }
    @Override
    public void exitPrefix_set_stanza(Prefix_set_stanzaContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitS_hostname(S_hostnameContext ctx) {
        makeAndAdd(ctx);
    }
    @Override
    public void exitS_vrf_definition(S_vrf_definitionContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitS_router_static(S_router_staticContext ctx){
        makeAndAdd(ctx);
    }
    @Override
    public void exitAs_path_set_stanza(As_path_set_stanzaContext ctx){
        makeAndAdd(ctx);
    }

    @Override public void exitS_router_vrrp(CiscoParser.S_router_vrrpContext ctx) { }
    @Override public void exitS_router_eigrp(CiscoParser.S_router_eigrpContext ctx) { }
    @Override public void exitS_router_eigrp_tail(CiscoParser.S_router_eigrp_tailContext ctx) { }
    @Override public void exitS_router_ospfv3(CiscoParser.S_router_ospfv3Context ctx) { }

    @Override public void exitRs_address_family(CiscoParser.Rs_address_familyContext ctx) { makeAndAdd(ctx, "address-family"); }
    @Override public void exitBgp_address_family(CiscoParser.Bgp_address_familyContext ctx) { makeAndAdd(ctx, "address-family");}

    @Override
    public void exitRouter_rip_stanza(Router_rip_stanzaContext ctx){
        makeAndAdd(ctx);
    }


    @Override
    public void exitS_vlan(S_vlanContext ctx){
        makeAndAdd(ctx);
    }

}
