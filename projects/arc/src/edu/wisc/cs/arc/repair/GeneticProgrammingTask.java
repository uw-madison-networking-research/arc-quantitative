package edu.wisc.cs.arc.repair;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.configs.Config;
import edu.wisc.cs.arc.graphs.*;
import edu.wisc.cs.arc.policies.Policy;
import edu.wisc.cs.arc.verifiers.VerificationTasks;
import org.batfish.datamodel.Configuration;

import java.util.*;
import java.util.concurrent.Callable;

/**
 * Created by amahmood on 7/6/17.
 */
public class GeneticProgrammingTask implements Callable<GeneticProgrammingResult>{

    private Individual original;
    private MutationOperator mutation_op;
    private Map<String, List<Stanza>> localizedFaults;
    private Map<String, Configuration> genericConfigurations;
    private Logger logger;
    private Settings settings;
    private List<Policy> violatedPolicicies;
    private Set<Flow> violatedFlows;
    private String hostname;

    public GeneticProgrammingTask(Settings settings,
                                  List<Policy> policies,
                                  Individual original,
                                  String hostname,
                                  MutationOperator mutation_op,
                                  Map<String, List<Stanza>> localizedFaults,
                                  Set<Flow> violatedFlows) {
        this.settings = settings;
        this.violatedPolicicies = policies;
        this.logger = settings.getLogger();
        this.original = original;
        this.hostname = hostname;
        this.mutation_op = mutation_op;
        this.localizedFaults = localizedFaults;
        this.violatedFlows = violatedFlows;

        this.genericConfigurations = new HashMap<String, Configuration>();
        this.genericConfigurations.putAll(original.getGenericConfigurations());
    }


    /**
     * Simulates a single run of an individual by performing a mutation and scoring the mutant
     */
    @Override
    public GeneticProgrammingResult call(){

        int errorScore = Integer.MAX_VALUE;
        boolean validBuild = true;

        //Build config object by using mutated tokens
        Config mutantConfig = original.getConfig(hostname);
        GeneticProgrammingResult mutant = new GeneticProgrammingResult(mutantConfig);

        try {
            mutant = mutation_op.mutate(mutantConfig, localizedFaults);
            this.genericConfigurations.put(hostname,
                    mutant.getMutant().getGenericConfiguration());
        } catch (Exception e){//FIXME: Should I be catching this?
            logger.debug("Unable to parse configs");
            validBuild = false;
        }

        if (validBuild) {
            Map<Flow, ProcessGraph> violatedFlowETGs = null;

            try {//FIXME: What about this..?
                violatedFlowETGs = EtgTasks.generateFlowETGs(this.settings,
                        this.violatedFlows,
                        this.genericConfigurations);
            } catch (NullPointerException e) {
                logger.debug("Failed to create flows");
            }

            if (violatedFlowETGs != null) {

                //Check Policies File
                Collection<List<DirectedEdge>> violations =
                        VerificationTasks.verifyPolicies(settings,this.violatedPolicicies, violatedFlowETGs)
                                         .values();
                logger.info(violations.size() + " violatedPolicicies violated: " + violations);
                errorScore = violations.size();
                mutant.setScore(errorScore);
            }
        }

        return mutant;
    }
}


