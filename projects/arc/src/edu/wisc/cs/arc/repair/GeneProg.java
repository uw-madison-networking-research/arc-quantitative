package edu.wisc.cs.arc.repair;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.configs.Config;
import edu.wisc.cs.arc.graphs.*;
import edu.wisc.cs.arc.policies.Policy;
import edu.wisc.cs.arc.policies.PolicyFile;
import edu.wisc.cs.arc.verifiers.*;

import org.batfish.datamodel.Configuration;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.*;

/**
 * Implements a pseudo-genetic programming algorithm to fix router configurations
 * Created by amahmood on 6/8/17.
 */
public class GeneProg {

    private final static Mutation.MUTATION_TYPE[] MUTATION_TYPES = Mutation.MUTATION_TYPE.values();
    private final static int NUM_MUTATION_TYPES = MUTATION_TYPES.length;
    private final static int MAX_GENERATIONS = 10;
    private final static int POPULATION_SIZE = 500;
    private final static int TARGET_SCORE = 0;
    private final static double FAILURE_THRESHOLD = 1.2;

    private Settings settings;
    private Logger logger;

    private double selectionThreshold;
    private Map<Policy, List<DirectedEdge>> violations = new HashMap<>();
    private GeneticProgrammingResult bestFitIndividual;
    private int total_iters;
    private List<PolicyGroup> policyGroups;
    private Map<Flow,List<Policy>> policies;
    private List<Policy> violatedPolicies;
    private Set<Flow> violatedFlows;
    private Random random;
    private Map<String, Configuration> genericConfigurations;
    private Map<String, Config> configs;
    private Map<String, List<Stanza>> localizedFaults;
    private Insertion insertOp;
    private Deletion deleteOp;
    private Replacement replaceOP;
    private long startTime;
    private int numHosts;
    private List<String> hostSearchSpace;
    private Map<String, Map<String, List<Stanza>>> hostsToStanzas;

    public GeneProg(Settings settings, List<PolicyGroup> policyGroups2,
            Map<String, Config> configs){
        this.settings = settings;
        this.logger = this.settings.getLogger();
        this.policyGroups = policyGroups2;
        this.configs = configs;
        this.insertOp = new Insertion(settings);
        this.deleteOp = new Deletion(settings);
        this.replaceOP = new Replacement(settings);

        this.random = new Random();
        this.initialize();
        bestFitIndividual = null;
        total_iters = 0;
    }

    private void initialize() {
        this.genericConfigurations = new ConcurrentHashMap<String, Configuration>();
        this.hostSearchSpace = new ArrayList<String>();
        this.violatedFlows = new HashSet<Flow>(); //Updated in localizedParameters

        for (Entry<String, Config> entry : configs.entrySet()){
            String hostname = entry.getKey();
            Config config = entry.getValue();
            this.genericConfigurations.put(hostname,
                    config.getGenericConfiguration());
        }

        // Extract map of stanzas from every original config
        this.hostsToStanzas = buildHostToStanzasMap();

        //Add all hosts to search space
        hostSearchSpace.addAll(configs.keySet());
        numHosts = hostSearchSpace.size();

        //Initialize gene pools of mutation operators
        insertOp.setGenePool(hostsToStanzas);
        replaceOP.setGenePool(hostsToStanzas);
    }

    private Map<String, Map<String, List<Stanza>>> buildHostToStanzasMap(){
        Map<String, Map<String, List<Stanza>>> hostsToStanzas = new HashMap<>();
        for (String host : configs.keySet()) {
            Map<String, List<Stanza>> _stanzaMap = (new StanzaBuilder()).parseStanzas(configs.get(host), host);
            hostsToStanzas.put(host, _stanzaMap);
        }
        return hostsToStanzas;
    }

    public void localizeOnly(Map<Policy, List<DirectedEdge>> violations, boolean smart){
        Map<String, Map<String, List<Stanza>>> hostsToStanzas = buildHostToStanzasMap();
        this.violations = violations;
        if (!smart){
            faultLocalizeSimple(violations, hostsToStanzas);
        }else{ //this.violations DOES NOT EQUAL violations
            faultLocalizeSmart(violations, hostsToStanzas);
        }
    }


    public void run() {

        //Population is a list of a config objects
        List<Individual> population = new ArrayList<Individual>(POPULATION_SIZE);
        this.policies = PolicyFile.loadPolicies(settings.getPoliciesRepairFile());
        boolean successful = false;

        //Start timing
        startTime = System.currentTimeMillis();

        boolean needToRun = localizeParameters();
        if (!needToRun) {
            return;
        }

        int generation = 0;
        int numThreads = Runtime.getRuntime().availableProcessors();

        //Initialize population
        for (int i = 0; i < POPULATION_SIZE; i++) {
            population.add(new Individual(configs, genericConfigurations));
        }

        //For every possible faulty config according to violations
        while (generation < MAX_GENERATIONS) {

            // Create a thread pool
            ExecutorService threadPool =
                    Executors.newFixedThreadPool(numThreads);

            //Reset bestfit
            bestFitIndividual = null;

            //Run the Genetic Algorithm
            successful = singleRun(population, threadPool);

            if (successful){
                break;
            }

            if (null != bestFitIndividual) {
                logger.info("*** Best fit of the generation " + generation + " *** ");
                logger.info("Error Score: " + bestFitIndividual.getScore());
                System.out.println(bestFitIndividual.getMutant().getText());
            }

            generation++;
            total_iters += POPULATION_SIZE;
        }

        long runTime = System.currentTimeMillis() - startTime;
        if (successful) {
            printAnalysis(bestFitIndividual, generation, runTime);
        } else {
            printAnalysis(generation, POPULATION_SIZE, runTime);
        }

    }


    /**
     * Performs a single run of the algorithm and returns whether a solution was found or not
     * @return Whether algorithms was successful in finding solution or not
     */
    public boolean singleRun( List<Individual> population,
                          ExecutorService threadPool){

      MutationOperator mutationOp = null; //safest temp init
        Mutation.MUTATION_TYPE type = null;
        String hostname;
        int iter = 0, totalFitness = 0;

        List<Future<GeneticProgrammingResult>> futureScores = new ArrayList<>(POPULATION_SIZE);
        logger.info("Population Size: " + population.size());

        for (Individual member : population) {
            System.out.println("\t_____________________________________________________");
            logger.info("Individual number " + iter + ":");

            //Randomly selecting a mutation operation
            type = MUTATION_TYPES[random.nextInt(NUM_MUTATION_TYPES)];
            logger.info("Choosing Mutation Operator: " + type);
            switch (type) {
                case INSERTION:
                    mutationOp = insertOp;
                    break;

                case DELETION:
                    mutationOp = deleteOp;
                    break;

                case REPLACEMENT:
                    mutationOp = replaceOP;
                    break;
            }

            hostname = hostSearchSpace.get(random.nextInt(numHosts));
            System.out.println("Localizing to host: " + hostname);

            mutationOp.setMutantHost(hostname);

            GeneticProgrammingTask task =
                    new GeneticProgrammingTask(
                            settings,
                            violatedPolicies,
                            member,
                            hostname,
                            mutationOp,
                            localizedFaults,
                            violatedFlows);
            futureScores.add(threadPool.submit(task));
        }

        List<GeneticProgrammingResult> results = new ArrayList<>(POPULATION_SIZE);
        totalFitness =  analyzeFutures(futureScores, threadPool, results);

        if (null != bestFitIndividual && bestFitIndividual.getScore() == TARGET_SCORE) {
            return true;
        }

        calculateSelectionProbabilities(results, totalFitness);

        updatePopulation(population, results);

        return false;
    }

    /**
     * Gets the results from the threaded pool
     * and calculates fitness score for every mutant
     * Sets best fit individual if successful mutant found
     * @return totalFitness sum of fitness scores of all the mutants
     */
    public int analyzeFutures(List<Future<GeneticProgrammingResult>> futureScores,
                               ExecutorService threadPool, List<GeneticProgrammingResult> results){

        int iter = 0, errorScore = Integer.MAX_VALUE, fitness = 0, totalFitness = 0;
        boolean foundCorrectMutant = false;
        try {

            for (Future<GeneticProgrammingResult> future : futureScores) {
                iter++;
                GeneticProgrammingResult result = future.get();

                errorScore = result.getScore();

                //Found potential mutant!
                if (errorScore == TARGET_SCORE) {

                    if (verifyResult(result)){
                        total_iters += iter;
                        bestFitIndividual = result;
                        foundCorrectMutant = true;
                        break;
                    }
                    else { //Reset error score since mutation introduced more problems
                        result.setScore(Integer.MAX_VALUE);
                        errorScore = Integer.MAX_VALUE;
                    }
                }

                if (errorScore < selectionThreshold){
                    fitness = Math.abs(violatedPolicies.size() - errorScore);
                }else {
                    fitness = 0;
                }
                result.setScore(fitness);
                totalFitness += fitness;

                results.add(result);
            }

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        finally {
            shutdownAndAwaitTermination(threadPool);
        }

        return totalFitness;
    }

    public boolean verifyResult(GeneticProgrammingResult bestResult){

        logger.info("Verifying possible correct mutation");

        //FIXME: SHOULD PROBABLY USE GENERIC CONFIGURATIONS FROM
        // THE INDIVIDUAL ASSOCIATED WITH THIS RESULT
        Config _config = bestResult.getMutant();
        Map<String, Configuration> _genericConfigurations = new HashMap<>();
        _genericConfigurations.putAll(this.genericConfigurations);
        _genericConfigurations.put(_config.getHostname(), _config.getGenericConfiguration());

        Map<Flow, ProcessGraph> flowETGs =
                EtgTasks.generateFlowETGs(this.settings,
                        _genericConfigurations,
                        this.policyGroups);

        Collection<List<DirectedEdge>> _violations =
                VerificationTasks.verifyPolicies(settings, this.policies, flowETGs).values();

        if (_violations.size() == 0){
            logger.info("No errors in configs!");
            return true;
        }
        else {
            logger.info("Rejecting mutation: Additional violations were introduced...");
            return false;
        }

    }

    /**
     * Calculates survival probability using total fitness
     */
    public void calculateSelectionProbabilities(List<GeneticProgrammingResult> results, int totalFitness){

        float probability = 0;
        if (totalFitness > 0) {
            for (GeneticProgrammingResult result : results) {
                probability = result.getScore() / totalFitness;
                result.setSelectionProbability(probability);
            }
        }
    }


    /**
     * Updates the population with according to survival probabilities of results
     * @param population list of current population
     * @param rankedResults results that have been selection probabilities
     */
    private void updatePopulation(List<Individual> population,
                                  List<GeneticProgrammingResult> rankedResults) {

        float probability = 0;

        //TODO: UPDATE CONFIGS CONSIDERING DEPENDENCIES
        Config _config;
        for (int i = 0; i < POPULATION_SIZE; i++) {
            _config = null;

            //Simulating mutant selection with probability
            for (GeneticProgrammingResult result : rankedResults) {
                probability = random.nextFloat();
                if (probability <= result.getSelectionProbability()) {
                    _config = result.getMutant();
                    break;
                }
            }

            //If found valid config
            if (null != _config) {
                population.get(i).updateConfig(_config.getHostname(), _config);
            }
        }

    }

    public void printAnalysis(GeneticProgrammingResult result,
                              int generation,
                              long time){
        Mutation finalMutation = result.getMutation();

        //FIXME: Some statements are not useful anymore
        logger.info("Found correct build!");
        logger.info(result.getMutant().getText());
        logger.info("\tFaulty Host: " + result.getMutant().getHostname());
        logger.info("\tGeneration: " + generation);
        logger.info("\tIndividual Number: " + (total_iters - generation*POPULATION_SIZE));
        logger.info("\tMutation Operator Used: " + finalMutation.getMutationType());
        logger.info("\tMutation Used: " + finalMutation.getMutationText().trim());
        logger.info("\tMutation Position: " + finalMutation.getMutationLine());
        logger.info("\tStarting population Size: " + POPULATION_SIZE);
        logger.info("\tTime: " + time + " ms");
    }


    public void printAnalysis(int generation,
                              int population,
                              long time){

        logger.info("Unable to fix config...");
        logger.info("\tTotal Generations: " + generation);
        logger.info("\tPopulation Size: " + population);
        logger.info("\tTime: " + time + " ms");
    }

    /**
     * Uses policy verification to localize policies, update host search space
     * and determine violated flows
     * @return true upon successful localization, false when no violation found
     */
    public boolean localizeParameters(){

        Map<Flow, ProcessGraph> flowETGs =
                EtgTasks.generateFlowETGs(this.settings,
                        this.genericConfigurations,
                        this.policyGroups);

        Map<Policy, List<DirectedEdge>> verifierResult =
                VerificationTasks.verifyPolicies(settings, this.policies, flowETGs);
        violations = verifierResult;

        if (violations.values().size() == 0){
            logger.info("No errors in configs!");
            return false;
        }

        logger.info(violations.values().size() + " policies violated");
        System.out.println("Violation: " + violations.values().toString());

        //Add localized host to the front of the list if any were found
        localizedFaults = faultLocalizeSimple(violations, hostsToStanzas);
        if (localizedFaults.keySet().size() != 0){
            logger.info("*** LOCALIZED HOSTS : " + localizedFaults.keySet());
            for (String host: localizedFaults.keySet()) {
                hostSearchSpace.remove(host);
                hostSearchSpace.add(0,host);
            }
        }

        //Getting flows involved in violations
        violatedPolicies = new ArrayList<Policy>(verifierResult.keySet()) ;
        for (Policy policy: violatedPolicies){
            violatedFlows.add(policy.getTrafficClass());
            System.out.println("Violated Flow: " + policy.getTrafficClass());
        }

        selectionThreshold = FAILURE_THRESHOLD * violatedPolicies.size();

        return true;
    }

    /**
     * Returns a list of possible stanza types that may be responsible for the violations
     *
     */
    private Map<String, List<Stanza>> faultLocalizeSimple(Map<Policy, List<DirectedEdge>> violations,
                                                             Map<String,Map<String,List<Stanza>>> hostsToStanzas){
        Collection<List<DirectedEdge>> counterExamples = violations.values();
        Map<String, List<Stanza>> faultyStanzas = new HashMap<>();
        List<Stanza> interfaceStanzas, routerStanzas;
        HashSet<String> locLinesText = new HashSet<>();
        Map<Policy.PolicyType,Integer> policiesCountType = new HashMap<>();
        for (Policy policy: violations.keySet()){
            // System.out.println(policy.getType() + "  ->  " + violations.get(policy).size() + " violations");
            // System.out.println(policy.getParameter());
            policiesCountType.put(policy.getType(), policiesCountType.containsKey(policy.getType())?policiesCountType.get(policy.getType())+1:1);
            List<DirectedEdge> edges = violations.get(policy);
            // for (DirectedEdge edge: edges){
            //     System.out.println(edge.getSourceInterface());
            // }
        }

        for (Policy.PolicyType policyType : policiesCountType.keySet()){
            System.out.println(policyType+" --> "+policiesCountType.get(policyType));
        }

        for (Entry<Policy, List<DirectedEdge>> violation:violations.entrySet()){
            logger.info(violation.getKey() + "  " + violation.getValue().size());
            List<DirectedEdge> counterExample = violation.getValue();
            for (DirectedEdge edge : counterExample) {
                Interface srcIface = edge.getSourceInterface();
                Interface dstIface = edge.getDestinationInterface();

                List<Interface> ifaces = new ArrayList<Interface>();
                if (srcIface != null) {
                    ifaces.add(srcIface);
                }
                if (dstIface != null) {
                    ifaces.add(dstIface);
                }
                // System.out.println(ifaces.size() + "SIZEIFACES  - > " + edge);
                for (Interface iface: ifaces) {
                    String deviceName = iface.getDevice().getName();
                    if (!hostsToStanzas.get(deviceName).containsKey(StanzaBuilder.INTERFACE)) continue;
                    // System.out.println(iface.getFullName());
                    //Save hostnames
                    if(!faultyStanzas.containsKey(deviceName)) {
                        faultyStanzas.put(deviceName,  new ArrayList<>());
                    }


                    interfaceStanzas = hostsToStanzas.get(deviceName).get(StanzaBuilder.INTERFACE);
                    // System.out.println(interfaceStanzas.size() + " number of interface stanzas");
                    //TODO: Optimize
                    for (Stanza stanza : interfaceStanzas) {
                        if (stanza.getInterfaceName().equals(iface.getName())) {
                            faultyStanzas.get(deviceName).add(stanza);
                        }
                    }
                }
            }
            //Get routers associated with the traffic class associated with the policy i.e the traffic class the policy applies to.
            for (String deviceName : hostsToStanzas.keySet()){
                routerStanzas = hostsToStanzas.get(deviceName).get(StanzaBuilder.ROUTER);
                if (routerStanzas==null) continue;
                for (Stanza routerStanza:routerStanzas){
                    if (routerStanza.connectsSubnet(violation.getKey().getTrafficClass().getSource())
                        || routerStanza.connectsSubnet(violation.getKey().getTrafficClass().getDestination()) ){
                        if(!faultyStanzas.containsKey(routerStanza.getHostname()))
                            faultyStanzas.put(routerStanza.getHostname(),  new ArrayList<>());
                        faultyStanzas.get(routerStanza.getHostname()).add(routerStanza);
                    }
                }
            }


            // this.logger.info("Number of faulty stanzas : " + faultyStanzas.keySet().size());

            //Printing line numbers to logger
            for (String host : faultyStanzas.keySet()){
                for (Stanza _stanza : faultyStanzas.get(host)) {
                    if (_stanza.isRouter())
                        locLinesText.add("HOST: "+host+" --> STANZA: RouterStanza --> LOCLINE: " +_stanza.getStartLine()+"-"+_stanza.getEndLine());
                    else
                        locLinesText.add("HOST: "+host+" --> STANZA: "+ _stanza.getInterfaceName() + " --> LOCLINE: " +_stanza.getStartLine()+"-"+_stanza.getEndLine());
                }
            }

        }
        for (String line: locLinesText){
            logger.info(line);
        }
        return faultyStanzas;
    }

    /**
     * Returns a list of possible stanza types that may be responsible for the violations
     *
     */
    private Map<String, List<Stanza>> faultLocalizeSmart(Map<Policy, List<DirectedEdge>> violations, Map<String,Map<String,List<Stanza>>> hostsToStanzas){
        //map from device names to list of faulty stanzas...
        Map <String, List<Stanza>> faultyStanzas = new HashMap<>();
        List<Stanza> stanzaList;
        for(Policy policy: violations.keySet()){
            List<DirectedEdge> counterExample = violations.get(policy);
            Policy.PolicyType policyType = policy.getType();
            switch(policyType){
                case ALWAYS_BLOCKED:
                    break;
                case ALWAYS_REACHABLE:
                    boolean commonSrc = true;
                    boolean commonDst = true;
                    Interface currentSrcIface = counterExample.get(0).getSourceInterface();
                    Interface currentDstIface = counterExample.get(0).getDestinationInterface();
                    for (DirectedEdge edge : counterExample){
                        if (!currentSrcIface.equals(edge.getSourceInterface())){
                            commonSrc =false;
                        }
                        if (!currentDstIface.equals(edge.getDestinationInterface())){
                            commonDst =false;
                        }
                    }
                    HashSet<Interface> faultyIfaces = new HashSet<>();
                    for(DirectedEdge edge : counterExample){
                        if (!commonSrc){
                            faultyIfaces.add(edge.getDestinationInterface());
                        }
                        if (!commonDst){
                            faultyIfaces.add(edge.getSourceInterface());
                        }if (commonSrc && commonDst){
                            faultyIfaces.add(edge.getDestinationInterface());
                            faultyIfaces.add(edge.getSourceInterface());
                        }
                    }
                    break;
                case ALWAYS_ISOLATED:
                    break;
                default:
                    //impossible right now
            }
        }
        return null;
    }


    /**
     * Taken from Oracle ExecutorService JavaDocs
     */
    void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(5, TimeUnit.SECONDS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

}
