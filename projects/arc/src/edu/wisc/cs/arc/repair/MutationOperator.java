package edu.wisc.cs.arc.repair;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.configs.Config;
import org.antlr.v4.runtime.Token;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by amahmood on 6/12/17.
 */
public abstract class MutationOperator {

    public static final int SEED = -1; //7
    public static final int FAULT_WEIGHT = 1;
    public static final int TOTAL_WEIGHT = 10;
    public static final String NEWLINE = "\n";
    public static final String EMPTY_STRING = "";
    public static final String HOSTNAME = "hostname";
    public static final String INTERFACE_STANZA = "interface";

    public Random random = (SEED > -1) ? new Random(SEED):new Random();
    public Map<String, List<String>> genePool = new HashMap<String, List<String>>();
    public Map<String, Map<String,List<Stanza>>> hostToStanza = new HashMap<String, Map<String,List<Stanza>>>();
    public String mutantHost;
    public Logger logger;
    public Settings settings;
    public Map<String, List<Stanza>> localizedFaults;
    public List<String> hostList;
    public List<String> typeList;
    public int randBound;
    public int mutationPos = 0;
    public List<String> givenLines = null;

    /**
     * Assign the space from which mutations can be derived
     * @param stanzas List of stanzas which can be used to perform the mutation
     * Creates a gene pool which is a set of stanzas containing tokens
     * */
    public void setGenePool(Map<String, Map<String,List<Stanza>>> stanzas){

        hostToStanza = stanzas;

        Map<String, Set<String>> _genePool = new HashMap<String, Set<String>>();

        //Build a map of stanza type to list of unique statements
        for(String host: stanzas.keySet()){
            Set<String> stanzaTypes = stanzas.get(host).keySet();
            List<Stanza> stanzaList;

            for (String type: stanzaTypes ) {

                // Changing hostnames causes incorrect configurations to be updated
                if(type.equals(HOSTNAME)){
                    //Skip this stanza
                    continue;
                }

                if (!_genePool.containsKey(type)) {
                    _genePool.put(type, new HashSet<String>());
                }

                //Add all stanza statements to this map
                stanzaList = stanzas.get(host).get(type);
                HashSet<String> statementSet = new HashSet<String>();

                for (Stanza stanza : stanzaList) {

                    //Build stanza text from tokens
                    StringBuilder fullStanza = new StringBuilder();
                    for (Token tok : stanza.getTokenList()) {
                        fullStanza.append(tok.getText());
                    }

                    //Split into statements and add to set
                    for (String statement : fullStanza.toString().split(NEWLINE)) {
                        statementSet.add(statement.trim());
                    }
                }

                //Update Map
                _genePool.get(type).addAll(statementSet);
            }
        }

        int count = 0;
        for (String type: _genePool.keySet()){
            genePool.put(type, new ArrayList<String>(_genePool.get(type)));
            count += _genePool.get(type).size();
        }

        typeList = new ArrayList<String>(genePool.keySet());
        randBound = typeList.size();

//        for (String type:
//             genePool.keySet()) {
//            System.out.println(type+"--> ");
//            System.out.println(genePool.get(type));
//        }
    }

    /**
     * Perform mutation on input token list and return the mutated form
     * If mutation fails, original config is returned
     * @param original List of tokens on which to perform the mutation
     * @return Mutated list of tokens
     */
    abstract GeneticProgrammingResult mutate(Config original);

    /**
     * Perform mutation on input token list and return the mutated form
     * @param original List of tokens on which to perform the mutation
     * @param localizedHosts Map of hosts and their stanzas that will be weighted more
     * @return Mutated list of tokens
     */
    public GeneticProgrammingResult mutate(Config original,
                         Map<String, List<Stanza>> localizedHosts){
        setMutantHost(original.getHostname());
        setLocalizedFaults(localizedHosts);
        return mutate(original);
    }

    /**
     *  Uses map of interface type to statements associated with that interface
     *  to generate mutation text
     *  @return Mutation object with the generated text
     */
    public Mutation getMutation(){

        String type = typeList.get(random.nextInt(randBound));
        System.out.println("Choosing stanza type: " + type );

        List<String> stmntList = genePool.get(type);
        int bound = stmntList.size();
        String stmnt = stmntList.get(random.nextInt(bound))+NEWLINE;
        System.out.println("Choosing Mutation: ");
        System.out.println("\t" + stmnt);

        Mutation mutation = new Mutation();
        mutation.setHostUsed("N/A");
        mutation.setHostLine(-1);
        mutation.setMutationText(stmnt);

        //Setting up insertion position
        mutationPos = getMutationPosition(settings, type);
        mutation.setMutationLine(mutationPos);

        return mutation;
    }

    /**
     * Gets a line number to insert a mutation into according to the type of stanza
     * or according to a line list from a file specified by settings
     * @return Line number in configuration file of mutant host
     */
    public int getMutationPosition(Settings settings, String stanzaType){

        if (settings.shouldUseRepairLines()) {
            if (null == givenLines) {
                setLocalizationList(settings.getRepairLinesFile());
            }
            int randNum = random.nextInt(givenLines.size());

            return Integer.parseInt(givenLines.get(randNum));
        }

        //FIXME?: Chooses random stanza if file does not have the one given
        List<Stanza> stanzaList;
        Map<String, List<Stanza>> stanzaMap = hostToStanza.get(mutantHost);

        if (stanzaMap.containsKey(stanzaType)) {
            stanzaList = stanzaMap.get(stanzaType);
            //Pick localized interface faults with higher probability
            //TODO: Add more stanza types: router,acl
            int randNum = random.nextInt(TOTAL_WEIGHT);
            if ( stanzaType.equals(INTERFACE_STANZA) &&
                    localizedFaults.containsKey(mutantHost) &&
                    randNum < FAULT_WEIGHT){
                stanzaList = localizedFaults.get(mutantHost);
            }
        }
        else { //Choose randomly
            Object[] availableStanzas = stanzaMap.keySet().toArray();
            int randnum = random.nextInt(availableStanzas.length);
            stanzaList = stanzaMap.get(availableStanzas[randnum]);
        }

        Stanza stanza = stanzaList.get(random.nextInt(stanzaList.size()));
        int linePos = random.nextInt(stanza.getEndLine() - stanza.getStartLine() + 1) + stanza.getStartLine();
        System.out.println("Choosing mutation position: " + linePos);
        return linePos;
    }

    public List<String> setLocalizationList(String filename){
        givenLines = new ArrayList<String>();
        System.out.println("USING FILE: " + filename);
        FileReader fr = null;
        BufferedReader br = null;
        try {
            fr = new FileReader(filename);
            br = new BufferedReader(fr);

            String currLine;

            while((currLine = br.readLine())!=null){
                System.out.println(currLine);
                String lineNum = currLine.split(":")[0];
                givenLines.add(lineNum.substring(1, lineNum.length()));
            }
            System.out.println("*** LINE LIST: " + givenLines);

            br.close();
            fr.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return givenLines;
    }

    /**
     * Builds a new Config object from raw configuration text
     * @param rawMutantText Config text that has been mutated
     * @param original Original config
     * @return Mutated config or original if mutation failed
     */
    public Config buildConfig(StringBuilder rawMutantText, Config original){
        Config mutatedConfig = null;
        try {
            mutatedConfig = new Config(rawMutantText.toString(), original.getHostname(), logger);
        } catch(Exception e){
            logger.debug("Unable to parse mutated config");
            mutatedConfig = original;
        }
        return mutatedConfig;
    }

    public Map<String, List<String>> getGenePool() {
        return genePool;
    }

    public String getMutantHost() {
        return mutantHost;
    }

    public void setMutantHost(String mutantHost) {
        this.mutantHost = mutantHost;
    }

    public Map<String, List<Stanza>> getLocalizedFaults() {
        return localizedFaults;
    }

    public void setLocalizedFaults(Map<String, List<Stanza>> localizedFaults) {
        this.localizedFaults = localizedFaults;
    }


}
