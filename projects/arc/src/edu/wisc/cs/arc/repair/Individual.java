package edu.wisc.cs.arc.repair;


import edu.wisc.cs.arc.configs.Config;
import org.batfish.datamodel.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * Simulates one member of the population
 * Created by amahmood on 7/28/17.
 */

public class Individual {
    private Map<String, Config> configs;
    private Map<String, Configuration> genericConfigurations;

    public Individual(Map<String, Config> configs, Map<String, Configuration> genericConfigurations) {
        this.configs = new HashMap<String, Config>();
        this.genericConfigurations = new HashMap<String, Configuration>();

        ///// Copying content to prevent thread contention /////
        this.genericConfigurations.putAll(genericConfigurations);
        this.configs.putAll(configs);
    }

    public Config getConfig(String hostname) {
        return configs.get(hostname);
    }

    public void updateConfig(String hostname, Config newConfig) {
        this.configs.put(hostname, newConfig);
        this.genericConfigurations.put(hostname, newConfig.getGenericConfiguration());
    }

    public Map<String, Config> getConfigs() {
        return configs;
    }

    public void setConfigs(Map<String, Config> configs) {
        this.configs = configs;
    }

    public Map<String, Configuration> getGenericConfigurations() {
        return genericConfigurations;
    }

    public void setGenericConfigurations(Map<String, Configuration> genericConfigurations) {
        this.genericConfigurations = genericConfigurations;
    }
}
