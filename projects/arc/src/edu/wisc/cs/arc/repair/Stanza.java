package edu.wisc.cs.arc.repair;

import edu.wisc.cs.arc.graphs.PolicyGroup;
import org.antlr.v4.runtime.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smaug on 6/21/17.
 * Stanza class that stores information about a configuration stanza
 */
public class Stanza {

    private int startLine;
    private int endLine;
    private String type;
    private String hostname;

    private List<? extends Token> tokenList;
    private ArrayList<String> tokenStringList; //tokens as strings

    private boolean isInterfaceStanza;
    private String interfaceName; //Optional member variable to identify stanza primarily used for iFaces
    private ArrayList<String> interfaceAclNames; //Optional member variable to store list of names of acls associated with the interface

    private List<PolicyGroup> routerSubnets; //Optional member variable (list of policy groups)

    private String aclName; //Optional member variable for ACL stanzas

    private boolean isRouterStanza;
    private String routerProtocol;

    private boolean isAclStanza;
    private boolean isExtendedAcl;  // Optional
    private boolean isStandardAcl;  // Optional


    // private Map<String,

    public Stanza(int startLine, int endLine, String type, String hostname, List<? extends Token> tokenList, String interfaceName) {
        this(startLine,endLine,type,hostname,tokenList);
        this.interfaceName = interfaceName;
    }

    public Stanza(int startLine, int endLine, String type, String hostname, List<? extends Token> tokenList) {
        this.startLine = startLine;
        this.endLine = endLine;
        this.type = type;
        if (type.equals("extended-access-list")){
            isAclStanza = isExtendedAcl = true;
            isStandardAcl = false;
        } else if (type.equals("access-list")){
            isAclStanza = isStandardAcl = true;
            isExtendedAcl = false;
        }
        this.hostname = hostname;
        this.tokenList = tokenList;
        tokenStringList = new ArrayList<>();
        for(Token token:tokenList){
            tokenStringList.add(token.getText().toLowerCase());
        }
    }

    public void setStartLine(int startLine) {
        this.startLine = startLine;
    }

    public void setEndLine(int endLine) {
        this.endLine = endLine;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setRouter(List<PolicyGroup> policyGroups){
        this.isRouterStanza = true;
        this.routerSubnets = policyGroups;
        // System.out.println("Hostname : " + hostname + " Line Range :" + this.startLine + "--" + this.endLine +  " This Router has " + policyGroups.size() + " subnets associated..");
        //set a boolean that checks router stanza to true here, and set the subnet to the input policyGroup
    }

    public void setInterface(String interfaceName, ArrayList<String> aclNames){
        this.isInterfaceStanza = true;
        this.setInterfaceName(interfaceName);
        this.setInterfaceAclNames(aclNames);
    }

    private void setInterfaceAclNames(ArrayList<String> aclNames){
        this.interfaceAclNames = aclNames;
    }
    private void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public void setTokenList(List<? extends Token> tokenList) {
        this.tokenList = tokenList;
    }

    public void setAclName(String aclName){
        this.isAclStanza=true;
        this.aclName=aclName;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public List<String> getInterfaceAclNames() {
        if (null == interfaceAclNames) {
            interfaceAclNames = new ArrayList<>(0);
        }
        return interfaceAclNames;
    }

    public String getRouterProtocol() {
        return routerProtocol;
    }

    public void setRouterProtocol(String routerProtocol) {
        this.routerProtocol = routerProtocol;
    }

    public int getStartLine() {
        return startLine;
    }

    public int getStartIdx() {
        return this.getTokenList().get(0).getStartIndex();
    }

    public int getEndIdx() {
        int lastTokenIdx = this.getTokenList().get(this.tokenList.size() - 1).getStopIndex();
        int lastNl = getStartIdx() + getText().lastIndexOf('\n');
        if (lastTokenIdx > lastNl) {
            return lastTokenIdx + 1;
        } else {
            return lastNl + 1;
        }
    }

    public int getEndLine() {
        return endLine;
    }

    public String getType() {
        return type;
    }

    public void setExtendedAcl(boolean extendedAcl) {
        isExtendedAcl = extendedAcl;
        if (extendedAcl){
            isAclStanza = true;
            isStandardAcl = !extendedAcl;
        }
    }

    public void setStandardAcl(boolean standardAcl) {
        isStandardAcl = standardAcl;
        if (standardAcl){
            isAclStanza = true;
            isExtendedAcl = !standardAcl;
        }
    }

    public String getHostname() {
        return hostname;
    }

    public List<? extends Token> getTokenList() {
        return tokenList;
    }

    public String getAclName(){
        return this.aclName;
    }

    public boolean isRouter(){
        return isRouterStanza;
    }

    public boolean isAclStanza() {
        return isAclStanza;
    }

    public boolean isExtendedAcl() {
        return isExtendedAcl;
    }

    public boolean isStandardAcl() {
        return isStandardAcl;
    }

    public boolean contains(String tokenString){
        return tokenStringList.contains(tokenString.trim().toLowerCase());
    }

    public boolean connectsSubnet(PolicyGroup policyGroup){
        if (!isRouterStanza) return false;
        for (PolicyGroup connectedSubnet: this.routerSubnets){
            if (policyGroup.equals(connectedSubnet)){
                return true;
            }
        }
        return false;
    }

    public String getText() {
        return String.join("", this.tokenStringList);
    }

    @Override
    public String toString() {
        return "Stanza{" +
                "startLine=" + startLine +
                ", endLine=" + endLine +
                ", type='" + type + '\'' +
                ", hostname='" + hostname + '\'' +
                ", interfaceName='" + interfaceName + '\'' +
                '}';
    }

}
