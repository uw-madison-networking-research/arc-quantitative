package edu.wisc.cs.arc.repair.graph.configwriters;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.configs.Config;
import edu.wisc.cs.arc.repair.Stanza;
import edu.wisc.cs.arc.repair.StanzaBuilder;

import java.util.*;

/**
 * Parent class for every CiscoIOS Editor class.
 * @author Franklin van Nes (fvannes@colgate.edu)
 */
class CiscoIosEditor {
    Map<String, List<Stanza>> stanzaMap;
    Map<Config, List<ConfigInsert>> configInserts = new HashMap<>();
    Logger logger = Logger.getInstance();

    public void writeToConfigs() {
        for (Config config : configInserts.keySet()) {
            writeToConfig(config);
        }
    }

    /**
     * Writes the currently available config inserts to the current configuration
     * and regenerates the stanzaMap.
     */
    protected void writeToConfig(Config config){
        // Order configs
        List<ConfigInsert> inserts = configInserts.get(config);
        if (inserts == null || inserts.size() == 0) {
            return;
        }
        inserts.sort(
                Comparator.comparingInt(ConfigInsert::getInsertIndex).reversed()
        );
        // Insert changes into StringBuilder
        StringBuilder sb = new StringBuilder(config.getText());
        for (ConfigInsert insert : inserts) {
            Logger.getInstance().debug(insert.toString(config.getFile().getName()));
            switch(insert.getMethod()) {
                case INSERT:
                    sb.insert(insert.getInsertIndex(), insert.getSnippet());
                    break;
                case OVERWRITE:
                    sb.replace(insert.getInsertIndex(), insert.getExitIndex(),
                            insert.getSnippet());
                    break;
                case DELETE:
                    sb.delete(insert.getInsertIndex(), insert.getExitIndex());
                    break;
            }
        }
        inserts.clear();
        config.refresh(sb.toString());
        // update stanza map.
        this.stanzaMap = new StanzaBuilder().parseStanzas(config, config.getHostname());
    }

    protected void addInsert(Config config, ConfigInsert insert) {
        if (this.configInserts.containsKey(config)) {
            configInserts.get(config).add(insert);
        } else {
            List<ConfigInsert> newList = new ArrayList<>();
            newList.add(insert);
            configInserts.put(config, newList);
        }
    }

}
