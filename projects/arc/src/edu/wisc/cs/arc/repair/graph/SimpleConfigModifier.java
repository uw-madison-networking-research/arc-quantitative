package edu.wisc.cs.arc.repair.graph;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.PolicyGroup;
import edu.wisc.cs.arc.graphs.ProcessGraph;
import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.modifiers.ModifierException;
import edu.wisc.cs.arc.repair.RepairException;
import edu.wisc.cs.arc.repair.VirtualDirectedEdge;
import edu.wisc.cs.arc.repair.graph.GraphModification.Action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Modifies a set of configurations using a simple set of templates.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class SimpleConfigModifier extends ConfigModifier {

	private List<ModificationPair> configMods = new ArrayList<>();

	/**
	 * Create a modifier.
	 * @param etgs the current extended topology graphs
	 * @param graphModifications A grouped object of tcETGs, dETGs, and the aETG modifications.
	 * @param settings
	 */
	public SimpleConfigModifier(Map<Flow, ProcessGraph> etgs,
			ConfigModificationGroup graphModifications,
			Settings settings) {
		super(etgs, graphModifications, settings);
	}

	/**
	 * Prunes away unnecessary modifications and edges without needed modifications.
	 */
    private void filterModifications(){
        Map<Flow, List<GraphModification<ProcessVertex>>> tcMods =
                this.graphModifications.getTcMods();
		Map<PolicyGroup, List<GraphModification<ProcessVertex>>> dMods =
				this.graphModifications.getDestMods();
		List<GraphModification<ProcessVertex>> baseMods =
				this.graphModifications.getBaseMod();

		// DEBUG STATS
		int tcModCount = 0;
		int dModCount = 0;
		int aModCount = 0;
		if (null != baseMods) {
			aModCount = baseMods.size();
		}
		for (List<GraphModification<ProcessVertex>> modifications : tcMods.values()) {
			tcModCount += modifications.size();
		}
        for (List<GraphModification<ProcessVertex>> modifications : dMods.values()) {
            dModCount += modifications.size();
		}

		logger.debug(String.format("Before filtering:\n" +
										"\ttcMod size: %d\n" +
										"\tdMod size: %d\n" +
										"\tbaseMod size: %d\n",
										tcModCount, dModCount, aModCount));


		// TODO: See my notes regarding ignoring implicitly permitted/blocked edges.
        // If there exists tcMods with the same edge and type as a dMod, we can discard it.
		for (Flow flow : tcMods.keySet()) {
			List<GraphModification<ProcessVertex>> dModList = dMods.get(flow.getDestination());
			if (null == dModList){
			    continue;
            }

			List<GraphModification<ProcessVertex>> tcModsList = tcMods.get(flow);
			if (null == tcModsList) {
			    continue;
            }
            for (GraphModification<ProcessVertex> dMod : dModList) {
                    if (tcModsList.contains(dMod)){
                        tcModsList.remove(dMod);
						logger.debug("dMod|tcMod override found. Discarding tcMod");
					}
				}

		}

		// Discover destination mods overridden by base mods.
		for (GraphModification<ProcessVertex> baseMod : baseMods) {
			for (List<GraphModification<ProcessVertex>> dModList : dMods.values()) {
				if (dModList.contains(baseMod)){
					logger.debug("aMod|dMod override found. Discarding dMod");
					logger.debug(String.format("aMod: %s == dMod: %s\n", baseMod, dModList.get(dModList.indexOf(baseMod))));
					dModList.remove(baseMod);
				}
			}
        }

		// Remove Flows/Traffic Classes without modifications.
		List<Flow> tcModDiscard = new ArrayList<>();

		for (Flow flow : tcMods.keySet()) {
			if (tcMods.get(flow).size() == 0){
				tcModDiscard.add(flow);
			}
		}
		for (Flow flow : tcModDiscard) {
			tcMods.remove(flow);
		}

		// Remove PolicyGroups without modifications.
		List<PolicyGroup> dModDiscard = new ArrayList<>();

		for (PolicyGroup policyGroup : dMods.keySet()) {
			if (dMods.get(policyGroup).size() == 0) {
				dModDiscard.add(policyGroup);
			}
		}

		for (PolicyGroup discardGroup : dModDiscard) {
			dMods.remove(discardGroup);
		}

		// DEBUG STATS
		tcModCount = dModCount = aModCount = 0;
		if (null != baseMods) {
			aModCount = baseMods.size();
		}
		for (List<GraphModification<ProcessVertex>> modifications : tcMods.values()) {
			tcModCount += modifications.size();
		}
		for (List<GraphModification<ProcessVertex>> modifications : dMods.values()) {
			dModCount += modifications.size();
		}

		logger.debug(String.format("After filtering:\n" +
										"\ttcMod size: %d\n" +
										"\tdMod size: %d\n" +
										"\tbaseMod size: %d\n",
										tcModCount, dModCount, aModCount));
	}

	private void convertModifications() {
		// iterate through each modification level (aMod, dMod, and tcMod) and output needed changes.
		logger.info("--------\nConfigurations required for traffic class modifications\n--------");

		for (Map.Entry<Flow, List<GraphModification<ProcessVertex>>> tcMods : this.graphModifications.getTcMods().entrySet()) {
			PolicyGroup[] policyGroups = new PolicyGroup[]{tcMods.getKey().getSource(), tcMods.getKey().getDestination()};
			for (GraphModification<ProcessVertex> tcMod : tcMods.getValue()) {
				convertTrafficClassGraphModToConfigMod(policyGroups, tcMod);
			}
		}
		logger.info("--------\nConfigurations required for destination modifications\n--------");

		for (Map.Entry<PolicyGroup, List<GraphModification<ProcessVertex>>> dMods : this.graphModifications.getDestMods().entrySet()) {
			for (GraphModification<ProcessVertex> dMod : dMods.getValue()) {
				PolicyGroup[] policyGroups = new PolicyGroup[]{dMods.getKey()};
				convertDestinationGraphModToConfigMod(policyGroups, dMod);
			}
		}
		logger.info("--------\nConfigurations required for base modifications\n--------");
		for (GraphModification<ProcessVertex> baseMod : this.graphModifications.getBaseMod()) {
			convertBaseGraphModToConfigMod(baseMod);
		}
	}

	private void convertBaseGraphModToConfigMod(GraphModification<ProcessVertex> baseMod) {
		String action = String.format("For Edge %s: ", baseMod.getEdge());
		if (EdgeType.INTER_DEVICE == baseMod.getEdge().getType()) {

			if (Action.ADD == baseMod.getAction()) {
				action += "Enable routing";
				this.configMods.add(new ModificationPair(
						ConfigModification.Action.ADD,
						ConfigModification.Stanza.INTERFACE,
						ConfigModification.Substanza.INCOMING_FILTER,
						baseMod));
			} else if (Action.REMOVE == baseMod.getAction()) {
				action += "Disable routing";
				this.configMods.add(new ModificationPair(
						ConfigModification.Action.REMOVE,
						ConfigModification.Stanza.INTERFACE,
						ConfigModification.Substanza.INCOMING_FILTER,
						baseMod));
			}

		} else if (EdgeType.INTRA_DEVICE == baseMod.getEdge().getType()) {
			if (Action.ADD == baseMod.getAction()) {
				action += "Enable redistribution";
				this.configMods.add(new ModificationPair(
						ConfigModification.Action.ADD,
						ConfigModification.Stanza.INTERFACE,
						ConfigModification.Substanza.OSPF_REDISTRIBUTION,  // OR BGP?
						baseMod));
			} else if (Action.REMOVE == baseMod.getAction()) {
				action += "Disable redistribution";
				this.configMods.add(new ModificationPair(
						ConfigModification.Action.REMOVE,
						ConfigModification.Stanza.INTERFACE,
						ConfigModification.Substanza.OSPF_REDISTRIBUTION,  // OR BGP?
						baseMod));
			}

		} else {
			logger.error(String.format("Unexpected edge type: %s ACTION: %s\n",
					baseMod.getEdge().getType(), baseMod.getAction()));
			throw new ModifierException(String.format("Unexpected edge type: %s ACTION: %s\n",
					baseMod.getEdge().getType(), baseMod.getAction()));
		}
		logger.info(String.format(action + "\n", baseMod.getEdge()));
	}

	private void convertTrafficClassGraphModToConfigMod(PolicyGroup[] policyGroups, GraphModification<ProcessVertex> tcMod){
		String action = String.format("For Edge %s -> %s: ", tcMod.getEdge().getSource(),
				tcMod.getEdge().getDestination());
		if (EdgeType.INTER_DEVICE == tcMod.getEdge().getType()) {

			if (Action.ADD == tcMod.getAction()) {
				action += "Remove from ACL";
				this.configMods.add(new ModificationPair(
						ConfigModification.Action.REMOVE,
						ConfigModification.Stanza.STANDARD_ACL,
						ConfigModification.Substanza.LINE,
						ModificationPair.Level.TRAFFIC_CLASS,
						policyGroups,
						tcMod));

			} else if (Action.REMOVE == tcMod.getAction()) {
				action += "Add to ACL";
				this.configMods.add(new ModificationPair(
						ConfigModification.Action.ADD,
						ConfigModification.Stanza.STANDARD_ACL,
						ConfigModification.Substanza.LINE,
						ModificationPair.Level.TRAFFIC_CLASS,
						policyGroups,
						tcMod));
			}

		} else if (EdgeType.INTRA_DEVICE == tcMod.getEdge().getType()) {
			logger.fatal("Unexpected event occurred: Attempt to modify intra-device edge.");
			throw new ModifierException("Unexpected event occurred: " +
					"Attempt to modify intra-device edge.");
		} else if (EdgeType.ENDPOINT == tcMod.getEdge().getType()){

			if (Action.REMOVE == tcMod.getAction()) {
				action += "Add to ACL";
				this.configMods.add(new ModificationPair(
						ConfigModification.Action.ADD,
						ConfigModification.Stanza.STANDARD_ACL,
						ConfigModification.Substanza.LINE,
						ModificationPair.Level.TRAFFIC_CLASS,
						policyGroups,
						tcMod));

			} else if (Action.ADD == tcMod.getAction()){
				action += "Remove from ACL";
				this.configMods.add(new ModificationPair(
						ConfigModification.Action.REMOVE,
						ConfigModification.Stanza.STANDARD_ACL,
						ConfigModification.Substanza.LINE,
						ModificationPair.Level.TRAFFIC_CLASS,
						policyGroups,
						tcMod));
			}
		} else {
			// TODO: Handle any edge cases.
			logger.error(String.format("Unexpected edge type: %s ACTION: %s\n",
					tcMod.getEdge().getType(), tcMod.getAction()));
			throw new ModifierException(String.format("Unexpected edge type: %s ACTION: %s\n",
					tcMod.getEdge().getType(), tcMod.getAction()));
		}
		logger.info(String.format(action + "\n", tcMod.getEdge()));
	}

	private void convertDestinationGraphModToConfigMod(PolicyGroup[] policyGroups, GraphModification<ProcessVertex> dMod){
		String action = String.format("For Edge %s: ", dMod.getEdge());
		if (EdgeType.INTER_DEVICE == dMod.getEdge().getType()) {

			if (Action.ADD == dMod.getAction()) {
				if (this.graphModifications.getBaseEtg().containsEdgeExcludeBlocked(dMod.getEdge())){
					action += "Disable route filter";
					this.configMods.add(new ModificationPair(
							ConfigModification.Action.REMOVE,
							ConfigModification.Stanza.ROUTER,
							ConfigModification.Substanza.INCOMING_FILTER,
							ModificationPair.Level.DESTINATION,
							policyGroups,
							dMod));
				} else {
					action += "Add static route";
					this.configMods.add(new ModificationPair(
							ConfigModification.Action.ADD,
							ConfigModification.Stanza.ROUTER,
							ConfigModification.Substanza.STATIC_ROUTE,
							ModificationPair.Level.DESTINATION,
							policyGroups,
							dMod));
				}
			} else if (dMod.getAction() == Action.REMOVE) {
				if (this.graphModifications.getBaseEtg().containsEdgeExcludeBlocked(dMod.getEdge())){
					action += "Remove static route";
					this.configMods.add(new ModificationPair(
							ConfigModification.Action.REMOVE,
							ConfigModification.Stanza.ROUTER,
							ConfigModification.Substanza.STATIC_ROUTE,
							ModificationPair.Level.DESTINATION,
							policyGroups,
							dMod));
				} else {
					action += "Enable route filter";
					this.configMods.add(new ModificationPair(
							ConfigModification.Action.ADD,
							ConfigModification.Stanza.ROUTER,
							ConfigModification.Substanza.INCOMING_FILTER,
							ModificationPair.Level.DESTINATION,
							policyGroups,
							dMod));
				}
			}

		} else if (dMod.getEdge().getType() == EdgeType.INTRA_DEVICE) {

			if (Action.ADD == dMod.getAction()) {
				action += "Remove route filter";
				this.configMods.add(new ModificationPair(
						ConfigModification.Action.REMOVE,
						ConfigModification.Stanza.ROUTER,
						ConfigModification.Substanza.INCOMING_FILTER,
						ModificationPair.Level.DESTINATION,
						policyGroups,
						dMod));

			} else if (Action.REMOVE == dMod.getAction()) {
				action += "Add route filter";
				this.configMods.add(new ModificationPair(
						ConfigModification.Action.ADD,
						ConfigModification.Stanza.ROUTER,
						ConfigModification.Substanza.INCOMING_FILTER,
						ModificationPair.Level.DESTINATION,
						policyGroups,
						dMod));
			}
		} else {
			logger.error(String.format("Unexpected edge type: %s ACTION: %s\n",
					dMod.getEdge().getType(), dMod.getAction()));
			throw new ModifierException(String.format("Unexpected edge type: %s ACTION: %s\n",
					dMod.getEdge().getType(), dMod.getAction()));

		}
		logger.info(String.format(action + "\n", dMod.getEdge()));
	}

	@Override
	public List<ModificationPair> getModifications() {
		filterModifications();
		convertModifications();
		return this.configMods;
	}

	/**
	 * Group graph modifications by the edge they impact.
	 * @return graph modifications grouped by edge
	 */
	private Map<VirtualDirectedEdge<ProcessVertex>, Map<Flow,Action>>
			groupByEdge() {
		Map<VirtualDirectedEdge<ProcessVertex>,Map<Flow,Action>> groupedByEdge =
			new HashMap<VirtualDirectedEdge<ProcessVertex>,Map<Flow,Action>>();

		// Iterate over all modifications
		for (Flow flow : this.graphModifications.getTcMods().keySet()) {
			List<GraphModification<ProcessVertex>> mods =
					this.graphModifications.getTcMods().get(flow);
			for (GraphModification<ProcessVertex> mod : mods) {
				if (!groupedByEdge.containsKey(mod.getEdge())) {
					groupedByEdge.put(mod.getEdge(),
							new HashMap<Flow,Action>());
				}
				groupedByEdge.get(mod.getEdge()).put(flow, mod.getAction());
			}
		}

		return groupedByEdge;
	}

	/**
	 * Translate an edge modification that impacts a single flow into a
	 * configuration modification.
	 * @param edge the edge that was modified
	 * @param flow the flow impacted by the edge modification
	 * @param action how was the edge modified?
	 */
	private void modifyForSingleFlow(VirtualDirectedEdge<ProcessVertex> edge,
			Flow flow, Action action) {
		// Translation depends on edge type and type of modification
		if (EdgeType.INTER_DEVICE == edge.getType()
				&& Action.ADD == action) {
			// TODO
		} else if (EdgeType.INTER_DEVICE == edge.getType()
				&& Action.REMOVE == action) {
			// TODO
		} else if (EdgeType.INTRA_DEVICE == edge.getType()
				&& Action.ADD == action) {
			// TODO
		} else if (EdgeType.INTRA_DEVICE == edge.getType()
				&& Action.REMOVE == action) {
			// TODO
		} else {
			// Should not have any other types of modifications
			throw new RepairException("Should not " + action + " "
					+ edge.getType() + " for a single flow");
		}
	}

	/**
	 * Translate an edge modification that impacts multiple flows into a
	 * configuration modification.
	 * @param edge the edge that was modified
	 * @param actionsByFlow the flows impacted by the edge modification and the
	 * 			type of modification made for each flow
	 */
	private void modifyForMultipleFlows(VirtualDirectedEdge<ProcessVertex> edge,
			Map<Flow,Action> actionsByFlow) {
		// Determine if the action is the same for all flows
		Action commonAction = null;
		for (Action action : actionsByFlow.values()) {
			if (null == commonAction) {
				commonAction = action;
				continue;
			}
			if (action != commonAction) {
				commonAction = null;
				break;
			}
		}

		// Handle multiple different actions
		if (null == commonAction) {
			// TODO
			return;
		}

		// Translation depends on edge type and type of modification
		if (EdgeType.INTER_DEVICE == edge.getType()
				&& Action.ADD == commonAction) {
			// TODO
		} else if (EdgeType.INTER_DEVICE == edge.getType()
				&& Action.REMOVE == commonAction) {
			// TODO
		} else if (EdgeType.INTRA_DEVICE == edge.getType()
				&& Action.ADD == commonAction) {
			// TODO
		} else if (EdgeType.INTRA_DEVICE == edge.getType()
				&& Action.REMOVE == commonAction) {
			// TODO
		} else {
			// Should not have any other types of modifications
			throw new RepairException("Should not " + commonAction + " "
					+ edge.getType() + " for multiple flows");
		}
	}
}
