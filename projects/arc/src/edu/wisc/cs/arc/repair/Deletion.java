package edu.wisc.cs.arc.repair;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.configs.Config;
import org.antlr.v4.runtime.Token;

/**
 * Created by smaug on 6/27/17.
 */
public class Deletion extends MutationOperator {

    public Deletion(Settings settings){

        this.settings = settings;
        this.logger = settings.getLogger();
    }

    public GeneticProgrammingResult mutate(Config original){

        Mutation mutation = new Mutation(Mutation.MUTATION_TYPE.DELETION);
        int linePos = getMutationPosition(original);
        mutation.setMutationLine(linePos);
        mutation.setMutationText("N/A");
        Config mutant = deleteMutation(original, linePos);

        return new GeneticProgrammingResult(mutant, mutation);
    }

    private int getMutationPosition(Config original){
        int endLine = original.getTokens().get(original.getTokens().size()-1).getLine();
        return random.nextInt(endLine);
    }

    private Config deleteMutation(Config original, int linePos){
        System.out.println("Choosing line: " + linePos);
        StringBuilder rawMutantText = new StringBuilder();

        for (Token tok : original.getTokens()) {

            if (tok.getLine() == linePos ){
                rawMutantText.append(EMPTY_STRING);
            }
            else {
                //Append tokens in normal order
                rawMutantText.append(tok.getText());
            }
        }

        return buildConfig(rawMutantText, original);
    }

}
