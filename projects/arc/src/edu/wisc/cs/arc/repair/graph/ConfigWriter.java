package edu.wisc.cs.arc.repair.graph;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.configs.Config;
import edu.wisc.cs.arc.repair.graph.configwriters.ModificationNotImplementedException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Abstract class that prototypes the necessary methods to implement ConfigModifications to
 * configuration files.
 *
 * @author Franklin van Nes (fvannes@colgate.edu)
 */
public abstract class ConfigWriter {

    /**
     * Configuration Insertions
     */
    protected Map<String, Config> originalConfigs;
    protected Map<String, Config> configs;  // configs

    private List<ModificationPair> successfulMods = new ArrayList<>();
    private List<Exception> modFailures = new ArrayList<Exception>();

    protected Logger logger = Logger.getInstance();

    public ConfigWriter(Map<String, Config> configs){
        this.originalConfigs = configs;
        this.configs = copyConfigs(originalConfigs);

    }

    /**
     * Writes modifications to the configuration files.
     *
     * @param modifications returned from ConfigModifier
     **/
    public void write(List<ModificationPair> modifications, String exportDir) {

        for (ModificationPair modPair : modifications) {
            try {
                ConfigModification modification = modPair.getConfigModification();
                switch (modification.getStanza()) {
                    case ROUTER:
                        if (ConfigModification.Action.ADD == modification.getAction()) {
                            if (ConfigModification.Substanza.STATIC_ROUTE == modification.getSubstanza()) {
                                enableStaticRoute(modPair);
                            } else if (ConfigModification.Substanza.INCOMING_FILTER == modification.getSubstanza()) {
                                enableRouteFilter(modPair);
                            }
                        } else if (ConfigModification.Action.REMOVE == modification.getAction()) {
                            if (ConfigModification.Substanza.STATIC_ROUTE == modification.getSubstanza()) {
                                disableStaticRoute(modPair);
                            } else if (ConfigModification.Substanza.INCOMING_FILTER == modification.getSubstanza()) {
                                disableRouteFilter(modPair);
                            }
                        }
                        break;
                    case INTERFACE:
                        if (ConfigModification.Action.ADD == modification.getAction()) {
                            if (ConfigModification.Substanza.OSPF_REDISTRIBUTION == modification.getSubstanza()) {
                                enableRouteRedistribution(modPair);
                            } else if (ConfigModification.Substanza.INCOMING_FILTER == modification.getSubstanza()) {
                                enableRouting(modPair);
                            }
                        } else if (ConfigModification.Action.REMOVE == modification.getAction()) {
                            if (ConfigModification.Substanza.OSPF_REDISTRIBUTION == modification.getSubstanza()) {
                                disableRouteRedistribution(modPair);
                            } else if (ConfigModification.Substanza.INCOMING_FILTER == modification.getSubstanza()) {
                                disableRouting(modPair);
                            }
                        }
                        break;
                    case STANDARD_ACL:
                        if (ConfigModification.Action.ADD == modification.getAction()) {
                            enableACL(modPair);
                        } else if (ConfigModification.Action.REMOVE == modification.getAction()) {
                            disableACL(modPair);
                        }
                        break;
                }
                successfulMods.add(modPair);
            } catch (Exception e) {
                modFailures.add(e);
            }
        }

        // Report successful and failed modifications.
        int numMods = successfulMods.size() + modFailures.size();
        logger.info("\n--- Successful modifications ---");
        logger.info(successfulMods.size() + "/" + numMods + " mods applied.");
        for (ModificationPair mod : successfulMods) {
            logger.info(mod.toString());
        }
        logger.info("\n\n--- Failed Modificaitons ---");
        logger.info(modFailures.size() + "/" + numMods + " mods failed.");
        for (Exception modFailure : modFailures) {
            modFailure.printStackTrace();
            logger.info("\n");
        }
        logger.info("\n");

        // Actually Write configuration insertion snippets to the configuration files.
        for (String hostname : originalConfigs.keySet()) {
            Config originalConfig = originalConfigs.get(hostname);
            Config newConfig = configs.get(hostname);
            if (exportDir.isEmpty()) {
                // Print the original and new configuration file.
                logger.debug("------\nOriginal\n------");
                logger.debug(originalConfig.getFile().getName());
                logger.debug(originalConfig.getText());
                logger.debug("------\nNew\n------");
                logger.debug(newConfig.getFile().getName());
                logger.debug(newConfig.getText());
                logger.info("Repaired configurations were not saved. To save new configurations, use " +
                        "the flag: -a \"exportdir <path/to/export/directory>\"");
            } else {
                // Write output to new files.
                Writer bw = null;
                try {
                    String filename = exportDir + newConfig.getFile().getName();
                    logger.info("Writing "+ filename);
                    File file = new File(filename);
                    if (!file.createNewFile()){
                        System.out.println("Overwriting existing file.");
                    }
                    FileWriter fw = new FileWriter(filename);
                    bw = new BufferedWriter(fw);
                    bw.write(newConfig.getText());
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (bw != null){
                            bw.close();
                        }
                    } catch (IOException e) {
                        logger.error("Could not close BufferedReader.");
                    }
                }


            }
        }
    }

    private Map<String, Config> copyConfigs(Map<String, Config> ogConfigs) {
        Map<String, Config> configs = new HashMap<>(ogConfigs.size());
        for (String key : ogConfigs.keySet()) {
            try {
                configs.put(key, new Config(
                        ogConfigs.get(key).getFile(),
                        Logger.getInstance()
                ));
            } catch (IOException e) {
                configs.put(key, new Config(
                        ogConfigs.get(key).getTokens(),
                        ogConfigs.get(key).getHostname(),
                        Logger.getInstance()
                ));
                logger.error("Couldn't generate config from file");
            }
        }
        return configs;
    }

    /**
     * Edits configuration files to write a route filter.
     *
     * @param modPair desired modification
     **/
    public abstract void enableRouteFilter(ModificationPair modPair);

    /**
     * Edits configuration files to find and remove a route filter.
     *
     * @param modPair desired modification
     **/
    public abstract void disableRouteFilter(ModificationPair modPair);

    /**
     * Edits configuration file ACLs to block mod traffic class
     *
     * @param modPair desired modification
     **/
    public abstract void enableACL(ModificationPair modPair) throws ModificationNotImplementedException;

    /**
     * Edits configuration file ACLs to allow mod traffic class
     *
     * @param modPair desired modification
     **/
    public abstract void disableACL(ModificationPair modPair) throws ModificationNotImplementedException;

    /**
     * Edits configuration files to generate a static route.
     *
     * @param modPair desired modification
     */
    public abstract void enableStaticRoute(ModificationPair modPair) throws ModificationNotImplementedException;

    /**
     * Edits configuration files to remove a static route.
     *
     * @param modPair desired modification
     */
    public abstract void disableStaticRoute(ModificationPair modPair) throws ModificationNotImplementedException;

    /**
     * Edits configuration files to enable route redistribution.
     *
     * @param modPair desired modification
     */
    public abstract void enableRouteRedistribution(ModificationPair modPair) throws ModificationNotImplementedException;

    /**
     * Edits configuration files to disable route redistribution.
     *
     * @param modPair desired modification
     */
    public abstract void disableRouteRedistribution(ModificationPair modPair) throws ModificationNotImplementedException;

    /**
     * Edits configuration files to enable routing.
     *
     * @param modPair desired modification
     */
    public abstract void enableRouting(ModificationPair modPair) throws ModificationNotImplementedException;

    /**
     * Edits configuration files to disable routing.
     *
     * @param modPair desired modification
     */
    public abstract void disableRouting(ModificationPair modPair) throws ModificationNotImplementedException;
}