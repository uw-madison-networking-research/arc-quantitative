package edu.wisc.cs.arc.repair;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.configs.Config;
import org.antlr.v4.runtime.Token;

/**
 * Created by amahmood on 6/29/17.
 */
public class Replacement extends MutationOperator {

    public Replacement(Settings settings){

        this.settings = settings;
        this.logger = settings.getLogger();
    }

    public GeneticProgrammingResult mutate(Config original){

        Mutation mutation = getMutation();
        mutation.setMutationType(Mutation.MUTATION_TYPE.REPLACEMENT);
        Config mutant = replaceMutation(original, mutation.getMutationText());

        return new GeneticProgrammingResult(mutant, mutation);
    }

    private Config replaceMutation(Config original,
                                   String mutation) {
        StringBuilder rawMutantText = new StringBuilder();
        boolean inserted = false;
        for (Token tok : original.getTokens()) {

            //Replace statement with mutation
            if (tok.getLine() == mutationPos)  {
                if (!inserted){
                    rawMutantText.append(mutation);
                    inserted = true;
                }
                continue;
            }

            //Append tokens in normal order
            rawMutantText.append(tok.getText());
        }
        return buildConfig(rawMutantText, original);
    }

}
