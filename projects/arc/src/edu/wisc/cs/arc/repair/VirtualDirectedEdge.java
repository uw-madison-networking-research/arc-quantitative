package edu.wisc.cs.arc.repair;

import java.io.Serializable;

import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.Vertex;

/**
 * Represents an edge that could exist in an extended topology graph.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class VirtualDirectedEdge<V extends Vertex> implements Serializable {
    private static final long serialVersionUID = 4865960374291739940L;

	public static final double DEFAULT_WEIGHT = 0;
	
	/** Source vertex */
	private V source;
	
	/** Destination vertex */
	private V destination;
	
	/** Edge type */
	private EdgeType type;
	
	/** Edge weight */
	private double weight;
	
	/** Whether edge has a waypoint */
	private boolean waypoint;

    /**
	 * Create a possible edge.
	 * @param source source vertex
	 * @param destination destination vertex
	 */
	public VirtualDirectedEdge(V source, V destination, EdgeType type,
            double weight) {
		this.source = source;
		this.destination = destination;
		this.type = type;
		this.weight = weight;
	}
	
	/**
	 * Create a possible edge.
	 * @param source source vertex
	 * @param destination destination vertex
	 */
	public VirtualDirectedEdge(V source, V destination, EdgeType type) {
        this(source, destination, type, DEFAULT_WEIGHT);
	}
	
	/**
	 * Create a possible edge.
	 * @param edge existing edge
	 */
	public VirtualDirectedEdge(DirectedEdge<V> edge) {
		this(edge.getSource(), edge.getDestination(), edge.getType());
		this.weight = edge.getWeight();
	}
	
	/**
	 * Get the source of the edge.
	 * @return source vertex
	 */
	public V getSource() {
		return this.source;
	}
	
	/**
	 * Get the destination of the edge.
	 * @return destination vertex
	 */
	public V getDestination() {
		return this.destination;
	}
	
	/**
	 * Get the type of the edge.
	 * @return edge type
	 */
	public EdgeType getType() {
		return this.type;
	}
	
	/**
	 * Get the weight of the edge.
	 * @return edge weight
	 */
	public double getWeight() {
		return this.weight;
	}
	
	/**
     * Set the weight of the edge.
     * @param weight new edge weight
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    /**
     * Check if a waypoint is traversed when traversing this edge.
     * @return true if a waypoint is traversed, otherwise false
     */
    public boolean hasWaypoint() {
        return this.waypoint;
    }
    
    /**
     * Indicate that a waypoint is traversed when traversing this edge.
     */
    public void markWaypoint() {
        this.waypoint = true;
    }
	
	@Override
	public String toString() {
		return this.getSource().getName() + "->"
				+ this.getDestination().getName();
	}
	
	/**
	 * Determine if two edges have the same source and destination vertices.
	 * @param other edge to compare against
	 */
	public boolean equals(Object other) {
		if (other instanceof VirtualDirectedEdge) {
			VirtualDirectedEdge<V> otherEdge  = (VirtualDirectedEdge<V>)other;
			return (otherEdge.getSource().equals(this.getSource())
					&& (otherEdge.getDestination().equals(
							this.getDestination())));
		}
		if (other instanceof DirectedEdge) {
			DirectedEdge<V> otherEdge  = (DirectedEdge<V>)other;
			return (otherEdge.getSource().equals(this.getSource())
					&& (otherEdge.getDestination().equals(
							this.getDestination())));
		}
		return false;
	}
	
	@Override
    public int hashCode() {
        return this.toString().hashCode();
    }
}
