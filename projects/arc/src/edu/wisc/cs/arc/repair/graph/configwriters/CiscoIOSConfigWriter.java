package edu.wisc.cs.arc.repair.graph.configwriters;

import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.configs.Config;
import edu.wisc.cs.arc.graphs.PolicyGroup;
import edu.wisc.cs.arc.repair.Stanza;
import edu.wisc.cs.arc.repair.StanzaBuilder;
import edu.wisc.cs.arc.repair.graph.ConfigModification;
import edu.wisc.cs.arc.repair.graph.ConfigWriter;
import edu.wisc.cs.arc.repair.graph.ModificationPair;
import org.antlr.v4.runtime.Token;
import org.batfish.datamodel.Ip;
import org.batfish.datamodel.Prefix;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Writes configuration modifications to Cisco IOS router sourceConfig files with Cisco IOS syntax.
 *
 * @author Franklin van Nes (fvannes@colgate.edu)
 */
public class CiscoIOSConfigWriter extends ConfigWriter {

    public CiscoIOSConfigWriter(Map<String, Config> originalConfigs){
        super(originalConfigs);
    }

    @Override
    public void enableACL(ModificationPair modPair) throws ModificationNotImplementedException {
        logger.debug("enableACL called.");
        String sourceName = modPair.getGraphModification().getEdge().getSource().getName();
        String destName =modPair.getGraphModification().getEdge().getDestination().getName();

        String sourceHostname = sourceName.split("\\.")[0];
        String destHostname = destName.split("\\.")[0];

        Config destConfig;
        Config sourceConfig;

        if (destName.equals("DESTINATION")
                && !sourceName.equals("SOURCE")) {
            AclEditor.Direction direction = AclEditor.Direction.OUTGOING;
            sourceConfig = configs.get(sourceHostname);
            AclEditor editor = new AclEditor(sourceConfig);
            editor.enableAcl(modPair, direction);  // generates the snippets / configInserts
            if (editor.configInserts.size() == 0){
                throw new ModificationNotImplementedException(modPair,
                        "No inserts generated. Cause unknown.");
            }
            editor.writeToConfig(sourceConfig);
        } else if (sourceName.equals("SOURCE")
                && !destName.equals("DESTINATION")) {
            sourceConfig = getConfigWithInterfaces(modPair.getSource(), modPair.getSource());
            destConfig = configs.get(destHostname);
            AclEditor editor = new AclEditor(sourceConfig, destConfig);
            editor.enableAcl(modPair, AclEditor.Direction.INCOMING);
            if (editor.configInserts.size() == 0){
                throw new ModificationNotImplementedException(modPair,
                        "No inserts generated. Cause unknown.");
            }
            editor.writeToConfig(destConfig);
        } else if (sourceName.equals("SOURCE") && destName.equals("DESTINATION")) {
            sourceConfig = getConfigWithInterfaces(modPair.getSource(), modPair.getDestination());
            AclEditor editor = new AclEditor(sourceConfig);
            editor.enableAcl(modPair, AclEditor.Direction.INCOMING);
            if (editor.configInserts.size() == 0){
                throw new ModificationNotImplementedException(modPair,
                        "No inserts generated. Cause unknown.");
            }
            editor.writeToConfig(sourceConfig);
        } else {
            sourceConfig = configs.get(sourceHostname);
            destConfig = configs.get(destHostname);
            AclEditor editor = new AclEditor(sourceConfig, destConfig);
            editor.enableAcl(modPair, AclEditor.Direction.INCOMING);
            if (editor.configInserts.size() == 0){
                throw new ModificationNotImplementedException(modPair,
                        "No inserts generated. Cause unknown.");
            }
            editor.writeToConfig(destConfig);
        }



    }

    @Override
    public void disableACL(ModificationPair modPair) throws ModificationNotImplementedException {
        logger.debug("disableACL called.");
        Config sourceConfig;
        Config destConfig;
        if (modPair.getGraphModification().getEdge().getDestination().toString().equals("DESTINATION")){
            destConfig = getConfigWithInterfaces(modPair.getDestination(), modPair.getDestination());
        } else {
            destConfig = this.configs.get(modPair.getGraphModification().getEdge().
                    getDestination().getName().split("\\.")[0]);
        }

        if (modPair.getGraphModification().getEdge().getSource().toString().equals("SOURCE")) {
            sourceConfig = getConfigWithInterfaces(modPair.getSource(), modPair.getSource());
        } else {
            sourceConfig = this.configs.get(modPair.getGraphModification().getEdge().
                    getSource().getName().split("\\.")[0]);
        }
        AclEditor editor = new AclEditor(sourceConfig, destConfig);
        editor.removeAcl(modPair);
        if (editor.configInserts.size() == 0){
            throw new ModificationNotImplementedException(modPair,
                    "No inserts generated. Cause unknown.");
        }
        editor.writeToConfig(destConfig);
    }

    @Override
    public void enableStaticRoute(ModificationPair modPair) throws ModificationNotImplementedException {
        logger.debug("enableStaticRoute called.");
        Config sourceConfig = this.configs.get(modPair.getGraphModification().getEdge().
                getSource().getName().split("\\.")[0]);
        Config destConfig = this.configs.get(modPair.getGraphModification().getEdge().
                getDestination().getName().split("\\.")[0]);
        StaticRouteEditor editor = new StaticRouteEditor(sourceConfig, destConfig);
        editor.applyEnableMod(modPair);
        if (editor.configInserts.size() == 0){
            throw new ModificationNotImplementedException(modPair,
                    "No inserts generated. Cause unknown.");
        }
        editor.writeToConfig(sourceConfig);

    }

    @Override
    public void disableStaticRoute(ModificationPair modPair) throws ModificationNotImplementedException {
        logger.debug("disableStaticRoute called.");
        Config sourceConfig = this.configs.get(modPair.getGraphModification().getEdge().
                getSource().getName().split("\\.")[0]);
        Config destConfig = this.configs.get(modPair.getGraphModification().getEdge().
                getDestination().getName().split("\\.")[0]);
        StaticRouteEditor editor = new StaticRouteEditor(sourceConfig, destConfig);
        editor.applyDisableMod(modPair);
        if (editor.configInserts.size() == 0){
            throw new ModificationNotImplementedException(modPair,
                    "No inserts generated. Cause unknown.");
        }
        editor.writeToConfig(sourceConfig);

    }

    @Override
    public void enableRouteFilter(ModificationPair modPair) {
        logger.debug("enableRouteFilter called.");

    }

    @Override
    public void disableRouteFilter(ModificationPair modPair) {
        logger.debug("disableRouteFilter called.");

    }

    @Override
    public void enableRouteRedistribution(ModificationPair modPair) throws ModificationNotImplementedException {
        logger.debug("enableRouteRedistribution called.");
        Config sourceConfig = this.configs.get(modPair.getGraphModification().getEdge().
                getSource().getName().split("\\.")[0]);
        Config destConfig = this.configs.get(modPair.getGraphModification().getEdge().
                getDestination().getName().split("\\.")[0]);
        RouteRedistributionEditor editor = new RouteRedistributionEditor(sourceConfig, destConfig);
        editor.applyEnableMod(modPair);
        if (editor.configInserts.size() == 0){
            throw new ModificationNotImplementedException(modPair,
                    "No inserts generated. Cause unknown.");
        }
        editor.writeToConfig(sourceConfig);

    }

    @Override
    public void disableRouteRedistribution(ModificationPair modPair) throws ModificationNotImplementedException {
        logger.debug("disableRouteRedistribution called.");
        Config sourceConfig = this.configs.get(modPair.getGraphModification().getEdge().
                getSource().getName().split("\\.")[0]);
        Config destConfig = this.configs.get(modPair.getGraphModification().getEdge().
                getDestination().getName().split("\\.")[0]);
        RouteRedistributionEditor editor = new RouteRedistributionEditor(sourceConfig, destConfig);
        editor.applyDisableMod(modPair);
        if (editor.configInserts.size() == 0){
            throw new ModificationNotImplementedException(modPair,
                    "No inserts generated. Cause unknown.");
        }
        editor.writeToConfig(sourceConfig);
    }

    @Override
    public void enableRouting(ModificationPair modPair) throws ModificationNotImplementedException {
        logger.debug("enableRouting called.");
        String destHostname = modPair.getGraphModification().getEdge().getDestination()
                .getName().split("\\.")[0];
        String sourceHostname = modPair.getGraphModification().getEdge().getSource()
                .getName().split("\\.")[0];

        Config sourceConfig = configs.get(sourceHostname);
        Config destConfig = configs.get(destHostname);

        RoutingEditor editor = new RoutingEditor(sourceConfig, destConfig);
        editor.applyMod(modPair);
        if (editor.configInserts.size() == 0){
            throw new ModificationNotImplementedException(modPair,
                    "No inserts generated. Cause unknown.");
        }
        editor.writeToConfig(sourceConfig);
    }

    @Override
    public void disableRouting(ModificationPair modPair) throws ModificationNotImplementedException {
        logger.debug("disableRouting called.");
        String destHostname = modPair.getGraphModification().getEdge().getDestination()
                .getName().split("\\.")[0];
        String sourceHostname = modPair.getGraphModification().getEdge().getSource()
                .getName().split("\\.")[0];

        Config sourceConfig = configs.get(sourceHostname);
        Config destConfig = configs.get(destHostname);

        RoutingEditor editor = new RoutingEditor(sourceConfig, destConfig);
        editor.applyMod(modPair);
        if (editor.configInserts.size() == 0){
            throw new ModificationNotImplementedException(modPair,
                    "No inserts generated. Cause unknown.");
        }
        editor.writeToConfig(sourceConfig);

    }

    /**
     * Finds and returns the router configuration that connects to both the source and destination
     * IPs specified.
     * @param source Policy Group
     * @param destination Policy Group
     * @return Config that connects to the given source and destination.
     */
    private Config getConfigWithInterfaces(PolicyGroup source, PolicyGroup destination) {
        for (Config config : this.configs.values()) {
            Map<String, List<Stanza>> stanzaMap = new StanzaBuilder().parseStanzas(config, config.getHostname());
            List<Stanza> interfaces = stanzaMap.get(StanzaBuilder.INTERFACE);
            if (interfaces == null || interfaces.size() == 0) {
                continue;
            }
            boolean sourceFound, destFound;
            sourceFound = destFound = false;
            Pattern p = Pattern.compile("ip\\s+address\\s+(?<address>\\d+\\.\\d+\\.\\d+\\.\\d+)\\s+(?<mask>\\d+\\.\\d+\\.\\d+\\.\\d+)");
            for (Stanza stanza : interfaces) {
                Matcher m = p.matcher(stanza.getText());
                if (m.find()) {
                    if (source.contains(new Ip(m.group("address")))) {
                        sourceFound = true;
                    }
                    if (destination.contains(new Ip(m.group("address")))) {
                        destFound = true;
                    }
                }
            }
            if (sourceFound && destFound){
                return config;
            }
        }
        logger.error(String.format("Couldn't find a config that holds interfaces for %s and %s.", source.getStartIp().toString(), destination.getStartIp()));
        return null;
    }

    /**
     * Calculates the subnet mask given a start and end IPv4 address
     *
     * @param network A policy group
     * @return mask the full ip mask.
     */
    static String determineIpMask(PolicyGroup network) {
        return determineIpMask(network.getStartIp(), network.getEndIp());
    }


    /**
     * Calculates the subnet mask given a start and end IPv4 address
     *
     * @param startIp
     * @param endIp
     * @return mask the full ip mask.
     */
    static String determineIpMask(Ip startIp, Ip endIp) {
        return determineIpMask(startIp.toString(), endIp.toString());
    }

    /**
     * Calculates the subnet mask given a start and end IPv4 address
     *
     * @param startIp the lower ip / the start of the subnet
     * @param endIp   the higher ip / the end of the subnet
     * @return mask the full ip mask.
     */
    static String determineIpMask(String startIp, String endIp) {
        String[] mask = new String[4];
        String[] startIpArray = startIp.split("\\.");
        String[] endIpArray = endIp.split("\\.");

        for (int i = 0; i < endIpArray.length; i++) {
            // Convert String to hex
            int startHex = Integer.parseInt(startIpArray[i], 16);
            int endHex = Integer.parseInt(endIpArray[i], 16);

            mask[i] = Integer.toString((endHex - startHex), 16);
        }
        return String.join(".", mask);
    }


    /**
     * Searches two configuration files to find the interfaces that join the two routers together.
     *
     * @param sourceConfig the Config object for the modification edge source.
     * @param destConfig   the Config object for the modification edge destination.
     * @return A map of interface IPs and IP wildcards.
     */
    public static List<Prefix> findMatchingEdges(Config sourceConfig, Config destConfig) {
        List<Stanza> sourceInterfaces = new StanzaBuilder().parseStanzas(
                sourceConfig, sourceConfig.getHostname()).get(StanzaBuilder.INTERFACE);
        List<Stanza> destInterfaces = new StanzaBuilder().parseStanzas(
                destConfig, destConfig.getHostname()).get(StanzaBuilder.INTERFACE);
        HashMap<Prefix, Ip> sourceIps = new HashMap<>();
        List<Prefix> matches = new ArrayList<>();

        Pattern pattern = Pattern.compile(
                /** This regex identifies the interface IP address and mask or wildcard.
                 * This would recognize the following stanza.
                 *  e.g.    ip address 11.0.1.3 255.0.0.0
                 *  and would return: interfaceIp = 11.0.1.3 interfaceMask = 255.0.0.0*/

                "ip\\s+address\\s+(?<interfaceIp>\\d+\\.\\d+\\.\\d+\\.\\d+)" +
                        "\\s+(?<interfaceMask>\\d+\\.\\d+\\.\\d+\\.\\d+)"
        );
        // Get all of the interface IPs from the source sourceConfig.
        for (Stanza stanza : sourceInterfaces) {
            // get the IP address for the interface.
            // Parse the stanza for the ip address and mask, and then calculate the start ip.
            Matcher matcher = pattern.matcher(stanza.getText());
            if (matcher.find()) {
                String interfaceIp = matcher.group("interfaceIp");
                String interfaceMask = matcher.group("interfaceMask");
                Prefix prefix = new Prefix(new Ip(interfaceIp), new Ip(interfaceMask));
                sourceIps.put(prefix.getNetworkPrefix(), prefix.getPrefixWildcard());
            }
        }

        // Find interface IPs in the destination sourceConfig that match those in the source sourceConfig.
        // These will be our shared edges/network.
        for (Stanza stanza : destInterfaces) {
            Matcher matcher = pattern.matcher(stanza.getText());
            if (matcher.find()) {
                String interfaceIp = matcher.group("interfaceIp");
                String interfaceMask = matcher.group("interfaceMask");
                Prefix prefix = new Prefix(new Ip(interfaceIp), new Ip(interfaceMask));
                if (sourceIps.containsKey(prefix.getNetworkPrefix())) {
                    matches.add(prefix);
                }
            }
        }
        return matches;
    }

    /**
     * Get the router protocol details for the given sourceConfig.
     * @param config
     * @return Map of routing protocol types to router protocol names. e.g. { "ospf": "101" }
     */
    static Map<String, String> findRouterProtocols(Config config) {
        Map<String, List<Stanza>> stanzaMap = new StanzaBuilder().parseStanzas(config, config.getHostname());
        Map<String, String> protocols = new HashMap<>();
        List<Stanza> routerStanzas = stanzaMap.get(StanzaBuilder.ROUTER);
        if (routerStanzas.size() == 0) {
            Logger.getInstance().debug("No routing protocols in place in " + config.getFile().getName());
            return protocols;
        }
        Pattern p = Pattern.compile("^router\\s+(?<protocol>\\w+)\\s+(?<name>\\d+)");
        for (Stanza routerStanza : routerStanzas) {
            Matcher m = p.matcher(routerStanza.getText());
            if (m.find()) {
                protocols.put(m.group("protocol"), m.group("name"));
            }
        }


        return protocols;

    }

}

class AclEditor extends CiscoIosEditor {

    private Config sourceConfig;
    private Config destConfig;
    private Config writingConfig;
    private ModificationPair modPair;

    enum Direction {
        INCOMING,
        OUTGOING
    }

    public AclEditor(Config enableConfig) {
        this.sourceConfig = enableConfig;
        this.destConfig = null;
        this.writingConfig = enableConfig;
    }

    public AclEditor(Config sourceConfig, Config destConfig) {
        this.sourceConfig = sourceConfig;
        this.destConfig = destConfig;
        this.stanzaMap = new StanzaBuilder().parseStanzas(destConfig, destConfig.getHostname());
        this.writingConfig = destConfig;

    }

    Map<Config, List<ConfigInsert>> enableAcl(ModificationPair modPair, Direction direction)
            throws ModificationNotImplementedException, NullPointerException {
        this.modPair = modPair;
        this.stanzaMap = new StanzaBuilder().parseStanzas(sourceConfig, sourceConfig.getHostname());
        boolean newAclCreated = false;


        // Find the interface stanza that we'll be modifying.
        Stanza targetInterfaceStanza = null;
        String targetAclName = "";
        List<Stanza> interfaces = stanzaMap.get(StanzaBuilder.INTERFACE);
        for (Stanza interfaceStanza : interfaces) {
            if (isTargetInterface(interfaceStanza)) {
                targetInterfaceStanza = interfaceStanza;
            }
        }

        if (null == targetInterfaceStanza) {
            throw new ModificationNotImplementedException(modPair, "enableAcl: No target interface found" +
                    " in source configuration file. Write configuration file chose.");
        }

        // Check if the target stanza already has an ACL in use.
        List<String> aclNames = targetInterfaceStanza.getInterfaceAclNames();
        if (aclNames.size() == 1) {
            // Target interface has ACL
            targetAclName = aclNames.get(0);
            if (numInterfacesSharingAcl(targetAclName, stanzaMap) > 1) {
                // Multiple interfaces share this ACL. Duplicate it before editing.
                String newAclName = duplicateAclToNewName(targetAclName, targetInterfaceStanza, true);
                targetAclName = newAclName;
                newAclCreated = true;
            }
            convertAllStandardAclsToExtended(targetAclName, stanzaMap);
        } else {
            // Create a new ACL just for this interface.
            targetAclName = newAcl();
            newAclCreated = true;
        }

        // Check for existing ACLs that need changing from permit to deny
        if (stanzaMap.containsKey(StanzaBuilder.ACL)) {
            List<Stanza> acls = stanzaMap.get(StanzaBuilder.ACL);
            for (Stanza acl : acls) {
                if (checkAndToggleExistingACL(acl, targetAclName)) {
                    return configInserts;
                }
            }
        }
        // Nothing toggled. Insert the new ACL.
        if (newAclCreated) {
            createAclPermitAllLine(targetAclName, stanzaMap);

            // Insert access-group at the end of the target stanza.
            List<? extends Token> tokens = targetInterfaceStanza.getTokenList();
            int insertIdx = tokens.get(0).getStartIndex() + targetInterfaceStanza.getText().length();


            String accessGroupSnippet;
            if (direction == Direction.OUTGOING) {
                accessGroupSnippet = String.format("\n    ip access-group %s out\n", targetAclName);
            } else {
                accessGroupSnippet = String.format("\n    ip access-group %s in\n", targetAclName);
            }
            this.addInsert(writingConfig, new ConfigInsert(
                    accessGroupSnippet,
                    ConfigInsert.Method.INSERT,
                    insertIdx,
                    insertIdx
            ));
        }
        // Create new access-list line.
        String accessListSnippet;
//        if (Direction.INCOMING == direction) {
        String sourceWildcard = CiscoIOSConfigWriter.determineIpMask(modPair.getSource());
        String destWildcard = CiscoIOSConfigWriter.determineIpMask(modPair.getDestination());

        accessListSnippet = String.format("\naccess-list %s deny ip %s %s %s %s\n",
                targetAclName,
                modPair.getSource().getStartIp(),
                sourceWildcard,
                modPair.getDestination().getStartIp(),
                destWildcard);

        int insertIdx = getAclInsertIdx(stanzaMap);
        this.addInsert(writingConfig, new ConfigInsert(
                accessListSnippet,
                ConfigInsert.Method.INSERT,
                insertIdx,
                insertIdx
        ));
        return configInserts;
    }

    /**
     * Enables the edge that was disabled due to an ACL. Removes the necessary ACLs from
     * the edge destination configuration file.
     * @param modPair modification
     * @return A list of configuration inserts/snippets to write to the config.
     */
    Map<Config, List<ConfigInsert>> removeAcl(ModificationPair modPair)
            throws ModificationNotImplementedException, NullPointerException {
        this.modPair = modPair;
        if (null == this.destConfig) {
            throw new ModificationNotImplementedException(modPair, "removeAcl: destination Config required" +
                    "to remove an ACL.");
        }
        PolicyGroup source = modPair.getSource();
        List<Stanza> interfaceStanzas = stanzaMap.get(StanzaBuilder.INTERFACE);
        List<Stanza> sourceMatchedAcls = getAcls(source);
        List<Prefix> matchingEdges = CiscoIOSConfigWriter.findMatchingEdges(sourceConfig, destConfig);


        // Find the interface stanza connecting to the edge source.
        if (matchingEdges.size() == 0){
            throw new ModificationNotImplementedException(modPair, "removeAcl: no interfaces found" +
                    "that connect the destination config to the source config.");
        }
        Stanza targetInterface = null;
        Prefix interfacePrefix = matchingEdges.get(0);
        for (Stanza stanza : interfaceStanzas) {
            if (stanza.getText().contains(interfacePrefix.getAddress().toString())) {
                targetInterface = stanza;
            }
        }
        if (null == targetInterface) {
            throw new ModificationNotImplementedException(
                    modPair,
                    "removeAcl: Could not find target interface.");
        }

        // Check that the matched ACLs are in use by the interface.
        Stanza targetAcl = null;
        String targetAclName = "";
        logger.debug("Source matched ACLS");
        for (Stanza sourceMatchedAcl : sourceMatchedAcls) {
            logger.debug(sourceMatchedAcl.getText());
        }
        for (Stanza acl : sourceMatchedAcls) {
            if (getAclName(acl).equals(getAccessGroupName(targetInterface))) {
                if (acl.isExtendedAcl()
                        && acl.getText().contains(modPair.getDestination().getStartIp().toString())) {
                    targetAcl = acl;
                    targetAclName = getAclName(acl);
                    break;
                } else if (targetAcl == null) {
                    targetAcl = acl;
                    targetAclName = getAclName(acl);
                }
            }
        }
        if (null == targetAcl) {
            logger.debug("Could not find the ACL belonging to target interface!");
            logger.debug("Assuming traffic is implicitly denied. Creating permit ACL.");
            for (Stanza iface : interfaceStanzas) {
                if (isTargetInterface(iface)) {
                    targetInterface = iface;
                    break;
                }
                String aclName = getAccessGroupName(targetInterface);
                if (null == aclName) {
                    // We can create a new ACL
                    aclName = newAcl();
                    // Add ACL to stanza access group
                    String snippet = String.format("\n    ip access-group %s in\n", aclName);
                    int start = targetInterface.getEndIdx();
                    this.addInsert(writingConfig, new ConfigInsert(
                            snippet,
                            ConfigInsert.Method.INSERT,
                            start,
                            start
                    ));

                } else {
                    // add the rule to the existing ACL. Duplicate and repeat if necessary.
                    if (numInterfacesSharingAcl(aclName, stanzaMap) > 1) {
                        aclName = duplicateAclToNewName(aclName, iface, true);
                    }
                }
                List<Stanza> acls = getAcls(aclName);
                if (acls.size() == 0) {
                    acls = stanzaMap.get(StanzaBuilder.ACL);
                }
                int start = acls.get(0).getStartIdx();

                String snippet = String.format("\naccess-list %s permit ip %s %s %s %s\n",
                        aclName,
                        modPair.getSource().getStartIp(),
                        CiscoIOSConfigWriter.determineIpMask(modPair.getSource()),
                        modPair.getDestination().getStartIp(),
                        CiscoIOSConfigWriter.determineIpMask(modPair.getDestination())
                );
                this.addInsert(writingConfig, new ConfigInsert(
                        snippet,
                        ConfigInsert.Method.INSERT,
                        start,
                        start
                ));
                return this.configInserts;  // Done!
            }


        }

        int numInUse = numInterfacesSharingAcl(targetAclName, stanzaMap);
        if (numInUse > 1) {
            // TODO: Test this case
            String newName = duplicateAclToNewName(targetAclName, targetInterface, true);
            writeToConfig(destConfig);
            // Try again. The target interface should be the only owner of the new ACL
            return removeAcl(modPair);
        }

        // Remove the target ACL
        int start = targetAcl.getStartIdx();
        int end = targetAcl.getEndIdx();
        this.addInsert(writingConfig, new ConfigInsert(
                "",
                ConfigInsert.Method.DELETE,
                start,
                end
        ));
        writeToConfig(sourceConfig);

        // If the only ACL left is the permit statement, delete the ACL and access group.
        List<Stanza> remaining = getAcls(targetAclName);
        if (remaining.size() == 1 && remaining.get(0).getText().contains("permit any")) {
            deleteAclAndGroup(targetAclName);
            writeToConfig(destConfig);
        }


        return configInserts;
    }

    private void deleteAclAndGroup(String name) {
        List<Stanza> acls = stanzaMap.get(StanzaBuilder.ACL);
        List<Stanza> interfaces = stanzaMap.get(StanzaBuilder.INTERFACE);
        // Delete ACLs
        for (Stanza acl : acls) {
            if (getAclName(acl).equals(name)) {
                int start = acl.getStartIdx();
                int end = acl.getEndIdx();
                this.addInsert(writingConfig, new ConfigInsert(
                        destConfig.getText().substring(start, end),
                        ConfigInsert.Method.DELETE,
                        start,
                        end
                ));
            }
        }
        // Delete access group lines
        for (Stanza stanza : interfaces) {
            if (getAccessGroupName(stanza) != null && getAccessGroupName(stanza).equals(name)) {
                int lineStart = stanza.getText().indexOf(String.format("ip access-group %s", name));
                int start = stanza.getStartIdx() + lineStart;
                int end = stanza.getStartIdx()+ stanza.getText().indexOf('\n', lineStart);
                this.addInsert(writingConfig, new ConfigInsert(
                        destConfig.getText().substring(start, end),
                        ConfigInsert.Method.DELETE,
                        start,
                        end
                ));
            }
        }
    }

    private Stanza getAcl(String name) {
        List<Stanza> acls = stanzaMap.get(StanzaBuilder.ACL);
        Pattern p = Pattern.compile(String.format("access-list\\s+%s\\s+", name));
        for (Stanza acl : acls) {
            Matcher m = p.matcher(acl.getText());
            if (m.find()) {
                return acl;
            }
        }
        return null;

    }

    private String getAclName(Stanza acl) {
        Pattern p = Pattern.compile("access-list\\s+(?<name>\\d+)\\s+");
        Matcher m = p.matcher(acl.getText());
        if (m.find()) {
            return m.group("name");
        }
        return null;
    }

    /**
     * Gets the name of the access-group in use by the given interface stanza.
     * If the stanza isn't using an ACL, the method returns null
     * @param interfaceStanza stanza.
     * @return Name of access group, or null if ACL is not in use by interface.
     */
    private String getAccessGroupName(Stanza interfaceStanza) {
        Pattern p = Pattern.compile("ip\\s+access-group\\s+(?<name>\\d+)");
        Matcher m = p.matcher(interfaceStanza.getText());
        if (m.find()) {
            return m.group("name");
        }
        return null;

    }

    private List<Stanza> getAcls(PolicyGroup source) throws ModificationNotImplementedException {
        List<Stanza> acls = stanzaMap.get(StanzaBuilder.ACL);
        List<Stanza> matches = new ArrayList<>();

        for (Stanza acl : acls) {
            String sourceStr = getAclSource(acl);
            if (sourceStr.equals("any") || source.contains(new Ip(sourceStr))) {
                matches.add(acl);
            }
        }
        return matches;
    }

    /**
     * Returns ACL with the given name for the current stanzaMap.
     * @param name Name of the ACL(s)
     * @return ACL stanzas that use the given name.
     */
    private List<Stanza> getAcls(String name) {
        List<Stanza> acls = stanzaMap.get(StanzaBuilder.ACL);
        List<Stanza> matches = new ArrayList<>();
        Pattern p = Pattern.compile(String.format("access-list\\s+%s", name));
        for (Stanza acl : acls) {
            if (p.matcher(acl.getText()).find()){
                matches.add(acl);
            }
        }
        return matches;
    }

    /**
     * Checks if the given interface matches with the network addresses specified in the
     * modification.
     * @param interfaceStanza an interface stanza.
     * @return True if the interface is the modification's target interface. False otherwise.
     */
    private boolean isTargetInterface(Stanza interfaceStanza) {
        // Parse the stanza for the ip address and mask, and then calculate the start ip.
        Pattern pattern = Pattern.compile(
                /** This regex identifies the interface IP address and mask.
                 * This would recognize the following stanza.
                 *  e.g.    ip address 11.0.1.3 255.0.0.0
                 *  and would return: interfaceIp = 11.0.1.3 interfaceMask = 255.0.0.0
                 */

                "ip\\s+address\\s+(?<interfaceIp>\\d+\\.\\d+\\.\\d+\\.\\d+)\\s+(?<interfaceMask>\\d+\\.\\d+\\.\\d+\\.\\d+)"
        );
        Matcher matcher = pattern.matcher(interfaceStanza.getText());
        String interfaceIp, interfaceMask;
        if (!matcher.find()) {
            return false;
        }
        interfaceIp = matcher.group("interfaceIp");
        interfaceMask = matcher.group("interfaceMask");
        Prefix prefix = new Prefix(new Ip(interfaceIp), new Ip(interfaceMask));
        Ip start; Ip end;
        if (modPair.getGraphModification().getEdge().getSource().getName().equals("SOURCE")){
            start = modPair.getSource().getStartIp();
            end = modPair.getSource().getEndIp();
        } else {
            start = modPair.getDestination().getStartIp();
            end = modPair.getDestination().getEndIp();
        }

        // MATCH - Check start and end IP of the modification match the interface.
        return prefix.getNetworkAddress().equals(start)
                && prefix.getEndAddress().equals(end);
    }

    private int numInterfacesSharingAcl(String aclName, Map<String, List<Stanza>> stanzaMap) {
        List<Stanza> interfaceAcls = stanzaMap.get(StanzaBuilder.INTERFACE);
        int count = 0;
        for (Stanza accessGroup : interfaceAcls) {
            if (accessGroup.contains(String.format("\nip access-group %s \n", aclName))) {
                count++;
            }
        }
        return count;
    }

    private void convertAllStandardAclsToExtended(String aclName, Map<String, List<Stanza>> stanzaMap)
            throws ModificationNotImplementedException {
        List<Stanza> interfaceStanzas = stanzaMap.get(StanzaBuilder.ACL);
        String newName = newAcl();
        for (Stanza stanza : interfaceStanzas) {
            if (stanza.getAclName().equals(aclName)) {
                convertStandardAclStanzaToExtendedAclStanza(stanza, newName);
                changeAccessGroup(stanza, aclName);
            }
        }
        // Update the access-group in interfaces.
//        writeToConfig(this.sourceConfig);
    }

    /**
     * Creates an insert that changes the access group for a given interface stanza.
     **/
    private void changeAccessGroup(Stanza interfaceStanza, String newGroupName) {
        Pattern p = Pattern.compile("ip\\s+access-group\\s+(?<oldName>\\d+)\\s+");
        Matcher m = p.matcher(interfaceStanza.getText());
        if (m.find()) {
            int oldLength = m.group("oldName").length();
            int start = interfaceStanza.getTokenList().get(0).getStartIndex()
                    + m.start("oldName");
            this.addInsert(writingConfig, new ConfigInsert(
                    newGroupName,
                    ConfigInsert.Method.OVERWRITE,
                    start,
                    start + oldLength
            ));
        }
    }

    private void convertStandardAclStanzaToExtendedAclStanza(Stanza stanza, String newName) throws ModificationNotImplementedException {
        if (stanza.isAclStanza() && stanza.isStandardAcl()) {
            // Regex searches for 0.0.0.0 0.0.0.0 = source IP and wildcard
            // add the 'any' keyword for source.
            Pattern p = Pattern.compile("access-list\\s+(?<identifier>\\d+)\\s+(?:permit|deny)\\s+(?<classIdx>)\\d+\\.\\d+\\.\\d+\\.\\d+\\s+\\d+\\.\\d+\\.\\d+");
            Matcher m = p.matcher(stanza.getText());
            int end = -1;
            int stanzaStart = stanza.getTokenList().get(0).getStartIndex();
            if (m.find()) {
                end = m.end();
            }
            if (end != -1) {
                end += stanzaStart;
                this.addInsert(writingConfig, new ConfigInsert(
                        " any",
                        ConfigInsert.Method.INSERT,
                        end,
                        end
                ));
                this.addInsert(writingConfig, new ConfigInsert(
                        "ip ",
                        ConfigInsert.Method.INSERT,
                        stanzaStart + m.start("classIdx"),
                        0
                ));
                stanza.setExtendedAcl(true);
                // Change the ACL number to something over 100.
                String currentName = m.group("identifier");
                if (null == currentName){
                    throw new ModificationNotImplementedException(modPair,
                            "convertStandardAclStanzaToExtendedStanza: Failed to convert standard ACL to extended ACL");
                } else {
                    int start = stanzaStart + stanza.getText().indexOf(" " + currentName + " ") + 1;
                    end = start + currentName.length();
                    this.addInsert(writingConfig, new ConfigInsert(
                            newName,
                            ConfigInsert.Method.OVERWRITE,
                            start,
                            end
                    ));
                }
            }
        }
    }

    /**
     * Duplicates an existing ACL under a new name and applies it to the given interface.
     * @param oldName Existing ACL name
     * @param newAclInterface The interface to apply the new ACL.
     * @return The name of the new ACL
     */
    private String duplicateAclToNewName(String oldName, Stanza newAclInterface, boolean toExtended) throws ModificationNotImplementedException {
        String newName = newAcl();
        List<Stanza> oldAclStanzas = getAcls(oldName);
        int start = oldAclStanzas.get(oldAclStanzas.size() - 1).getEndIdx() + 1;
        // By iterating backwards, we're assuming that we ensure the same order
        StringBuilder sb = new StringBuilder();
        for (Stanza stanza : oldAclStanzas) {
            // Add each stanza to String Builder and replace the old name with the new.
            if (toExtended) {
                String text = stanza.getText();
                String newText;
                Pattern p = Pattern.compile("access-list\\s+(?<name>\\d+)\\s+(?<type>(permit|deny))" +
                        "\\s+((?<source>\\d+.\\d+.\\d+.\\d+)\\s+(?<mask>\\d+.\\d+.\\d+.\\d+)|(?<any>any))");
                Matcher m = p.matcher(text);
                if (m.find()) {
                    if (null != m.group("type") && null != m.group("source") && null != m.group("mask")) {
                        newText = String.format("\naccess-list %s %s ip %s %s any\n", newName, m.group("type"),
                                m.group("source"), m.group("mask"));
                    } else if (null != m.group("type") && null != m.group("any")) {
                        newText = String.format("\naccess-list %s %s ip any any\n", newName, m.group("type"));
                    } else {
                        throw new ModificationNotImplementedException(modPair, "duplicateAclToNewName(): ACL stanza format not recognized: " + text);
                    }
                    sb.append(newText);
                    sb.append('\n');
                }
            } else {
                sb.append(stanza.getText().replace(String.format(" %s ", oldName), String.format(" %s ", newName)));
            }
        }
        this.addInsert(writingConfig, new ConfigInsert(
                sb.toString(),
                ConfigInsert.Method.INSERT,
                start,
                start  // doesn't matter
        ));

        // Change the interface stanza's access group to match the new name.
        start = newAclInterface.getStartIdx();
        Pattern p = Pattern.compile("ip\\s+access-group\\s+(?<name>\\d+)\\s+");
        Matcher m = p.matcher(newAclInterface.getText());
        if (m.find() && m.group("name").equals(oldName)) {
            start += m.start("name");
            int end = start + oldName.length();
            this.addInsert(writingConfig, new ConfigInsert(
                    newName,
                    ConfigInsert.Method.OVERWRITE,
                    start,
                    end
            ));
        } else {
            throw new ModificationNotImplementedException(modPair,
                    "duplicateAclToNewName(): Can't find locate interface stanza's existing access-group.");
        }
        return newName;
    }

    private String newAcl() {
        // We need to create a new extended ACL permit all statement, otherwise all traffic will be denied.
        HashSet<String> existingNames = new HashSet<>();
        List<Stanza> interfaceStanzas = stanzaMap.get(StanzaBuilder.INTERFACE);
        for (Stanza stanza : interfaceStanzas) {
            existingNames.addAll(stanza.getInterfaceAclNames());
        }
        int newName = 101;
        while (existingNames.contains(Integer.toString(newName))) {
            newName++;
        }
        return Integer.toString(newName);
    }

    private void createAclPermitAllLine(String aclName, Map<String, List<Stanza>> stanzaMap) throws ModificationNotImplementedException {
        List<Stanza> interfaceStanzas = stanzaMap.get(StanzaBuilder.ACL);
        int idx = getAclInsertIdx(stanzaMap);
        this.addInsert(writingConfig, new ConfigInsert(
                String.format("\naccess-list %s permit ip any any\n!\n", aclName),
                ConfigInsert.Method.INSERT,
                idx,
                idx
        ));
    }

    private int getAclInsertIdx(Map<String, List<Stanza>> stanzaMap) throws ModificationNotImplementedException {
        int idx;
        List<Stanza> aclList = stanzaMap.get(StanzaBuilder.ACL);
        if (null != aclList) {
            // Get the index after existing ACLs.
            Stanza lowestAcl = aclList.get(aclList.size() - 1);
            idx = sourceConfig.getText().indexOf(lowestAcl.getText()) + lowestAcl.getText().length() + 1;
        } else {
            // Get the index after the hostname
            List<Stanza> hostnames = stanzaMap.get("hostname");
            Stanza hostname = hostnames.get(hostnames.size() - 1);
            idx = hostname.getTokenList().get(hostname.getTokenList().size() - 1).getStopIndex();
            idx = sourceConfig.getText().indexOf('!', idx) + 1;
            if (idx == -1) {
                throw new ModificationNotImplementedException(modPair,
                        "Could not find a place to insert a new ACL");
            }
        }
        return idx;
    }

    /**
     * Check stanza for any existing Traffic Classes that must toggle between deny/permit to enable
     * the modification. This works for both adding and deleting an ACL edge.
     *
     * @param stanza An access-list stanza.
     * @return true if existing traffic class found in stanza and insertion created. Returns false otherwise.
     */
    private boolean checkAndToggleExistingACL(Stanza stanza, String aclName) throws ModificationNotImplementedException {
        // we should edit if our mod has the same destination and source IPs.
        if (modPair.getLevel() == ModificationPair.Level.TRAFFIC_CLASS) {
            String sourceIp, sourceMask, destIp, destMask;
            sourceIp = modPair.getSource().getStartIp().toString();
            sourceMask = CiscoIOSConfigWriter.determineIpMask(sourceIp, modPair.getSource().getEndIp().toString());
            destIp = modPair.getDestination().getStartIp().toString();
            destMask = CiscoIOSConfigWriter.determineIpMask(destIp, modPair.getDestination().getEndIp().toString());

            if (stanza.contains(sourceIp) && stanza.contains(sourceMask)
                    && stanza.contains(destIp) && stanza.contains(destMask)
                    && stanza.contains(aclName)) {
                // Check that source appears before destination. Otherwise we have the reverse ACL.
                if (stanza.getText().indexOf(sourceIp) < stanza.getText().indexOf(destIp)) {
                    // We've found a matching stanza, and we should change the access type (deny|permit)
                    int insertIdx;
                    if (ConfigModification.Action.ADD == modPair.getConfigModification().getAction()) {
                        insertIdx = stanza.getTokenList().get(0).getStartIndex() + stanza.getText().indexOf("permit");
                        if (insertIdx == -1) {
                            throw new ModificationNotImplementedException(modPair,
                                    "Failed to find a permit statement in relevant ACL stanza.");
                        }
                        int exitIdx = insertIdx += 6; // "permit".length()
                        this.addInsert(writingConfig, new ConfigInsert(
                                "deny",
                                ConfigInsert.Method.OVERWRITE,
                                insertIdx,
                                exitIdx
                        ));  // Switches "permit" to "deny"
                        return true;

                    } else {
                        insertIdx = stanza.getTokenList().get(0).getStartIndex() + stanza.getText().indexOf("deny");
                        if (insertIdx == -1) {
                            throw new ModificationNotImplementedException(modPair,
                                    "Failed to find a deny statement in relevant stanza.");
                        }
                        int exitIdx = insertIdx + 4;  // "deny".length()
                        this.addInsert(writingConfig, new ConfigInsert(
                                "permit",
                                ConfigInsert.Method.OVERWRITE,
                                insertIdx,
                                exitIdx
                        ));  // Switches "deny" to "permit"
                        return true;
                    }

                }
            }
        }
        return false;

    }

    private String getAclSource(Stanza stanza) throws ModificationNotImplementedException {
        if (stanza.isStandardAcl()) {
            Pattern p = Pattern.compile("access-list\\s+\\d+\\s+(permit|deny)\\s+" +
                    "((?<source>\\d+\\.\\d+\\.\\d+\\.\\d+)\\s+\\d+\\.\\d+\\.\\d+\\.\\d+|(?<altSource>any))");
            Matcher m = p.matcher(stanza.getText());
            if (m.find()) {
                String source = m.group("source");
                if (source == null) {
                    source = m.group("altSource");
                }
                return source;
            } else {
                throw new ModificationNotImplementedException(modPair,
                        "Standard ACL format not recognized. Expecting " +
                        "\"access-list # permit|deny #.#.#.# #.#.#.#\" but got "
                        + stanza.getText());
            }
        } else if (stanza.isExtendedAcl()) {
            Pattern p = Pattern.compile("access-list\\s+\\d+\\s+(permit|deny)\\s+ip\\s+" +
                    "((?<source>\\d+\\.\\d+\\.\\d+\\.\\d+)\\s+\\d+\\.\\d+\\.\\d+\\.\\d+|(?<altSource>any))" +
                    "\\s+" +
                    "((?<destination>\\d+\\.\\d+\\.\\d+\\.\\d+)\\s+\\d+\\.\\d+\\.\\d+\\.\\d+|(?<altDestination>any))");
            Matcher m = p.matcher(stanza.getText());
            if (m.find()) {
                String source = m.group("source");
                if (source == null) {
                    source = m.group("altSource");
                }
                return source;
            } else {
                throw new ModificationNotImplementedException(modPair,
                        "Extended ACL format not recognized. Expecting " +
                        "\"access-list # permit|deny ip #.#.#.# #.#.#.# #.#.#.# #.#.#.#\" but got "
                        + stanza.getText());
            }
        } else {
            throw new ModificationNotImplementedException(modPair,
                    "Not an ACL stanza.");
        }
    }
}

class RoutingEditor extends CiscoIosEditor {

    private Config destConfig;
    private Config sourceConfig;
    private Config writingConfig;
    private String hostname;
    private ModificationPair modPair;

    RoutingEditor(Config sourceConfig, Config destConfig) {
        this.sourceConfig = sourceConfig;
        this.destConfig = destConfig;
        this.stanzaMap = new StanzaBuilder().parseStanzas(sourceConfig, sourceConfig.getHostname());
        this.writingConfig = sourceConfig;
    }

    Map<Config, List<ConfigInsert>> applyMod(ModificationPair modPair) throws ModificationNotImplementedException {
        this.modPair = modPair;

        // Determine IP string of edge source so we can find the correct line.
        List<Prefix> edgeDestPrefix = CiscoIOSConfigWriter.findMatchingEdges(sourceConfig, destConfig);
        Map<String, String> protocols = CiscoIOSConfigWriter.findRouterProtocols(destConfig);

        for (Prefix edge : edgeDestPrefix) {
            String search = String.format("network %s %s", edge.getNetworkAddress(), edge.getSubnetMask());
            // Enable/Disable routing depending on the mod.
            switch (modPair.getConfigModification().getAction()) {
                case ADD:
                    // enable routing
                    for (Stanza router : stanzaMap.get(StanzaBuilder.ROUTER)) {
                        int start = router.getEndIdx();
                        String snippet = "";
                        if (router.getText().contains("router ospf")) {
                            snippet = String.format(
                                    "network %s %s area 0",
                                    edge.getNetworkAddress().toString(),
                                    edge.getPrefixWildcard().toString());
                        }
                        else if (router.getText().contains("router bgp")){
                            start = getBgpRouterIdxBeforeAddressFamily(router);
                            String neighboringBGP = protocols.get("bgp");
                            if (null == neighboringBGP) {
                                throw new ModificationNotImplementedException(modPair,
                                        "Could not find neighboring BGP instance.");
                            }

                            snippet = String.format(
                                    "neighbor %s remote-as %s",
                                    edge.getAddress(),
                                    neighboringBGP
                            );
                        }
                        if (!router.getText().contains(snippet)) {
                            this.addInsert(writingConfig, new ConfigInsert(
                                    "\n    " + snippet,
                                    ConfigInsert.Method.INSERT,
                                    start,
                                    start
                            ));
                            snippet = String.format("neighbor %s activate", edge.getAddress());
                            this.addInsert(writingConfig, new ConfigInsert(
                                    "\n        " + snippet,
                                    ConfigInsert.Method.INSERT,
                                    getBgpRouterAddressFamilyEndIdx(router),
                                    0
                            ));
                        }

                    }
                    break;
                case REMOVE:
                    // disable routing by removing the network line for the matching edge.
                    for (Stanza router : stanzaMap.get(StanzaBuilder.ROUTER)) {
                        if (router.contains(search)) {
                            int relativeIdx = router.getText().indexOf(search);
                            int start = router.getStartIdx() + relativeIdx;
                            int end = router.getStartIdx() + router.getText().indexOf(System.lineSeparator(), relativeIdx);
                            this.addInsert(writingConfig, new ConfigInsert(
                                    sourceConfig.getText().substring(start, end),
                                    ConfigInsert.Method.DELETE,
                                    start,
                                    end
                            ));
                        }
                    }
                    break;
            }
        }

        // Return the inserts.
        return configInserts;
    }

    private int getBgpRouterIdxBeforeAddressFamily(Stanza router) {
        int start = router.getStartIdx();
        int end = router.getEndIdx();
        int addressFamilyStart = start + router.getText().indexOf("address-family");
        if (addressFamilyStart == -1) {
            return end;
        }
        Pattern p = Pattern.compile("neighbor\\s+\\d+\\.\\d+\\.\\d+\\.\\d+\\s+remote-as\\s+\\d+");
        Matcher m = p.matcher(router.getText());
        int lastNeighborIdx = -1;
        if (m.find()) {
            lastNeighborIdx = m.end(m.groupCount());
        }
        int newLine = router.getText().indexOf('\n', lastNeighborIdx);
        if (newLine != -1) {
            return start + newLine;
        } else if (lastNeighborIdx != -1) {
            return start + lastNeighborIdx;
        } else {
            return end;
        }
    }

    private int getBgpRouterAddressFamilyEndIdx(Stanza router) {
         int start = router.getStartIdx();
         int end = router.getEndIdx();
         int exitLine = router.getText().indexOf("exit-address-family");
         int lastNewLine = router.getText().substring(0, exitLine).lastIndexOf('\n');
         if (lastNewLine != -1) {
             return start + lastNewLine;
         } else {
             return end;
         }
    }


}

class StaticRouteEditor extends CiscoIosEditor {

    private Config sourceConfig;
    private Config destConfig;
    private Config writingConfig;


    public StaticRouteEditor(Config sourceConfig, Config destConfig) {
        this.sourceConfig = sourceConfig;
        this.destConfig = destConfig;
        this.writingConfig = sourceConfig;
        stanzaMap = new StanzaBuilder().parseStanzas(this.sourceConfig, this.sourceConfig.getHostname());
    }

    Map<Config, List<ConfigInsert>> applyEnableMod(ModificationPair modPair) throws ModificationNotImplementedException {
        // Get the index for this new stanza. We're going to place it after the final interface.
        List<Stanza> interfaces = stanzaMap.get(StanzaBuilder.INTERFACE);
        Stanza target;
        if (interfaces != null) {
            target = interfaces.get(interfaces.size() - 1);
            int startIdx = target.getTokenList().get(0).getStartIndex() + target.getText().length() + 1;
            logger.debug(modPair.getDestination().toString());
            String destPrefix = modPair.getDestination().getStartIp().toString();
            String destMask = CiscoIOSConfigWriter.determineIpMask(
                    modPair.getDestination().getStartIp().toString(),
                    modPair.getDestination().getEndIp().toString());
            String nextHop;

            List<Prefix> matchingEdges = CiscoIOSConfigWriter.findMatchingEdges(sourceConfig, destConfig);
            if (matchingEdges.size() == 0) {
                throw new ModificationNotImplementedException(modPair,
                        "No matching edges found between " +
                sourceConfig.getHostname() + " and " + destConfig.getHostname());
            }
            nextHop = matchingEdges.get(0).getAddress().toString();  // Address of destination router.
            // TODO: Use the remaining edges for secondary and tertiary static routes.
            String snippet = String.format("!\nip route %s %s %s\n!\n", destPrefix, destMask, nextHop);
            this.addInsert(writingConfig, new ConfigInsert(
                    snippet,
                    ConfigInsert.Method.INSERT,
                    startIdx,
                    startIdx + snippet.length()
            ));
        }
        return this.configInserts;
    }

    Map<Config, List<ConfigInsert>> applyDisableMod(ModificationPair modPair) throws ModificationNotImplementedException {
        // DISABLE
        // Get any static route stanzas and check their destinations.
        List<Stanza> staticRoutes = stanzaMap.get(StanzaBuilder.STATIC_ROUTE);
        if (staticRoutes.size() != 0) {
            String destPrefix = modPair.getDestination().getStartIp().toString();
            String destMask = CiscoIOSConfigWriter.determineIpMask(
                    modPair.getDestination().getStartIp().toString(),
                    modPair.getDestination().getEndIp().toString());

            List<Prefix> matchingEdges = CiscoIOSConfigWriter.findMatchingEdges(sourceConfig, destConfig);
            if (matchingEdges.size() == 0) {
                throw new ModificationNotImplementedException(modPair,
                        "No matching edges found between " +
                        sourceConfig.getHostname() + " and " + destConfig.getHostname());
            }
            // Check every possible destination router to see if there's static route to it,
            // then eliminate it.
            for (Prefix matchingEdge : matchingEdges) {
                String nextHop = matchingEdge.getAddress().toString();

                Pattern p = Pattern.compile("ip\\s+route\\s+(?<prefix>\\d+\\.\\d+\\.\\d+\\.\\d+)" +
                        "\\s+(?<mask>\\d+\\.\\d+\\.\\d+\\.\\d+)" +
                        "\\s+(?<nextHop>\\d+\\.\\d+\\.\\d+\\.\\d+)");
                for (Stanza staticRoute : staticRoutes) {
                    Matcher m = p.matcher(staticRoute.getText());
                    if (m.find()) {
                        // Verify that the ip addresses match those included in the modification.
                        if (m.group("prefix").equals(destPrefix)
                                && m.group("mask").equals(destMask)
                                && m.group("nextHop").equals(nextHop)) {
                            // Found matching stanza. Delete this stanza.
                            int start = staticRoute.getStartIdx();
                            int end = staticRoute.getEndIdx();
                            this.addInsert(writingConfig, new ConfigInsert(
                                    "",
                                    ConfigInsert.Method.DELETE,
                                    start,
                                    end
                            ));
                        }
                    }
                }
            }
        }
        return this.configInserts;
    }
}

class RouteRedistributionEditor extends CiscoIosEditor {

    private Config sourceConfig;
    private Config destConfig;
    private Config writingConfig;
    private ModificationPair modPair;

    RouteRedistributionEditor(Config sourceConfig, Config destConfig) {
        this.sourceConfig = sourceConfig;
        this.destConfig = destConfig;
        this.stanzaMap = new StanzaBuilder().parseStanzas(sourceConfig, sourceConfig.getHostname());
        this.writingConfig = sourceConfig;
    }

    Map<Config, List<ConfigInsert>> applyEnableMod(ModificationPair modPair) throws ModificationNotImplementedException {
        this.modPair = modPair;
        Map<String, String> sourceProtocols = CiscoIOSConfigWriter.findRouterProtocols(sourceConfig);
        Map<String, String> destProtocols = CiscoIOSConfigWriter.findRouterProtocols(destConfig);

        List<String> diffProtocols = new ArrayList<>();
        List<String> commonProtocols = new ArrayList<>();
        for (String s : destProtocols.keySet()) {
            if (sourceProtocols.containsKey(s)) {
                commonProtocols.add(s);
            } else {
                diffProtocols.add(s);
                logger.debug(String.format("Destination needs %s redistribution", s));
            }
        }
        logger.debug("Different Protocol size: " + diffProtocols.size());
        logger.debug("Common Protocol size: " + commonProtocols.size());
        // Find the stanzas that use the common protocols.
        for (String commonProtocol : commonProtocols) {
            Stanza stanza = getRouterStanza(commonProtocol);
            // Redistribute the differing protocols by inserting a route redistribution line into the stanza.
            int secondLineStart = stanza.getText().indexOf('\n') + 1;
            for (String diffProtocol : diffProtocols) {
                String snippet = generateRedistributeSnippet(diffProtocol, sourceProtocols);
                this.addInsert(writingConfig, new ConfigInsert(
                        snippet,
                        ConfigInsert.Method.INSERT,
                        secondLineStart,
                        secondLineStart
                ));
            }
        }
        return this.configInserts;
    }

    Map<Config, List<ConfigInsert>> applyDisableMod(ModificationPair modPair) throws ModificationNotImplementedException {
        this.modPair = modPair;
        Map<String, String> sourceProtocols = CiscoIOSConfigWriter.findRouterProtocols(sourceConfig);
        Map<String, String> destProtocols = CiscoIOSConfigWriter.findRouterProtocols(destConfig);

        List<String> diffProtocols = new ArrayList<>();
        List<String> commonProtocols = new ArrayList<>();
        for (String s : destProtocols.keySet()) {
            if (sourceProtocols.containsKey(s)) {
                commonProtocols.add(s);
            } else {
                diffProtocols.add(s);
                logger.debug(String.format("Destination needs %s redistribution", s));
            }
        }
        logger.debug("Different Protocol size: " + diffProtocols.size());
        logger.debug("Common Protocol size: " + commonProtocols.size());
        for (String commonProtocol : commonProtocols) {
            Stanza s = getRouterStanza(commonProtocol);
            if (s.getText().contains("redistribute")) {
                int startIdx = s.getText().indexOf("redistribute");
                int endIdx = s.getText().indexOf('\n', startIdx + 1);
                this.addInsert(writingConfig, new ConfigInsert(
                        "",
                        ConfigInsert.Method.DELETE,
                        s.getStartIdx() + startIdx,
                        s.getStartIdx() + endIdx
                ));
            }
        }

        return this.configInserts;
    }

    private String generateRedistributeSnippet(String protocol, Map<String, String> sourceProtocols) {
        String result = "redistribute " +
                protocol +
                " " +
                sourceProtocols.get(protocol);
        if (protocol.equals("ospf")) {
            result += " match internal external 1 external 2\n";
        } else {
            result += " subnets\n";
        }
        return result;
    }

    private Stanza getRouterStanza(String commonProtocol) throws ModificationNotImplementedException {
        List<Stanza> routerStanzas = stanzaMap.get(StanzaBuilder.ROUTER);
        for (Stanza routerStanza : routerStanzas) {
            String firstLine = routerStanza.getText().substring(0, routerStanza.getText().indexOf('\n'));
            if (firstLine.contains(commonProtocol)) {
                return routerStanza;
            }
        }
        throw new ModificationNotImplementedException(modPair,
                "Could not find router stanza with " + commonProtocol + " protocol.");
    }

}