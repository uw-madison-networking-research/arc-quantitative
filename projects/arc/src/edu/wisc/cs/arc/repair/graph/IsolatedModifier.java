package edu.wisc.cs.arc.repair.graph;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.*;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.Vertex.VertexType;
import edu.wisc.cs.arc.policies.Policy;
import edu.wisc.cs.arc.repair.RepairException;
import edu.wisc.cs.arc.repair.VirtualDirectedEdge;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.alg.flow.EdmondsKarpMFImpl;
import org.jgrapht.alg.flow.GusfieldGomoryHuCutTree;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;

import java.util.*;

public class IsolatedModifier extends GraphModifier {
	
	public IsolatedModifier(Map<Flow, List<Policy>> policies, 
			DeviceGraph deviceEtg, ProcessGraph baseEtg,
			Map<Flow, ProcessGraph> flowEtgs, Settings settings) {
		super(policies, deviceEtg, baseEtg, null, flowEtgs, settings);
	}

	@Override
	public ConfigModificationGroup getModifications(){
		Map<Flow, List<GraphModification<ProcessVertex>>> modifications =
			new LinkedHashMap<Flow, List<GraphModification<ProcessVertex>>>();
		
		// Check whether to assume all policies are broken or only repair for
		// explicitly broken policies
		Set<Flow> brokenFlows = this.policies.keySet();
		if (settings.shouldOnlyRepairBrokenETGs()) {
			brokenFlows = this.getBrokenFlows();
			logger.debug("COUNT: brokenFlows "+brokenFlows.size());
		}
		
		for (Flow flow : brokenFlows) {
			if (!this.etgs.containsKey(flow)) {
				logger.warn("No ETG for "+flow.toString());
				continue;
			}
			logger.debug(flow.toString());
			List<Policy> policies = this.policies.get(flow);
			if (policies.size() > 1) {
				throw new RepairException(
						"Cannot handle more than one policy per flow");
			}
			// TEMPORARY CODE for testing always-reachable heuristic
			/*if (policies.get(0).getType() != PolicyType.ALWAYS_REACHABLE) {
				continue;
			}*/
			
			// Apply repair for each policy
			long startTime = System.currentTimeMillis();
			for (Policy policy : policies) {
				List<GraphModification<ProcessVertex>> modsForFlow = 
						this.repair(flow, policy);
				if (null == modsForFlow) {
					logger.debug("UNSATISFIABLE");
					return null;
				}
				modifications.put(flow, modsForFlow);
			}
			long endTime = System.currentTimeMillis();
			System.out.println("TIMEONE: " 
        			+ this.settings.getModifierAlgorithm()
        			+ " " + (endTime - startTime) + " ms "
        			+ flow.toString());
		}
		
		// Log modifications
		logger.debug("Modifications:");
		for (Flow flow : modifications.keySet()) {
			List<GraphModification<ProcessVertex>> modsForFlow = 
					modifications.get(flow);
			if (modsForFlow.size() > 0) {
				logger.debug(flow.toString());
				for (GraphModification<ProcessVertex> mod : modsForFlow) {
					logger.debug("\t" + mod.toString());
				}
			}
		}
		
		return new ConfigModificationGroup(modifications, null, null);
	}
	
	private List<GraphModification<ProcessVertex>> repair(Flow flow, 
			Policy policy) {
		ProcessGraph etg = this.etgs.get(flow);
		switch(policy.getType()) {
		case ALWAYS_BLOCKED:
			return this.repairAlwaysBlocked(etg);
		case ALWAYS_WAYPOINT:
			return this.repairAlwaysWaypoint(etg);
		case ALWAYS_REACHABLE:
			return this.repairAlwaysReachable(etg,
					(Integer)policy.getParameter());
		default:
			break;
		}
		return null;
	}
	
	/**
	 * Compute the repairs for an always blocked policy by finding the edges
	 * contained in the min-cut between the source and destination vertices.
	 * @param etg the ETG on which to compute a repair
	 * @return the list of edges to remove
	 */
	private List<GraphModification<ProcessVertex>> repairAlwaysBlocked(
			ProcessGraph etg) {
		logger.debug("Blocked");
		
		// Get unit-weight graph
		DefaultDirectedWeightedGraph<ProcessVertex,DirectedEdge<ProcessVertex>> 
				unitWeightGraph = etg.getUnitWeightCopy();
		
		// Compute min-cut
		GusfieldGomoryHuCutTree<ProcessVertex,DirectedEdge<ProcessVertex>> minCutAlgo = 
				new GusfieldGomoryHuCutTree<ProcessVertex,DirectedEdge<ProcessVertex>>(
						unitWeightGraph);
		minCutAlgo.calculateMinCut(etg.getFlowSourceVertex(), 
				etg.getFlowDestinationVertex());
		Set<DirectedEdge<ProcessVertex>> cutEdges = minCutAlgo.getCutEdges();
		
		// Remove every edge in minCut
		List<GraphModification<ProcessVertex>> modifications = 
				new ArrayList<GraphModification<ProcessVertex>>();
		for (DirectedEdge<ProcessVertex> edge : cutEdges) {
			modifications.add(new GraphModification<ProcessVertex>(
					GraphModification.Action.REMOVE,
					new VirtualDirectedEdge<ProcessVertex>(edge)));
		}
		return modifications;
	}
	
	/**
	 * Compute the repairs for an always traverse waypoint policy by removing
	 * all waypoint vertices and finding all edges contained in the min-cut
	 * between the source and destination vertices.
	 * @param etg the ETG on which to compute a repair
	 * @return the list of edges to remove
	 */
	private List<GraphModification<ProcessVertex>> repairAlwaysWaypoint(
			ProcessGraph etg) {
		logger.debug("Waypoint");
		
		// Get unit-weight graph with waypoint vertices removed
		DefaultDirectedWeightedGraph<ProcessVertex,DirectedEdge<ProcessVertex>> 
				unitWeightGraph = etg.getUnitWeightCopy();
		for (ProcessVertex vertex : unitWeightGraph.vertexSet()) {
			if (VertexType.WAYPOINT == vertex.getType()) {
				unitWeightGraph.removeVertex(vertex);
			}
		}
		
		// Compute min-cut
		GusfieldGomoryHuCutTree<ProcessVertex,DirectedEdge<ProcessVertex>> minCutAlgo = 
				new GusfieldGomoryHuCutTree<ProcessVertex,DirectedEdge<ProcessVertex>>(
						unitWeightGraph);
		minCutAlgo.calculateMinCut(etg.getFlowSourceVertex(), 
				etg.getFlowDestinationVertex());
		Set<DirectedEdge<ProcessVertex>> cutEdges = minCutAlgo.getCutEdges();
		
		// Remove every edge in minCut
		List<GraphModification<ProcessVertex>> modifications = 
				new ArrayList<GraphModification<ProcessVertex>>();
		for (DirectedEdge<ProcessVertex> edge : cutEdges) {
			modifications.add(new GraphModification<ProcessVertex>(
					GraphModification.Action.REMOVE,
					new VirtualDirectedEdge<ProcessVertex>(edge)));
		}
		return modifications;
	}
	
	private List<GraphModification<ProcessVertex>> repairAlwaysReachable(
			ProcessGraph etg, int k) {
		logger.debug("Reachable (< " + k + ")");
		
		// Get unit-weight graph
		DefaultDirectedWeightedGraph<ProcessVertex,DirectedEdge<ProcessVertex>> 
				unitWeightGraph = etg.getUnitWeightCopy();
		
		// Start with empty list of modifications
		List<GraphModification<ProcessVertex>> modifications = 
				new ArrayList<GraphModification<ProcessVertex>>();
		
		// Continue to try to augment graph while max flow is too low
		double maxFlow, prevMaxFlow = -1;
		do {
			maxFlow = this.augmentGraph(etg, k, unitWeightGraph, modifications);
			if (maxFlow < 0 || maxFlow == prevMaxFlow) {
				return null;
			}
			prevMaxFlow = maxFlow;
		} while(maxFlow < k);
		
		return modifications;
	}
	
	private double augmentGraph(ProcessGraph etg, double k,
				DefaultDirectedWeightedGraph<ProcessVertex,
				DirectedEdge<ProcessVertex>> currGraph, 
				List<GraphModification<ProcessVertex>> modifications) {
		
		// Compute max-cut
		EdmondsKarpMFImpl<ProcessVertex,DirectedEdge<ProcessVertex>>
			maxFlowAlgo = new EdmondsKarpMFImpl<ProcessVertex,
			DirectedEdge<ProcessVertex>>(currGraph);
		double maxFlow = maxFlowAlgo.calculateMaximumFlow(
			etg.getFlowSourceVertex(), etg.getFlowDestinationVertex());
		logger.debug("Current ETG (max-flow="+maxFlow+")");
		for (DirectedEdge<ProcessVertex> edge : currGraph.edgeSet()) {
			logger.debug("\t" + edge.toString());
		}
		if (maxFlow >= k) {
			return maxFlow;
		}
		
		// Compute residual graph
		// Start with unit-weight graph
		DefaultDirectedWeightedGraph<ProcessVertex,DirectedEdge<ProcessVertex>> 
			residualGraph = etg.getUnitWeightCopy();
		
		// Add possible edges
		List<VirtualDirectedEdge<ProcessVertex>> possibleEdges = 
				this.getPossibleBaseEdges();
		possibleEdges.addAll(this.getPossibleStaticRouteEdges(
				etg.getFlow().getDestination()));
		possibleEdges.addAll(this.getPossibleEndpointEdges(etg.getFlow(),
		        etg.getFlowSourceVertex(), etg.getFlowDestinationVertex()));
		for (VirtualDirectedEdge<ProcessVertex> edge : possibleEdges) {
			if (!residualGraph.containsEdge(edge.getSource(), 
					edge.getDestination())) {
				if (!residualGraph.containsVertex(edge.getSource())) {
					residualGraph.addVertex(edge.getSource());
				}
				if (!residualGraph.containsVertex(edge.getDestination())) {
					residualGraph.addVertex(edge.getDestination());
				}
				DirectedEdge<ProcessVertex> newEdge = residualGraph.addEdge(
						edge.getSource(), edge.getDestination());
				if (EdgeType.INTER_DEVICE == edge.getType()) {
					residualGraph.setEdgeWeight(newEdge, 1);
				}
				else {
					residualGraph.setEdgeWeight(newEdge, 
							DirectedEdge.INFINITE_WEIGHT);
				}
			}
		}	
		logger.debug("Possibilities Graph");
		for (DirectedEdge<ProcessVertex> edge : residualGraph.edgeSet()) {
			logger.debug("\t" + edge.toString());
		}
			
		// Remove edges whose capacity is fully consumed
		Map<DirectedEdge<ProcessVertex>, Double> maxFlowPerEdge = 
				maxFlowAlgo.getMaximumFlow(etg.getFlowSourceVertex(), 
					etg.getFlowDestinationVertex()).getFlow();
		if (maxFlow > 0) {
			logger.debug("Flow");
			for (DirectedEdge<ProcessVertex> edge : maxFlowPerEdge.keySet()) {
				double flow = maxFlowPerEdge.get(edge);
				double capacity = residualGraph.getEdgeWeight(edge);
				String remove = " ";
				if (capacity <= flow + 0.1) {
					remove = "X";
					residualGraph.removeEdge(edge.getSource(),
							edge.getDestination());
				}
				logger.debug("      " + remove + " " + edge.toString() + " - "
						+ flow);
			}
			logger.debug("Residual Graph");
			for (DirectedEdge<ProcessVertex> edge : residualGraph.edgeSet()) {
				logger.debug("\t" + edge.toString());
			}
		} else {
			logger.debug("Residual Graph");
			logger.debug("\t(same as above)");
		}
		
		// Find the shortest path from the source to the sink
		List<DirectedEdge<ProcessVertex>> shortestPath = 
				DijkstraShortestPath.findPathBetween(residualGraph, 
						etg.getFlowSourceVertex(), 
						etg.getFlowDestinationVertex()).getEdgeList();
		// If no shortest path, then the policy cannot be satisfied
		if (null == shortestPath) {
			logger.debug("No path available");
			return -1;
		}
		
		// Leave one out and try again
		logger.debug("Path");
		for (DirectedEdge<ProcessVertex> edge : shortestPath) {
			logger.debug("\t"+edge.toString());
			if (currGraph.containsEdge(edge.getSource(), 
					edge.getDestination())) {
				continue;
			}
			residualGraph.removeEdge(edge.getSource(), edge.getDestination());
		}
		
		while (true) {
			List<DirectedEdge<ProcessVertex>> shortestPathB = 
					DijkstraShortestPath.findPathBetween(residualGraph, 
							etg.getFlowSourceVertex(), 
							etg.getFlowDestinationVertex()).getEdgeList();
			// If no shortest path, then the policy cannot be satisfied
			if (shortestPathB != null && shortestPathB.size() 
					== shortestPath.size()) {
				for (int j = 0; j < shortestPathB.size() 
						&& j < shortestPath.size(); j++) {
					if (shortestPathB.get(j).getSource() == 
							shortestPath.get(j).getSource()) {
						continue;
					}
					if (shortestPathB.get(j).getSource().toString().compareTo(
							shortestPath.get(j).getSource().toString()) < 0) {
						shortestPath = shortestPathB;
						logger.debug("Use option B");
						break;
					}
					break;
				}
				
				// Leave one out and try again
				logger.debug("PathB");
				for (DirectedEdge<ProcessVertex> edge : shortestPathB) {
					logger.debug("\t"+edge.toString());
					if (currGraph.containsEdge(edge.getSource(), 
							edge.getDestination())) {
						continue;
					}
					residualGraph.removeEdge(edge.getSource(),
							edge.getDestination());
				}
			} else {
				break;
			}
		}
		
		// Add edges in the path not in the original graph
		logger.debug("Path");
		for (DirectedEdge<ProcessVertex> edge : shortestPath) {
			logger.debug("\t"+edge.toString());
			if (currGraph.containsEdge(edge.getSource(), 
					edge.getDestination())) {
				continue;
			}
			// Add to list of modifications
			modifications.add(new GraphModification<ProcessVertex>(
					GraphModification.Action.ADD,
					new VirtualDirectedEdge<ProcessVertex>(edge)));
			
			// Add to unit weight graph
			if (!currGraph.containsVertex(edge.getSource())) {
				currGraph.addVertex(edge.getSource());
			}
			if (!currGraph.containsVertex(edge.getDestination())) {
				currGraph.addVertex(edge.getDestination());
			}
			DirectedEdge<ProcessVertex> newEdge = currGraph.addEdge(
					edge.getSource(), edge.getDestination());
			if (EdgeType.INTER_DEVICE == edge.getType()) {
				currGraph.setEdgeWeight(newEdge, 1);
			}
			else {
				currGraph.setEdgeWeight(newEdge, 
						DirectedEdge.INFINITE_WEIGHT);
			}
		}
		
		maxFlowAlgo = new EdmondsKarpMFImpl<ProcessVertex,
				DirectedEdge<ProcessVertex>>(currGraph);
		maxFlow = maxFlowAlgo.calculateMaximumFlow(
			etg.getFlowSourceVertex(), etg.getFlowDestinationVertex());
		logger.debug("New max-flow="+maxFlow);
		logger.debug("Flow");
		maxFlowPerEdge = maxFlowAlgo.getMaximumFlow(
			etg.getFlowSourceVertex(), etg.getFlowDestinationVertex()).getFlow();;
		for (DirectedEdge<ProcessVertex> edge : maxFlowPerEdge.keySet()) {
			double flow = maxFlowPerEdge.get(edge);
			logger.debug("\t" + edge.toString() + " - " + flow);
		}
		return maxFlow;
	}
}