package edu.wisc.cs.arc.verifiers;

import java.util.List;
import java.util.Map;

import org.jgrapht.alg.shortestpath.DijkstraShortestPath;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;
import edu.wisc.cs.arc.graphs.Flow;

/**
 * Checks if a flow always traverses a waypoint.
 * @author Aaron Gember-Jacobson (agember@cs.wisc.edu)
 */
@SuppressWarnings("rawtypes")
public class AlwaysWaypoint extends Verifier {
	
	/**
	 * Construct a verifier.
	 * @param etgs the extended topology graphs to use for verification
	 * @param settings the settings to use during verification
	 */
	public AlwaysWaypoint(Map<Flow, ? extends ExtendedTopologyGraph> etgs, 
			Settings settings) {
		super(etgs, settings);
	}

	/**
	 * Check the property for a specific flow.
	 * @param flow flow for which to check the property
	 * @param arg unused
	 * @return verification result
	 */
	@Override
	public VerifierResult verify(Flow flow, Object arg) {
		// Get ETG
		ExtendedTopologyGraph etg = this.etgs.get(flow);
		if (null == etg) {
			throw new VerifierException("No ETG for flow "+flow);
		}

		// Clone and prune ETG
		ExtendedTopologyGraph etgClone = (ExtendedTopologyGraph) etg.clone();
		etgClone.prune(true);

		// Check if a path exists
		List<DirectedEdge> path = DijkstraShortestPath.findPathBetween(
				etgClone.getGraph(), etgClone.getFlowSourceVertex(),
				etgClone.getFlowDestinationVertex()).getEdgeList();
		return new VerifierResult((null == path), path);
	}
}
