package edu.wisc.cs.arc.verifiers;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jgrapht.alg.shortestpath.DijkstraShortestPath;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.repair.VirtualDirectedEdge;
/**
 * Checks if a flow is always blocked.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
@SuppressWarnings("rawtypes")
public class PrimaryPath extends Verifier {
	
	/**
	 * Construct a verifier.
	 * @param etgs the extended topology graphs to use for verification
	 * @param settings the settings to use during verification
	 */
	public PrimaryPath(Map<Flow, ? extends ExtendedTopologyGraph> etgs,
			Settings settings) {
		super(etgs, settings);
	}

	/**
	 * Check the property for a specific flow.
	 * @param flow flow for which to check the property
	 * @param arg unused
	 * @return true if the property holds, otherwise false
	 */
	@Override
	public VerifierResult verify(Flow flow, Object arg) {
		// Get argument
		List<VirtualDirectedEdge> desiredPath = null;
		if (arg != null) {
			// Verify argument types
			if (!(arg instanceof List<?>)) {
				throw new VerifierException("Argument must be a list");
			}
			List<?> listArg = (List<?>)arg;
			desiredPath = new LinkedList<VirtualDirectedEdge>();
			for (Object item : listArg) {
				if (!(item instanceof VirtualDirectedEdge)) {
					throw new VerifierException("Argument items must be edges");
				}
				desiredPath.add((VirtualDirectedEdge)item);
			}
		}
		
		// Get ETG
		ExtendedTopologyGraph etg = this.etgs.get(flow);
		if (null == etg) {
			throw new VerifierException("No ETG for flow "+flow);
		}
		
		// Find the shortest path to the destination
		List<DirectedEdge> primaryPath = DijkstraShortestPath.findPathBetween(
				etg.getGraph(), etg.getFlowSourceVertex(), 
				etg.getFlowDestinationVertex()).getEdgeList();
		
		// If there is no shortest path to the destination, then the flow does
		// not have a primary path
		if (null == primaryPath) {
			return new VerifierResult(false, new LinkedList<DirectedEdge>());
		}
		
		// If traffic is blocked on an edge along the path, then the flow does 
		// not have a primary path
		List<DirectedEdge> partialPath = new LinkedList<DirectedEdge>();
		for (DirectedEdge edge : primaryPath) {
			if (edge.isBlocked()) {
				return new VerifierResult(false, partialPath);
			}
			partialPath.add(edge);
		}
		
		// If a desired path is provided, make sure it equals the computed path
		if (desiredPath != null) {
			return new VerifierResult(desiredPath.equals(primaryPath), 
					primaryPath);
		}
		
		// Return the computed path
		return new VerifierResult(true, primaryPath);
	}
}
