package edu.wisc.cs.arc.verifiers;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import edu.wisc.cs.arc.Driver;
import edu.wisc.cs.arc.Logger;
import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.DeviceGraph;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.ProcessGraph;
import edu.wisc.cs.arc.policies.Policy;
import edu.wisc.cs.arc.policies.PolicyException;
import edu.wisc.cs.arc.policies.PolicyFile;
import edu.wisc.cs.arc.policies.Policy.PolicyType;
import edu.wisc.cs.arc.quantverifiers.CongestionFree;
import edu.wisc.cs.arc.quantverifiers.RepairScheduler;
import edu.wisc.cs.arc.repair.VirtualDirectedEdge;
import edu.wisc.cs.arc.virl.Scenario;
import edu.wisc.cs.arc.virl.VirlOutputParser;

import gurobi.*;
/**
 * Perform tasks related to verification.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class VerificationTasks {
    
    /**
     * Run verifiers specified in settings.
     * @param settings
     * @param flowEtgs the per-flow ETGs to use for verification
     * @param deviceEtg the physical topology
     */
    public static void runVerifiers(Settings settings,
            Map<Flow, ? extends ExtendedTopologyGraph> flowEtgs,
            DeviceGraph deviceEtg) {

        // Verify currently blocked
        Map<Flow, VerifierResult> currentlyBlockedResults = null;
        if (settings.shouldVerifyCurrentlyBlocked()) {
            // Run verifier
            currentlyBlockedResults = runVerifier(
                    new CurrentlyBlocked(flowEtgs, settings));
        }

        // Verify equivalence
        if (settings.shouldVerifyEquivalence()) {
            // Deserialize the ETGs to compare against
            Map<Flow,ProcessGraph> comparisonEtgs =
                    new LinkedHashMap<Flow,ProcessGraph>();
            try {
                FileInputStream fileIn = new FileInputStream(
                        settings.getComparisonETGsFile());
                ObjectInputStream objIn = new ObjectInputStream(fileIn);
                try {
                    while (true) {
                        Object obj = objIn.readObject();
                        if (obj instanceof ProcessGraph) {
                            ProcessGraph processEtg = (ProcessGraph)obj;
                            comparisonEtgs.put(processEtg.getFlow(),processEtg);
                        }
                    }
                } catch (EOFException e) {
                    // Stop reading
                } finally {
                    objIn.close();
                    fileIn.close();
                }
            } catch(IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            // Run verifier
            runVerifier(new Equivalent(flowEtgs, settings), comparisonEtgs);
        }

        // Verify always blocked
        Map<Flow, VerifierResult> alwaysBlockedResults = null;
        if (settings.shouldVerifyAlwaysBlocked()) {
            // Run verifier
            alwaysBlockedResults = runVerifier(
                    new AlwaysBlocked(flowEtgs, settings));
        }

        // Verify always reachable
        Map<Flow, VerifierResult> alwaysReachableResults = null;
        if (settings.shouldVerifyAlwaysReachable()) { 
            // Run verifier
            alwaysReachableResults = runVerifier(
                    new AlwaysReachable(flowEtgs, settings), 
                    settings.getAlwaysReachableFailureCount());
        }
        
        // Verify always traverse waypoint
        Map<Flow, VerifierResult> alwaysWaypointResults = null;
        if (settings.shouldVerifyAlwaysWaypoint()) {
            // Run verifier
            alwaysWaypointResults = runVerifier(
                    new AlwaysWaypoint(flowEtgs, settings));
        }

        // Verify always isolated
        if (settings.shouldVerifyAlwaysIsolated()) {
             // Run verifier
             runVerifierMultiArg(new AlwaysIsolated(flowEtgs, settings), 
                     new ArrayList<Flow>(flowEtgs.keySet()));
        }
        
        // Verify primary path
        Map<Flow, VerifierResult> primaryPathsResults = null;
        if (settings.shouldVerifyPrimaryPath()) {
        	// Run verifier
        	primaryPathsResults = runVerifier(
        			new PrimaryPath(flowEtgs, settings));
        }

        // Verify path equivalent
        if (settings.shouldVerifyPathEquivalent()) {
            // Limit to internal flows, since we are not path-equivalent for 
            // flows with an external source/destination
            Map<Flow, ExtendedTopologyGraph> internalFlowEtgs =
                    new LinkedHashMap<Flow, ExtendedTopologyGraph>();
            for (Flow flow : flowEtgs.keySet()) {
                if (flow.getSource().isInternal()
                        && flow.getDestination().isInternal()) {
                    internalFlowEtgs.put(flow, flowEtgs.get(flow));
                }
            }
            
            // Get list of scenarios
            VirlOutputParser virlOutputParser = new VirlOutputParser(
                    settings.getFIBfile(), deviceEtg, settings.getLogger());
            Map<Scenario,Map<Flow,VerifierResult>> results =
                    new LinkedHashMap<Scenario,Map<Flow,VerifierResult>>();

            // Run verification
            runVerifierMultiArg(new PathEquivalent(internalFlowEtgs, 
                    settings), virlOutputParser.parse());
        }

        // Ouantitative Verify Congestion-Free
        if (settings.shouldVerifyCongestionFree()) {
            Map<Flow, ExtendedTopologyGraph> internalFlowEtgs =
                    new LinkedHashMap<Flow, ExtendedTopologyGraph>();
            for (Flow flow : flowEtgs.keySet()) {
                if (flow.getSource().isInternal()
                        && flow.getDestination().isInternal()) {
                    internalFlowEtgs.put(flow, flowEtgs.get(flow));
                }
            }

            try {
                System.out.println("Quantitative Verifier");
                CongestionFree cfVerifier = new CongestionFree(internalFlowEtgs, deviceEtg, settings);
                cfVerifier.verify();
            } catch (GRBException e) {
                System.out.println(e.getMessage());
            }
        }

        if (settings.shouldRepairCongestionFree()) {
            Map<Flow, ExtendedTopologyGraph> internalFlowEtgs =
                    new LinkedHashMap<Flow, ExtendedTopologyGraph>();
            for (Flow flow : flowEtgs.keySet()) {
                if (flow.getSource().isInternal()
                        && flow.getDestination().isInternal()) {
                    internalFlowEtgs.put(flow, flowEtgs.get(flow));
                }
            }

            try {
                System.out.println("Quantitative Repair");
                RepairScheduler cfRepair = new RepairScheduler(internalFlowEtgs, deviceEtg, settings);
                cfRepair.repair();
            } catch (GRBException e) {
                System.out.println(e.getMessage());
            }
        }

        // Save policies
        if (settings.shouldSavePolicies()) {
            VerificationTasks.generatePolicies(settings, alwaysBlockedResults,
                    alwaysReachableResults, alwaysWaypointResults, 
                    primaryPathsResults);
        }
    }
    
    /**
     * Run a specific verifier.
     * @param verifier the verifier to run
     * @param settings
     * @return the verification results for each flow
     */
    private static Map<Flow, VerifierResult> runVerifier(Verifier verifier) {
        return runVerifier(verifier, null);
    }
    
    /**
     * Run a specific verifier.
     * @param verifier the verifier to run
     * @param arg optional additional argument to pass to the verifier
     * @return the verification results for each flow
     */
    private static Map<Flow, VerifierResult> runVerifier(
            Verifier verifier, Object arg) {
        return runVerifierMultiArg(verifier, 
                Arrays.asList(new Object[]{arg})).get(arg);
    }
    
    /**
     * Run a specific verifier for every provided argument.
     * @param verifier the verifier to run
     * @param args optional additional arguments to pass to the verifier
     * @return the verification results for each argument for each flow
     */
    private static Map<Object, Map<Flow, VerifierResult>> runVerifierMultiArg(
            Verifier verifier, List<? extends Object> args) {
        Logger logger = verifier.settings.getLogger();
        
        // Run verification
        long startTime = System.currentTimeMillis();
        Map<Object, Map<Flow, VerifierResult>> results = 
                new LinkedHashMap<Object, Map<Flow, VerifierResult>>();
        for (Object arg : args) {
            results.put(arg, verifier.verifyAll(arg));
        }
        long endTime = System.currentTimeMillis();
        
        // Output time to verify
        String verifierName = verifier.getClass().getSimpleName();
        logger.info("TIME: " + verifierName + " " + (endTime-startTime) +" ms");

        // Output results
        if (!verifier.settings.shouldSummarizeVerificationResults()) {
            logger.info("*** " + verifierName + " ***");
            logger.info("RESULTS_START: " + verifierName);
            for (Object arg : results.keySet()) {
                String argString = "";
                if (arg != null) {
                    argString = " " + arg.toString();
                }
                    
                Map<Flow, VerifierResult> argResults = results.get(arg);
                for (Entry<Flow, VerifierResult> result : argResults.entrySet()) {
                    logger.info("\t" + result.getValue().propertyHolds()
                            + "\t" + result.getKey() + argString);
                }
            }
            logger.info("RESULTS_END: " + verifierName);
        }
        return results;
    }
    
	/**
	 * Generate and save policies from verification results.
	 * @param settings
	 * @param alwaysBlocked results from the always blocked verifier
	 * @param alwaysReachable results from the always reachable verifier
     * @param alwaysWaypoint results from the always traverse waypoint verifier
	 * @param primaryPath results from the primary path verifier
	 */
	public static void generatePolicies(Settings settings,
			Map<Flow, VerifierResult> alwaysBlocked,
			Map<Flow, VerifierResult> alwaysReachable,
			Map<Flow, VerifierResult> alwaysWaypoint,
			Map<Flow, VerifierResult> primaryPath) {
		Logger logger = settings.getLogger();
		logger.info("*** Save Policies ***");
		logger.info("RESULTS_START: Policies");

		// Generate policies from verification results
		List<Policy> policies = new ArrayList<Policy>();
		if (alwaysBlocked != null) {
			for (Entry<Flow, VerifierResult> entry : alwaysBlocked.entrySet()) {
				if (entry.getValue().propertyHolds()) {
					Policy policy = new Policy(Policy.PolicyType.ALWAYS_BLOCKED,
							entry.getKey());
					policies.add(policy);
					logger.info(policy.toString());
				}
			}
		}
		if (alwaysReachable != null) {
			for (Entry<Flow, VerifierResult> entry : alwaysReachable.entrySet()) {
				if (entry.getValue().propertyHolds()) {
					Policy policy = new Policy(
							Policy.PolicyType.ALWAYS_REACHABLE, entry.getKey(),
							settings.getAlwaysReachableFailureCount());
					policies.add(policy);
					logger.info(policy.toString());
				}
			}
		}
		if (alwaysWaypoint != null) {
            for (Entry<Flow, VerifierResult> entry : alwaysWaypoint.entrySet()) {
                if (entry.getValue().propertyHolds()) {
                    Policy policy = new Policy(Policy.PolicyType.ALWAYS_WAYPOINT,
                            entry.getKey());
                    policies.add(policy);
                    logger.info(policy.toString());
                }
            }
        }
		if (primaryPath != null) {
		    for (Entry<Flow, VerifierResult> entry : primaryPath.entrySet()) {
		        if (entry.getValue().propertyHolds()) {
					List<VirtualDirectedEdge> path =
                            new LinkedList<VirtualDirectedEdge>();
                    for (DirectedEdge edge :
                        entry.getValue().getSatisfyingExample()) {
                        path.add(new VirtualDirectedEdge(edge));
                    }
                    Policy policy = new Policy(Policy.PolicyType.PRIMARY_PATH,
                            entry.getKey(), path);
                    policies.add(policy);
					logger.info(policy.toString());
		        }
		    }
		}
		logger.info("RESULTS_END: Policies");
        System.out.println("COUNT: policies " + policies.size());

		// Save policies
		try {
			PolicyFile.savePolicies(policies, settings.getPoliciesSaveFile());
		} catch(PolicyException e) {
			logger.error("Failed to save policies");
			e.printStackTrace();
		}
	}
    
    /**
     * Verify whether a set of provided policies are satisfied and provides
     * counter examples for the violated policies.
     * @param settings
     * @param policiesByFlow the policies to verify, grouped by flow
     * @param flowEtgs the per-flow ETGs to use for verification
     * @return a list of violated policies with corresponding counter examples
     */
    public static Map<Policy, List<DirectedEdge>> verifyPolicies(
            Settings settings, Map<Flow, List<Policy>> policiesByFlow,
            Map<Flow, ? extends ExtendedTopologyGraph> flowEtgs) {
        // Flatten list of policies
        List<Policy> policies = new ArrayList<Policy>();
        for (List<Policy> policiesForFlow : policiesByFlow.values()) {
            policies.addAll(policiesForFlow);
        }
        
        return verifyPolicies(settings, policies, flowEtgs);
    }
    
    /**
     * Verify whether a set of provided policies are satisfied and provides
     * counter examples for the violated policies.
     * @param settings
     * @param policies the policies to verify
     * @param flowEtgs the per-flow ETGs to use for verification
     * @return a list of violated policies with corresponding counter examples
     */
    public static Map<Policy, List<DirectedEdge>> verifyPolicies(
            Settings settings, List<Policy> policies,
            Map<Flow, ? extends ExtendedTopologyGraph> flowEtgs) {

        AlwaysBlocked verifierAB = new AlwaysBlocked(flowEtgs, settings);
        AlwaysReachable verifierAR = new AlwaysReachable(flowEtgs, settings);
        AlwaysWaypoint verifierAW = new AlwaysWaypoint(flowEtgs, settings);
        AlwaysIsolated verifierAI = new AlwaysIsolated(flowEtgs, settings);
        PrimaryPath verifierPP = new PrimaryPath(flowEtgs, settings);

        Map<Policy, List<DirectedEdge>> violatedPolicies = 
                new LinkedHashMap<Policy, List<DirectedEdge>>();
        
        for (Policy policy : policies){
            // Make sure ETG exists for flow to which policy applies
            Flow policyFlow = policy.getTrafficClass();
            if (!flowEtgs.containsKey(policyFlow)) {
                continue;
            }
            
            VerifierResult verifierResult = null;
            switch(policy.getType()){
                case ALWAYS_BLOCKED:
                    verifierResult = verifierAB.verify(policyFlow, null);
                    break;
                case ALWAYS_REACHABLE:
                    verifierResult = verifierAR.verify(policyFlow,
                    		policy.getParameter());
                    break;
                case ALWAYS_WAYPOINT:
                    verifierResult = verifierAW.verify(policyFlow, null);
                    break;
                case ALWAYS_ISOLATED:
                    verifierResult = verifierAI.verify(policyFlow, null);
                    break;
                case PRIMARY_PATH:
                    verifierResult = verifierPP.verify(policyFlow,
                    		policy.getParameter());
                    break;
            }
            if (verifierResult!=null){
                if (!verifierResult.propertyHolds()){
                    if (null == verifierResult.getCounterExample()){
                        throw new VerifierException("No counter-example for " 
                                + policy); 
                    }
                    violatedPolicies.put(policy, 
                            verifierResult.getCounterExample());
                }
            }
        }

        return violatedPolicies;
    }
}
