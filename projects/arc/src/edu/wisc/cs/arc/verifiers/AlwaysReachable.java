
package edu.wisc.cs.arc.verifiers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.HashSet;
import javafx.util.Pair; 

import org.jgrapht.Graphs;
import org.jgrapht.alg.flow.GusfieldGomoryHuCutTree;
import org.jgrapht.graph.SimpleWeightedGraph;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.Device;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.Process;
import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.graphs.Vertex;

/**
 * Checks if a source and destination can always communicate when there are
 * fewer than k failures in the network.
 * @author Aaron Gember-Jacobson (agember@cs.wisc.edu)
 */
@SuppressWarnings("rawtypes")
public class AlwaysReachable extends Verifier {

	/**
	 * Construct a verifier.
	 * @param etgs the extended topology graphs to use for verification
	 * @param settings the settings to use during verification
	 */
	public AlwaysReachable(Map<Flow, ? extends ExtendedTopologyGraph> etgs,
			Settings settings) {
		super(etgs, settings);
	}

	/**
	 * Check the property for a specific flow.
	 * @param flow flow for which to check the property
	 * @param arg maximum number of link failures to tolerate
	 * @return verification result
	 */
	@Override
	public VerifierResult verify(Flow flow, Object arg) {
		if (!(arg instanceof Integer)) {
			throw new VerifierException("Argument must be an integer");
		}
		int maxFailuresExclusive = (Integer)arg;

		// Get ETG
		ExtendedTopologyGraph etg = this.etgs.get(flow);
		if (null == etg) {
			throw new VerifierException("No ETG for flow "+flow);
		}

		HashSet<Pair<String, String>> deviceEdges = new HashSet<>();

		// Create pruned, unit-weight undirected graph
		SimpleWeightedGraph<Vertex,DirectedEdge> unitWeightGraph =
				new SimpleWeightedGraph<Vertex,DirectedEdge>(
						DirectedEdge.class);
		Graphs.addAllVertices(unitWeightGraph, etg.getGraph().vertexSet());
		Iterator<DirectedEdge> iterator = etg.getEdgesIterator();
		while (iterator.hasNext()) {
			DirectedEdge edge = iterator.next();

			// Prune blocked edges
			if (edge.isBlocked()){
				continue;
			}

			if (EdgeType.INTER_DEVICE == edge.getType()) {
				String r1 = ((ProcessVertex) edge.getSource()).getProcess().getDevice().toString();
				String r2 = ((ProcessVertex) edge.getDestination()).getProcess().getDevice().toString();
				if (!deviceEdges.contains(new Pair<String, String>(r1, r2)) 
					&& !deviceEdges.contains(new Pair<String, String>(r2, r1))) {
					DirectedEdge newEdge = unitWeightGraph.addEdge(edge.getSource(),
							edge.getDestination());
					
					deviceEdges.add(new Pair<String, String>(r1, r2));
					if (newEdge != null) {
						unitWeightGraph.setEdgeWeight(newEdge, 1);
					}
				}
			} else {
				DirectedEdge newEdge = unitWeightGraph.addEdge(edge.getSource(),
					edge.getDestination());
				
				if (newEdge != null) {
					unitWeightGraph.setEdgeWeight(newEdge,
						DirectedEdge.INFINITE_WEIGHT);
				}
			}
		}

		// We can tolerate up to maxFailures failures and still have
		// reachability if the min cut (or max flow) onthe unit weight graph is
		// at least one unit more than maxFailures
		GusfieldGomoryHuCutTree<Vertex,DirectedEdge> minCutAlgorithm =
				new GusfieldGomoryHuCutTree<Vertex,DirectedEdge>(unitWeightGraph);
		minCutAlgorithm.calculateMinCut(etg.getFlowSourceVertex(),
				etg.getFlowDestinationVertex());
		ArrayList<DirectedEdge> counter_examples =new ArrayList(minCutAlgorithm.getCutEdges());

		double minCut = minCutAlgorithm.getCutCapacity();
        /*if (flow.getDestination().getStartIp().asLong()==0) {
            settings.getLogger().debug("MINCUT: " + minCut + " " + flow);
		}*/
		// System.out.println("Mincut is " + Double.toString(minCut));
		// for (DirectedEdge e: counter_examples) {
		// 	System.out.println(e.toString());
		// }
		boolean pass = minCut > maxFailuresExclusive;

		return new VerifierResult(pass,pass?null: counter_examples);
	}
}
