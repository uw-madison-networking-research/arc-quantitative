package edu.wisc.cs.arc.verifiers;

import java.util.List;

import edu.wisc.cs.arc.graphs.DirectedEdge;

/**
 * Stores the results of a verifier.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class VerifierResult {
    /** Does the property hold? */
    private boolean propertyHolds;
    
    /** Counter or satisfying example */
    private List<DirectedEdge> example;
    
    /**
     * Creates a verifier result.
     * @param propertyHolds true if the property holds, otherwise false
     * @param example counter example if the property doesn't hold, 
     *          otherwise a satisfying example
     */
    public VerifierResult(boolean propertyHolds, List<DirectedEdge> example) {
        this.propertyHolds = propertyHolds;
        this.example = example;
    }
    
    /**
     * Does the property hold?
     * @return true if the property holds, otherwise false
     */
    public boolean propertyHolds() {
        return this.propertyHolds;
    }
    
    /**
     * Gets the counter-example.
     * @return counter-example, or null if the property holds
     */
    public List<DirectedEdge> getCounterExample() {
        return (this.propertyHolds ? null : this.example);
    }
    
    /**
     * Gets the satisfying example.
     * @return counter-example, or null if the property holds
     */
    public List<DirectedEdge> getSatisfyingExample() {
        return (this.propertyHolds ? this.example : null);
    }
}
