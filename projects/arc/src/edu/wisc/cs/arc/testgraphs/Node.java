package edu.wisc.cs.arc.testgraphs;

import java.util.HashSet;
import java.io.IOException;

/**
 * Represents a node (vertex) in a graph.
 * @author Ruchit Shrestha (rshrestha@colgate.edu)
 */
public class Node{

    /** Name of the node */
    private String name;

    /** Set of all OUTGOING edges of the node */
    private HashSet<Edge> edgeList;

    /**
     * Create a Node.
     * @param name name of the Node
     */
    public Node(String name){
        this.name = name.toLowerCase();
        edgeList = new HashSet<Edge>();
    }

    /**
     * Add an edge to the node
     * @param target target of the edge to be created
     * @param weight cost of the edge to be created
     * @param blocked boolean indicating if the link should be blocked
     */
    public void addEdge(String target, double weight, boolean blocked){

        edgeList.add(new Edge(target, weight, blocked));
    }

    /**
     * Get the name of the Node
     * @return name of the Node
     */
    public String getName(){
        return name;
    }

    /**
     * Get the list of outgoing edges of the node
     * @return list of edges from the node
     */
    public HashSet<Edge> getEdgeList(){
        return edgeList;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;

        if (!(obj instanceof Node)) {
            if (null == obj) {
                System.out.println("Extra vertex: " + this.name);
            }
            return false;
        }

        Node other = (Node) obj;
        if (this.name.equals(other.name)){
            if (this.edgeList.equals(other.edgeList)) {
                return true;
            } else {
                for (Edge edge : other.edgeList) {
                    if (!this.edgeList.contains(edge)) {
                        System.out.println("\tMissing edge: " + this.name + " "
                                + edge);
                    }
                }
                for (Edge edge : this.edgeList) {
                    if (!other.edgeList.contains(edge)) {
                        System.out.println("\tExtra edge: " + this.name + " "
                                + edge);
                    }
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode(){
        int result = 17;
        result = 31*result +name.hashCode();
        result = 31*result +edgeList.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(this.name);
        for (Edge edge : this.edgeList) {
            result.append("\n\t\t" + edge);
        }
        return result.toString();
    }
}
