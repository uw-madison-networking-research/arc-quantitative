package edu.wisc.cs.arc.testgraphs;

/**
 * Edge represents a directed link in a .gv graph file.
 * @author Ruchit Shrestha (rshrestha@colgate.edu=)
 */
import java.io.IOException;

public class Edge{
    /** Name of the target node */
    private String target;

    /** Cost of the edge */
    private double weight;

    /** Is the edge blocked? */
    private boolean blocked;

    /**
     * Create an edge.
     * @param t name of the target Node
     * @param w cost of the edge
     * @param b boolean indicating if edge is blocked
     */
    public Edge(String t, double w, boolean b){
        target = t.toLowerCase();
        weight = w;
        blocked = b;
    }

    /**
     * Get the name of target node of the edge.
     * @return name of target node
     */
    public String getNode(){
        return target;
    }

    /*
     * Get the weight associated with the directed edge.
     * @return Cost (Weight) of the edge.
     */
    public double getWeight(){
        return weight;
    }

    @Override
    public boolean equals(Object obj){

        if (obj instanceof Edge){
            Edge other = (Edge)obj;
            return (this.target.equals(other.target)
                    && (this.weight == other.weight)
                    && (this.blocked == other.blocked));
        }
        return false;
    }

    @Override
    public int hashCode(){
        return this.toString().hashCode();
    }

    @Override
    public String toString() {
        return "-[" + weight + (blocked ? " blocked" : "") + "]-> " + target;
    }

}
