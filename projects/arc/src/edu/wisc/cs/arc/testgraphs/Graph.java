package edu.wisc.cs.arc.testgraphs;

import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;

import java.util.HashMap;
import java.util.Set;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.BufferedReader;

/**
 * A FlowETG graph of a particular traffic class.
 * @author Aaron Gember-Jacobson (agember@cs.wisc.edu)
 * @author Ruchit Shrestha (rshrestha@colgate.edu)
 */
public class Graph{
    /** Map of name of nodes in graph to corresponding Node objects */
    private HashMap<String,Node> nodeMap;

    /**
     * Create a Graph Object
     */
    public Graph(){
        nodeMap = new HashMap<String,Node>();
    }

    /**
     * Add a new node to the graph
     * @param name name of the node
     * @param node Node object containing the node's information
     */
    public void addNode(String name, Node node){
        nodeMap.put(name.toLowerCase(), node);
    }

    /**
     * Creates a graph object from an extended topology graph (ETG).
     * @param etg the extended topology graph to convert
     * @return Graph object encapsulating the etg
     */
    public static Graph etgToGraphObject(ExtendedTopologyGraph etg) {
        return toGraphObject(new StringReader(etg.toGraphviz()));
    }

    /**
     * Creates a graph object from an .gv file.
     * @param fileName path to the .gv file to be used for creating the Graph.
     * @return Graph object representing the gv file
     */
    public static Graph gvToGraphObject(String fileName){
        try {
            return toGraphObject(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Creates a Graph object from a Reader for the contents of .gv fileName
     * @param input reader corresponding to the contents of the graphviz format of etg
     * @return graph object created from the input gv file contents
     */
    private static Graph toGraphObject(Reader input){
        Graph graph = new Graph();
        HashMap<String, String> srcToDst = null; //represents a Collection of all edges in the graph
        BufferedReader bufferedReader = null;

        try{
            bufferedReader = new BufferedReader(input);
            String line;
            String[] splitResult;
            srcToDst = new HashMap<String, String>();
            while ((line = bufferedReader.readLine())!= null){
                // all relevant lines with edges in the gv file take the form : [spaces]"SRC"->"DST" [nodeName="COST"]
                // the following split should contain 7 pieces if line represents an edge
                splitResult = line.split("\"");
                if (splitResult.length > 6){
                    //THIS MEANS THE LINE CONTAINS A LINK OF THE FORM:
                    // "SRC NAME" -> "DST NAME" [nodeName = "WEIGHT"]
                    // Obtain parts of line
                    String srcName = splitResult[1];
                    String dstName = splitResult[3];
                    String nodeName = splitResult[5];

                    // Obtain source and destination vertices
                    srcToDst.put(srcName, dstName);
                    Node srcNode = graph.nodeMap.get(srcName);
                    if (null == srcNode) {
                        srcNode = new Node(srcName);
                        graph.addNode(srcName, srcNode);
                    }
                    Node dstNode = graph.nodeMap.get(dstName);
                    if (null == dstNode) {
                        dstNode = new Node(dstName);
                        graph.addNode(dstName, dstNode);
                    }

                    // Obtain edge weight
                    String[] nodeNameParts = nodeName.split(" ");
                    String weight = nodeNameParts[0];

                    // Determine if edge is blocked
                    boolean blocked = false;
                    // If a nodeName has multiple parts, then it must contain blocked
                    if (nodeNameParts.length > 1) {
                        blocked = true;
                    }

                    // Add edge
                    srcNode.addEdge(dstNode.getName(), Double.parseDouble(weight),
                            blocked);
                } else {
                    if (splitResult.length>1){
                        //this is checking to see if the line is a description of the nodes that come at the end of GraphViz files.
                        //Takes the form: <"NODE"[shape=SOMESHAPE, style=FILLED/other, fillcolor=SOMECOLOR]>
                        String nodeName = splitResult[1]; //this is the name of the node being described.

                        //check if the node already exists in the graph's nodemap
                        //if not, this must be an ISOLATED node in the ETG.
                        if (!srcToDst.containsKey(nodeName)&&!srcToDst.containsValue(nodeName)&& splitResult[0].trim().length()==0){
                            //the following lines add all isolated nodes in the gv file to the Graph object.
                            //the last test in the if condition is for avoiding the last line of GV files of the format <nodeName="SubnetSRC -> SubnetDST">
                            //this information could potentially be included in the graph information

                            Node isoNode = new Node(nodeName);
                            graph.addNode(nodeName,isoNode);
                        }
                    }
                }
            }
        }catch(IOException ioe){
            ioe.printStackTrace();
        }finally{
            if (bufferedReader!=null){
                try{
                    bufferedReader.close();
                }catch(IOException ioe){
                    ioe.printStackTrace();
                }
            }
        }
        return graph;
    }


    /**
     * Get a map of all nodes in the graph
     * @return map of names of nodes in the graph to the corresponding Node objects
     */
    public HashMap<String,Node> getNodeMap(){
        return nodeMap;
    }

    @Override
    public boolean equals(Object obj){
        if (obj instanceof Graph){
            Graph other = (Graph) obj;
            if (this.nodeMap.size() != other.nodeMap.size()){
                System.out.println("Unindentical cardinality!");
                return false;
            }
            return this.nodeMap.equals(other.nodeMap);
        }

        return false;
    }

    @Override
    public int hashCode(){
        return nodeMap.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Node node : nodeMap.values()) {
            result.append("\t" + node + "\n");
        }
        return result.toString();
    }
}
