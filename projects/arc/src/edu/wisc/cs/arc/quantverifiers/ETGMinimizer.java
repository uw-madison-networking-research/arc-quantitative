package edu.wisc.cs.arc.quantverifiers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.util.Pair; 

import org.jgrapht.alg.shortestpath.DijkstraShortestPath;

import edu.wisc.cs.arc.graphs.Device;
import edu.wisc.cs.arc.graphs.DeviceGraph;
import edu.wisc.cs.arc.graphs.DeviceVertex;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.Process;
import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.graphs.Vertex;
import edu.wisc.cs.arc.graphs.Vertex.VertexType;
import edu.wisc.cs.arc.verifiers.VerifierException;
import gurobi.*;

public class ETGMinimizer implements Runnable {
     /** The Congestion-free verifier */
    private CongestionFree cfVerifier = null;
    private RepairScheduler cfRepair = null;

    private Flow flow;

	/** The ETG to minimize */
    private ExtendedTopologyGraph etg;

    /** The device ETG */
    private DeviceGraph deviceEtg;

    /** Maximum number of simultaneous link failures */
    private int maxLinkFailures;

    private GRBEnv env;
    private GRBModel _solver;

    private Map<Vertex, GRBVar> bestRouteVariables;
    private HashMap<Device, Map<Device, GRBVar>> linkFailureVariables;

    private HashMap<Pair<DeviceVertex, DeviceVertex>, Double> linkFailureProb;
    private double threshold;

    public ETGMinimizer(CongestionFree cfVerifier, Flow flow,
        ExtendedTopologyGraph etg, DeviceGraph deviceEtg, 
    	int maxLinkFailures) {
        this.cfVerifier = cfVerifier;
        this.flow = flow;
    	this.etg = etg;
    	this.deviceEtg = deviceEtg;
    	this.maxLinkFailures = maxLinkFailures;

    	try {
            env = new GRBEnv("quark.log");
            env.set(GRB.IntParam.LogToConsole, 0);
            _solver = new GRBModel(env);
        } catch (GRBException e) {
            e.printStackTrace();
        }

        bestRouteVariables = new HashMap<Vertex, GRBVar>();
        linkFailureVariables = new HashMap<Device, Map<Device, GRBVar>>();
    }

    public ETGMinimizer(RepairScheduler cfRepair, Flow flow,
        ExtendedTopologyGraph etg, DeviceGraph deviceEtg, 
    	int maxLinkFailures) {
        this.cfRepair = cfRepair;
        this.flow = flow;
    	this.etg = etg;
    	this.deviceEtg = deviceEtg;
    	this.maxLinkFailures = maxLinkFailures;

    	try {
            env = new GRBEnv("quark.log");
            env.set(GRB.IntParam.LogToConsole, 0);
            _solver = new GRBModel(env);
        } catch (GRBException e) {
            e.printStackTrace();
        }

        bestRouteVariables = new HashMap<Vertex, GRBVar>();
        linkFailureVariables = new HashMap<Device, Map<Device, GRBVar>>();
    }


    GRBVar getBestRouteVar(Vertex v) throws GRBException {
        if (bestRouteVariables.get(v) == null) {
            bestRouteVariables.put(v, 
                    _solver.addVar(0.0, 10000, 0.0, GRB.CONTINUOUS,
                        "Best:" + v.toString()));
        }
        return bestRouteVariables.get(v);
    }

    GRBVar getLinkFailureVar(Process srcp, Process dstp) throws GRBException {
        return getLinkFailureVar(srcp.getDevice(), dstp.getDevice());
    }

    GRBVar getLinkFailureVar(Device src, Device dst) throws GRBException {
        if (src == dst) { 
            throw new VerifierException("Does not hold for intra-device edges");
        }

        if (linkFailureVariables.get(src) == null) {
            linkFailureVariables.put(src, new HashMap<Device, GRBVar>());
        } 

        if (linkFailureVariables.get(src).get(dst) == null) {
            GRBVar failVar = _solver.addVar(0, 1, 0, GRB.BINARY,
                "Fail:" + src.toString() + ":" + dst.toString());
            linkFailureVariables.get(src).put(dst, failVar);

            // Add failVar to (dst,src) 
            if (linkFailureVariables.get(dst) == null) {
                linkFailureVariables.put(dst, new HashMap<Device, GRBVar>());
            }
            linkFailureVariables.get(dst).put(src, failVar);
        }
        return linkFailureVariables.get(src).get(dst);
    }

    private void initializeVariables() throws GRBException {
    	Iterator<Vertex> vertexItr = etg.getVerticesIterator();
            
        while (vertexItr.hasNext()) {
            Vertex v = vertexItr.next();
            getBestRouteVar(v);
        }

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
            
            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex)e.getSource();
                DeviceVertex dstdv = (DeviceVertex)e.getDestination();
                getLinkFailureVar(srcdv.getDevice(), dstdv.getDevice());
            }
        }
        _solver.update();
    }

    private void addBestRouteConstraints() throws GRBException {       
        Iterator<Vertex> vertexItr = etg.getVerticesIterator();
        
        while (vertexItr.hasNext()) {
            Vertex v = vertexItr.next();

            if (v.getType() == VertexType.DESTINATION) {
                _solver.addConstr(getBestRouteVar(v), GRB.EQUAL, 0.0, "DST");
            }

            Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
            for (DirectedEdge e : edges) {
                // If link is not failed, triangle inequality holds
                GRBLinExpr bestSmallest = new GRBLinExpr();
                bestSmallest.addTerm(1.0, getBestRouteVar(
                    (Vertex) e.getDestination()));
                bestSmallest.addConstant(e.getWeight());
                bestSmallest.addTerm(-1.0, getBestRouteVar(v));

                if (e.getType() == EdgeType.INTER_DEVICE) {
                    ProcessVertex srcpv = (ProcessVertex)e.getSource();
                    ProcessVertex dstpv = (ProcessVertex)e.getDestination();
                    GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(),
                        dstpv.getProcess());

                    bestSmallest.addTerm(100000, linkFail);
                }

                _solver.addConstr(bestSmallest, GRB.GREATER_EQUAL, 0.0, "");
            }
        }
    }

    public void useFailureProbabilities(HashMap<Pair<DeviceVertex, DeviceVertex>, Double> lfp, double thres) {
        this.linkFailureProb = lfp;
        this.threshold = thres;
    }

    /*
    private void addLinkFailureBoundConstraints() throws GRBException {
        HashSet<GRBVar> linkFailureVars = new HashSet<GRBVar>();
        GRBLinExpr failureSum = new GRBLinExpr();

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
            
            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex)e.getSource();
                DeviceVertex dstdv = (DeviceVertex)e.getDestination();
                GRBVar linkFail = getLinkFailureVar(srcdv.getDevice(),
                    dstdv.getDevice());
                if (! linkFailureVars.contains(linkFail)) {
                    failureSum.addTerm(1.0, linkFail);
                    linkFailureVars.add(linkFail);
                }
            }
        }
        
        _solver.addConstr(failureSum, GRB.LESS_EQUAL, this.maxLinkFailures, "");
    }
    */

    private void addLinkFailureBoundConstraints() throws GRBException {
        HashSet<GRBVar> linkFailureVars = new HashSet<GRBVar>();
        GRBLinExpr failureSum = new GRBLinExpr();

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
            
            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex)e.getSource();
                DeviceVertex dstdv = (DeviceVertex)e.getDestination();
                GRBVar linkFail = getLinkFailureVar(srcdv.getDevice(),
                    dstdv.getDevice());
                if (! linkFailureVars.contains(linkFail)) {
                    failureSum.addTerm(1.0, linkFail);
                    linkFailureVars.add(linkFail);
                }
            }
        }
        
        _solver.addConstr(failureSum, GRB.EQUAL, this.maxLinkFailures, "linkFailureBound");
    }

    private void maximizeSourceDistance() throws GRBException {
    	/* Add source distance objective */
    	Vertex src = etg.getFlowSourceVertex();
    	GRBLinExpr objective = new GRBLinExpr();
    	objective.addTerm(1.0, getBestRouteVar(src));
        _solver.setObjective(objective, GRB.MAXIMIZE);
    }

    public void printModelToFile() throws GRBException {
		try {
	    	BufferedWriter writer = new BufferedWriter(new FileWriter(".quark-etg-minimizer.log"));
	    	
	    	for (GRBVar var : _solver.getVars()) {
	    		writer.write(var.get(GRB.StringAttr.VarName) + "\t" +
	    			Double.toString(var.get(GRB.DoubleAttr.X)) + "\n");
	    	}
	    	writer.close();
	    } catch (IOException e) {}
	}

    public void run() {
        try {
            minimize();
        } catch (GRBException e) {}
    }

    /** returns the minimized ETG */
    public int minimize() throws GRBException {
    	initializeVariables();
    	addBestRouteConstraints();
    	addLinkFailureBoundConstraints();
    	maximizeSourceDistance();
        // printModelToFile();
        // _solver.write(".quark.lp");
        _solver.optimize();

        Vertex src = etg.getFlowSourceVertex();
        double maxSourceDist = getBestRouteVar(src).get(GRB.DoubleAttr.X);

        if (maxSourceDist == 10000) {
            // System.out.println("Distance is Infinity! Debug!");
            // this.cfVerifier.cutFlows.put(this.flow, true);
            // System.out.println("Cut flow " + this.flow.toString());
        }

       	boolean edgePruned;
        int edgeCount = etg.getEdgeCount();
       	int prunedEdgeCount = 0;
       	double distFromSrc = 0;
       	double distToDst = 0;

        ExtendedTopologyGraph etgClone = (ExtendedTopologyGraph) etg.clone();

        DijkstraShortestPath<Vertex, DirectedEdge<Vertex>> shortestPathComputer 
        = new DijkstraShortestPath<Vertex, DirectedEdge<Vertex>>(etg.getGraph());

        double sourceDist =  shortestPathComputer.getPathWeight(
                etg.getFlowSourceVertex(), etg.getFlowDestinationVertex());

        if (sourceDist == Double.POSITIVE_INFINITY) {
            // System.out.println("Something wrong with this ETG" + sourceDist);
            if (this.cfVerifier != null)
                this.cfVerifier.etgs.remove(flow);
            if (this.cfRepair != null)
                this.cfRepair.etgs.remove(flow);
            return -1;
        }

        if (sourceDist > maxSourceDist) {
            maxSourceDist = sourceDist;
        }


        List<DirectedEdge<Vertex>> prunedEdges = new ArrayList<DirectedEdge<Vertex>>();
   		Iterator<DirectedEdge<Vertex>> edgeItr = etg.getEdgesIterator();
   		while (edgeItr.hasNext()) {
            DirectedEdge<Vertex> e = edgeItr.next();
            if (e.getType() == EdgeType.ENDPOINT) continue;

            distFromSrc = shortestPathComputer.getPathWeight(
            	etg.getFlowSourceVertex(), e.getSource());
            distToDst = shortestPathComputer.getPathWeight(
            	e.getDestination(), etg.getFlowDestinationVertex());
            if (distFromSrc + e.getWeight() + distToDst > maxSourceDist) {
            	// Can prune the following edge.
            	prunedEdges.add(e);
            	prunedEdgeCount += 1;
            }
        }
        // Remove pruned edges from ETG
        for (DirectedEdge e : prunedEdges) {
            etg.removeEdge(e);
        }

        List<Vertex> prunedVertices = new ArrayList<Vertex>();
        Iterator<Vertex> vertexItr = etg.getVerticesIterator();
        while(vertexItr.hasNext()) {
            Vertex v = vertexItr.next();
            if (v.getType() == VertexType.SOURCE || v.getType() == VertexType.DESTINATION) continue;

            if (etg.getIncomingEdges(v).size() == 0 || etg.getOutgoingEdges(v).size() == 0) {
                // System.out.println("Vertex is redundant. Remove " + v.toString());
                prunedVertices.add(v);
            }
        }

        for (Vertex v : prunedVertices) {
            etg.removeVertex(v);
        }

        // make sure ETG is still intact in terms of connectivity
        shortestPathComputer = new DijkstraShortestPath<Vertex, DirectedEdge<Vertex>>(etg.getGraph());
        double dist = shortestPathComputer.getPathWeight(etg.getFlowSourceVertex(), etg.getFlowDestinationVertex());
        if (dist == Double.POSITIVE_INFINITY) {
            System.out.println("Something wrong with this ETG");
            System.out.println(maxSourceDist);
            System.out.println(dist);
            shortestPathComputer = new DijkstraShortestPath<Vertex, DirectedEdge<Vertex>>(etgClone.getGraph());
            for (DirectedEdge e : prunedEdges) {
                distFromSrc = shortestPathComputer.getPathWeight(
                    etgClone.getFlowSourceVertex(), e.getSource());
                distToDst = shortestPathComputer.getPathWeight(
                    e.getDestination(), etgClone.getFlowDestinationVertex());
                System.out.println(e.toString() + ": " + Double.toString(distFromSrc) + " " + Double.toString(distToDst) + " " + Double.toString(e.getWeight()));
            }
            etg = etgClone;
            return -1;
        }

        //System.out.println("Edges pruned: " + Integer.toString(prunedEdgeCount) + "/" + Integer.toString(edgeCount) + " in iterations " + Integer.toString(numIterations));
        return prunedEdgeCount;
    }
}