package edu.wisc.cs.arc.quantverifiers;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.Device;
import edu.wisc.cs.arc.graphs.DeviceVertex;
import edu.wisc.cs.arc.graphs.DeviceGraph;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.Process;
import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.graphs.Vertex;
import edu.wisc.cs.arc.graphs.Vertex.VertexType;
import edu.wisc.cs.arc.quantverifiers.ETGMinimizer;
import edu.wisc.cs.arc.quantverifiers.ModelValidator;
import edu.wisc.cs.arc.verifiers.AlwaysReachable;
import edu.wisc.cs.arc.verifiers.VerifierResult;
import edu.wisc.cs.arc.verifiers.VerifierException;

import gurobi.*;

import javafx.util.Pair;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Set;
import org.apache.commons.math3.distribution.WeibullDistribution;

class EdgeCount {
    public DirectedEdge e;
    public int count;

    public EdgeCount(DirectedEdge e, int count) {
        this.e = e;
        this.count = count;
    }
}

class CompareEdge implements Comparator<EdgeCount> {
    // Used for sorting in descending order of countance
    public int compare(EdgeCount a, EdgeCount b) {
        return (int) (b.count - a.count);
    }
}

public class CongestionFree {

    /** The ETGs to use for verification */
    public ConcurrentHashMap<Flow, ExtendedTopologyGraph> etgs;

    /** Settings for the verification process */
    public Settings settings;

    /** The device ETG */
    private DeviceGraph deviceEtg;

    /** Maximum number of simultaneous link failures */
    private int maxLinkFailures = 2;

    private boolean partitionedVerification = false;
    private int totalPartitions = 0;
    private int partitionNo = 0;

    private double totalVolume = 0; 

    private boolean parallelize = true;

    private GRBEnv env;
    private GRBModel _solver;

    private HashMap<Flow, GRBVar> flowVariables;
    private HashMap<Flow, Map<Vertex, GRBVar>> bestRouteVariables;
    private HashMap<Flow, Map<Vertex, Map<Vertex, GRBVar>>> forwardVariables;
    private HashMap<Device, Map<Device, GRBVar>> linkFailureVariables;
    private HashMap<Device, Map<Device, GRBVar>> linkCongestionVariables;
    private HashMap<Flow, Map<Vertex, GRBVar>> maxForwardVariables;
    private HashMap<Flow, Map<Vertex, GRBVar>> minForwardVariables;

    private HashMap<Flow, GRBVar> deltaSourceVariables;
    private HashMap<Flow, Map<Vertex, Map<Vertex, GRBVar>>> deltaVariables;
    private HashMap<Flow, Map<Vertex, GRBVar>> maxDeltaVariables;
    private HashMap<Flow, Map<Vertex, GRBVar>> minDeltaVariables;

    public ConcurrentHashMap<Flow, Boolean> cutFlows;
    public DirectedEdge<DeviceVertex> congestedLink;
    private GRBVar networkCongestionVar;
    private GRBConstr networkCongestion;
    private boolean checkCongestion = true;
    private PriorityQueue linkQueue;
    private HashMap<Pair<Device, Device>, HashSet<Flow>> linkFlowMap;
    private HashMap<Pair<Device, Device>, Integer> linkPartitionMap;
    public HashMap<Pair<Device, Device>, Double> linkCapacities;
    private HashMap<Pair<DeviceVertex, DeviceVertex>, Double> linkFailureProb;
    private double threshold = 0.01;

    private ModelValidator validator;

    public CongestionFree(Map<Flow, ExtendedTopologyGraph> etgs, DeviceGraph deviceEtg, Settings settings) {
        this.etgs = new ConcurrentHashMap(etgs);
        this.settings = settings;
        this.deviceEtg = deviceEtg;

        this.maxLinkFailures = this.settings.getCongestionFreeFailureCount();

        this.cutFlows = new ConcurrentHashMap<Flow, Boolean>();
        this.linkFlowMap = new HashMap<Pair<Device, Device>, HashSet<Flow>>();
        this.linkQueue = new PriorityQueue<EdgeCount>(1, new CompareEdge());

        this.totalPartitions = this.settings.getVerificationPartitioned();
        this.partitionNo = this.settings.getPartitionNo();
        if (this.totalPartitions > 0 && this.partitionNo >= 0){
            // Valid partitioning
            this.partitionedVerification = true;
        }
        this.linkPartitionMap = new HashMap<Pair<Device, Device>, Integer>();

        this.linkCapacities = new HashMap<Pair<Device, Device>, Double>();
        initializeLinkCapacities();
    }

    public void minimizeETGsParallel() throws GRBException {
        int numThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService pool = Executors.newFixedThreadPool(numThreads);

        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Runnable minimizeTask = new ETGMinimizer(this, flow, etg, deviceEtg, this.maxLinkFailures);
            // ((ETGMinimizer) minimizeTask).useFailureProbabilities(this.linkFailureProb, this.threshold);
            pool.execute(minimizeTask);
        }

        pool.shutdown();
        try {
            pool.awaitTermination(5000, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.out.println("Minimization interrupted. EXIT");
            System.exit(0);
        }
    }

    public void minimizeETGs() throws GRBException {
        int initialEdgeCount = 0;
        int prunedEdgeCount = 0;
        long startTime = System.currentTimeMillis();
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            initialEdgeCount += etg.getEdgeCount();
            int edgesPruned = new ETGMinimizer(this, flow, etg, deviceEtg, this.maxLinkFailures).minimize();
            if (edgesPruned == -1) {
                // Remove this ETG
                System.out.println(flow.toString());
            } else if (edgesPruned == -2) {
                // this.cutFlows.add(flow);
            } else {
                prunedEdgeCount += edgesPruned;
            }
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Time taken to minimize ETGs: " + (endTime - startTime) + "ms");
        System.out.println("Edges pruned are " + prunedEdgeCount + "/" + initialEdgeCount);
    }

    GRBModel getSolver() throws GRBException {
        return _solver;
    }

    GRBVar getFlowVar(Flow f) throws GRBException {
        if (flowVariables.get(f) == null) {
            flowVariables.put(f, _solver.addVar(0.0, 1.0, 0.0, GRB.BINARY, "Flow:" + f.toString()));
        }
        return flowVariables.get(f);
    }

    GRBVar getBestRouteVar(Flow f, Vertex v) throws GRBException {
        if (bestRouteVariables.get(f) == null) {
            bestRouteVariables.put(f, new HashMap<Vertex, GRBVar>());
        }

        if (bestRouteVariables.get(f).get(v) == null) {
            bestRouteVariables.get(f).put(v,
                    _solver.addVar(0.0, 10000, 0.0, GRB.CONTINUOUS, "Best:" + v.toString() + ":" + f.toString()));
        }
        return bestRouteVariables.get(f).get(v);
    }

    GRBVar getForwardVar(Flow f, Vertex src, Vertex dst) throws GRBException {
        if (forwardVariables.get(f) == null) {
            forwardVariables.put(f, new HashMap<Vertex, Map<Vertex, GRBVar>>());
        }

        if (forwardVariables.get(f).get(src) == null) {
            forwardVariables.get(f).put(src, new HashMap<Vertex, GRBVar>());
        }

        if (forwardVariables.get(f).get(src).get(dst) == null) {
            forwardVariables.get(f).get(src).put(dst, _solver.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS,
                    "Fwd:" + f.toString() + ":" + src.toString() + "->" + dst.toString()));
        }
        return forwardVariables.get(f).get(src).get(dst);
    }

    GRBVar getMaxForwardVar(Flow f, Vertex v) throws GRBException {
        if (maxForwardVariables.get(f) == null) {
            maxForwardVariables.put(f, new HashMap<Vertex, GRBVar>());
        }

        if (maxForwardVariables.get(f).get(v) == null) {
            maxForwardVariables.get(f).put(v,
                    _solver.addVar(0.0, 1, 0.0, GRB.CONTINUOUS, "MaxFwd:" + f.toString() + ":" + v.toString()));
        }
        return maxForwardVariables.get(f).get(v);
    }

    GRBVar getMinForwardVar(Flow f, Vertex v) throws GRBException {
        if (minForwardVariables.get(f) == null) {
            minForwardVariables.put(f, new HashMap<Vertex, GRBVar>());
        }

        if (minForwardVariables.get(f).get(v) == null) {
            minForwardVariables.get(f).put(v,
                    _solver.addVar(0.0, 1, 0.0, GRB.CONTINUOUS, "MinFwd:" + f.toString() + ":" + v.toString()));
        }
        return minForwardVariables.get(f).get(v);
    }

    GRBVar getDeltaSourceVar(Flow f) throws GRBException {
        if (deltaSourceVariables.get(f) == null) {
            deltaSourceVariables.put(f, _solver.addVar(0.0, 0.5, 0.0, GRB.CONTINUOUS, "DeltaSrc:" + f.toString()));
        }
        return deltaSourceVariables.get(f);
    }

    GRBVar getDeltaVar(Flow f, Vertex src, Vertex dst) throws GRBException {
        if (deltaVariables.get(f) == null) {
            deltaVariables.put(f, new HashMap<Vertex, Map<Vertex, GRBVar>>());
        }

        if (deltaVariables.get(f).get(src) == null) {
            deltaVariables.get(f).put(src, new HashMap<Vertex, GRBVar>());
        }

        if (deltaVariables.get(f).get(src).get(dst) == null) {
            deltaVariables.get(f).get(src).put(dst, _solver.addVar(0.0, 0.5, 0.0, GRB.CONTINUOUS,
                    "Delta:" + f.toString() + ":" + src.toString() + "->" + dst.toString()));
        }
        return deltaVariables.get(f).get(src).get(dst);
    }

    GRBVar getMaxDeltaVar(Flow f, Vertex v) throws GRBException {
        if (maxDeltaVariables.get(f) == null) {
            maxDeltaVariables.put(f, new HashMap<Vertex, GRBVar>());
        }

        if (maxDeltaVariables.get(f).get(v) == null) {
            maxDeltaVariables.get(f).put(v,
                    _solver.addVar(0.0, 0.5, 0.0, GRB.CONTINUOUS, "MaxDelta:" + f.toString() + ":" + v.toString()));
        }
        return maxDeltaVariables.get(f).get(v);
    }

    GRBVar getMinDeltaVar(Flow f, Vertex v) throws GRBException {
        if (minDeltaVariables.get(f) == null) {
            minDeltaVariables.put(f, new HashMap<Vertex, GRBVar>());
        }

        if (minDeltaVariables.get(f).get(v) == null) {
            minDeltaVariables.get(f).put(v,
                    _solver.addVar(0.0, 0.5, 0.0, GRB.CONTINUOUS, "MinDelta:" + f.toString() + ":" + v.toString()));
        }
        return minDeltaVariables.get(f).get(v);
    }

    GRBVar getLinkFailureVar(Process srcp, Process dstp) throws GRBException {
        return getLinkFailureVar(srcp.getDevice(), dstp.getDevice());
    }

    GRBVar getLinkFailureVar(Device src, Device dst) throws GRBException {
        if (src == dst) {
            throw new VerifierException("Does not hold for intra-device edges");
        }

        if (linkFailureVariables.get(src) == null) {
            linkFailureVariables.put(src, new HashMap<Device, GRBVar>());
        }
        if (linkFailureVariables.get(src).get(dst) == null) {
            GRBVar failVar = _solver.addVar(0, 1, 0, GRB.BINARY, "Fail:" + src.toString() + ":" + dst.toString());
            linkFailureVariables.get(src).put(dst, failVar);

            // Add failVar to (dst,src)
            if (linkFailureVariables.get(dst) == null) {
                linkFailureVariables.put(dst, new HashMap<Device, GRBVar>());
            }
            linkFailureVariables.get(dst).put(src, failVar);
        }
        return linkFailureVariables.get(src).get(dst);
    }

    GRBVar getLinkCongestionVar(Device src, Device dst) throws GRBException {
        if (src == dst) {
            throw new VerifierException("Does not hold for intra-device edges");
        }

        if (linkCongestionVariables.get(src) == null) {
            linkCongestionVariables.put(src, new HashMap<Device, GRBVar>());
        }

        if (linkCongestionVariables.get(src).get(dst) == null) {
            GRBVar failVar = _solver.addVar(0, 1, 0, GRB.BINARY, "Cong:" + src.toString() + ":" + dst.toString());
            linkCongestionVariables.get(src).put(dst, failVar);
        }
        return linkCongestionVariables.get(src).get(dst);
    }

    private void initializeVariables() throws GRBException {
        flowVariables = new HashMap<Flow, GRBVar>();
        bestRouteVariables = new HashMap<Flow, Map<Vertex, GRBVar>>();
        linkFailureVariables = new HashMap<Device, Map<Device, GRBVar>>();
        linkCongestionVariables = new HashMap<Device, Map<Device, GRBVar>>();
        forwardVariables = new HashMap<Flow, Map<Vertex, Map<Vertex, GRBVar>>>();
        maxForwardVariables = new HashMap<Flow, Map<Vertex, GRBVar>>();
        minForwardVariables = new HashMap<Flow, Map<Vertex, GRBVar>>();

        deltaSourceVariables = new HashMap<Flow, GRBVar>();
        deltaVariables = new HashMap<Flow, Map<Vertex, Map<Vertex, GRBVar>>>();
        maxDeltaVariables = new HashMap<Flow, Map<Vertex, GRBVar>>();
        minDeltaVariables = new HashMap<Flow, Map<Vertex, GRBVar>>();

        for (Flow flow : etgs.keySet()) {
            getFlowVar(flow);

            getDeltaSourceVar(flow);

            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                getBestRouteVar(flow, v);
                getMaxForwardVar(flow, v);
                getMinForwardVar(flow, v);

                getMinDeltaVar(flow, v);
                getMaxDeltaVar(flow, v);

                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    getForwardVar(flow, v, e.getDestination());

                    getDeltaVar(flow, v, e.getDestination());

                    // GRBLinExpr zero = new GRBLinExpr();
                    // zero.addTerm(1.0, getDeltaVar(flow, v, e.getDestination()));
                    // _solver.addConstr(zero, GRB.LESS_EQUAL, 0.5, "");
                }
            }
        }

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex) e.getSource();
                DeviceVertex dstdv = (DeviceVertex) e.getDestination();
                getLinkFailureVar(srcdv.getDevice(), dstdv.getDevice());
                
                if (this.checkCongestion) {
                    getLinkCongestionVar(srcdv.getDevice(), dstdv.getDevice());
                    getLinkCongestionVar(dstdv.getDevice(), srcdv.getDevice());
                }
            }
        }
        this.networkCongestionVar = _solver.addVar(0, 1, 0, GRB.BINARY, "NETCONGVAR");
        _solver.update();
    }

    private void initializeLinkCapacities () {
        // Read/Generate Link Capacities for 40/100 Gbps
        try {
            File linkcapacityFile = new File(settings.getConfigsDirectory() + 
                    "/linkcapacities.csv");
            FileReader fileReader = new FileReader(linkcapacityFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] fields = line.split(",");
                if (fields.length == 3) {
                    Device src = findDevice(fields[0]);
                    Device dst = findDevice(fields[1]);
                    double capacity = Double.parseDouble(fields[2]);
                    // System.out.println(fields[0] + " " + fields[1] + " " + fields[2]);
                    if (src != null && dst != null) { 
                        this.linkCapacities.put(new Pair(src,dst), capacity);
                        this.linkCapacities.put(new Pair(dst,src), capacity);
                    }
                }
            }
        } catch (IOException ioe) {
            // Generate link capacitie
            try {            
                Random random = new Random();
                BufferedWriter writer = new BufferedWriter(
                    new FileWriter(settings.getConfigsDirectory() + "/linkcapacities.csv"));
                Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
                while (deviceItr.hasNext()) {
                    DeviceVertex v = deviceItr.next();
                    Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

                    for (DirectedEdge e : edges) {
                        DeviceVertex srcdv = (DeviceVertex) e.getSource();
                        DeviceVertex dstdv = (DeviceVertex) e.getDestination();

                        double linkCap = (random.nextInt(2) == 0) ? 40.0 : 100.0;
                        this.linkCapacities.put(new Pair(srcdv.getDevice(), dstdv.getDevice()),
                            linkCap);
                        this.linkCapacities.put(new Pair(dstdv.getDevice(), srcdv.getDevice()),
                            linkCap);
                        
                        writer.write(srcdv.getDevice().toString() + "," + dstdv.getDevice().toString() 
                            + "," + Double.toString(linkCap) + "\n");
                    }
                }
                writer.close();
            } catch (IOException ioe1) {
                System.out.println("ERROR: Cannot write link capacities to file");
            }
        }
    }

    // this.linkEtgMap.get(this.congestedLink)

    private void addForwardFlowConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();
            if (this.cutFlows.containsKey(flow)) {
                // Add sanity constraints
                while (vertexItr.hasNext()) {
                    Vertex v = vertexItr.next();
                    Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                    for (DirectedEdge e : edges) {
                        GRBLinExpr sanityConstraint = new GRBLinExpr();
                        sanityConstraint.addTerm(1.0, getForwardVar(flow, v, e.getDestination()));
                        sanityConstraint.addTerm(-1.0, getFlowVar(flow));
                        _solver.addConstr(sanityConstraint, GRB.LESS_EQUAL, 0.0, "");
                    }
                }
            }

            vertexItr = etg.getVerticesIterator();
            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();

                if (v.getType() == VertexType.SOURCE) {
                    Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                    GRBLinExpr srcOutEdges = new GRBLinExpr();
                    for (DirectedEdge e : edges) {
                        srcOutEdges.addTerm(1.0, getForwardVar(flow, v, e.getDestination()));
                    }
                    if (this.cutFlows.containsKey(flow)) {
                        srcOutEdges.addTerm(-1.0, getFlowVar(flow));
                    } else {
                        srcOutEdges.addConstant(-1.0);

                        GRBLinExpr flowConstr = new GRBLinExpr();
                        flowConstr.addTerm(1.0, getFlowVar(flow));
                        _solver.addConstr(flowConstr, GRB.EQUAL, 1.0, "");
                    }
                    _solver.addConstr(srcOutEdges, GRB.EQUAL, 0.0, "SRCFLOW-" + flow.toString());
                } else if (v.getType() == VertexType.DESTINATION) {
                    Set<DirectedEdge<Vertex>> edges = etg.getIncomingEdges(v);
                    GRBLinExpr dstInEdges = new GRBLinExpr();
                    for (DirectedEdge e : edges) {
                        dstInEdges.addTerm(1.0, getForwardVar(flow, e.getSource(), v));
                    }
                    if (this.cutFlows.containsKey(flow)) {
                        dstInEdges.addTerm(-1.0, getFlowVar(flow));
                    } else {
                        dstInEdges.addConstant(-1.0);
                    }
                    _solver.addConstr(dstInEdges, GRB.EQUAL, 0.0, "DSTFLOW-" + flow.toString());
                } else {
                    // Not source or destination. Forward Flow is preserved
                    GRBLinExpr fwdFlow = new GRBLinExpr();

                    Set<DirectedEdge<Vertex>> edges = etg.getIncomingEdges(v);
                    for (DirectedEdge e : edges) {
                        fwdFlow.addTerm(1.0, getForwardVar(flow, e.getSource(), v));
                    }

                    edges = etg.getOutgoingEdges(v);
                    for (DirectedEdge e : edges) {
                        fwdFlow.addTerm(-1.0, getForwardVar(flow, v, e.getDestination()));
                    }
                    // Incoming fwd flow - outgoing fwd flow is zero
                    _solver.addConstr(fwdFlow, GRB.EQUAL, 0.0, "FLOW-" + v.toString() + "-" + flow.toString());
                }
            }
        }
    }

    private void addLoadBalancingConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();

                if (v.getType() != VertexType.SOURCE && v.getType() != VertexType.DESTINATION) {
                    Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);

                    if (edges.size() <= 1)
                        continue; // No load balancing required

                    // Add load balancing constraints
                    for (DirectedEdge e : edges) {
                        // Add max Fwd bound
                        GRBLinExpr maxForwardBound = new GRBLinExpr();
                        maxForwardBound.addTerm(1.0, getForwardVar(flow, v, e.getDestination()));

                        maxForwardBound.addTerm(-1.0, getMaxForwardVar(flow, v));

                        _solver.addConstr(maxForwardBound, GRB.LESS_EQUAL, 0.0, "ABSUPPERBOUND");

                        GRBLinExpr minForwardBound = new GRBLinExpr();
                        minForwardBound.addTerm(1.0, getMinForwardVar(flow, v));
                        minForwardBound.addTerm(-1.0, getForwardVar(flow, v, e.getDestination()));
                        minForwardBound.addTerm(10.0, getBestRouteVar(flow, v));
                        minForwardBound.addTerm(-10.0, getBestRouteVar(flow, e.getDestination()));
                        minForwardBound.addConstant(-10.0 * e.getWeight());

                        if (e.getType() == EdgeType.INTER_DEVICE) {
                            ProcessVertex srcpv = (ProcessVertex) e.getSource();
                            ProcessVertex dstpv = (ProcessVertex) e.getDestination();
                            GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(), dstpv.getProcess());

                            minForwardBound.addTerm(-10, linkFail);
                        }

                        _solver.addConstr(minForwardBound, GRB.LESS_EQUAL, 0.0, "ABSLOWERBOUND");
                    }

                    // For ECMP-style load-balancing, lower bound == upper bound
                    GRBLinExpr ecmpConstraint = new GRBLinExpr();
                    ecmpConstraint.addTerm(-1.0, getMinForwardVar(flow, v));
                    ecmpConstraint.addTerm(1.0, getMaxForwardVar(flow, v));
                    _solver.addConstr(ecmpConstraint, GRB.EQUAL, 0.0, "ECMP" + flow.toString() + "@" + v.toString());
                }
            }
        }
    }

    private void addBestRouteConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();

                if (v.getType() == VertexType.DESTINATION) {
                    _solver.addConstr(getBestRouteVar(flow, v), GRB.EQUAL, 0.0, "DST" + flow.toString());
                }

                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    // If link is not failed, triangle inequality holds
                    GRBLinExpr bestSmallest = new GRBLinExpr();
                    bestSmallest.addTerm(1.0, getBestRouteVar(flow, e.getDestination()));

                    bestSmallest.addConstant(e.getWeight());
                    bestSmallest.addTerm(-1.0, getBestRouteVar(flow, v));

                    if (e.getType() == EdgeType.INTER_DEVICE) {
                        ProcessVertex srcpv = (ProcessVertex) e.getSource();
                        ProcessVertex dstpv = (ProcessVertex) e.getDestination();
                        GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(), dstpv.getProcess());

                        bestSmallest.addTerm(100000, linkFail);
                    }

                    _solver.addConstr(bestSmallest, GRB.GREATER_EQUAL, 0.0, "bestSmallest");
                }
            }
        }
    }

    private void addShortestPathsConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            GRBLinExpr sumForwardFlow = new GRBLinExpr();
            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    sumForwardFlow.addTerm(e.getWeight(), getForwardVar(flow, v, e.getDestination()));
                }
            }
            sumForwardFlow.addTerm(-1.0, getBestRouteVar(flow, etg.getFlowSourceVertex()));
            _solver.addConstr(sumForwardFlow, GRB.EQUAL, 0.0, "Shortest" + flow.toString());
        }
    }

    private void addShortestPathsObjective(GRBLinExpr objective) throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            GRBLinExpr sumForwardFlow = new GRBLinExpr();
            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                objective.addTerm(-1.0, getBestRouteVar(flow, v));

                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    objective.addTerm(e.getWeight(), getForwardVar(flow, v, e.getDestination()));
                }
            }
        }
    }

    private void addValidForwardConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    if (e.getType() == EdgeType.INTER_DEVICE) {
                        ProcessVertex srcpv = (ProcessVertex) e.getSource();
                        ProcessVertex dstpv = (ProcessVertex) e.getDestination();
                        GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(), dstpv.getProcess());

                        GRBLinExpr validFwd = new GRBLinExpr();
                        validFwd.addTerm(1.0, getForwardVar(flow, v, e.getDestination()));
                        validFwd.addTerm(1, linkFail);
                        _solver.addConstr(validFwd, GRB.LESS_EQUAL, 1.0, "validFwd");
                    }
                }
            }
        }
    }

    private void addDeltaBoundConstraints(double f) throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();
            // Add sanity constraints
            if (cutFlows.contains(flow)) {
                GRBLinExpr sanityConstraint = new GRBLinExpr();
                sanityConstraint.addTerm(1.0, getDeltaSourceVar(flow));
                sanityConstraint.addTerm(-1.0, getFlowVar(flow));
                _solver.addConstr(sanityConstraint, GRB.LESS_EQUAL, 0.0, "DELTASANITY" + flow.toString());
            }

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    GRBLinExpr shortestPath = new GRBLinExpr();
                    shortestPath.addTerm(1.0, getDeltaVar(flow, v, e.getDestination()));
                    shortestPath.addTerm(-1.0, getForwardVar(flow, v, e.getDestination()));
                    _solver.addConstr(shortestPath, GRB.LESS_EQUAL, 0.0, "SPDELTA" + flow.toString() + e.toString());

                    GRBLinExpr sanityConstraint = new GRBLinExpr();
                    sanityConstraint.addTerm(1.0, getDeltaVar(flow, v, e.getDestination()));
                    sanityConstraint.addTerm(-1.0, getDeltaSourceVar(flow));
                    _solver.addConstr(sanityConstraint, GRB.LESS_EQUAL, 0.0,
                            "SANITYDELTA" + flow.toString() + e.toString());
                }
            }
        }

        GRBLinExpr deltaBound = new GRBLinExpr();
        
        for (Flow flow : etgs.keySet()) {
            this.totalVolume += flow.getTrafficCharacteristic();
            deltaBound.addTerm(flow.getTrafficCharacteristic(), getDeltaSourceVar(flow));
        }
        System.out.println("Total Flow Volume is " + Double.toString(this.totalVolume));
        if (f > 99) {
            System.out.println("Minimizing error margin");
            _solver.setObjective(deltaBound, GRB.MINIMIZE);
        } else {
            _solver.addConstr(deltaBound, GRB.LESS_EQUAL, f * this.totalVolume, "");
        }
    }

    private void addDeltaConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                if (v.getType() == VertexType.SOURCE) {
                    Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                    GRBLinExpr srcOutEdges = new GRBLinExpr();
                    for (DirectedEdge e : edges) {
                        srcOutEdges.addTerm(1.0, getDeltaVar(flow, v, e.getDestination()));
                    }
                    srcOutEdges.addTerm(-1.0, getDeltaSourceVar(flow));
                    _solver.addConstr(srcOutEdges, GRB.EQUAL, 0.0, "SRCFLOWDELTA-" + flow.toString());
                } else if (v.getType() == VertexType.DESTINATION) {
                    Set<DirectedEdge<Vertex>> edges = etg.getIncomingEdges(v);
                    GRBLinExpr dstInEdges = new GRBLinExpr();
                    for (DirectedEdge e : edges) {
                        dstInEdges.addTerm(1.0, getDeltaVar(flow, e.getSource(), v));
                    }

                    dstInEdges.addTerm(-1.0, getDeltaSourceVar(flow));
                    _solver.addConstr(dstInEdges, GRB.EQUAL, 0.0, "DSTFLOWDELTA-" + flow.toString());
                } else {
                    // Not source or destination. Delta is preserved
                    GRBLinExpr delta = new GRBLinExpr();

                    Set<DirectedEdge<Vertex>> edges = etg.getIncomingEdges(v);
                    for (DirectedEdge e : edges) {
                        delta.addTerm(1.0, getDeltaVar(flow, e.getSource(), v));
                    }

                    edges = etg.getOutgoingEdges(v);
                    for (DirectedEdge e : edges) {
                        delta.addTerm(-1.0, getDeltaVar(flow, v, e.getDestination()));
                    }
                    // Incoming fwd flow - outgoing fwd flow is zero
                    _solver.addConstr(delta, GRB.EQUAL, 0.0, "DELTA-" + v.toString() + "-" + flow.toString());
                }
            }
        }
    }

    private void addLoadBalancingDeltaConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();

                if (v.getType() != VertexType.SOURCE && v.getType() != VertexType.DESTINATION) {
                    Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);

                    if (edges.size() <= 1)
                        continue; // No load balancing required

                    // Add load balancing constraints
                    for (DirectedEdge e : edges) {
                        // Add max Fwd bound
                        GRBLinExpr maxForwardBound = new GRBLinExpr();
                        maxForwardBound.addTerm(1.0, getDeltaVar(flow, v, e.getDestination()));

                        maxForwardBound.addTerm(-1.0, getMaxDeltaVar(flow, v));

                        _solver.addConstr(maxForwardBound, GRB.LESS_EQUAL, 0.0, "ABSUPPERBOUND");

                        GRBLinExpr minForwardBound = new GRBLinExpr();
                        minForwardBound.addTerm(1.0, getMinDeltaVar(flow, v));
                        minForwardBound.addTerm(-1.0, getDeltaVar(flow, v, e.getDestination()));
                        minForwardBound.addTerm(10.0, getBestRouteVar(flow, v));
                        minForwardBound.addTerm(-10.0, getBestRouteVar(flow, e.getDestination()));
                        minForwardBound.addConstant(-10.0 * e.getWeight());

                        if (e.getType() == EdgeType.INTER_DEVICE) {
                            ProcessVertex srcpv = (ProcessVertex) e.getSource();
                            ProcessVertex dstpv = (ProcessVertex) e.getDestination();
                            GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(), dstpv.getProcess());

                            minForwardBound.addTerm(-10, linkFail);
                        }

                        _solver.addConstr(minForwardBound, GRB.LESS_EQUAL, 0.0, "ABSLOWERBOUND");
                    }

                    // For ECMP-style load-balancing, lower bound == upper bound
                    GRBLinExpr ecmpConstraint = new GRBLinExpr();
                    ecmpConstraint.addTerm(-1.0, getMinDeltaVar(flow, v));
                    ecmpConstraint.addTerm(1.0, getMaxDeltaVar(flow, v));
                    _solver.addConstr(ecmpConstraint, GRB.EQUAL, 0.0, "ECMP" + flow.toString() + "@" + v.toString());
                }
            }
        }
    }

    private void addValidDeltaConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    if (e.getType() == EdgeType.INTER_DEVICE) {
                        ProcessVertex srcpv = (ProcessVertex) e.getSource();
                        ProcessVertex dstpv = (ProcessVertex) e.getDestination();
                        GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(), dstpv.getProcess());

                        GRBLinExpr validFwd = new GRBLinExpr();
                        validFwd.addTerm(1.0, getDeltaVar(flow, v, e.getDestination()));
                        validFwd.addTerm(1, linkFail);
                        _solver.addConstr(validFwd, GRB.LESS_EQUAL, 1.0, "validDelta");
                    }
                }
            }
        }
    }

    private void addLinkFailureBoundConstraints() throws GRBException {
        HashSet<GRBVar> linkFailureVars = new HashSet<GRBVar>();
        GRBLinExpr failureSum = new GRBLinExpr();

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex) e.getSource();
                DeviceVertex dstdv = (DeviceVertex) e.getDestination();
                GRBVar linkFail = getLinkFailureVar(srcdv.getDevice(), dstdv.getDevice());
                if (!linkFailureVars.contains(linkFail)) {
                    failureSum.addTerm(1.0, linkFail);
                    linkFailureVars.add(linkFail);
                }
            }
        }

        _solver.addConstr(failureSum, GRB.EQUAL, this.maxLinkFailures, "linkFailureBound");
    }

    private GRBLinExpr getNormalizedLinkUtilization(Device src, Device dst) throws GRBException {
        GRBLinExpr normalizedLinkUtil = new GRBLinExpr();
        double linkCap = this.linkCapacities.get(new Pair(src,dst));
        if (this.linkCapacities.get(new Pair(src,dst)) == null) linkCap = 100.0;
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                ProcessVertex v = (ProcessVertex) vertexItr.next();
                /** Assuming device names are unique */
                try {
                    if (v.getProcess().getDevice().getName().equals(src.getName())) {
                        Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                        for (DirectedEdge e : edges) {
                            if (e.getType() == EdgeType.INTER_DEVICE && ((ProcessVertex) e.getDestination())
                                    .getProcess().getDevice().getName().equals(dst.getName())) {
                                normalizedLinkUtil.addTerm(flow.getTrafficCharacteristic() / linkCap,
                                        getForwardVar(flow, v, e.getDestination()));
                                normalizedLinkUtil.addTerm(flow.getTrafficCharacteristic() / linkCap,
                                        getDeltaVar(flow, v, e.getDestination()));
                            }
                        }
                    }
                } catch (NullPointerException ne) {
                    continue;
                }
            }
        }
        return normalizedLinkUtil;
    }

    private void createLinkFlowMap() {
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        for (Pair link: this.linkPartitionMap.keySet()){
            this.linkFlowMap.put(link, new HashSet<Flow>());
        }

        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();
            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    if (e.getType() == EdgeType.INTER_DEVICE) {
                        // System.out.println(flow.toString() + " " + e.toString());
                        Device src = ((ProcessVertex) v).getProcess().getDevice();
                        Device dst = ((ProcessVertex) e.getDestination()).getProcess().getDevice();
                        if (this.linkFlowMap.get(new Pair(src, dst)) == null) {
                            this.linkFlowMap.put(new Pair(src, dst), new HashSet<Flow>());
                        }
                        if (!this.linkFlowMap.get(new Pair<>(src, dst)).contains(flow)) {
                            this.linkFlowMap.get(new Pair<>(src, dst)).add(flow);
                        }
                    }
                }
            }
        }
    }

    private void addLinkCongestionConstraints(GRBLinExpr objective) throws GRBException {
        DeviceVertex srcdv = (DeviceVertex) this.congestedLink.getSource();
        DeviceVertex dstdv = (DeviceVertex) this.congestedLink.getDestination();
        GRBLinExpr normalizedLinkUtil = getNormalizedLinkUtilization(srcdv.getDevice(), dstdv.getDevice());

        // Add congestion constraint for particular link
        this.networkCongestion = _solver.addConstr(normalizedLinkUtil, GRB.GREATER_EQUAL, 1.0,
                "Congestion-" + this.congestedLink.toString());

        System.out.println("Checking congestion for link " + this.congestedLink.toString());
    }

    private void addCongestionConstraints(GRBLinExpr objective) throws GRBException {
        System.out.println("Checking Network-wide Congestion");
        GRBLinExpr networkCongestion = new GRBLinExpr();
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
            for (DirectedEdge e : edges) {
                Device src = ((DeviceVertex) e.getSource()).getDevice();
                Device dst = ((DeviceVertex) e.getDestination()).getDevice();
                
                // src -> dst
                if (!this.partitionedVerification ||  
                    this.linkPartitionMap.get(new Pair(src, dst)) == this.partitionNo) {
                    GRBLinExpr normalizedLinkUtil1 = getNormalizedLinkUtilization(src, dst);
                    normalizedLinkUtil1.addTerm(-1.0, getLinkCongestionVar(src, dst));
                    _solver.addConstr(normalizedLinkUtil1, GRB.GREATER_EQUAL, 0.0, "Congestion1-" + e.toString());
                    
                    networkCongestion.addTerm(1.0, getLinkCongestionVar(src, dst));
                }

                // dst -> src
                if (!this.partitionedVerification || 
                    this.linkPartitionMap.get(new Pair(dst, src)) == this.partitionNo) {
                    GRBLinExpr normalizedLinkUtil2 = getNormalizedLinkUtilization(dst, src);
                    normalizedLinkUtil2.addTerm(-1.0, getLinkCongestionVar(dst, src));
                    _solver.addConstr(normalizedLinkUtil2, GRB.GREATER_EQUAL, 0.0, "Congestion2-" + e.toString());    
                    networkCongestion.addTerm(1.0, getLinkCongestionVar(dst, src));
                }
            }
        }

        if (this.checkCongestion) {
            /* Add network congestion constraint */
            _solver.addConstr(networkCongestion, GRB.GREATER_EQUAL, 1.0, "networkCongestion");
        } else {
            /* Add network congestion objective */
            objective.addTerm(1.0, this.networkCongestionVar);
        }
    }

    // private boolean addLinkCongestionConstraints(GRBLinExpr objective) throws
    // GRBException {
    // GRBLinExpr networkCongestion = new GRBLinExpr();
    // Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
    // while (deviceItr.hasNext()) {
    // DeviceVertex v = deviceItr.next();
    // Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

    // for (DirectedEdge e : edges) {
    // DeviceVertex srcdv = (DeviceVertex)e.getSource();
    // DeviceVertex dstdv = (DeviceVertex)e.getDestination();
    // GRBLinExpr normalizedLinkUtil = getNormalizedLinkUtilization(
    // srcdv.getDevice(), dstdv.getDevice());

    // objective.add(normalizedLinkUtil);
    // }
    // }

    // return true;
    // }

    private void extractLinkFailureScenario() throws GRBException {
        HashSet<GRBVar> linkFailureVars = new HashSet<GRBVar>();
        HashSet<GRBVar> allLinkFailureVars = new HashSet<GRBVar>();
        GRBLinExpr thisFailScenario = new GRBLinExpr();
        GRBLinExpr noFailScenario = new GRBLinExpr();

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex) e.getSource();
                DeviceVertex dstdv = (DeviceVertex) e.getDestination();
                GRBVar linkFail = getLinkFailureVar(srcdv.getDevice(), dstdv.getDevice());

                // getModelUtilization(srcdv.getDevice(), dstdv.getDevice());
                // getModelUtilization(dstdv.getDevice(), srcdv.getDevice());

                if (linkFail.get(GRB.DoubleAttr.X) == 1 && !linkFailureVars.contains(linkFail)) {
                    thisFailScenario.addTerm(1.0, linkFail);
                    linkFailureVars.add(linkFail);
                }

                if (!allLinkFailureVars.contains(linkFail)) {
                    noFailScenario.addTerm(1.0, linkFail);
                    allLinkFailureVars.add(linkFail);
                }
            }
        }
        if (linkFailureVars.size() == 0) {
            System.out.println("System is congested without failures!");
            _solver.addConstr(noFailScenario, GRB.GREATER_EQUAL, 1.0, "UniqueFailure");

        } else {
            thisFailScenario.addConstant(-1.0 * (linkFailureVars.size() - 1));
            _solver.addConstr(thisFailScenario, GRB.LESS_EQUAL, 0.0, "UniqueFailure");
        }
    }

    private double getModelUtilization(Device src, Device dst) throws GRBException {
        double util = 0.0;
        double flowutil = 0.0;
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                ProcessVertex v = (ProcessVertex) vertexItr.next();
                /** Assuming device names are unique */
                try {
                    if (v.getProcess().getDevice().getName() == src.getName()) {
                        Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                        for (DirectedEdge e : edges) {
                            if (e.getType() == EdgeType.INTER_DEVICE && ((ProcessVertex) e.getDestination())
                                    .getProcess().getDevice().getName() == dst.getName()) {
                                util += flow.getTrafficCharacteristic()
                                        * (getForwardVar(flow, v, e.getDestination()).get(GRB.DoubleAttr.X)
                                                + getDeltaVar(flow, v, e.getDestination()).get(GRB.DoubleAttr.X));

                                flowutil += flow.getTrafficCharacteristic()
                                        * (getForwardVar(flow, v, e.getDestination()).get(GRB.DoubleAttr.X));
                            }
                        }
                    }
                } catch (NullPointerException ne) {
                    continue;
                }
            }
        }
        // System.out.println(src.toString() + " --> " + dst.toString() + " : " +
        // Double.toString(util) + " -- " + Double.toString(flowutil));
        return util;
    }

    public boolean addConstraints(GRBLinExpr objective, boolean onlyCongestion) throws GRBException {
        // if (this.linkQueue.size() == 0) return false;
        // this.congestedLink = ((EdgeCount) this.linkQueue.poll()).e;

        // if (this.networkCongestion != null) {
        // _solver.remove(this.networkCongestion);
        // }
        /** Add congestion constraints */
        if (this.checkCongestion)
            addCongestionConstraints(objective);
        else
            addLinkCongestionConstraints(objective);
        System.out.println("Added Congestion constraints");

        if (!onlyCongestion) {
            addForwardFlowConstraints();
            System.out.println("Added Forward-Flow constraints");
            addValidForwardConstraints();
            addLinkFailureBoundConstraints();
            System.out.println("Added Link Bound constraints");
            addBestRouteConstraints();
            System.out.println("Added Best Route constraints");

            addLoadBalancingConstraints();
            System.out.println("Adding the load balancing constraints");
            addShortestPathsConstraints();
            System.out.println("Adding the shortest paths constraints");

            addDeltaConstraints();
            addValidDeltaConstraints();
            addLoadBalancingDeltaConstraints();
            addDeltaBoundConstraints(settings.getDeltaBound());
            System.out.println("Adding the Delta constraints");

            // Add Verifier-wide objective to solver
            // _solver.setObjective(objective, GRB.MAXIMIZE);
        }
        return true;
    }

    public Device findDevice(String dev){
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            if (v.getDevice().toString().equals(dev)) return v.getDevice();
        }
        return null;
    }

    public void verify() throws GRBException {

        // WeibullDistribution probdist = new WeibullDistribution(0.8, 0.0001);
        // this.linkFailureProb = new HashMap<>();

        // Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        // while (deviceItr.hasNext()) {
        // DeviceVertex v = deviceItr.next();
        // Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

        // for (DirectedEdge e : edges) {
        // DeviceVertex srcdv = (DeviceVertex)e.getSource();
        // DeviceVertex dstdv = (DeviceVertex)e.getDestination();

        // if (! linkFailureProb.containsKey(new Pair<>(srcdv, dstdv))) {
        // linkFailureProb.put(new Pair<>(srcdv, dstdv), (Double) probdist.sample());
        // }
        // }
        // }

        if (this.maxLinkFailures == 0) {
            // Verify if base case is congested or not.
            this.validator = new ModelValidator(this, this.etgs, this.deviceEtg);
            validator.checkCongestion(new HashSet<DirectedEdge>(), false);
            System.exit(0);
        }

        // Minimize ETGs
        long startTime = System.currentTimeMillis();
        if (settings.shouldMinimizeETGs()) {
            minimizeETGsParallel();
        }
        long endTime = System.currentTimeMillis();
        long minimizeTime = (endTime - startTime);
        System.out.println("Time taken to minimize ETGs: " + (endTime - startTime) + "ms");
        System.out.println("Number of ETGs are " + etgs.keySet().size());

        if (this.partitionedVerification) {
            try {
                File partitionFile = new File(settings.getConfigsDirectory() + 
                    "/v" + Integer.toString(this.maxLinkFailures) + "partitions" + this.totalPartitions + ".csv");
                FileReader fileReader = new FileReader(partitionFile);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String[] fields = line.split(",");
                    if (fields.length == 3) {
                        Device src = findDevice(fields[0]);
                        Device dst = findDevice(fields[1]);
                        int partition = Integer.parseInt(fields[2]);
                        // Random random = new Random();
                        // int partition = random.nextInt(4);
                        // System.out.println(fields[0] + " " + fields[1] + " " + fields[2]);
                        if (src != null && dst != null) { 
                            this.linkPartitionMap.put(new Pair(src,dst), partition);
                        }
                    }
                }
            } catch (IOException e) {
                // Pre-computed partitions dont exist. Find verification partition
                VerificationSplitter splitter = new VerificationSplitter(this.etgs, this.deviceEtg, this.settings);
                this.linkPartitionMap = splitter.split();
                try {
                    BufferedWriter writer = new BufferedWriter(
                    new FileWriter(settings.getConfigsDirectory() + 
                    "/v" + Integer.toString(this.maxLinkFailures) + "partitions" + this.totalPartitions + ".csv"));
                    for (Pair link: this.linkPartitionMap.keySet()){
                        writer.write(link.getKey().toString() + "," +
                            link.getValue().toString() + "," + Integer.toString(this.linkPartitionMap.get(link)) + "\n");                            
                    }
                    writer.close();
                } catch (IOException ioe) {
                    System.out.println("ERROR: Cant write verification partitions to file.");
                }
            }

            // Prune etgs which are not needed in partition.
            createLinkFlowMap();
            ConcurrentHashMap<Flow, ExtendedTopologyGraph> partitionEtgs = new ConcurrentHashMap<Flow, ExtendedTopologyGraph>();
            for (Pair link: this.linkPartitionMap.keySet()){
                if (this.linkPartitionMap.get(link) == this.partitionNo) {
                    // Get ETGs corresponding to this link
                    if (!linkFlowMap.containsKey(link)) continue;
                    HashSet<Flow> pertinentFlows = linkFlowMap.get(link);
                    // System.out.println("Number of flows is " + Integer.toString(pertinentFlows.size()));
                    for (Flow flow : pertinentFlows) {
                        partitionEtgs.put(flow, etgs.get(flow));
                    }
                }
            }

            this.etgs = partitionEtgs;
            System.out.println("\n*\n*\n====PARTITION #" + Integer.toString(this.partitionNo) + " =====");
            System.out.println("Number of ETGs in partition are " + Integer.toString(etgs.size()));
        }

        // Find mincut of ETGs for correct formulation
        startTime = System.currentTimeMillis();
        AlwaysReachable minCut = new AlwaysReachable(new ConcurrentHashMap<Flow, ExtendedTopologyGraph>(this.etgs),
                this.settings);
        long totalEdges = 0;
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            VerifierResult result = minCut.verify(flow, this.maxLinkFailures);
            // System.out.println("Mincut " + flow.toString() + " " +
            // Integer.toString(this.maxLinkFailures) + " " +
            // Boolean.toString(result.propertyHolds()));
            totalEdges += etg.getEdgeCount();
            if (!result.propertyHolds()) {
                this.cutFlows.put(flow, true);
            }
            // this.cutFlows.put(flow, true); // @TODO: Adding all flows to cutflow
        }
        // classifyLinks();

        try {
            env = new GRBEnv("quark.log");
            // env.set(GRB.IntParam.LogToConsole, 0);
            // env.set(GRB.IntParam.BarHomogeneous, 1);
            // env.set(GRB.IntParam.Method, 1);
            env.set(GRB.DoubleParam.MIPGap, 0.1);
            env.set(GRB.DoubleParam.TimeLimit, 3600);
            _solver = new GRBModel(env);
        } catch (GRBException e) {
            e.printStackTrace();
        }
        // Check for congestion under arbitrary maxLinkFailures
        initializeVariables();

        // Verifier-wide objective
        GRBLinExpr objective = new GRBLinExpr();

        addConstraints(objective, false);
        _solver.update();
        endTime = System.currentTimeMillis();
        long constraintAddTime = (endTime - startTime);
        System.out.println("Time taken to add constraints : " + (endTime - startTime) + "ms");

        while (true) {
            startTime = System.currentTimeMillis();
            _solver.optimize();
            endTime = System.currentTimeMillis();
            long constraintSolveTime = (endTime - startTime);
            System.out.println("Time taken to solve constraints : " + (endTime - startTime) + "ms");

            try {
                double objval = 0.0;
                if (_solver.get(GRB.IntAttr.Status) == GRB.Status.OPTIMAL || _solver.get(GRB.IntAttr.Status) == GRB.Status.TIME_LIMIT ) {
                    objval = _solver.get(GRB.DoubleAttr.ObjVal);
                }
                BufferedWriter writer = new BufferedWriter(
                        new FileWriter(System.getProperty("user.home") + "/QARC-Timing"));
                writer.write("MT/CAT/CST\t" + Long.toString(minimizeTime) + "\t" + Long.toString(constraintAddTime)
                        + "\t" + Long.toString(constraintSolveTime) + "\t"
                        + Integer.toString(_solver.get(GRB.IntAttr.Status)) + "\t" 
                        + Double.toString(objval/this.totalVolume) + "\n");
                writer.close();
            } catch (IOException e) {}

            this.validator = new ModelValidator(this, this.etgs, this.deviceEtg);
            if (_solver.get(GRB.IntAttr.Status) == GRB.Status.OPTIMAL) {
                validator.validate(true);
                extractLinkFailureScenario();
                break;
            } else {
                // _solver.computeIIS();
                // _solver.write(".quark-unsat.ilp");
                // System.exit(0);
                validator.validate(false);
                if (this.checkCongestion || !addConstraints(objective, true)) {
                    System.out.println("No more solutions");
                    break;
                }
            }
        }
    }
}
