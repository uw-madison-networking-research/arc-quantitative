package edu.wisc.cs.arc.quantverifiers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javafx.util.Pair;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.Device;
import edu.wisc.cs.arc.graphs.DeviceGraph;
import edu.wisc.cs.arc.graphs.DeviceVertex;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.Process;
import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.graphs.Vertex;
import edu.wisc.cs.arc.graphs.Vertex.VertexType;
import edu.wisc.cs.arc.verifiers.VerifierException;
import gurobi.*;

public class ETGCongestionRepair implements Runnable {

    /** The ETGs to use for repair */
    private ExtendedTopologyGraph etg;

    private Flow flow;
    
    /** Settings for the verification process */
    private Settings settings;
    
    /** The device ETG */
    private DeviceGraph deviceEtg;

    /** Maximum number of simultaneous link failures */
    private int maxLinkFailures = 2; 

    private GRBEnv env;
    private GRBModel _solver;

    private HashMap<Vertex, GRBVar> bestRouteVariables;
    private HashMap<Vertex, Map<Vertex, GRBVar>> forwardVariables;
    private HashMap<Vertex, GRBVar> reachVariables;
    private HashMap<Device, Map<Device, GRBVar>> linkFailureVariables;
    private HashMap<Vertex, GRBVar> maxForwardVariables;
    private HashMap<Vertex, GRBVar> minForwardVariables;

    Map<Pair<Device, Device>, Double> newCapacities;


    public ETGCongestionRepair(Flow flow, ExtendedTopologyGraph etg, 
            DeviceGraph deviceEtg, Settings settings, Map<Pair<Device, Device>, Double> newCapacities) {
        this.flow = flow;
        this.etg = etg;
        this.settings = settings;
        this.deviceEtg = deviceEtg;
        this.newCapacities = newCapacities;
        this.maxLinkFailures = this.settings.getCongestionFreeFailureCount();
    }

    GRBModel getSolver() throws GRBException { 
        return _solver;
    }

    GRBVar getBestRouteVar(Vertex v) throws GRBException {
        if (bestRouteVariables.get(v) == null) {
            bestRouteVariables.put(v, 
                    _solver.addVar(0.0, 10000, 0.0, GRB.CONTINUOUS,
                        "Best:" + v.toString() + ":" + this.flow.toString()));
        }
        return bestRouteVariables.get(v);
    }

    GRBVar getForwardVar(Vertex src, Vertex dst) throws GRBException {
        if (forwardVariables.get(src) == null) {
            forwardVariables.put(src, new HashMap<Vertex, GRBVar>());
        }

        if (forwardVariables.get(src).get(dst) == null) {
            forwardVariables.get(src).put(dst, 
                    _solver.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS,
                        "Fwd:" + this.flow.toString() + ":" + 
                        src.toString() + "->" + dst.toString()));
        }
        return forwardVariables.get(src).get(dst);
    }

    GRBVar getMaxForwardVar(Vertex v) throws GRBException {
        if (maxForwardVariables.get(v) == null) {
            maxForwardVariables.put(v, 
                    _solver.addVar(0.0, 1, 0.0, GRB.CONTINUOUS,
                        "MaxFwd:" + this.flow.toString() + ":" + 
                        v.toString()));
        }
        return maxForwardVariables.get(v);
    }

    GRBVar getMinForwardVar(Vertex v) throws GRBException {
        if (minForwardVariables.get(v) == null) {
            minForwardVariables.put(v, 
                    _solver.addVar(0.0, 1, 0.0, GRB.CONTINUOUS,
                        "MinFwd:" + this.flow.toString() + ":" + 
                        v.toString()));
        }
        return minForwardVariables.get(v);
    }

    
    GRBVar getLinkFailureVar(Process srcp, Process dstp) throws GRBException {
        return getLinkFailureVar(srcp.getDevice(), dstp.getDevice());
    }

    GRBVar getLinkFailureVar(Device src, Device dst) throws GRBException {
        if (src == dst) { 
            throw new VerifierException("Does not hold for intra-device edges");
        }

        if (linkFailureVariables.get(src) == null) {
            linkFailureVariables.put(src, new HashMap<Device, GRBVar>());
        } 
        if (linkFailureVariables.get(src).get(dst) == null) {
            GRBVar failVar = _solver.addVar(0, 1, 0, GRB.BINARY,
                "Fail:" + src.toString() + ":" + dst.toString());
            linkFailureVariables.get(src).put(dst, failVar);

            // Add failVar to (dst,src) 
            if (linkFailureVariables.get(dst) == null) {
                linkFailureVariables.put(dst, new HashMap<Device, GRBVar>());
            }
            linkFailureVariables.get(dst).put(src, failVar);
        }
        return linkFailureVariables.get(src).get(dst);
    }

    private void initializeVariables() throws GRBException {
        bestRouteVariables = new HashMap<Vertex, GRBVar>();
        linkFailureVariables = new HashMap<Device, Map<Device, GRBVar>>();
        forwardVariables = new HashMap<Vertex, Map<Vertex, GRBVar>>();
        maxForwardVariables = new HashMap<Vertex, GRBVar>();
        minForwardVariables = new HashMap<Vertex, GRBVar>();
        reachVariables = new HashMap<Vertex, GRBVar>();

        
        Iterator<Vertex> vertexItr = this.etg.getVerticesIterator();
        
        while (vertexItr.hasNext()) {
            Vertex v = vertexItr.next();
            getBestRouteVar(v);
            getMaxForwardVar(v);
            getMinForwardVar(v);

            Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
            for (DirectedEdge e : edges) {
                getForwardVar(v, e.getDestination());
            }
        }


        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
            
            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex)e.getSource();
                DeviceVertex dstdv = (DeviceVertex)e.getDestination();
                getLinkFailureVar(srcdv.getDevice(), dstdv.getDevice());
            }
        }
        _solver.update();
    }

    // this.linkEtgMap.get(this.congestedLink)

    private void addForwardFlowConstraints() throws GRBException {  
        Iterator<Vertex> vertexItr = this.etg.getVerticesIterator();
        
        while (vertexItr.hasNext()) {
            Vertex v = vertexItr.next();

            if (v.getType() == VertexType.SOURCE) {
                Set<DirectedEdge<Vertex>> edges = this.etg.getOutgoingEdges(v);
                GRBLinExpr srcOutEdges = new GRBLinExpr();
                for (DirectedEdge e : edges) {
                    srcOutEdges.addTerm(1.0, 
                        getForwardVar(v, e.getDestination()));
                }
                _solver.addConstr(srcOutEdges, GRB.EQUAL, 1.0,
                    "SRCFLOW");
            } else if (v.getType() == VertexType.DESTINATION) {
                Set<DirectedEdge<Vertex>> edges = this.etg.getIncomingEdges(v);
                GRBLinExpr dstInEdges = new GRBLinExpr();
                for (DirectedEdge e : edges) {
                    dstInEdges.addTerm(1.0, 
                        getForwardVar(e.getSource(), v));
                }
                _solver.addConstr(dstInEdges, GRB.EQUAL, 1.0,
                    "DSTFLOW" );
            } else {
                // Not source or destination. Forward Flow is preserved
                GRBLinExpr fwdFlow = new GRBLinExpr();

                Set<DirectedEdge<Vertex>> edges = this.etg.getIncomingEdges(v);
                for (DirectedEdge e : edges) {
                    fwdFlow.addTerm(1.0, 
                        getForwardVar(e.getSource(), v));
                }
                
                edges = this.etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    fwdFlow.addTerm(-1.0, 
                        getForwardVar(v, e.getDestination()));
                }
                // Incoming fwd flow - outgoing fwd flow is zero
                _solver.addConstr(fwdFlow, GRB.EQUAL, 0.0,
                    "FLOW-" + v.toString());
            }
        }
    }
    

    private void addLoadBalancingConstraints() throws GRBException {
        Iterator<Vertex> vertexItr = this.etg.getVerticesIterator();
        
        while (vertexItr.hasNext()) {
            Vertex v = vertexItr.next();

            if (v.getType() != VertexType.SOURCE && v.getType() != 
                VertexType.DESTINATION) {
                Set<DirectedEdge<Vertex>> edges = this.etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    // Add max Fwd bound
                    GRBLinExpr maxForwardBound = new GRBLinExpr();
                    maxForwardBound.addTerm(1.0, 
                        getForwardVar(v, e.getDestination()));
                    
                    maxForwardBound.addTerm(-1.0, getMaxForwardVar(v));

                    _solver.addConstr(maxForwardBound, GRB.LESS_EQUAL, 0.0, 
                        "ABSUPPERBOUND" + e.toString());

                    GRBLinExpr minForwardBound = new GRBLinExpr();
                    minForwardBound.addTerm(1.0, getMinForwardVar(v));
                    minForwardBound.addTerm(-1.0, getForwardVar(v, e.getDestination()));
                    minForwardBound.addTerm(1.0, getBestRouteVar(v));
                    minForwardBound.addTerm(-1.0, getBestRouteVar(e.getDestination()));
                    minForwardBound.addConstant(-1.0 * e.getWeight());

                    if (e.getType() == EdgeType.INTER_DEVICE) {
                        ProcessVertex srcpv = (ProcessVertex)e.getSource();
                        ProcessVertex dstpv = (ProcessVertex)e.getDestination();
                        GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(),
                            dstpv.getProcess());

                        minForwardBound.addTerm(-2, linkFail);
                    }


                    _solver.addConstr(minForwardBound, GRB.LESS_EQUAL, 0.0, 
                        "ABSLOWERBOUND" + e.toString());
                }

                // For ECMP-style load-balancing, lower bound == upper bound
                GRBLinExpr ecmpConstraint = new GRBLinExpr();
                ecmpConstraint.addTerm(-1.0, getMinForwardVar(v));
                ecmpConstraint.addTerm(1.0, getMaxForwardVar(v));
                _solver.addConstr(ecmpConstraint, GRB.EQUAL, 0.0, 
                    "ECMP@" + v.toString());
            }
        }
    }

    private void addBestRouteConstraints() throws GRBException {
        Iterator<Vertex> vertexItr = this.etg.getVerticesIterator();
        
        while (vertexItr.hasNext()) {
            Vertex v = vertexItr.next();

            if (v.getType() == VertexType.DESTINATION) {
                _solver.addConstr(getBestRouteVar(v), GRB.EQUAL, 0.0,
                    "DST" + flow.toString());
            }

            Set<DirectedEdge<Vertex>> edges = this.etg.getOutgoingEdges(v);
            for (DirectedEdge e : edges) {
                // If link is not failed, triangle inequality holds
                GRBLinExpr bestSmallest = new GRBLinExpr();
                bestSmallest.addTerm(1.0, getBestRouteVar(e.getDestination()));
                bestSmallest.addConstant(e.getWeight());
                bestSmallest.addTerm(-1.0, getBestRouteVar(v));

                if (e.getType() == EdgeType.INTER_DEVICE) {
                    ProcessVertex srcpv = (ProcessVertex)e.getSource();
                    ProcessVertex dstpv = (ProcessVertex)e.getDestination();
                    GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(),
                        dstpv.getProcess());

                    bestSmallest.addTerm(100000, linkFail);
                }

                _solver.addConstr(bestSmallest, GRB.GREATER_EQUAL, 0.0, "bestSmallest");
            }
        }
    }
    

    private void addShortestPathsConstraints() throws GRBException {
        Iterator<Vertex> vertexItr = this.etg.getVerticesIterator();
        
        GRBLinExpr sumForwardFlow = new GRBLinExpr();
        while (vertexItr.hasNext()) {
            Vertex v = vertexItr.next();
            Set<DirectedEdge<Vertex>> edges = this.etg.getOutgoingEdges(v);
            for (DirectedEdge e : edges) {
                sumForwardFlow.addTerm(e.getWeight(), 
                    getForwardVar(v, e.getDestination()));
            }
        }
        sumForwardFlow.addTerm(-1.0, getBestRouteVar(
            this.etg.getFlowSourceVertex()));
        _solver.addConstr(sumForwardFlow, GRB.EQUAL, 0.0, "Shortest"); 
    }
    
    private void addValidForwardConstraints() throws GRBException {
        Iterator<Vertex> vertexItr = this.etg.getVerticesIterator();
        
        while (vertexItr.hasNext()) {
            Vertex v = vertexItr.next();
            Set<DirectedEdge<Vertex>> edges = this.etg.getOutgoingEdges(v);
            for (DirectedEdge e : edges) {
                if (e.getType() == EdgeType.INTER_DEVICE) {
                    ProcessVertex srcpv = (ProcessVertex)e.getSource();
                    ProcessVertex dstpv = (ProcessVertex)e.getDestination();
                    GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(),
                        dstpv.getProcess());

                    GRBLinExpr validFwd = new GRBLinExpr();
                    validFwd.addTerm(1.0, 
                        getForwardVar(v, e.getDestination()));
                    validFwd.addTerm(1, linkFail);
                    _solver.addConstr(validFwd, GRB.LESS_EQUAL, 1.0, "validFwd");
                }
            }
        }
    }

    private void addLinkFailureBoundConstraints() throws GRBException {
        HashSet<GRBVar> linkFailureVars = new HashSet<GRBVar>();
        GRBLinExpr failureSum = new GRBLinExpr();

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
            
            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex)e.getSource();
                DeviceVertex dstdv = (DeviceVertex)e.getDestination();
                GRBVar linkFail = getLinkFailureVar(srcdv.getDevice(),
                    dstdv.getDevice());
                if (! linkFailureVars.contains(linkFail)) {
                    failureSum.addTerm(1.0, linkFail);
                    linkFailureVars.add(linkFail);
                }
            }
        }
        
        _solver.addConstr(failureSum, GRB.EQUAL, this.maxLinkFailures, "linkFailureBound");
    }

    private GRBLinExpr getNormalizedLinkUtilization(Device src, Device dst) throws GRBException {
        GRBLinExpr normalizedLinkUtil = new GRBLinExpr();
        
        Iterator<Vertex> vertexItr = etg.getVerticesIterator();
            
        while (vertexItr.hasNext()) {
            ProcessVertex v = (ProcessVertex) vertexItr.next();
            /** Assuming device names are unique */
            try {
                if (v.getProcess().getDevice().getName().equals(src.getName())) {
                    Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                    for (DirectedEdge e : edges) {
                        if (e.getType() == EdgeType.INTER_DEVICE && 
                            ((ProcessVertex) e.getDestination()).getProcess().
                            getDevice().getName().equals(dst.getName())) {
                            /** @TODO Use actual Link and traffic Char */ 
                            normalizedLinkUtil.addTerm(this.flow.getTrafficCharacteristic(), 
                                getForwardVar(v, e.getDestination()));
                        }   
                    }
                }
            } catch (NullPointerException ne) {
                continue;
            }
        }
        return normalizedLinkUtil;
    }

    public void run() {
        try {
            repair();
        } catch (GRBException e) {}
    }

    public void repair() throws GRBException {
        try {
            env = new GRBEnv("quark.log");
            env.set(GRB.IntParam.LogToConsole, 0);
            env.set(GRB.DoubleParam.TimeLimit, 300);
            // env.set(GRB.IntParam.BarHomogeneous, 1);
            // env.set(GRB.IntParam.Method, 1);
            _solver = new GRBModel(env);
        } catch (GRBException e) {
            e.printStackTrace();
        }
 
        long startTime = System.currentTimeMillis();

        initializeVariables();
        addForwardFlowConstraints();
        addValidForwardConstraints();
        addLinkFailureBoundConstraints();
        addBestRouteConstraints();
        addLoadBalancingConstraints();
        addShortestPathsConstraints();

        

        Iterator<DirectedEdge<DeviceVertex>> linkItr = deviceEtg.getEdgesIterator();
        while (linkItr.hasNext()) {
            DirectedEdge<DeviceVertex> link = linkItr.next();
            Device src = ((DeviceVertex)link.getSource()).getDevice();
            Device dst = ((DeviceVertex)link.getDestination()).getDevice();
            _solver.setObjective(getNormalizedLinkUtilization(src, dst), GRB.MAXIMIZE);
            _solver.optimize();

            if (_solver.get(GRB.IntAttr.Status) == GRB.Status.OPTIMAL) {
                double maxCapacity = _solver.get(GRB.DoubleAttr.ObjVal);
                this.newCapacities.put(new Pair(src,dst), maxCapacity);
            } else {
                System.out.println("Error: Repair does not exist. Exit");
                System.exit(0);
            }

            _solver.setObjective(getNormalizedLinkUtilization(dst, src), GRB.MAXIMIZE);
            _solver.optimize();
            if (_solver.get(GRB.IntAttr.Status) == GRB.Status.OPTIMAL) {
                double maxCapacity = _solver.get(GRB.DoubleAttr.ObjVal);
                this.newCapacities.put(new Pair(dst, src), maxCapacity);
            } else {
                System.out.println("Error: Repair does not exist. Exit");
                System.exit(0);
            }
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Time taken for repairing one class : " + (endTime - startTime) + "ms");
    }
}