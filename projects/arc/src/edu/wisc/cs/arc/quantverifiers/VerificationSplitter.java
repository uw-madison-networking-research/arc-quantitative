package edu.wisc.cs.arc.quantverifiers;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.Device;
import edu.wisc.cs.arc.graphs.DeviceVertex;
import edu.wisc.cs.arc.graphs.DeviceGraph;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.Process;
import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.graphs.Vertex;
import edu.wisc.cs.arc.graphs.Vertex.VertexType;
import edu.wisc.cs.arc.quantverifiers.ETGMinimizer;
import edu.wisc.cs.arc.quantverifiers.ModelValidator;
import edu.wisc.cs.arc.verifiers.AlwaysReachable;
import edu.wisc.cs.arc.verifiers.VerifierResult;
import edu.wisc.cs.arc.verifiers.VerifierException;

import gurobi.*;

import javafx.util.Pair; 
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import org.apache.commons.math3.distribution.WeibullDistribution;


public class VerificationSplitter {
    /** The ETGs to use for verification */
    public ConcurrentHashMap<Flow, ExtendedTopologyGraph> etgs;
    
    /** Settings for the verification process */
    private Settings settings;
    
    /** The device ETG */
    private DeviceGraph deviceEtg;

    /** Maximum number of partitions */
    private int numPartitions = 4; 

    private int totalLinks = 0;

    private GRBEnv env;
    private GRBModel _solver;

    public GRBVar maxFlowPartition;
    private HashMap<Device, Map<Device, Map<Integer, GRBVar>>> linkPartitionVariables;
    private HashMap<Flow, Map<Integer, GRBVar>> flowPartitionVariables;


    public VerificationSplitter(Map<Flow, ExtendedTopologyGraph> etgs, 
            DeviceGraph deviceEtg, Settings settings) {
    
        this.etgs = new ConcurrentHashMap(etgs);
        this.settings = settings;
        this.deviceEtg = deviceEtg;
        this.numPartitions = this.settings.getVerificationPartitioned();
    }

    private void initializeVariables() throws GRBException {
        linkPartitionVariables = new HashMap<Device, Map<Device, Map<Integer, GRBVar>>>();
        flowPartitionVariables = new HashMap<Flow, Map<Integer, GRBVar>>();

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex)e.getSource();
                DeviceVertex dstdv = (DeviceVertex)e.getDestination();
                this.totalLinks += 2;
                for (int p = 0; p < this.numPartitions; p++) {
                    getLinkVar(srcdv.getDevice(), dstdv.getDevice(), p);
                    getLinkVar(dstdv.getDevice(), srcdv.getDevice(), p);
                }
            }
        }

        for (Flow flow : etgs.keySet()) {
            for (int p = 0; p < this.numPartitions; p++) {
                getFlowVar(flow, p);
            }
        }

        maxFlowPartition = _solver.addVar(0.0, 10000, 0.0, GRB.CONTINUOUS,
            "MAXFLOWPARTITION");
        
        _solver.update();
    }

    public GRBVar getLinkVar(Device src, Device dst, int partition) throws GRBException {
        if (src == dst) { 
            throw new VerifierException("Does not hold for intra-device edges");
        }

        if (linkPartitionVariables.get(src) == null) {
            linkPartitionVariables.put(src, new HashMap<Device, Map<Integer, GRBVar>>());
        } 
        if (linkPartitionVariables.get(src).get(dst) == null) {
            linkPartitionVariables.get(src).put(dst, new HashMap<Integer, GRBVar>());
        } 
        if (linkPartitionVariables.get(src).get(dst).get(partition) == null) {
            GRBVar linkVar = _solver.addVar(0, 1, 0, GRB.BINARY,
                "LINK:" + src.toString() + ":" + dst.toString() + 
                ":" + Integer.toString(partition));
            linkPartitionVariables.get(src).get(dst).put(partition, linkVar);
        }
        return linkPartitionVariables.get(src).get(dst).get(partition);
    }

    public GRBVar getFlowVar(Flow f, int partition) throws GRBException {
        if (flowPartitionVariables.get(f) == null) {
            flowPartitionVariables.put(f, new HashMap<Integer, GRBVar>());
        }
        if (flowPartitionVariables.get(f).get(partition) == null) {
            GRBVar flowVar = _solver.addVar(0, 1, 0, GRB.BINARY,
                "FLOW:" + f.toString() + ":" + Integer.toString(partition));
            flowPartitionVariables.get(f).put(partition, flowVar);
        }
        return flowPartitionVariables.get(f).get(partition);
    }

    private void addLinkAssignedConstraints() throws GRBException {
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
            for (DirectedEdge e : edges) {
                Device src = ((DeviceVertex)e.getSource()).getDevice();
                Device dst = ((DeviceVertex)e.getDestination()).getDevice();

                GRBLinExpr linkConstr1 = new GRBLinExpr();
                for (int p = 0; p < this.numPartitions; p++) {
                    linkConstr1.addTerm(1.0, getLinkVar(src, dst, p));
                }
                _solver.addConstr(linkConstr1, GRB.EQUAL, 1.0, 
                    "ASSIGN" + src.toString() + ":" + dst.toString());
                
                GRBLinExpr linkConstr2 = new GRBLinExpr();
                for (int p = 0; p < this.numPartitions; p++) {
                    linkConstr2.addTerm(1.0, getLinkVar(dst, src, p));
                }
                _solver.addConstr(linkConstr2, GRB.EQUAL, 1.0, 
                    "ASSIGN" + dst.toString() + ":" + src.toString());
            }
        }        
    }

    private void addPartitionSizeConstraints() throws GRBException {
        for (int p = 0; p < this.numPartitions; p++) {
            GRBLinExpr partitionSizeConstr = new GRBLinExpr();
            Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
            while (deviceItr.hasNext()) {
                DeviceVertex v = deviceItr.next();
                Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    Device src = ((DeviceVertex)e.getSource()).getDevice();
                    Device dst = ((DeviceVertex)e.getDestination()).getDevice();
                    partitionSizeConstr.addTerm(1.0, getLinkVar(src, dst, p));
                    partitionSizeConstr.addTerm(1.0, getLinkVar(dst, src, p));
                }
            }
            int partitionSize = (this.totalLinks) / (this.numPartitions);
            _solver.addConstr(partitionSizeConstr, GRB.GREATER_EQUAL, partitionSize - 1, 
                "SIZE-" + Integer.toString(p));
        }  
    }

    private void addFlowAssignedConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) { 
            ExtendedTopologyGraph etg = etgs.get(flow);

            for (int p = 0; p < this.numPartitions; p++) {
                Iterator<Vertex> vertexItr = etg.getVerticesIterator();

                GRBLinExpr flowAssignedConstr = new GRBLinExpr();
                flowAssignedConstr.addTerm(1000.0, getFlowVar(flow, p));

                while (vertexItr.hasNext()) {
                    Vertex v = vertexItr.next();
                    Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                    for (DirectedEdge e : edges) {
                        if (e.getType() == EdgeType.INTER_DEVICE) {
                            Device src = ((ProcessVertex) v).getProcess().getDevice();
                            Device dst = ((ProcessVertex) e.getDestination()).getProcess().getDevice();
                            flowAssignedConstr.addTerm(-1.0, getLinkVar(src, dst, p));
                        }
                    }
                }
                _solver.addConstr(flowAssignedConstr, GRB.GREATER_EQUAL, 0.0, 
                    "FLOWASSIGN-" + flow.toString() + ":" + Integer.toString(p));
            }
        }
    }

    private void addFlowPartitionObjective() throws GRBException {
        for (int p = 0; p < this.numPartitions; p++) {
            GRBLinExpr maxConstr = new GRBLinExpr();
            maxConstr.addTerm(1.0, this.maxFlowPartition);
            for (Flow flow : etgs.keySet()) {
                maxConstr.addTerm(-1.0, getFlowVar(flow, p));
            }
            _solver.addConstr(maxConstr, GRB.GREATER_EQUAL, 0.0, "MAXFLOW-" + Integer.toString(p));
        }

        GRBLinExpr objective = new GRBLinExpr();
        objective.addTerm(1.0, this.maxFlowPartition);
        _solver.setObjective(objective, GRB.MINIMIZE);
    }
    

    public HashMap<Pair<Device, Device>, Integer> split() throws GRBException {
        try {
            env = new GRBEnv("quark.log");
            // env.set(GRB.IntParam.LogToConsole, 0);
            // env.set(GRB.IntParam.BarHomogeneous, 1);
            // env.set(GRB.IntParam.Method, 1);
            env.set(GRB.DoubleParam.TimeLimit, 3600);
            env.set(GRB.DoubleParam.MIPGap, 0.1);
            _solver = new GRBModel(env);
        } catch (GRBException e) {
            e.printStackTrace();
        }

        initializeVariables();
        addLinkAssignedConstraints();
        addPartitionSizeConstraints();
        addFlowAssignedConstraints();
        addFlowPartitionObjective();
        
        _solver.update();

        long startTime = System.currentTimeMillis();
        _solver.optimize();
        long endTime = System.currentTimeMillis();
        long constraintSolveTime = (endTime - startTime);
        System.out.println("Time taken to solve constraints : " + (endTime - startTime) + "ms");

        HashMap<Pair<Device, Device>, Integer> linkPartitionMap = new HashMap<Pair<Device, Device>, Integer>();
        if (_solver.get(GRB.IntAttr.Status) == GRB.Status.OPTIMAL || _solver.get(GRB.IntAttr.Status) == GRB.Status.TIME_LIMIT) {
            try {
                BufferedWriter writer = new BufferedWriter(
                        new FileWriter(System.getProperty("user.home") + "/QARC-Partitions", true));
                writer.write(this.settings.getConfigsDirectory()  + "\t" + 
                        Integer.toString(this.settings.getCongestionFreeFailureCount()) + "\t" + 
                        Integer.toString(this.numPartitions) + "\t" +
                        Integer.toString(this.totalLinks) + "\t" +
                        Integer.toString(this.etgs.size()) + "\t" +
                        Double.toString(_solver.get(GRB.DoubleAttr.ObjVal)) + "\t" + 
                        Long.toString(constraintSolveTime) + "\n");
                writer.close();
            } catch (IOException e) {}
            
            if (_solver.get(GRB.DoubleAttr.ObjVal) < this.etgs.size() - 1) {
                Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
                while (deviceItr.hasNext()) {
                    DeviceVertex v = deviceItr.next();
                    Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
                    for (DirectedEdge e : edges) {
                        Device src = ((DeviceVertex)e.getSource()).getDevice();
                        Device dst = ((DeviceVertex)e.getDestination()).getDevice();

                        for (int p = 0; p < this.numPartitions; p++) {
                            if (getLinkVar(src, dst, p).get(GRB.DoubleAttr.X) > 0.99) {
                                linkPartitionMap.put(new Pair<>(src, dst), p);
                            }

                            if (getLinkVar(dst, src, p).get(GRB.DoubleAttr.X) > 0.99) {
                                linkPartitionMap.put(new Pair<>(dst, src), p);
                            }
                        }
                    }
                }
            } else {
                // Optimization yielded nothing useful. Use simple equal partitioning
                int currPartition = 0;
                Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
                while (deviceItr.hasNext()) {
                    DeviceVertex v = deviceItr.next();
                    Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
                    for (DirectedEdge e : edges) {
                        Device src = ((DeviceVertex)e.getSource()).getDevice();
                        Device dst = ((DeviceVertex)e.getDestination()).getDevice();
                        linkPartitionMap.put(new Pair<>(src, dst), currPartition);
                        linkPartitionMap.put(new Pair<>(dst, src), currPartition);
                        currPartition = (currPartition + 1) % this.numPartitions;
                    }
                }
            }
            return linkPartitionMap;
        }
        else {
            return null;
        }        
    }
}