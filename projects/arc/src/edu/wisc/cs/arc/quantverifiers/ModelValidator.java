package edu.wisc.cs.arc.quantverifiers;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.Device;
import edu.wisc.cs.arc.graphs.DeviceVertex;
import edu.wisc.cs.arc.graphs.DeviceGraph;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.Process;
import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.graphs.Vertex;
import edu.wisc.cs.arc.graphs.Vertex.VertexType;
import edu.wisc.cs.arc.quantverifiers.CongestionFree;
import edu.wisc.cs.arc.verifiers.VerifierException;

import gurobi.*;

import javafx.util.Pair; 
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.Double;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import org.jgrapht.alg.shortestpath.DijkstraShortestPath;


class VertexDistance {
    public Vertex v;
    public double dist;

    public VertexDistance(Vertex v, double dist) {
        this.v = v;
        this.dist = dist;
    }
}

class CompareVertex implements Comparator<VertexDistance>
{
    // Used for sorting in descending order of distance
    public int compare(VertexDistance a, VertexDistance b)
    {
        return (int) (b.dist - a.dist);
    }
}

public class ModelValidator {
	/** The Congestion-free verifier */
    private CongestionFree cfVerifier;

    /** The ETGs to use for verification */
    private Map<Flow, ? extends ExtendedTopologyGraph> etgs;
     
    /** The device ETG */
    private DeviceGraph deviceEtg;

	public ModelValidator(CongestionFree cfVerifier, 
		Map<Flow, ? extends ExtendedTopologyGraph> etgs,
		DeviceGraph deviceEtg) {
		this.cfVerifier = cfVerifier;
		this.etgs = etgs;
		this.deviceEtg = deviceEtg;
	}

	public void printModelToFile() throws GRBException {
		try {
	    	BufferedWriter writer = new BufferedWriter(new FileWriter(".quark-solution.log"));
	    	
	    	for (GRBVar var : cfVerifier.getSolver().getVars()) {
	    		writer.write(var.get(GRB.StringAttr.VarName) + "\t" +
	    			Double.toString(var.get(GRB.DoubleAttr.X)) + "\n");
	    	}
	    	writer.close();
	    } catch (IOException e) {}
	}

	public boolean validateFlowETG(Flow flow, ExtendedTopologyGraph etg) 
		throws GRBException {
        DijkstraShortestPath<Vertex, DirectedEdge<Vertex>> shortestPathComputer 
        = new DijkstraShortestPath<Vertex, DirectedEdge<Vertex>>(etg.getGraph());

        double sp = shortestPathComputer.getPathWeight(etg.getFlowSourceVertex(), etg.getFlowDestinationVertex());
        if (Double.isInfinite(sp)) {
            System.out.println("Flow not reachable " + flow.toString() + " " + Double.toString(sp));
            System.exit(0);
        } 

        // List<String> path = new ArrayList<String>();
        // Iterator<Vertex> vertexItr = etg.getVerticesIterator();
        // while (vertexItr.hasNext()) {
        //     Vertex v = vertexItr.next();
        //     double maxFlow = 0;

        //     Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
        //     for (DirectedEdge e : edges) {
        //         double fwdFlow = (cfVerifier.getForwardVar(flow, v, e.getDestination())).get(GRB.DoubleAttr.X);
        //         if (fwdFlow > 0 && maxFlow == 0) {
        //             maxFlow = fwdFlow;
        //         } else if (fwdFlow > 0 && maxFlow > 0 && (Integer) 10000*maxFlow != (Integer) 10000*fwdFlow) {
        //             System.out.println("ECMP violated.");
        //             for (DirectedEdge e1 : edges) {
        //                 double f = (cfVerifier.getForwardVar(flow, v, e1.getDestination())).get(GRB.DoubleAttr.X);
        //                 System.out.print(Double.toString(f) + " ");
        //             }
        //             return false;
        //         }
        //     }
        // }
        return true;
	}

    private Set<DirectedEdge> extractLinkFailureScenario() throws GRBException {
        HashSet<DirectedEdge> failedLinks = new HashSet<DirectedEdge>();

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
            
            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex)e.getSource();
                DeviceVertex dstdv = (DeviceVertex)e.getDestination();
                GRBVar linkFail = cfVerifier.getLinkFailureVar(srcdv.getDevice(),
                    dstdv.getDevice());

                if (linkFail.get(GRB.DoubleAttr.X) == 1) {
                    failedLinks.add(e);
                }
            }
        }
        if (failedLinks.size() >= 1) {
            System.out.println("Congestion under failure===");
            for (DirectedEdge link : failedLinks) {
                System.out.println(link.toString());
            }
            System.out.println("====================");
        }
        return failedLinks;
    }

    public boolean checkCongestion(Set<DirectedEdge> failedLinks, boolean validate) throws GRBException {
        long startTime = System.currentTimeMillis();
        HashMap<Pair<DeviceVertex, DeviceVertex>, Double> linkUtil = new HashMap<Pair<DeviceVertex, DeviceVertex>, Double>();
        int ecmpViolations = 0;
        int bestRouteViolations = 0;
        int extraFlowViolations = 0;

        HashMap<Flow, Double> newTrafficCharacteristics = new HashMap<Flow, Double>();
        for (Flow flow : etgs.keySet()) {
            if (validate) {
                if (cfVerifier.getFlowVar(flow).get(GRB.DoubleAttr.X) >= 0.99) {
                    // Active flow. Check extra traffic.
                    double extraFlow = cfVerifier.getDeltaSourceVar(flow).get(GRB.DoubleAttr.X);
                    newTrafficCharacteristics.put(flow, flow.getTrafficCharacteristic() * (1+extraFlow));
                    // System.out.println("Flow " + flow.toString() + " := " + Double.toString(newTrafficCharacteristics.get(flow)) + " " + Double.toString(flow.getTrafficCharacteristic()));
                }
                else {
                    newTrafficCharacteristics.put(flow, 0.0);
                }
            }
            else {
                newTrafficCharacteristics.put(flow, flow.getTrafficCharacteristic());
            }
        }

        if (validate) {
            int deltaFlowViolations = 0;
            int zeroFlowViolations = 0;
            for (Flow flow : etgs.keySet()) {
                ExtendedTopologyGraph etg = etgs.get(flow);
                Iterator<Vertex> vertexItr = etg.getVerticesIterator();
                while (vertexItr.hasNext()) {
                    Vertex v = vertexItr.next();
                    Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                    for (DirectedEdge e : edges) {
                        double modelFlow = cfVerifier.getForwardVar(flow, v, e.getDestination()).get(GRB.DoubleAttr.X);
                        double deltaFlow = cfVerifier.getDeltaVar(flow, v, e.getDestination()).get(GRB.DoubleAttr.X);
                        if (deltaFlow - modelFlow > 0.01) {
                            // System.out.println("Delta Flow Violations? " + Double.toString(modelFlow) 
                            // + " " + Double.toString(deltaFlow));
                            deltaFlowViolations++;
                        }
                        // if (deltaFlow > 0.01) zeroFlowViolations++;
                    }
                }
            }
            System.out.println("Number of delta Flow Violations = " + Integer.toString(deltaFlowViolations) + " zero flow " + Integer.toString(zeroFlowViolations));
        }

        
        // Initialize link utilizations
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex)e.getSource();
                DeviceVertex dstdv = (DeviceVertex)e.getDestination();
                linkUtil.put(new Pair<DeviceVertex, DeviceVertex>(srcdv, dstdv), 0.0);
                linkUtil.put(new Pair<DeviceVertex, DeviceVertex>(dstdv, srcdv), 0.0);
            }
        }

        int disconnected = 0;
        for (Flow flow : etgs.keySet()) {
            double extraFlowFactor = newTrafficCharacteristics.get(flow) / flow.getTrafficCharacteristic() - 1;

            if (validate) {
                int flowViolations = 0;
                if (cfVerifier.cutFlows.contains(flow) && cfVerifier.getFlowVar(flow).get(GRB.DoubleAttr.X) <= 0.001) {
                    // Check if all Fwd is set to 0.
                    ExtendedTopologyGraph etg = etgs.get(flow);
                    Iterator<Vertex> vertexItr = etg.getVerticesIterator();
                    
                    while (vertexItr.hasNext()) {
                        Vertex v = vertexItr.next();
                        Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                        for (DirectedEdge e : edges) {
                            if (cfVerifier.getForwardVar(flow, v, e.getDestination()).get(GRB.DoubleAttr.X) >= 0.001){
                                flowViolations += 1;
                            }
                        }   
                    }
                }
                if (flowViolations > 0) {
                    System.out.println("FLOW VIOLATIONS! for " + flow.toString());
                    System.exit(0);
                }
            }
                  
            if (newTrafficCharacteristics.get(flow) == 0.0) {
                continue; // No traffic
            }

            ExtendedTopologyGraph etg = (ExtendedTopologyGraph) etgs.get(flow).clone();
            
            // Remove failed links
            List<DirectedEdge<Vertex>> failedEdges = new ArrayList<DirectedEdge<Vertex>>();

            Iterator<DirectedEdge<Vertex>> edgeItr = etg.getEdgesIterator();
            while (edgeItr.hasNext()) {
                DirectedEdge<Vertex> e = edgeItr.next();
                if (e.getType() == EdgeType.INTER_DEVICE) {
                    for (DirectedEdge link : failedLinks) {
                        if ((getDeviceEdge(e).getKey() == (DeviceVertex) link.getSource() && 
                            getDeviceEdge(e).getValue() == (DeviceVertex) link.getDestination()) || 
                            (getDeviceEdge(e).getKey() == (DeviceVertex) link.getDestination() && 
                            getDeviceEdge(e).getValue() == (DeviceVertex) link.getSource()) ) {
                            failedEdges.add(e);
                        }
                    }
                }
            }

            for (DirectedEdge e : failedEdges) {
                etg.removeEdge(e);
            }

            DijkstraShortestPath<Vertex, DirectedEdge<Vertex>> shortestPathComputer 
            = new DijkstraShortestPath<Vertex, DirectedEdge<Vertex>>(etg.getGraph());

            double dist = shortestPathComputer.getPathWeight(etg.getFlowSourceVertex(), etg.getFlowDestinationVertex());
            if (dist == Double.POSITIVE_INFINITY) {
                disconnected++;
                continue;
            }

            HashMap<Vertex, Double> vertexFlow = new HashMap<Vertex, Double>();
            PriorityQueue vertexQueue = new PriorityQueue<VertexDistance>(1, new CompareVertex());
            Vertex dstV = etg.getFlowDestinationVertex();

            // Set vertex flows to 0
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();
            while(vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                vertexFlow.put(v, 0.0);
            }

            vertexFlow.put(etg.getFlowSourceVertex(), newTrafficCharacteristics.get(flow));
            vertexQueue.add(new VertexDistance(etg.getFlowSourceVertex(), 
                shortestPathComputer.getPathWeight(etg.getFlowSourceVertex(),
                    etg.getFlowDestinationVertex())));

            while (vertexQueue.size() != 0) {
                Vertex v = ((VertexDistance) vertexQueue.poll()).v;
                if (v.getType() == VertexType.DESTINATION) break;
                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                Set<DirectedEdge<Vertex>> nextHops = new HashSet<DirectedEdge<Vertex>>();
                double minDist = shortestPathComputer.getPathWeight(v, dstV);

                // Verify best route variables
                if (validate) {
                    if (Math.abs(cfVerifier.getBestRouteVar(flow, v).get(GRB.DoubleAttr.X) - minDist) > 0.0001) {
                        System.out.println("Violation of best route " + v.toString() + ":->" + 
                           Double.toString(cfVerifier.getBestRouteVar(flow, v).get(GRB.DoubleAttr.X))+ "-----" + Double.toString(minDist));
                        
                        System.out.println(newTrafficCharacteristics.get(flow));
                        bestRouteViolations += 1;
                    }
                }

                for (DirectedEdge e : edges) {
                    dist = shortestPathComputer.getPathWeight(e.getDestination(), dstV) + e.getWeight();
                    // System.out.println("Traversing edge " + e.toString() + " " + Double.toString(dist) + " " + Double.toString(minDist));
                    if (dist <= minDist) {
                        // System.out.println("\t" + e.getDestination().toString());
                        nextHops.add(e);
                    }
                }
                if (nextHops.size() == 0) {
                    System.out.println("Weird, should not happen");
                    //System.exit(0);
                }
                
                double vflow = vertexFlow.get(v) / nextHops.size();
                for (DirectedEdge nh : nextHops) {
                    /* Check for ECMP violations */
                    if (validate) {
                        double modelFlow = cfVerifier.getForwardVar(flow, nh.getSource(), nh.getDestination()).get(GRB.DoubleAttr.X) + 
                        cfVerifier.getDeltaVar(flow, nh.getSource(), nh.getDestination()).get(GRB.DoubleAttr.X) ;
                        if (Math.abs(vflow/flow.getTrafficCharacteristic() - modelFlow) >= 0.0001) {
                            System.out.println("ECMP Violation " + flow.toString() + " " + nh.toString() + " " + 
                                Double.toString(Math.abs(vflow/flow.getTrafficCharacteristic())) 
                                    + "<>" + Double.toString(modelFlow));
                            ecmpViolations += 1;
                        }
                    }

                    if (getDeviceEdge(nh) != null) {
                       
                        double flowOnLink = vflow;
                        if (validate) {
                            double modelFlow = (cfVerifier.getForwardVar(flow, nh.getSource(),  nh.getDestination())).get(GRB.DoubleAttr.X);
                            flowOnLink = modelFlow * flow.getTrafficCharacteristic();

                            double deltaFlow = (cfVerifier.getDeltaVar(flow, nh.getSource(),  nh.getDestination())).get(GRB.DoubleAttr.X);
                            flowOnLink += deltaFlow * flow.getTrafficCharacteristic();

                            // System.out.println("HEEREE " + flow.toString() + " ---- " + Double.toString(extraFlowFactor) + " " + Double.toString(modelFlow) + " " + Double.toString(deltaFlow));

                            if (Math.abs(deltaFlow/modelFlow - extraFlowFactor) > 0.01) {
                                extraFlowViolations += 1;
                                
                            }
                            // System.out.println(flow.toString() + "->" + Double.toString(flow.getTrafficCharacteristic()) + "->" +  Double.toString(flowOnLink));
                        }

                        Pair<DeviceVertex, DeviceVertex> link = getDeviceEdge(nh);
                        linkUtil.put(link, linkUtil.get(link) + flowOnLink);
                    }

                    vertexFlow.put(nh.getDestination(), 
                        vertexFlow.get(nh.getDestination()) + vflow);

                    boolean inQueue = false;
                    Iterator<VertexDistance> queueItr = vertexQueue.iterator();
                    while(queueItr.hasNext()) {
                        VertexDistance vd = queueItr.next();
                        if (vd.v == nh.getDestination()) inQueue = true;
                    }
                    if (!inQueue) {
                        vertexQueue.add(new VertexDistance(nh.getDestination(),
                            shortestPathComputer.getPathWeight(
                                nh.getDestination(), dstV)));
                    }
                }
            }
        }
        
        System.out.println("Best Route Violations =" + Integer.toString(bestRouteViolations));
        System.out.println("ECMP Violations =" + Integer.toString(ecmpViolations));
        System.out.println("Extra flow Violations =" + Integer.toString(extraFlowViolations));
        
        boolean congestedLink = false;
        double alu = 0;
        int linkCount = 0;
        double mlu = 0;

     
        // System.out.println("Model validator utils");
        for (Pair<DeviceVertex, DeviceVertex> link : linkUtil.keySet()) {
            double linkCap = cfVerifier.linkCapacities.get(new Pair(link.getKey().getDevice(), link.getValue().getDevice()));
            if (cfVerifier.linkCapacities.get(new Pair(link.getKey().getDevice(), link.getValue().getDevice())) == null) 
                linkCap = 100.0;
            // System.out.println(link.getKey().toString() + " ==> " + link.getValue().toString() + " " + 
            //     Double.toString(linkUtil.get(link)));
            if (linkUtil.get(link)/linkCap > mlu) {mlu = linkUtil.get(link)/linkCap;}
            alu += linkUtil.get(link)/linkCap;
            linkCount++;
            if (linkUtil.get(link) >= linkCap - 0.1) {
                System.out.println(link.getKey().toString() + " ==> " + link.getValue().toString() + " " + 
                    Double.toString(linkUtil.get(link)) + "/" + Double.toString(linkCap));
                congestedLink = true;
            }
        }
           
        if (congestedLink) {
            System.out.println("Yes, congestion");
        }
        if (!congestedLink) {
            System.out.println("No, no congestion");
        }

        if (disconnected > 0) {
            System.out.println("Disconnected ETGs are " + Integer.toString(disconnected));
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(System.getProperty("user.home")+"/QARC-Utilization"));
            writer.write("MLU/ALU\t" + Double.toString(mlu) + "\t" + Double.toString(alu/linkCount) + "\n");
            writer.close();

            writer = new BufferedWriter(new FileWriter(System.getProperty("user.home")+"/QARC-Networks", true));
            writer.write("Network/Links/Flows\t" + cfVerifier.settings.getConfigsDirectory() + "\t" + Integer.toString(linkCount) + "\t" + Integer.toString(etgs.keySet().size()) + "\n");
            writer.close();
        } catch (IOException e) {}
        System.out.println("Baseline is " + Long.toString(System.currentTimeMillis() - startTime));
        return congestedLink;
    }

    public Pair<DeviceVertex, DeviceVertex> getDeviceEdge(DirectedEdge<Vertex> e) {
        // System.out.println(((ProcessVertex) e.getSource()).getProcess().getDevice().toString());
        try {
            Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
            while (deviceItr.hasNext()) {
                DeviceVertex v = deviceItr.next();
                Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);
                
                for (DirectedEdge<DeviceVertex> edge : edges) {
                    // System.out.println(((DeviceVertex) edge.getSource()).getDevice().toString());

                    if (((DeviceVertex) edge.getSource()).getDevice() == 
                        ((ProcessVertex) e.getSource()).getProcess().getDevice() && 
                        ((DeviceVertex) edge.getDestination()).getDevice() == 
                        ((ProcessVertex) e.getDestination()).getProcess().getDevice()) {
                            return new Pair<DeviceVertex, DeviceVertex>(edge.getSource(), edge.getDestination());
                    } else if (((DeviceVertex) edge.getDestination()).getDevice() == 
                        ((ProcessVertex) e.getSource()).getProcess().getDevice() && 
                        ((DeviceVertex) edge.getSource()).getDevice() == 
                        ((ProcessVertex) e.getDestination()).getProcess().getDevice()) {
                            return new Pair<DeviceVertex, DeviceVertex>(edge.getDestination(), edge.getSource());
                    }
                }
            }
            return null;
        } catch(NullPointerException nexp) {
            return null;
        }
    }

    public boolean verifyCongestion() throws GRBException{
        boolean congestion = false;
        int scenario = 0;
        Iterator<DirectedEdge<DeviceVertex>> linkItr1 = deviceEtg.getEdgesIterator();
        while(linkItr1.hasNext()) {
            DirectedEdge<DeviceVertex> link1 = linkItr1.next();
            Iterator<DirectedEdge<DeviceVertex>> linkItr2 = deviceEtg.getEdgesIterator();
            while(linkItr2.hasNext()) {
                HashSet<DirectedEdge> failedLinks = new HashSet<DirectedEdge>();
                DirectedEdge<DeviceVertex> link2 = linkItr2.next();
                if (link1 != link2) {
                    failedLinks.add(link1);
                    failedLinks.add(link2);
                    System.out.println("Checking congestion for " + Integer.toString(scenario));
                    scenario++;
                    if (checkCongestion(failedLinks, false)) {
                        congestion = true;
                    }
                }
            }
        }
        if (congestion) {
            System.out.println("Yes, congestion exists");
        } else {
            System.out.println("Proven no congestion");
        }
        return congestion;
    }

	public boolean validate(boolean satisfiable) throws GRBException {
		try {
            if (satisfiable)
                return checkCongestion(extractLinkFailureScenario(), satisfiable);
            else
                return checkCongestion(new HashSet<DirectedEdge>(), satisfiable);
    	} catch(GRBException e) {
    		System.out.println(e.getMessage());
            return false;
    	}
	}
}