package edu.wisc.cs.arc.quantverifiers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.Random;
import javafx.util.Pair;

import edu.wisc.cs.arc.Settings;
import edu.wisc.cs.arc.graphs.Device;
import edu.wisc.cs.arc.graphs.DeviceGraph;
import edu.wisc.cs.arc.graphs.DeviceVertex;
import edu.wisc.cs.arc.graphs.DirectedEdge;
import edu.wisc.cs.arc.graphs.DirectedEdge.EdgeType;
import edu.wisc.cs.arc.graphs.ExtendedTopologyGraph;
import edu.wisc.cs.arc.graphs.Flow;
import edu.wisc.cs.arc.graphs.Process;
import edu.wisc.cs.arc.graphs.ProcessVertex;
import edu.wisc.cs.arc.graphs.Vertex;
import edu.wisc.cs.arc.graphs.Vertex.VertexType;
import edu.wisc.cs.arc.verifiers.AlwaysReachable;
import edu.wisc.cs.arc.verifiers.VerifierException;
import edu.wisc.cs.arc.verifiers.VerifierResult;
import gurobi.*;

public class RepairScheduler {
    /** The ETGs to use for verification */
    public ConcurrentHashMap<Flow, ExtendedTopologyGraph> etgs;

    /** Settings for the verification process */
    private Settings settings;

    /** The device ETG */
    private DeviceGraph deviceEtg;

    /** Maximum number of simultaneous link failures */
    private int maxLinkFailures = 2;

    private boolean parallelize = true;

    private boolean partitionedRepair = false;
    private int totalPartitions = 0;
    private int partitionNo = 0;

    private int numberLinks = 0;

    private GRBEnv env;
    private GRBModel _solver;

    private HashMap<Flow, GRBVar> flowVariables;
    private HashMap<Flow, Map<Vertex, GRBVar>> bestRouteVariables;
    private HashMap<Flow, Map<Vertex, Map<Vertex, GRBVar>>> forwardVariables;
    private HashMap<Device, Map<Device, GRBVar>> linkFailureVariables;
    private HashMap<Device, Map<Device, GRBVar>> linkCongestionVariables;
    private HashMap<Flow, Map<Vertex, GRBVar>> maxForwardVariables;
    private HashMap<Flow, Map<Vertex, GRBVar>> minForwardVariables;
    private GRBVar Zero;

    public HashMap<Pair<Device, Device>, Double> linkCapacities;
    private ConcurrentHashMap<Pair<Device, Device>, Double> newCapacities;

    private ConcurrentHashMap<Flow, Boolean> cutFlows;

    private long repairTime;

    public RepairScheduler(Map<Flow, ExtendedTopologyGraph> etgs, DeviceGraph deviceEtg, Settings settings) {
        this.etgs = new ConcurrentHashMap(etgs);
        this.settings = settings;
        this.deviceEtg = deviceEtg;
        this.maxLinkFailures = this.settings.getCongestionFreeFailureCount();
        this.cutFlows = new ConcurrentHashMap<Flow, Boolean>();
        this.newCapacities = new ConcurrentHashMap<Pair<Device, Device>, Double>();
        this.linkCapacities = new HashMap<Pair<Device, Device>, Double>();
        initializeLinkCapacities();

        this.totalPartitions = this.settings.getVerificationPartitioned();
        this.partitionNo = this.settings.getPartitionNo();
        if (this.totalPartitions > 0 && this.partitionNo >= 0) {
            // Valid partitioning
            this.partitionedRepair = true;
        }

    }

    public Device findDevice(String dev) {
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            if (v.getDevice().toString().equals(dev))
                return v.getDevice();
        }
        return null;
    }

    private void initializeLinkCapacities() {
        // Read/Generate Link Capacities for 40/100 Gbps
        try {
            File linkcapacityFile = new File(settings.getConfigsDirectory() + "/linkcapacities.csv");
            FileReader fileReader = new FileReader(linkcapacityFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] fields = line.split(",");
                if (fields.length == 3) {
                    Device src = findDevice(fields[0]);
                    Device dst = findDevice(fields[1]);
                    double capacity = Double.parseDouble(fields[2]);
                    // System.out.println(fields[0] + " " + fields[1] + " " + fields[2]);
                    if (src != null && dst != null) {
                        this.linkCapacities.put(new Pair(src, dst), capacity);
                        this.linkCapacities.put(new Pair(dst, src), capacity);
                    }
                }
            }
        } catch (IOException ioe) {
            // Generate link capacities
            try {
                Random random = new Random();
                BufferedWriter writer = new BufferedWriter(
                        new FileWriter(settings.getConfigsDirectory() + "/linkcapacities.csv"));
                Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
                while (deviceItr.hasNext()) {
                    DeviceVertex v = deviceItr.next();
                    Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

                    for (DirectedEdge e : edges) {
                        DeviceVertex srcdv = (DeviceVertex) e.getSource();
                        DeviceVertex dstdv = (DeviceVertex) e.getDestination();

                        double linkCap = (random.nextInt(2) == 0) ? 40.0 : 100.0;
                        this.linkCapacities.put(new Pair(srcdv.getDevice(), dstdv.getDevice()), linkCap);
                        this.linkCapacities.put(new Pair(dstdv.getDevice(), srcdv.getDevice()), linkCap);

                        writer.write(srcdv.getDevice().toString() + "," + dstdv.getDevice().toString() + ","
                                + Double.toString(linkCap) + "\n");
                    }
                }
                writer.close();
            } catch (IOException ioe1) {
                System.out.println("ERROR: Cannot write link capacities to file");
            }
        }

        // New Capacities
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

            for (DirectedEdge<DeviceVertex> e : edges) {
                Device src = e.getSource().getDevice();
                Device dst = e.getDestination().getDevice();
                newCapacities.put(new Pair(src, dst), 0.0);
                newCapacities.put(new Pair(dst, src), 0.0);
            }
        }
    }

    private void findCutFlows() {
        AlwaysReachable minCut = new AlwaysReachable(new ConcurrentHashMap<Flow, ExtendedTopologyGraph>(this.etgs),
        this.settings);
        for (Flow flow : etgs.keySet()) {
            VerifierResult result = minCut.verify(flow, 1);
            if (!result.propertyHolds()) {
                this.cutFlows.put(flow, true);
            }
        }
    }

    GRBVar getFlowVar(Flow f) throws GRBException {
        if (flowVariables.get(f) == null) {
            flowVariables.put(f, _solver.addVar(0.0, 1.0, 0.0, GRB.BINARY, "Flow:" + f.toString()));
        }
        return flowVariables.get(f);
    }

    GRBVar getBestRouteVar(Flow f, Vertex v) throws GRBException {
        if (bestRouteVariables.get(f) == null) {
            bestRouteVariables.put(f, new HashMap<Vertex, GRBVar>());
        }

        if (bestRouteVariables.get(f).get(v) == null) {
            bestRouteVariables.get(f).put(v,
                    _solver.addVar(0.0, 10000, 0.0, GRB.CONTINUOUS, "Best:" + v.toString() + ":" + f.toString()));
        }
        return bestRouteVariables.get(f).get(v);
    }

    GRBVar getForwardVar(Flow f, Vertex src, Vertex dst) throws GRBException {
        if (forwardVariables.get(f) == null) {
            forwardVariables.put(f, new HashMap<Vertex, Map<Vertex, GRBVar>>());
        }

        if (forwardVariables.get(f).get(src) == null) {
            forwardVariables.get(f).put(src, new HashMap<Vertex, GRBVar>());
        }

        if (forwardVariables.get(f).get(src).get(dst) == null) {
            forwardVariables.get(f).get(src).put(dst, _solver.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS,
                    "Fwd:" + f.toString() + ":" + src.toString() + "->" + dst.toString()));
        }
        return forwardVariables.get(f).get(src).get(dst);
    }

    GRBVar getMaxForwardVar(Flow f, Vertex v) throws GRBException {
        if (maxForwardVariables.get(f) == null) {
            maxForwardVariables.put(f, new HashMap<Vertex, GRBVar>());
        }

        if (maxForwardVariables.get(f).get(v) == null) {
            maxForwardVariables.get(f).put(v,
                    _solver.addVar(0.0, 1, 0.0, GRB.CONTINUOUS, "MaxFwd:" + f.toString() + ":" + v.toString()));
        }
        return maxForwardVariables.get(f).get(v);
    }

    GRBVar getMinForwardVar(Flow f, Vertex v) throws GRBException {
        if (minForwardVariables.get(f) == null) {
            minForwardVariables.put(f, new HashMap<Vertex, GRBVar>());
        }

        if (minForwardVariables.get(f).get(v) == null) {
            minForwardVariables.get(f).put(v,
                    _solver.addVar(0.0, 1, 0.0, GRB.CONTINUOUS, "MinFwd:" + f.toString() + ":" + v.toString()));
        }
        return minForwardVariables.get(f).get(v);
    }

    GRBVar getLinkFailureVar(Process srcp, Process dstp) throws GRBException {
        return getLinkFailureVar(srcp.getDevice(), dstp.getDevice());
    }

    GRBVar getLinkFailureVar(Device src, Device dst) throws GRBException {
        if (src == dst) {
            throw new VerifierException("Does not hold for intra-device edges");
        }

        if (linkFailureVariables.get(src) == null) {
            linkFailureVariables.put(src, new HashMap<Device, GRBVar>());
        }
        if (linkFailureVariables.get(src).get(dst) == null) {
            GRBVar failVar;
            failVar = _solver.addVar(0, 1, 0, GRB.BINARY, "Fail:" + src.toString() + ":" + dst.toString());

            linkFailureVariables.get(src).put(dst, failVar);

            // Add failVar to (dst,src)
            if (linkFailureVariables.get(dst) == null) {
                linkFailureVariables.put(dst, new HashMap<Device, GRBVar>());
            }
            linkFailureVariables.get(dst).put(src, failVar);
        }
        return linkFailureVariables.get(src).get(dst);
    }

    GRBVar getLinkFailureVar(Device src, Device dst, boolean zero) throws GRBException {
        if (src == dst) {
            throw new VerifierException("Does not hold for intra-device edges");
        }

        if (zero == false)
            return getLinkFailureVar(src, dst);

        if (linkFailureVariables.get(src) == null) {
            linkFailureVariables.put(src, new HashMap<Device, GRBVar>());
        }
        if (linkFailureVariables.get(src).get(dst) == null) {
            linkFailureVariables.get(src).put(dst, this.Zero);

            // Add failVar to (dst,src)
            if (linkFailureVariables.get(dst) == null) {
                linkFailureVariables.put(dst, new HashMap<Device, GRBVar>());
            }
            linkFailureVariables.get(dst).put(src, this.Zero);
        }
        return linkFailureVariables.get(src).get(dst);
    }

    private void initializeVariables() throws GRBException {
        flowVariables = new HashMap<Flow, GRBVar>();
        bestRouteVariables = new HashMap<Flow, Map<Vertex, GRBVar>>();
        linkFailureVariables = new HashMap<Device, Map<Device, GRBVar>>();
        linkCongestionVariables = new HashMap<Device, Map<Device, GRBVar>>();
        forwardVariables = new HashMap<Flow, Map<Vertex, Map<Vertex, GRBVar>>>();
        maxForwardVariables = new HashMap<Flow, Map<Vertex, GRBVar>>();
        minForwardVariables = new HashMap<Flow, Map<Vertex, GRBVar>>();

        for (Flow flow : etgs.keySet()) {
            getFlowVar(flow);

            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                getBestRouteVar(flow, v);
                getMaxForwardVar(flow, v);
                getMinForwardVar(flow, v);

                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    getForwardVar(flow, v, e.getDestination());
                }
            }
        }

        this.Zero = _solver.addVar(0, 0, 0, GRB.CONTINUOUS, "Zero");

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

            for (DirectedEdge e : edges) {
                Device src = ((DeviceVertex) e.getSource()).getDevice();
                Device dst = ((DeviceVertex) e.getDestination()).getDevice();
                getLinkFailureVar(src, dst);
                this.numberLinks += 1;
            }
        }

        _solver.update();
    }

    private void addForwardFlowConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();

                if (v.getType() == VertexType.SOURCE) {
                    Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                    GRBLinExpr srcOutEdges = new GRBLinExpr();
                    for (DirectedEdge e : edges) {
                        srcOutEdges.addTerm(1.0, getForwardVar(flow, v, e.getDestination()));
                    }
                    if (this.cutFlows.containsKey(flow)) {
                        srcOutEdges.addTerm(-1.0, getFlowVar(flow));
                    } else {
                        srcOutEdges.addConstant(-1.0);
                    }
                    _solver.addConstr(srcOutEdges, GRB.EQUAL, 0.0, "SRCFLOW-" + flow.toString());
                } else if (v.getType() == VertexType.DESTINATION) {
                    Set<DirectedEdge<Vertex>> edges = etg.getIncomingEdges(v);
                    GRBLinExpr dstInEdges = new GRBLinExpr();
                    for (DirectedEdge e : edges) {
                        dstInEdges.addTerm(1.0, getForwardVar(flow, e.getSource(), v));
                    }
                    if (this.cutFlows.containsKey(flow)) {
                        dstInEdges.addTerm(-1.0, getFlowVar(flow));
                    } else {
                        dstInEdges.addConstant(-1.0);
                    }
                    _solver.addConstr(dstInEdges, GRB.EQUAL, 0.0, "DSTFLOW-" + flow.toString());
                } else {
                    // Not source or destination. Forward Flow is preserved
                    GRBLinExpr fwdFlow = new GRBLinExpr();

                    Set<DirectedEdge<Vertex>> edges = etg.getIncomingEdges(v);
                    for (DirectedEdge e : edges) {
                        fwdFlow.addTerm(1.0, getForwardVar(flow, e.getSource(), v));
                    }

                    edges = etg.getOutgoingEdges(v);
                    for (DirectedEdge e : edges) {
                        fwdFlow.addTerm(-1.0, getForwardVar(flow, v, e.getDestination()));
                    }
                    // Incoming fwd flow - outgoing fwd flow is zero
                    _solver.addConstr(fwdFlow, GRB.EQUAL, 0.0, "FLOW-" + v.toString() + "-" + flow.toString());
                }
            }
        }
    }

    private void addLoadBalancingConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();

                if (v.getType() != VertexType.SOURCE && v.getType() != VertexType.DESTINATION) {
                    Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);

                    if (edges.size() <= 1)
                        continue; // No load balancing required

                    // Add load balancing constraints
                    for (DirectedEdge e : edges) {
                        // Add max Fwd bound
                        GRBLinExpr maxForwardBound = new GRBLinExpr();
                        maxForwardBound.addTerm(1.0, getForwardVar(flow, v, e.getDestination()));

                        maxForwardBound.addTerm(-1.0, getMaxForwardVar(flow, v));

                        _solver.addConstr(maxForwardBound, GRB.LESS_EQUAL, 0.0, "ABSUPPERBOUND");

                        GRBLinExpr minForwardBound = new GRBLinExpr();
                        minForwardBound.addTerm(1.0, getMinForwardVar(flow, v));
                        minForwardBound.addTerm(-1.0, getForwardVar(flow, v, e.getDestination()));
                        minForwardBound.addTerm(10.0, getBestRouteVar(flow, v));
                        minForwardBound.addTerm(-10.0, getBestRouteVar(flow, e.getDestination()));
                        minForwardBound.addConstant(-10.0 * e.getWeight());

                        if (e.getType() == EdgeType.INTER_DEVICE) {
                            ProcessVertex srcpv = (ProcessVertex) e.getSource();
                            ProcessVertex dstpv = (ProcessVertex) e.getDestination();
                            GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(), dstpv.getProcess());

                            minForwardBound.addTerm(-10, linkFail);
                        }

                        _solver.addConstr(minForwardBound, GRB.LESS_EQUAL, 0.0, "ABSLOWERBOUND");
                    }

                    // For ECMP-style load-balancing, lower bound == upper bound
                    GRBLinExpr ecmpConstraint = new GRBLinExpr();
                    ecmpConstraint.addTerm(-1.0, getMinForwardVar(flow, v));
                    ecmpConstraint.addTerm(1.0, getMaxForwardVar(flow, v));
                    _solver.addConstr(ecmpConstraint, GRB.EQUAL, 0.0, "ECMP" + flow.toString() + "@" + v.toString());
                }
            }
        }
    }

    private void addBestRouteConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();

                if (v.getType() == VertexType.DESTINATION) {
                    _solver.addConstr(getBestRouteVar(flow, v), GRB.EQUAL, 0.0, "DST" + flow.toString());
                }

                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    // If link is not failed, triangle inequality holds
                    GRBLinExpr bestSmallest = new GRBLinExpr();
                    bestSmallest.addTerm(1.0, getBestRouteVar(flow, e.getDestination()));

                    bestSmallest.addConstant(e.getWeight());
                    bestSmallest.addTerm(-1.0, getBestRouteVar(flow, v));

                    if (e.getType() == EdgeType.INTER_DEVICE) {
                        ProcessVertex srcpv = (ProcessVertex) e.getSource();
                        ProcessVertex dstpv = (ProcessVertex) e.getDestination();
                        GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(), dstpv.getProcess());

                        bestSmallest.addTerm(100000, linkFail);
                    }

                    _solver.addConstr(bestSmallest, GRB.GREATER_EQUAL, 0.0, "bestSmallest");
                }
            }
        }
    }

    private void addShortestPathsConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            GRBLinExpr sumForwardFlow = new GRBLinExpr();
            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    sumForwardFlow.addTerm(e.getWeight(), getForwardVar(flow, v, e.getDestination()));
                }
            }
            sumForwardFlow.addTerm(-1.0, getBestRouteVar(flow, etg.getFlowSourceVertex()));
            _solver.addConstr(sumForwardFlow, GRB.EQUAL, 0.0, "Shortest" + flow.toString());
        }
    }

    private void addValidForwardConstraints() throws GRBException {
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                Vertex v = vertexItr.next();
                Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                for (DirectedEdge e : edges) {
                    if (e.getType() == EdgeType.INTER_DEVICE) {
                        ProcessVertex srcpv = (ProcessVertex) e.getSource();
                        ProcessVertex dstpv = (ProcessVertex) e.getDestination();
                        GRBVar linkFail = getLinkFailureVar(srcpv.getProcess(), dstpv.getProcess());

                        GRBLinExpr validFwd = new GRBLinExpr();
                        validFwd.addTerm(1.0, getForwardVar(flow, v, e.getDestination()));
                        validFwd.addTerm(1, linkFail);
                        _solver.addConstr(validFwd, GRB.LESS_EQUAL, 1.0, "validFwd");
                    }
                }
            }
        }
    }

    private void addLinkFailureBoundConstraints() throws GRBException {
        HashSet<GRBVar> linkFailureVars = new HashSet<GRBVar>();
        GRBLinExpr failureSum = new GRBLinExpr();

        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

            for (DirectedEdge e : edges) {
                DeviceVertex srcdv = (DeviceVertex) e.getSource();
                DeviceVertex dstdv = (DeviceVertex) e.getDestination();
                GRBVar linkFail = getLinkFailureVar(srcdv.getDevice(), dstdv.getDevice());
                if (!linkFailureVars.contains(linkFail)) {
                    failureSum.addTerm(1.0, linkFail);
                    linkFailureVars.add(linkFail);
                }
            }
        }

        _solver.addConstr(failureSum, GRB.EQUAL, 8, "linkFailureBound");
    }

    private GRBLinExpr getLinkUtilization(Device src, Device dst) throws GRBException {
        GRBLinExpr linkUtil = new GRBLinExpr();
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Iterator<Vertex> vertexItr = etg.getVerticesIterator();

            while (vertexItr.hasNext()) {
                ProcessVertex v = (ProcessVertex) vertexItr.next();
                /** Assuming device names are unique */
                try {
                    if (v.getProcess().getDevice().getName().equals(src.getName())) {
                        Set<DirectedEdge<Vertex>> edges = etg.getOutgoingEdges(v);
                        for (DirectedEdge e : edges) {
                            if (e.getType() == EdgeType.INTER_DEVICE && ((ProcessVertex) e.getDestination())
                                    .getProcess().getDevice().getName().equals(dst.getName())) {
                                linkUtil.addTerm(flow.getTrafficCharacteristic(),
                                        getForwardVar(flow, v, e.getDestination()));
                            }
                        }
                    }
                } catch (NullPointerException ne) {
                    continue;
                }
            }
        }
        return linkUtil;
    }

    private void addNoCongestionConstraints() throws GRBException {
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

            for (DirectedEdge e : edges) {
                Device src = ((DeviceVertex) e.getSource()).getDevice();
                Device dst = ((DeviceVertex) e.getDestination()).getDevice();
                GRBLinExpr linkUtil1 = getLinkUtilization(src, dst);
                _solver.addConstr(linkUtil1, GRB.LESS_EQUAL, linkCapacities.get(new Pair(src, dst)),
                        "NoCongestion1-" + e.toString());

                GRBLinExpr linkUtil2 = getLinkUtilization(dst, src);
                _solver.addConstr(linkUtil2, GRB.LESS_EQUAL, linkCapacities.get(new Pair(dst, src)),
                        "NoCongestion2-" + e.toString());

            }
        }
    }

    private void addMaximalFailureObjective(GRBLinExpr objective) throws GRBException {
        GRBLinExpr progress = new GRBLinExpr();
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

            for (DirectedEdge e : edges) {
                Device src = ((DeviceVertex) e.getSource()).getDevice();
                Device dst = ((DeviceVertex) e.getDestination()).getDevice();
                GRBVar linkFail = getLinkFailureVar(src, dst);
                objective.addTerm(1.0, linkFail);
                progress.addTerm(1.0, linkFail);
            }
        }
        _solver.addConstr(progress, GRB.GREATER_EQUAL, 1.0, "Progress");
    }

    public void minimizeETGsParallel() throws GRBException {
        int numThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService pool = Executors.newFixedThreadPool(numThreads);

        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            Runnable minimizeTask = new ETGMinimizer(this, flow, etg, deviceEtg, this.maxLinkFailures);
            pool.execute(minimizeTask);
        }

        pool.shutdown();
        try {
            pool.awaitTermination(5000, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.out.println("Minimization interrupted. EXIT");
            System.exit(0);
        }
    }

    public void minimizeETGs() throws GRBException {
        int initialEdgeCount = 0;
        int prunedEdgeCount = 0;
        long startTime = System.currentTimeMillis();
        for (Flow flow : etgs.keySet()) {
            ExtendedTopologyGraph etg = etgs.get(flow);
            initialEdgeCount += etg.getEdgeCount();
            int edgesPruned = new ETGMinimizer(this, flow, etg, deviceEtg, this.maxLinkFailures).minimize();
            if (edgesPruned == -1) {
                // Remove this ETG
                System.out.println(flow.toString());
            } else if (edgesPruned == -2) {
                // this.cutFlows.add(flow);
            } else {
                prunedEdgeCount += edgesPruned;
            }
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Time taken to minimize ETGs: " + (endTime - startTime) + "ms");
        System.out.println("Edges pruned are " + prunedEdgeCount + "/" + initialEdgeCount);
    }

    public void repair() throws GRBException {
        if (settings.shouldMinimizeETGs()) {
            minimizeETGsParallel();
        }

        if (this.partitionedRepair) {
            try {
                File repairCapacitiesFile = new File(settings.getConfigsDirectory() + "/r"
                        + Integer.toString(this.maxLinkFailures) + "capacities.csv");
                FileReader fileReader = new FileReader(repairCapacitiesFile);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String[] fields = line.split(",");
                    Device src = findDevice(fields[0]);
                    Device dst = findDevice(fields[1]);
                    double newCap = Double.parseDouble(fields[2]);
                    this.newCapacities.put(new Pair(src,dst), newCap);
                }
                findCongestionFreeSchedule();
            } catch (IOException e) {
                findCapacities();
                try {
                    BufferedWriter writer = new BufferedWriter(
                        new FileWriter(settings.getConfigsDirectory() + "/r"
                        + Integer.toString(this.maxLinkFailures) + "capacities.csv"));
                    for (Pair link: this.newCapacities.keySet()) {
                            writer.write(link.getKey().toString() + "," + 
                            link.getValue().toString() + "," + Double.toString(this.newCapacities.get(link)) + "\n");                            
                    }
                    writer.close();
                }
                catch (IOException e1) {
                    System.out.println("Cant write new capacities to file");
                    System.exit(0);
                }

            }
        } else {
            findCapacities();
            try {
                BufferedWriter writer = new BufferedWriter(
                    new FileWriter(settings.getConfigsDirectory() + "/r"
                    + Integer.toString(this.maxLinkFailures) + "capacities.csv"));
                for (Pair link: this.newCapacities.keySet()) {
                        writer.write(link.getKey().toString() + "," + 
                        link.getValue().toString() + "," + Double.toString(this.newCapacities.get(link)) + "\n");                            
                }
                writer.close();
            }
            catch (IOException e1) {
                System.out.println("Cant write new capacities to file");
                System.exit(0);
            }
            findCongestionFreeSchedule();
        }
    }

    public void findCapacities() throws GRBException {
        Map<Flow, Map<Pair<Device, Device>, Double>> etgCapacities = new ConcurrentHashMap<Flow, Map<Pair<Device, Device>, Double>>();

        long startTime = System.currentTimeMillis();
        // List<Long> etgTimes = new ArrayList<Long>();

        int index = 0;
        for (Flow flow : etgs.keySet()) {
            if (!this.partitionedRepair || index % this.totalPartitions == this.partitionNo) {
                System.out.println(index);
                ExtendedTopologyGraph etg = etgs.get(flow);
                Map<Pair<Device, Device>, Double> capacities = new HashMap<Pair<Device, Device>, Double>();
                etgCapacities.put(flow, capacities);
                // long etgstartTime = System.currentTimeMillis();
                new ETGCongestionRepair(flow, etg, this.deviceEtg, this.settings, etgCapacities.get(flow)).repair();
                // etgTimes.add(System.currentTimeMillis() - etgstartTime);
            }
            index++;
        }

        index = 0;
        for (Flow flow : etgs.keySet()) {
            if (!this.partitionedRepair || index % this.totalPartitions == this.partitionNo) {
                Map<Pair<Device, Device>, Double> capacities = etgCapacities.get(flow);
                for (Pair link : capacities.keySet()) {
                    while (true) {
                        Double oldcap = newCapacities.get(link);
                        if (newCapacities.replace(link, oldcap, oldcap + capacities.get(link)))
                            break;
                    }
                }
            }
            index++;
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Time taken to repair ETGs: " + (endTime - startTime) + "ms");
        this.repairTime = endTime - startTime;
        try {
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(System.getProperty("user.home") + "/QARC-Repair-Timing"));
            writer.write("RT\t" + Long.toString(this.repairTime));
            writer.close();
        } catch (IOException e) {
        }
    }

    private int addChangedLinkCapacityConstraints() throws GRBException {
        int pending_changes = 0;
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

            for (DirectedEdge e : edges) {
                Device src = ((DeviceVertex) e.getSource()).getDevice();
                Device dst = ((DeviceVertex) e.getDestination()).getDevice();

                if ((this.newCapacities.get(new Pair(src, dst)) <= this.linkCapacities.get(new Pair(src, dst))) && 
                    (this.newCapacities.get(new Pair(dst, src)) <= this.linkCapacities.get(new Pair(dst, src)))) {
                    GRBVar linkFail = getLinkFailureVar(src, dst);

                    GRBLinExpr changed = new GRBLinExpr();
                    changed.addTerm(1.0, linkFail);
                    _solver.addConstr(changed, GRB.EQUAL, 0.0, "changed-" + e.toString());
                } else {
                    pending_changes++;
                    // System.out.println(e.toString());
                    // System.out.println(Double.toString(this.linkCapacities.get(new Pair(src, dst))) + " => " + 
                    //     Double.toString(this.newCapacities.get(new Pair(src, dst))));
                    //     System.out.println(Double.toString(this.linkCapacities.get(new Pair(dst, src))) + " => " + 
                    //     Double.toString(this.newCapacities.get(new Pair(dst, src))));
                }
            }
        }
        System.out.println("Number of pending changes: " + Integer.toString(pending_changes));
        return pending_changes;
    }

    private boolean changeCapacities() throws GRBException {
        boolean changedFlag = false;
        Iterator<DeviceVertex> deviceItr = deviceEtg.getVerticesIterator();
        while (deviceItr.hasNext()) {
            DeviceVertex v = deviceItr.next();
            Set<DirectedEdge<DeviceVertex>> edges = deviceEtg.getOutgoingEdges(v);

            for (DirectedEdge e : edges) {
                Device src = ((DeviceVertex) e.getSource()).getDevice();
                Device dst = ((DeviceVertex) e.getDestination()).getDevice();
                GRBVar linkFail = getLinkFailureVar(src, dst);

                double changed = linkFail.get(GRB.DoubleAttr.X);
                if (changed > 0.99) {
                    // Change link capacity
                    Pair<Device, Device> link1 = new Pair(src, dst);
                    linkCapacities.replace(link1, linkCapacities.get(link1), newCapacities.get(link1));
                    Pair<Device, Device> link2 = new Pair(dst, src);
                    linkCapacities.replace(link2, linkCapacities.get(link2), newCapacities.get(link2));
                    changedFlag = true;
                }
            }
        }
        return changedFlag;
    }

    public void findCongestionFreeSchedule() throws GRBException {
        try {
            env = new GRBEnv("quark.log");
            env.set(GRB.DoubleParam.TimeLimit, 3600);
            env.set(GRB.DoubleParam.MIPGap, 0.1);
            // env.set(GRB.IntParam.BarHomogeneous, 1);
            // env.set(GRB.IntParam.Method, 1);
            _solver = new GRBModel(env);
        } catch (GRBException e) {
            e.printStackTrace();
        }

        long startTime = System.currentTimeMillis();
        int pending_changes = 0;
        boolean repairFound = true;
        int stage = 1;

        _solver.set(GRB.IntAttr.ModelSense, -1);
        findCutFlows();
        initializeVariables();
        addForwardFlowConstraints();
        addValidForwardConstraints();
        addBestRouteConstraints();
        addLoadBalancingConstraints();
        addShortestPathsConstraints();
        addNoCongestionConstraints();
        GRBLinExpr objective = new GRBLinExpr();
        addMaximalFailureObjective(objective);
        _solver.setObjective(objective, GRB.MAXIMIZE);
        // addLinkFailureBoundConstraints();
        pending_changes = addChangedLinkCapacityConstraints();
        int total_changes = pending_changes;

        while (pending_changes > 0) {
            _solver.optimize();

            if (_solver.get(GRB.IntAttr.Status) == GRB.Status.OPTIMAL) {
                double maxFailures = _solver.get(GRB.DoubleAttr.ObjVal);
                System.out.println("Stage " + Integer.toString(stage) + " Changes are :" + Double.toString(maxFailures));
                if (!changeCapacities()) {
                    System.out.println("Could not find a repair because nothing changed?");
                    repairFound = false;
                    break;
                }
                pending_changes = addChangedLinkCapacityConstraints();
                stage++;
            } else {
                System.out.println("Could not find a repair");
                // _solver.computeIIS();
                // _solver.write(".quark-unsat.ilp");
                repairFound = false;
                break;
            }
        }

        long scheduleTime = System.currentTimeMillis() - startTime;
        try {
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(System.getProperty("user.home") + "/QARC-Repair-Timing"));
            writer.write("ST/Changes/Stages\t" + Long.toString(scheduleTime)
                    + "\t" + Boolean.toString(repairFound) + "\t" + Integer.toString(total_changes) + "\t"
                    + Integer.toString(stage - 1) + "\t");
            writer.close();
        } catch (IOException e) {
        }
    }
}