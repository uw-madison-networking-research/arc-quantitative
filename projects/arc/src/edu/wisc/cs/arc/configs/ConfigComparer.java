package edu.wisc.cs.arc.configs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.batfish.datamodel.Ip;
import org.batfish.datamodel.RoutingProtocol;
import org.batfish.representation.cisco.BgpProcess;
import org.batfish.representation.cisco.BgpRedistributionPolicy;
import org.batfish.representation.cisco.ExtendedAccessList;
import org.batfish.representation.cisco.ExtendedAccessListLine;
import org.batfish.representation.cisco.OspfProcess;
import org.batfish.representation.cisco.OspfRedistributionPolicy;
import org.batfish.representation.cisco.StandardAccessList;
import org.batfish.representation.cisco.StandardAccessListLine;

import edu.wisc.cs.arc.graphs.Device;
import edu.wisc.cs.arc.graphs.Interface;
import edu.wisc.cs.arc.graphs.Process;
import edu.wisc.cs.arc.repair.graph.ConfigModification;
import edu.wisc.cs.arc.repair.graph.ConfigModification.Action;
import edu.wisc.cs.arc.repair.graph.ConfigModification.Stanza;
import edu.wisc.cs.arc.repair.graph.ConfigModification.Substanza;

/**
 * Compares the configurations of two devices.
 * @author Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class ConfigComparer {
    
    /** Devices to compare */
    private Device deviceA;
    private Device deviceB;
    
    /** Referenced ACLs */
    private List<String> refAcls;
    
    /**
     * Compare the configurations of two devices.
     * @param deviceA original device configuration
     * @param deviceB new device configuration
     */
    public ConfigComparer(Device deviceA, Device deviceB) {
        this.deviceA = deviceA;
        this.deviceB = deviceB;
    }
    
    /**
     * Get a textual representation of the differences between the devices'
     * configurations.
     * @return a textual representation of configuration differences
     */
    public String toString() {
        String result = "";
        List<ConfigModification> modifications = this.getDifferences();
        for (ConfigModification modification : modifications) {
            result += modification.toString() + "\n";
        }
        return result;
    }
    
    /**
     * Get a list of differences between the devices' configurations.
     * @return a list of configuration differences
     */
    public List<ConfigModification> getDifferences() {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
        
        this.refAcls = new ArrayList<String>();
        
        modifications.addAll(this.compareInterfaces());
        modifications.addAll(this.compareRoutingProcesses());
        modifications.addAll(this.compareStandardAcls());
        modifications.addAll(this.compareExtendedAcls());
        // TODO: Compare route maps
        return modifications; 
    }
    
    /**
     * Compare the devices' interfaces.
     * @return a list of modifications
     */
    private List<ConfigModification> compareInterfaces() {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
        
        // Build map of interfaces for devices
        Map<String,Interface> ifacesA = new LinkedHashMap<String,Interface>();
        for (Interface ifaceA : deviceA.getInterfaces()) {
            ifacesA.put(ifaceA.getName(), ifaceA);
        }
        Map<String,Interface> ifacesB = new LinkedHashMap<String,Interface>();
        for (Interface ifaceB : deviceB.getInterfaces()) {
            ifacesB.put(ifaceB.getName(), ifaceB);
            Interface ifaceA = ifacesA.get(ifaceB.getName());
            // Determine if an interface was added
            if (null == ifaceA) {
                modifications.add(new ConfigModification(Action.ADD, 
                        Stanza.INTERFACE, ifaceB.getName()));
            } else {
                modifications.addAll(this.compareInterfaces(ifaceA, ifaceB));
            }
        }
        
        // Determine which interfaces were removed
        for (Interface ifaceA : deviceA.getInterfaces()) {
            if (!ifacesB.containsKey(ifaceA.getName())) {
                modifications.add(new ConfigModification(Action.REMOVE, 
                        Stanza.INTERFACE, ifaceA.getName()));
            }
        }
        
        return modifications;
    }
    
    /**
     * Compare an interface.
     * @param ifaceA interface on deviceA
     * @param ifaceB interface on deviceB
     * @return a list of modifications
     */
    private List<ConfigModification> compareInterfaces(Interface ifaceA,
            Interface ifaceB) {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
        
        // Compare prefix
        if (ifaceA.hasPrefix() && !ifaceB.hasPrefix()) {
            modifications.add(new ConfigModification(Action.REMOVE,
                    Stanza.INTERFACE, ifaceA.getName(), Substanza.PREFIX,
                    ifaceA.getPrefix().toString()));
        } else if (!ifaceA.hasPrefix() && ifaceB.hasPrefix()) {
            modifications.add(new ConfigModification(Action.ADD,
                    Stanza.INTERFACE, ifaceA.getName(), Substanza.PREFIX,
                    ifaceB.getPrefix().toString()));
        } else if (ifaceA.hasPrefix() && ifaceB.hasPrefix() 
                && !ifaceA.getPrefix().equals(ifaceB.getPrefix())) {
            modifications.add(new ConfigModification(Action.CHANGE,
                    Stanza.INTERFACE, ifaceA.getName(), Substanza.PREFIX,
                    ifaceA.getPrefix().toString() + " -> " 
                    + ifaceB.getPrefix().toString()));
        }
        
        // Compare active
        if (ifaceA.getActive() != ifaceB.getActive()) {
            modifications.add(new ConfigModification(Action.CHANGE,
                    Stanza.INTERFACE, ifaceA.getName(), Substanza.ACTIVE,
                    "" + ifaceA.getActive() + " -> " + ifaceB.getActive()));
        }
        
        // Compare OSPF cost
        if (ifaceA.getOspfCost() != null && ifaceB.getOspfCost() == null) {
            modifications.add(new ConfigModification(Action.REMOVE,
                    Stanza.INTERFACE, ifaceA.getName(), Substanza.OSPF_COST,
                    ifaceA.getOspfCost().toString()));
        } else if (ifaceA.getOspfCost() == null && ifaceB.getOspfCost() != null) {
            modifications.add(new ConfigModification(Action.ADD,
                    Stanza.INTERFACE, ifaceA.getName(), Substanza.OSPF_COST,
                    ifaceB.getOspfCost().toString()));
        } else if (ifaceA.getOspfCost() != null && ifaceB.getOspfCost() != null 
                && !ifaceA.getOspfCost().equals(ifaceB.getOspfCost())) {
            modifications.add(new ConfigModification(Action.CHANGE,
                    Stanza.INTERFACE, ifaceA.getName(), Substanza.OSPF_COST,
                    ifaceA.getOspfCost().toString() + "->" 
                    + ifaceB.getOspfCost().toString()));
        }
        
        // Compare incoming filter
        if (ifaceA.getIncomingFilter() != null 
                && ifaceB.getIncomingFilter() == null) {
            modifications.add(new ConfigModification(Action.REMOVE,
                    Stanza.INTERFACE, ifaceA.getName(), 
                    Substanza.INCOMING_FILTER, ifaceA.getIncomingFilter()));
        } else if (ifaceA.getIncomingFilter() == null 
                && ifaceB.getIncomingFilter() != null) {
            modifications.add(new ConfigModification(Action.ADD,
                    Stanza.INTERFACE, ifaceA.getName(), 
                    Substanza.INCOMING_FILTER, ifaceB.getIncomingFilter()));
        } else if (ifaceA.getIncomingFilter() != null 
                && ifaceB.getIncomingFilter() != null 
                && !ifaceA.getIncomingFilter().equals(
                        ifaceB.getIncomingFilter())) {
            modifications.add(new ConfigModification(Action.CHANGE,
                    Stanza.INTERFACE, ifaceA.getName(), 
                    Substanza.INCOMING_FILTER, ifaceA.getIncomingFilter()
                    + "->" + ifaceB.getIncomingFilter()));
        }
        
        // Compare outgoing filter
        if (ifaceA.getOutgoingFilter() != null 
                && ifaceB.getOutgoingFilter() == null) {
            modifications.add(new ConfigModification(Action.REMOVE,
                    Stanza.INTERFACE, ifaceA.getName(), 
                    Substanza.OUTGOING_FILTER, ifaceA.getOutgoingFilter()));
        } else if (ifaceA.getOutgoingFilter() == null 
                && ifaceB.getOutgoingFilter() != null) {
            modifications.add(new ConfigModification(Action.ADD,
                    Stanza.INTERFACE, ifaceA.getName(), 
                    Substanza.OUTGOING_FILTER, ifaceB.getOutgoingFilter()));
        } else if (ifaceA.getOutgoingFilter() != null 
                && ifaceB.getOutgoingFilter() != null 
                && !ifaceA.getOutgoingFilter().equals(
                        ifaceB.getOutgoingFilter())) {
            modifications.add(new ConfigModification(Action.CHANGE,
                    Stanza.INTERFACE, ifaceA.getName(), 
                    Substanza.OUTGOING_FILTER, ifaceA.getOutgoingFilter()
                    + "->" + ifaceB.getOutgoingFilter()));
        }
        
        // Store referenced filters
        if (ifaceA.getIncomingFilter() != null) {
        	this.refAcls.add(ifaceA.getIncomingFilter());
        }
        if (ifaceB.getIncomingFilter() != null) {
        	this.refAcls.add(ifaceB.getIncomingFilter());
        }
        if (ifaceA.getOutgoingFilter() != null) {
        	this.refAcls.add(ifaceA.getOutgoingFilter());
        }
        if (ifaceB.getOutgoingFilter() != null) {
        	this.refAcls.add(ifaceB.getOutgoingFilter());
        }
        
        // TODO: Compare access VLAN
        
        // TODO: Compare allowed VLANs
        
        // TODO: Compare subInterfaces?
        
        // TODO: Compare channel group
        
        return modifications;
    }
    
    /**
     * Compare the devices' routing processes.
     * @return a list of modifications
     */
    private List<ConfigModification> compareRoutingProcesses() {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
        
        // Build map of processes for devices
        Map<String,Process> processesA = new LinkedHashMap<String,Process>();
        for (Process processA : deviceA.getRoutingProcesses()) {
            processesA.put(processA.getName(), processA);
        }
        Map<String,Process> processesB = new LinkedHashMap<String,Process>();
        for (Process processB : deviceB.getRoutingProcesses()) {
            processesB.put(processB.getName(), processB);
            Process processA = processesA.get(processB.getName());
            // Determine if a routing process was added
            if (null == processA) {
                modifications.add(new ConfigModification(Action.ADD, 
                        Stanza.ROUTER, processB.getName()));
            } else {
                modifications.addAll(
                        this.compareRoutingProcesses(processA, processB));
            }
        }
        
        // Determine which routing processes were removed
        for (Process processA : deviceA.getRoutingProcesses()) {
            if (!processesB.containsKey(processA.getName())) {
                modifications.add(new ConfigModification(Action.REMOVE, 
                        Stanza.ROUTER, processA.getName()));
            }
        }
        
        return modifications;
    }
    
    /**
     * Compare a routing processes.
     * @param processA routing process on deviceA
     * @param processB routing process on deviceB
     * @return a list of modifications
     */
    private List<ConfigModification> compareRoutingProcesses(Process processA,
            Process processB) {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
        
        // Compare administrative distances
        if (processA.getAdministrativeDistance() 
                != processB.getAdministrativeDistance()) {
            modifications.add(new ConfigModification(Action.CHANGE,
                    Stanza.ROUTER, processA.getName(), 
                    Substanza.ADMINISTRATIVE_DISTANCE,
                    "" + processA.getAdministrativeDistance() + " -> " 
                    + processB.getAdministrativeDistance()));
        }
        
        // Compare interfaces
        Map<String,Interface> ifacesA = new LinkedHashMap<String,Interface>();
        for (Interface ifaceA : processA.getInterfaces()) {
            ifacesA.put(ifaceA.getName(), ifaceA);
        }
        Map<String,Interface> ifacesB = new LinkedHashMap<String,Interface>();
        for (Interface ifaceB : processB.getInterfaces()) {
            ifacesB.put(ifaceB.getName(), ifaceB);
            if (!ifacesA.containsKey(ifaceB.getName())) {
                modifications.add(new ConfigModification(Action.ADD, 
                        Stanza.ROUTER, processB.getName(),
                        Substanza.INTERFACE, ifaceB.getName()));
            }
        }
        for (Interface ifaceA : processA.getInterfaces()) {
            if (!ifacesB.containsKey(ifaceA.getName())) {
                modifications.add(new ConfigModification(Action.REMOVE, 
                        Stanza.ROUTER, processA.getName(),
                        Substanza.INTERFACE, ifaceA.getName()));
            }
        }
        // FIXME: Look at BGP neighbors, OSPF networks and interfaces, and
        // static route next hop instead of generic list of interfaces?
        
        // Router-type-specific comparisons
        switch(processA.getType()) {
        case BGP:
            modifications.addAll(this.compareBgpProcesses(processA, processB));
            break;
        case OSPF:
            modifications.addAll(this.compareOspfProcesses(processA, processB));
            break;
        case STATIC:
            // TODO: Anything to compare?
            break;
        }
        
        return modifications;
    }
    
    /**
     * Compare BGP routing processes.
     * @param processA BGP process on deviceA
     * @param processB BGP process on deviceB
     * @return a list of modifications
     */
    private List<ConfigModification> compareBgpProcesses(Process processA,
            Process processB) {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
        
        // FIXME
        /*BgpProcess bgpA = processA.getBgpConfig();
        BgpProcess bgpB = processB.getBgpConfig();
        
        // Compare neighbors
        for (Ip peerA : bgpA.getIpPeerGroups().keySet()) {
            if(!bgpB.getIpPeerGroups().containsKey(peerA)) {
                modifications.add(new ConfigModification(Action.REMOVE, 
                        Stanza.ROUTER, processA.getName(),
                        Substanza.BGP_NEIGHBOR, peerA.toString()));
            }
        }
        for (Ip peerB: bgpB.getIpPeerGroups().keySet()) {
            if(!bgpA.getIpPeerGroups().containsKey(peerB)) {
                modifications.add(new ConfigModification(Action.ADD, 
                        Stanza.ROUTER, processB.getName(),
                        Substanza.BGP_NEIGHBOR, peerB.toString()));
            }
        }
       
        // Compare redistribution
        for (RoutingProtocol protocolA : 
                bgpA.getRedistributionPolicies().keySet()) {
            if (!bgpB.getRedistributionPolicies().containsKey(protocolA)) {
                modifications.add(new ConfigModification(Action.REMOVE, 
                        Stanza.ROUTER, processA.getName(),
                        Substanza.BGP_REDISTRIBUTION, protocolA.toString()));
            } else {
                BgpRedistributionPolicy policyA = 
                        bgpA.getRedistributionPolicies().get(protocolA);
                BgpRedistributionPolicy policyB = 
                        bgpB.getRedistributionPolicies().get(protocolA);
                Integer metricA = policyA.getMetric();
                Integer metricB = policyB.getMetric();
                if (((null == metricA || null == metricB) && metricA != metricB)
                		|| (null != metricA && null != metricB 
                			&& !metricA.equals(metricB))) {
                    modifications.add(new ConfigModification(Action.CHANGE, 
                            Stanza.ROUTER, processA.getName(),
                            Substanza.BGP_REDISTRIBUTION, 
                            protocolA.toString()));
                }
            }
        }
        for (RoutingProtocol protocolB : 
            bgpB.getRedistributionPolicies().keySet()) {
            if (!bgpA.getRedistributionPolicies().containsKey(protocolB)) {
                modifications.add(new ConfigModification(Action.ADD, 
                        Stanza.ROUTER, processB.getName(),
                        Substanza.BGP_REDISTRIBUTION, protocolB.toString()));
            }
        }*/
        
        return modifications;
    }
    
    /**
     * Compare OSPF routing processes.
     * @param processA OSPF process on deviceA
     * @param processB OSPF process on deviceB
     * @return a list of modifications
     */
    private List<ConfigModification> compareOspfProcesses(Process processA,
            Process processB) {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
        
        // FIXME
        /*OspfProcess ospfA = processA.getOspfConfig();
        OspfProcess ospfB = processB.getOspfConfig();
       
        // Compare redistribution
        for (RoutingProtocol protocolA : 
                ospfA.getRedistributionPolicies().keySet()) {
            if (!ospfB.getRedistributionPolicies().containsKey(protocolA)) {
                modifications.add(new ConfigModification(Action.REMOVE, 
                        Stanza.ROUTER, processA.getName(),
                        Substanza.OSPF_REDISTRIBUTION, protocolA.toString()));
            } else {
                OspfRedistributionPolicy policyA = 
                        ospfA.getRedistributionPolicies().get(protocolA);
                OspfRedistributionPolicy policyB = 
                        ospfB.getRedistributionPolicies().get(protocolA);
                if (!policyA.getMetric().equals(policyB.getMetric())) {
                    modifications.add(new ConfigModification(Action.CHANGE, 
                            Stanza.ROUTER, processA.getName(),
                            Substanza.OSPF_REDISTRIBUTION, 
                            protocolA.toString()));
                }
            }
        }
        for (RoutingProtocol protocolB : 
            ospfB.getRedistributionPolicies().keySet()) {
            if (!ospfA.getRedistributionPolicies().containsKey(protocolB)) {
                modifications.add(new ConfigModification(Action.ADD, 
                        Stanza.ROUTER, processB.getName(),
                        Substanza.OSPF_REDISTRIBUTION, protocolB.toString()));
            }
        }*/
        
        return modifications;
    }
    
    /**
     * Compare the devices' standard ACLs.
     * @return a list of modifications
     */
    private List<ConfigModification> compareStandardAcls() {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
       
        // FIXME
        /*for (String aclNameA : deviceA.getStandardAclNames()) {
            StandardAccessList aclA = deviceA.getStandardAcl(aclNameA);
            StandardAccessList aclB = deviceB.getStandardAcl(aclNameA);
            // Determine if an ACL was removed
            if (null == aclB) {
                modifications.add(new ConfigModification(Action.REMOVE, 
                        Stanza.STANDARD_ACL, aclNameA));
            } else if (this.refAcls.contains(aclNameA)) {
            	modifications.addAll(this.compareStandardAcls(aclNameA, aclA, 
            			aclB));
            }
        }
        
        // Determine which ACLs were added
        for (String aclNameB : deviceB.getStandardAclNames()) {
            if (null == deviceA.getStandardAcl(aclNameB)) {
                modifications.add(new ConfigModification(Action.ADD, 
                        Stanza.STANDARD_ACL, aclNameB));
            }
        }*/
        
        return modifications;
    }
    
    /**
     * Compare standard ACLs.
     * @param aclA standard ACL on deviceA
     * @param aclB standard ACL on deviceB
     * @return a list of modifications
     */
    private List<ConfigModification> compareStandardAcls(String aclName,
            StandardAccessList aclA, StandardAccessList aclB) {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
      
        Iterator<StandardAccessListLine> iteratorA = aclA.getLines().iterator();
        Iterator<StandardAccessListLine> iteratorB = aclB.getLines().iterator();
        while(iteratorA.hasNext() && iteratorB.hasNext()) {
            StandardAccessListLine lineA = iteratorA.next();
            StandardAccessListLine lineB = iteratorB.next();
            if (lineA.getAction() != lineB.getAction()
                    || !lineA.getIpWildcard().equals(lineB.getIpWildcard())) {
                modifications.add(new ConfigModification(Action.CHANGE, 
                        Stanza.STANDARD_ACL, aclName, Substanza.LINE,
                        lineA.getAction() + " " + lineA.getIpWildcard() + " -> "
                        + lineB.getAction() + " "+ lineB.getIpWildcard()));
            }
        }
        
        if (iteratorA.hasNext() != iteratorB.hasNext()) {
            modifications.add(new ConfigModification(Action.CHANGE, 
                    Stanza.STANDARD_ACL, aclName));
        }
        
        return modifications;
    }
    
    /**
     * Compare the devices' extended ACLs.
     * @return a list of modifications
     */
    private List<ConfigModification> compareExtendedAcls() {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
        
        // FIXME
        /*for (String aclNameA : deviceA.getExtendedAclNames()) {
            ExtendedAccessList aclA = deviceA.getExtendedAcl(aclNameA);
            ExtendedAccessList aclB = deviceB.getExtendedAcl(aclNameA);
            // Determine if an ACL was removed
            if (null == aclB) {
                modifications.add(new ConfigModification(Action.REMOVE, 
                        Stanza.EXTENDED_ACL, aclNameA));
            } else if (this.refAcls.contains(aclNameA)) {
                modifications.addAll(this.compareExtendedAcls(aclNameA, aclA, 
                        aclB));
            }
        }
        
        // Determine which ACLs were added
        for (String aclNameB : deviceB.getExtendedAclNames()) {
            if (null == deviceA.getExtendedAcl(aclNameB)) {
                modifications.add(new ConfigModification(Action.ADD, 
                        Stanza.EXTENDED_ACL, aclNameB));
            }
        }*/
        
        return modifications;
    }
    
    /**
     * Compare extended ACLs.
     * @param aclA extended ACL on deviceA
     * @param aclB extended ACL on deviceB
     * @return a list of modifications
     */
    private List<ConfigModification> compareExtendedAcls(String aclName,
            ExtendedAccessList aclA, ExtendedAccessList aclB) {
        List<ConfigModification> modifications = 
                new ArrayList<ConfigModification>();
        
        List<String> linesA = new ArrayList<String>();
        for (ExtendedAccessListLine line : aclA.getLines()) {
        	linesA.add(line.getAction() + " " + line.getProtocol() + " " 
        			+ line.getSourceIpWildcard() + " "
        			+ line.getDestinationIpWildcard());
        	// FIXME: Add ports
        }
        
        List<String> linesB = new ArrayList<String>();
        for (ExtendedAccessListLine line : aclB.getLines()) {
        	linesB.add(line.getAction() + " " + line.getProtocol() + " "
        			+ line.getSourceIpWildcard() + " "
                    + line.getDestinationIpWildcard());
        	// FIXME: Add ports
        }
      
        int offsetA = 0;
        int offsetB = 0;
        for (int i = 0; i + offsetA < linesA.size() 
        		&& i + offsetB < linesB.size(); i++) {
            String lineA = linesA.get(i + offsetA);
            String lineB = linesB.get(i + offsetB);
            if (!lineA.equals(lineB)) {
            	List<String> restLinesA = linesA.subList(i + offsetA, 
            			linesA.size());
            	List<String> restLinesB = linesB.subList(i + offsetB, 
            			linesB.size());
            	int aInB = restLinesB.indexOf(lineA);
            	int bInA = restLinesA.indexOf(lineB);
            	if (aInB > 0) {
            		offsetB += aInB;
            		modifications.add(new ConfigModification(Action.CHANGE, 
	                        Stanza.EXTENDED_ACL, aclName,Substanza.LINE,
	                        "(null) -> " + lineB));
            	} else if (bInA > 0) {
            		offsetA += bInA;
            		modifications.add(new ConfigModification(Action.CHANGE, 
	                        Stanza.EXTENDED_ACL, aclName,Substanza.LINE,
	                        lineA + " -> (null)"));
            	}
            	else {
	                modifications.add(new ConfigModification(Action.CHANGE, 
	                        Stanza.EXTENDED_ACL, aclName,Substanza.LINE,
	                        lineA + " -> " + lineB));
            	}
            }
        }
        
        /*if (linesA.size() != linesB.size()) {
            modifications.add(new ConfigModification(Action.CHANGE, 
                    Stanza.EXTENDED_ACL, aclName));
        }*/
        
        // FIXME: Handle removals/additions at the end
        
        return modifications;
    }
}
