package edu.colgate.cs.fattree;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Saw Lin (slin@colgate.edu) and Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class Driver {

    private static FileWriter[] coreSwitches;

    private static FileWriter[][] aggSwitches;

    private static FileWriter[][] edgeSwitches;
    
    private static BufferedWriter waypointsBW;

    private static int k;

    private static int K_OVER_2;

    private static int numHosts;

    private static int numACLs;

    private static String CONFIG_FILES_DIR;

    private static Settings settings;

    private static int[] numHostsPerPodArr;


//    private static boolean CEH; // an edge switch will only have one interface for hosts to connect to

//    private static boolean BREAK_CORE_SWITCHES;

    private enum Type {
        CORE, AGGREGATE, EDGE
    }

    public enum Protocol {
        OSPF, BGP
    }
    
    private static final int BASE_OSPF_COST = 2;

    public static void main(String[] args) {
        Logger logger = new Logger(Logger.Level.DEBUG);

        // Load settings
        settings = null;
        try {
            settings = new Settings(args, logger);
        } catch (Exception e) {
            logger.fatal(e.getMessage());
            logger.fatal("Run with '-" + Settings.HELP + "' to show help");
            return;
        }
        logger.info("*** Settings ***");
        logger.info(settings.toString());

        // Prepare output directory
        CONFIG_FILES_DIR = settings.getConfigsDirectory();
        File dir = new File(CONFIG_FILES_DIR);
        if (!dir.exists() && !dir.mkdirs()) {
            logger.fatal("Failed to create directory: " + CONFIG_FILES_DIR);
            System.exit(1);
        }
        // Delete files in CONFIG_FILES_DIR because FileWriters need append mode
        for (File file : dir.listFiles()) {
            file.delete();
        }

        // Get port and switch counts
        k = settings.getNumPorts();
        K_OVER_2 = k >> 1;
        coreSwitches = new FileWriter[K_OVER_2*K_OVER_2];
        aggSwitches = new FileWriter[k][K_OVER_2];
        edgeSwitches = new FileWriter[k][K_OVER_2];

        // Get host and ACL counts
        numHosts = settings.getNumHosts();
        numACLs = settings.getNumACLs();

        // checking on the validity of number of hosts
        int maxNumHosts = (int) Math.pow((double) k, (double) 3)/4;
        if (numHosts > maxNumHosts) {
            numHosts = maxNumHosts;
        } else if (numHosts < 1) {
            numHosts = 1;
        }

        // distributing number of hosts
        numHostsPerPodArr = new int[k];
        int numHostsPerPod = numHosts/k;
        int extraNumHosts = numHosts%k;
        for (int i = 0; i < k; i++) {
            if (extraNumHosts > 0) {
                numHostsPerPodArr[i] = numHostsPerPod + 1;
                extraNumHosts --;
            } else {
                numHostsPerPodArr[i] = numHostsPerPod;
            }
        }

        // checking on the validity of number of ACLs
        /**
         * computing maxNumACLs (maximum number of traffic classes we can block) via the following formula
         * maxNumACLs = h1 (h2 + h3 + ... + h(k)) + h2 (h1 + h3 + .. + h(k)) + .. + hk (h1 + h2 + .. + h(k-1))
         * where h(i) stands for # of hosts in pod i
         *
         * For example, k = 4,
         * h1 (h2 + h3 + h4) + h2 (h1 + h3 + h4) + .. + hk (h1 + h2 + h3)
         *      =
         * h1h2 + h1h3 + h1h4 + h2h1 + h2h3 + h2h4 + h3h1 + h3h2 + h3h4 + h4h1 + h4h2 + h4h3
         *      =
         * 2(h1h2) + 2(h1h3) + 2(h1h4) + 2(h2h3) + 2(h2h4) + 2(h3h4)
         *
         * */
        int maxNumACLs = 0;
        for (int i= 0; i < k; i ++) {
            for (int j = i+1; j < k; j ++) {
                maxNumACLs += 2*numHostsPerPodArr[i]*numHostsPerPodArr[j];
            }
        }

        if (numACLs > maxNumACLs) {
            numACLs = maxNumACLs;
        } else if (numACLs < 0) {
            numACLs = 0;
        }
        
        logger.info("Generating config writers");
        try {
            for (Type type : Type.values()) {
                generateConfigFileWriters(type);
            }
            if (settings.hasWaypoints()) {
                waypointsBW = new BufferedWriter(new FileWriter(
                        CONFIG_FILES_DIR + "waypoints.csv", true));
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        logger.info("Creating topology");
        try {
            createFATTreeTopology(settings.getProtocol());
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        if (settings.hasWaypoints()) {
            try {
                waypointsBW.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param type - the switch type
     * Given type, generate cfg FileWriters and store them into a corresponding 1D/2D array
     * fileName: 'ci.cfg'   ,'c' stands for core, 'i', index
     *           'ai-j.cfg' ,'a' stands for aggregate, 'i', pod index, 'j', switch index within pod
     *           'ei-j.cfg' ,'e' stands for edge, 'i', pod index, 'j', switch index within pod
     */
    private static void generateConfigFileWriters(Type type) throws IOException {
        FileWriter fileWriter = null;
        if (type.equals(Type.CORE)) {
            for (int i = 0; i < coreSwitches.length; i++) {
                fileWriter = new FileWriter(CONFIG_FILES_DIR+"c"+i+".cfg", true);
                coreSwitches[i] = fileWriter;
            }
        } else {
            String filename = type == Type.AGGREGATE ? "agg" : "e";
            FileWriter[][] fileWriters = type == Type.AGGREGATE ? aggSwitches : edgeSwitches;
            for (int i = 0; i < k; i++) { // loop over each pod
                for (int j = 0; j < K_OVER_2; j++) {
                    String name = filename+i+"-"+j;
                    fileWriter = new FileWriter(CONFIG_FILES_DIR+name+".cfg", true);
                    fileWriters[i][j] = fileWriter;
                }
            }
        }
    }

    /**
     * Create links between core and agg switches and between agg and edge 
     * switches
     *
     * Given a pod index (podIdx), switch index (aggSwitchIdx) and port index 
     * (aggPortIdx) of an agg switch, to find the core switch and port
     * that links with the agg switch's port, we use the formula:
     * 		coreSwitchIdx = aggSwitchIdx * k/2 + aggPortIdx
     * Idea: we can partition (k/2)^2 core switches into (k/2) groups of (k/2) 
     * core switches
     *      - aggSwitchIdx: helps us find the right group of core switches
     *      - aggPortIdx: helps us find the right core switch within the group
     *
     * Which portIdx on the core switch we should link to is determined by 
     * podIdx (from an agg switch's point of view).
     *
     * Creating links between agg and edge layers is more straightforward
     *
     * For a given aggSwitch, the first available port is [k/2 + aggPortIdx] 
     * because we used the other k/2 to connect with core layer
     * aggPortIdx determines which edge switch with which current agg switch's 
     * port is to be linked
     *
     * Special Indexing is used to ensure that, for example:
     * 		pod 0 has hosts from 0 to (k/2)^2 -1
     * 		pod 1 has hosts from (k/2)^2 to 2*(k/2)^2 -1, etc.
     *
     * Assigning IPv4 address for an interface on a switch/router:
     * IP address is in the format: byte1.byte2.byte3.byte4
     *
     * Ifaces between: core and agg |     agg and edge  | edge and host
     * 	byte1      =        10      |         20        |      30
     *  byte2      =  coreSwitchIdx |       podIdx      |    podIdx
     *  byte3      =       podIdx   |     aggSwitchIdx  | aggPortIdx (edgeSwitchIdx)
     *  byte4      = 0(core)/1(agg) | aggPortIdx*2/(+1) | aggSwitchIdx*4 (*4 because mask is /30)
     */
    private static void createFATTreeTopology(Protocol protocol) throws IOException{
    	Logger logger = settings.getLogger();
        final int ospfCost = protocol.equals(Protocol.BGP) ? -1 : BASE_OSPF_COST;

        logger.info("Creating core switches");
        BufferedWriter[] coreSwitchBWs = new BufferedWriter[coreSwitches.length];
        FATSwitch[] coreSwitchFSs = new FATSwitch[coreSwitches.length];
        // constructs BufferedWriters for core switches
        for (int i= 0; i < coreSwitches.length; i++) { 
            coreSwitchBWs[i] = new BufferedWriter(coreSwitches[i]);
            coreSwitchFSs[i] = new FATSwitch(k, "c"+i);
        }
        
        logger.info("Preparing data structures");
        StringBuilder[] coreSwitchBGPNeighborNetworkSBs = 
        		protocol.equals(Protocol.BGP) ? new StringBuilder[coreSwitches.length] : null;

        BufferedWriter[] edgeSwtichBWs = new BufferedWriter[K_OVER_2];

//        System.out.println(String.format("numHostsPerPod: %1$d; extraNumHosts: %2$d", numHostsPerPod, extraNumHosts));

        // for collecting hostIPs for later-stage round-robin picking of traffic classes to block
        List[] hostIPs = null;
        if (numACLs > 0) {
            hostIPs = new List[k];
            for (int i = 0; i < k; i++) {
                hostIPs[i] = new ArrayList<String>();
            }
        }
        ArrayList<String> allIPs = new ArrayList<String>();

        /** ---------------- Populating the Cfg Files ------------------*/
        for (int podIdx= 0; podIdx < k; podIdx++) { // loop over each pod

            // distributing number of hosts in a round robin fashion
            // round robin distribution within a pod is guaranteed because of the way we loop over the aggregate switches
            // and populate the cfg files
            int numHostsForThisPod = numHostsPerPodArr[podIdx];

//            System.out.println(String.format("Pod No: %1$d; numHostsForThisPod: %2$d", podIdx, numHostsForThisPod));

            for (int j= 0; j < K_OVER_2; j++) { // constructs BufferedWriters for edge switches
                edgeSwtichBWs[j] = new BufferedWriter(edgeSwitches[podIdx][j]);
                edgeSwtichBWs[j].write("!\nversion 12.4\n!\nhostname e"+podIdx+"-"+j+"\n");
            }

            StringBuilder[] edgeSwitchBGPNeighborNetworkSBs = protocol.equals(Protocol.BGP) ? new StringBuilder[edgeSwitches.length] : null;

            // loop over each agg switch
            for (int aggSwitchIdx= 0; aggSwitchIdx < K_OVER_2; aggSwitchIdx++) { 
            	// constructs BufferedWriter for current agg switch
                BufferedWriter aggSwitchBW =
                        new BufferedWriter(aggSwitches[podIdx][aggSwitchIdx]); 
                FATSwitch aggSwitchFS = new FATSwitch(k, 
                		"agg"+podIdx+"-"+aggSwitchIdx);
                
                final int aggAclNum = 100+podIdx;
                
                if (settings.shouldIncludePodAcls()) {
	                aggSwitchFS.addACLLinesToSwitch(aggAclNum, true, "any", "",
	                        toDottedDecimal(20, podIdx, 0, 0), "0.0.255.255");
	                aggSwitchFS.addACLLinesToSwitch(aggAclNum, true, "any", "",
	                        toDottedDecimal(30, podIdx, 0, 0), "0.0.255.255");
	                aggSwitchFS.addACLPermitDenyAnyLine(aggAclNum, false);
                }

                StringBuilder aggSwitchBGPNeighborNetworkSB = protocol.equals(Protocol.BGP) ? new StringBuilder() : null;

                for (int aggPortIdx= 0; aggPortIdx < K_OVER_2; aggPortIdx++) {

                    // creating links between CORE and AGGREGATE layers
                	// core switch index that maps to current port of current agg switch
                    int coreSwitchIdx = aggSwitchIdx*K_OVER_2 + aggPortIdx;
                    
                    int coreAggOspfCost = ospfCost;
                    if (coreAggOspfCost > 0 && settings.shouldPreferCore()
                            && coreSwitchIdx == settings.getPreferredCore()) {
                        coreAggOspfCost--;
                    }
                    
                    String coreSwitchIfaceStr = generateIfaceConfigString(
                    		toDottedDecimal(10, coreSwitchIdx, podIdx, 0), 
                    		podIdx+1, coreAggOspfCost, false);
                    // to support ACLs in coreSwitch
                    coreSwitchFSs[coreSwitchIdx].addIfaceCfg(podIdx+1, 
                    		coreSwitchIfaceStr);

                    String aggSwitchIfaceStr = generateIfaceConfigString(
                    		toDottedDecimal(10, coreSwitchIdx, podIdx, 1), 
                    		aggPortIdx+1, coreAggOspfCost, false);
                    aggSwitchFS.addIfaceCfg(aggPortIdx+1, aggSwitchIfaceStr);
                    
                    // Add waypoints between CORE and AGGREGATE layers
                    if (coreSwitchIdx < settings.getNumCoresWithWaypoints()){
                        waypointsBW.write("c" + coreSwitchIdx + ",agg" + podIdx 
                                + "-" + aggSwitchIdx + "\n");
                    }

                    // creating links between AGGREGATE and EDGE layers
                    // aggPortIdx is the same as Idx used
                    BufferedWriter edgeSwitchBW = edgeSwtichBWs[aggPortIdx]; 
                    // to find the corresponding edge switch
                    int byte4 = aggPortIdx*2;
                    aggSwitchIfaceStr = generateIfaceConfigString(
                    		toDottedDecimal(20, podIdx, aggSwitchIdx, byte4), 
                    		aggPortIdx+1+K_OVER_2, ospfCost, false);
                    aggSwitchFS.addIfaceCfg(aggPortIdx+1+K_OVER_2, 
                    		aggSwitchIfaceStr);
                    edgeSwitchBW.write(generateIfaceConfigString(
                    		toDottedDecimal(20, podIdx, aggSwitchIdx, byte4+1), 
                    		aggSwitchIdx+1, ospfCost, false));

                    // creating links between EDGE layer and host
                    if (numHostsForThisPod > 0) {
                        // creating links between EDGE layer and host
                    	String hostAddr = toDottedDecimal(30, podIdx, 
                    			aggPortIdx, aggSwitchIdx*4);
                        edgeSwitchBW.write(generateIfaceConfigString(hostAddr, 
                        		aggSwitchIdx+1+K_OVER_2, -1, true));
                        numHostsForThisPod --;

                        if (hostIPs != null) { // collecting hostIPs for ACL lines
                            hostIPs[podIdx].add(hostAddr);
                        }
                        allIPs.add(hostAddr);
                    }
                    
                    if (settings.shouldIncludePodAcls()) {
                    	aggSwitchFS.addACLLineToIface(aggPortIdx+1, "in", 
                    			aggAclNum);
                    }

                    // if routers are running eBGP protocol
                    if (protocol.equals(Protocol.BGP)) {
                        // between CORE and AGGREGATE layers
                        StringBuilder coreSwitchBGPNeighborNetworkSB = coreSwitchBGPNeighborNetworkSBs[coreSwitchIdx];
                        if (coreSwitchBGPNeighborNetworkSB == null) {
                            coreSwitchBGPNeighborNetworkSBs[coreSwitchIdx] = new StringBuilder();
                        }
                        
                        int coreASN = 1 + coreSwitchIdx;
                        int aggASN = 1 + coreSwitches.length + podIdx*2 
                                + aggSwitchIdx;
                        int edgeASN = 1 + coreSwitches.length 
                                + aggSwitches.length + podIdx*2 + aggPortIdx;

                        generateBGPConfigForNeighborNetwork(
                                coreSwitchBGPNeighborNetworkSBs[coreSwitchIdx],
                                10, coreSwitchIdx, podIdx, 0, 1, false, aggASN);
                        generateBGPConfigForNeighborNetwork(
                                aggSwitchBGPNeighborNetworkSB, 10, 
                                coreSwitchIdx, podIdx, 1, 0, false,coreASN);

                        // between AGGREGATE and EDGE layers
                        generateBGPConfigForNeighborNetwork(
                                aggSwitchBGPNeighborNetworkSB, 20, podIdx, 
                                aggSwitchIdx, byte4, byte4+1, false, edgeASN);

                        StringBuilder edgeSwitchBGPNeighborNetworkSB = 
                                edgeSwitchBGPNeighborNetworkSBs[aggPortIdx];
                        if (edgeSwitchBGPNeighborNetworkSB == null) {
                            edgeSwitchBGPNeighborNetworkSBs[coreSwitchIdx] = 
                                    new StringBuilder();
                        }
                        generateBGPConfigForNeighborNetwork(
                                edgeSwitchBGPNeighborNetworkSBs[aggPortIdx], 20, 
                                podIdx, aggSwitchIdx, byte4+1, byte4, false,
                                aggASN);

                        // between EDGE and HOSTs
                        generateBGPConfigForNeighborNetwork(
                                edgeSwitchBGPNeighborNetworkSBs[aggPortIdx],
                                30, podIdx, aggPortIdx, aggSwitchIdx*4, aggSwitchIdx*4+1, true, -1);
                    }
                }

                aggSwitchBW.write(aggSwitchFS.getFATSwitchCfgStr());
                if (protocol.equals(Protocol.BGP)) {
                    aggSwitchBW.write(generateBGPConfigString(aggSwitchBGPNeighborNetworkSB.toString(),
                            1+coreSwitches.length+aggSwitchIdx));
                } else {
                    aggSwitchBW.write(generateOSPFConfigString(1, new int[]{10, 20}, new int[]{0, 1}, false));
                }
                aggSwitchBW.write("!\nend\n");
                aggSwitchBW.close();
            }

            for (int j= 0; j < K_OVER_2; j++) { // closing BufferedWriters for edge switches
                if (protocol.equals(Protocol.BGP)) {
                    edgeSwtichBWs[j].write(generateBGPConfigString(edgeSwitchBGPNeighborNetworkSBs[j].toString(),
                            coreSwitches.length + aggSwitches.length + 1 + j));
                } else {
                    edgeSwtichBWs[j].write(generateOSPFConfigString(1, new int[]{20}, new int[]{1, 2}, false));
                }
                edgeSwtichBWs[j].write("!\nend\n");
                edgeSwtichBWs[j].close();
            }
        }

        addACLs(hostIPs, coreSwitchFSs);

        for (int i= 0; i < coreSwitches.length; i++) { // closing BufferedWriters for core switches
            // needs to write the iFace and possibly ACL lines first before writing "router" lines
            coreSwitchBWs[i].write(coreSwitchFSs[i].getFATSwitchCfgStr());
            if (protocol.equals(Protocol.BGP)) {
                coreSwitchBWs[i].write(generateBGPConfigString(coreSwitchBGPNeighborNetworkSBs[i].toString(), i+1));
            } else {
                coreSwitchBWs[i].write(generateOSPFConfigString(1, new int[]{10}, new int[]{0}, settings.shouldBreakCoreSwitches()));
            }
            coreSwitchBWs[i].write("!\nend\n");
            coreSwitchBWs[i].close();
        }

        BufferedWriter policyWriter = new BufferedWriter(new FileWriter(CONFIG_FILES_DIR + "/policy.csv"));
        for (int i = 0; i < allIPs.size(); i++) {
            for (int j = 0; j < allIPs.size(); j++) {
                if (i != j) {
                    policyWriter.write(allIPs.get(i) + "," + allIPs.get(j) + ",100\n");
                }
            }
        }
        policyWriter.close();
    }

    private static void addACLs(List<String>[] hostIPs, FATSwitch[] coreSwitchFSs) {
        if (null == hostIPs) {
            return;
        }
        // configuring ACLs for each switch
        List<String> thisPodHosts, otherPodHosts;

        for (int p= 0; p < k; p++) { // picking pod
            if (numACLs <= 0) {
                return;
            }
            thisPodHosts = hostIPs[p];
            for (int j = 0; j < thisPodHosts.size(); j++) { // looping over (p)th pod's hosts
                if (numACLs <= 0) {
                    return;
                }
                for (int op = 0; op < k; op ++) { // picking other pod
                    otherPodHosts = hostIPs[op];
                    if (numACLs <= 0) {
                        return;
                    }
                    if (op != p) { // if pods are different
                        for (int oj = 0; oj < otherPodHosts.size(); oj++) { // looping over (op)th pod's hosts
                            if (numACLs <= 0) {
                                return;
                            }
                            // installing ACL on all core switches (without 
                            // waypoints)
                            for (int coreIdx=settings.getNumCoresWithWaypoints(); 
                                    coreIdx < coreSwitchFSs.length; coreIdx++) {
                                FATSwitch coreSwitchFS = coreSwitchFSs[coreIdx];
                                coreSwitchFS.addACLLinesToSwitch(113, false,
                                        otherPodHosts.get(oj), "0.0.0.3",
                                        thisPodHosts.get(j), "0.0.0.3");
                                coreSwitchFS.addACLLineToIface(p+1, "out", 113);
                                coreSwitchFS.addACLPermitDenyAnyLine(113, true);
                            }
                            numACLs --;
                        }
                    }
                }
            }
        }
    }

    private static void generateBGPConfigForNeighborNetwork(StringBuilder sb, 
    		int byte1, int byte2, int byte3, int byte4, int neighByte4,
            boolean toHost, int remoteAs) {
        // Handle edges to hosts separately so that ARC can distinguish between 
        // IP addr of iface to host and that to switch to make it simple to 
        // create traffic classes for testing purposes
        if (toHost) {  
            sb.append("\tnetwork ").append(byte1 +"."+ byte2 + "." + byte3 + "."
                    + byte4 + "/30\n");
        } else {
            sb.append("\tneighbor ");
            sb.append(byte1 +"."+ byte2 + "." + byte3 + "." + neighByte4);
            sb.append(" remote-as " + remoteAs + "\n");
            //sb.append("\tnetwork ").append(byte1 +"."+ byte2 + "." + byte3 + "."
            //        + byte4 + "/31\n");
        }

    }
    
    private static String generateIfaceConfigString(String ip, int portIdx, 
    		int ospfCost, boolean toHost) {
        StringBuilder sb = new StringBuilder("!\n")
                .append("interface GigabitEthernet0/"+ portIdx)
                .append("\n\tip address " + ip);
        // Use different subnet sizes so that ARC can distinguish between IP
        // addr of iface to host and that to switch to make it simple to create
        // traffic classes for testing purposes
        if (toHost) {
            sb.append("/30\n");
        } else {
            sb.append("/31\n");
        }
        if (ospfCost > 0) {
            sb.append("\tip ospf cost "+ospfCost+"\n");
        }
        return sb.toString();
    }

    private static String generateOSPFConfigString(int processID, int[] IpLayers, int[] areaIDs, boolean removeNetworkStanzas) {
        StringBuilder sb = new StringBuilder("!\nrouter ospf ").append(processID)
                .append("\n\tredistribute connected\n");
        if (!removeNetworkStanzas) {
            for (int i = 0; i < IpLayers.length; i++) {
                sb.append("\tnetwork " + IpLayers[i] + ".0.0.0 0.255.255.255 area " + areaIDs[i] +"\n");
            }
        }
        return sb.toString();
    }

    private static String generateBGPConfigString(String bgpNeighborNetwork, int ASNnumber) {
        StringBuilder sb = new StringBuilder("!\nrouter bgp ").append(ASNnumber+"\n")
                .append(bgpNeighborNetwork);
        return sb.toString();
    }
    
    /**
     * Create a dotted-decimal representation of an IP address.
     * @param byte1 the first byte of the IP address
     * @param byte2 the second byte of the IP address
     * @param byte3 the third byte of the IP address
     * @param byte4 the fourth byte of the IP address
     * @return a dotted-decimal representation of the IP address
     */
    private static String toDottedDecimal(int byte1, int byte2, int byte3, int byte4) {
    	return (byte1 + "." + byte2 + "." + byte3 + "." + byte4);
    }

}

