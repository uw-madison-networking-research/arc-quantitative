package edu.colgate.cs.fattree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Saw Lin (slin@colgate.edu)
 */
public class FATSwitch {
	
	private static final String VERSION = "!\nversion 12.4\n!\n";
	
	private String hostname;

    private StringBuilder aclStringBuilder;

    private StringBuilder[] iFaceStringBuilders;

    private Map<Integer, String> aclPermitDencyAnyACLNumToPermissionMap;

    private Map<Integer, Set<Integer>> portIdxToACLNumbers;

    public FATSwitch(int numPorts, String hostname) {
    	this.hostname = hostname;
        iFaceStringBuilders = new StringBuilder[numPorts];
        aclStringBuilder = new StringBuilder("!\n");
        portIdxToACLNumbers = new HashMap<>();
        aclPermitDencyAnyACLNumToPermissionMap = new HashMap<>();
    }

    public void addIfaceCfg(int portIdx, String iFaceStr) {
        iFaceStringBuilders[portIdx-1] = new StringBuilder(iFaceStr);
    }

    public void addACLLineToIface(int portIdx, String inOrOut, int aclNumber) {
        boolean bool1 = !portIdxToACLNumbers.containsKey(portIdx);
        if (bool1 || !portIdxToACLNumbers.get(portIdx).contains(aclNumber)) {
            // so that only no duplicate ACL commands are added to iFace
            if (bool1) {
                portIdxToACLNumbers.put(portIdx, new HashSet<>());
            }
            portIdxToACLNumbers.get(portIdx).add(aclNumber);

            iFaceStringBuilders[portIdx-1].append(String.format("\tip access-group %1$d %2$s\n", aclNumber, inOrOut));
        }
    }

    public void addACLLinesToSwitch(int aclNumber, boolean isPermit, String srcIP, String srcWildCardMask, String dstIP, String dstWildCardMask) {
        String permission = isPermit ? "permit" : "deny";
        aclStringBuilder.append(String.format("access-list %1$d %2$s ip %3$s %4$s %5$s %6$s\n",
                aclNumber, permission, srcIP, srcWildCardMask, dstIP, dstWildCardMask));
    }

    public void addACLPermitDenyAnyLine(int aclNumber, boolean isPermit) {
        String permission = isPermit ? "permit" : "deny";
        aclPermitDencyAnyACLNumToPermissionMap.put(aclNumber, permission);
    }

    public String getFATSwitchCfgStr() {
        StringBuilder sb = new StringBuilder(VERSION);
        sb.append("hostname " + this.hostname + "\n");
        String aclString = aclStringBuilder.toString();
        if (!aclString.equals("!\n")) {
            sb.append(aclString);
            for (int aclNumber : aclPermitDencyAnyACLNumToPermissionMap.keySet()) {
                String permission = aclPermitDencyAnyACLNumToPermissionMap.get(aclNumber);
                sb.append(String.format("access-list %1$d %2$s ip any any\n", aclNumber, permission));
            }
        }
        for (int i = 0; i < iFaceStringBuilders.length; i ++) {
            sb.append(iFaceStringBuilders[i].toString());
        }
        return sb.toString();
    }

}
