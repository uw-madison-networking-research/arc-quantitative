package edu.colgate.cs.fattree;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import edu.colgate.cs.fattree.Driver.Protocol;

/**
 * @author Saw Lin (slin@colgate.edu) and Aaron Gember-Jacobson (agemberjacobson@colgate.edu)
 */
public class Settings {
    public final static String HELP = "help";

    private final static String CONFIGS_DIRECTORY = "configs";
    private final static String NUM_PORTS = "numports";
    private final static String PROTOCOL = "protocol";
    private final static String NUM_HOSTS = "numhosts";
    private final static String NUM_ACLS = "numacls";
    private final static String BREAK_CORE_SWTICHES = "breakcore";
    private final static String WAYPOINTS = "waypoints";
    private final static String POD_ACLS = "podacls";
    private final static String PREFER_CORE = "prefercore";
    // ---------------------------------------------

    private String configsDirectory;

    private int numPorts;

    private Driver.Protocol protocol;

    private int numHosts;

    private int numACLs;

    private boolean breakCoreSwtiches;
    
    private int numCoresWithWaypoints;
    
    private boolean podacls;
    
    private int preferredCore;

    private Logger logger;

    public Settings(String[] args, Logger logger) throws Exception {
        this.logger = logger;

        Options options = this.getOptions();

        // Output help text, if help argument is passed
        for (String arg : args) {
            if (arg.equals("-"+HELP)) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.setLongOptPrefix("-");
                formatter.printHelp(Driver.class.getName(), options, true);
                System.exit(0);
            }
        }

        // Parse command line arguments
        CommandLineParser parser = new DefaultParser();
        CommandLine line = null;
        try {
            line = parser.parse(options, args);
        }
        catch(MissingOptionException e) {
            throw new ParseException("Missing required argument: -"
                    + e.getMissingOptions().get(0));
        }
        catch(MissingArgumentException e) {
            throw new ParseException("Missing argument for option "
                    + e.getOption().getLongOpt());
        }

        this.configsDirectory = line.getOptionValue(CONFIGS_DIRECTORY)+"/";
        try {
            this.numPorts = Integer.parseInt(
                    line.getOptionValue(NUM_PORTS, "2"));
        } catch (NumberFormatException e) {
            throw new ParseException(
                    "Number of ports is not a number");
        }
        if (this.numPorts % 2 != 0) {
            throw new Exception("Number of ports must be even");
        }
        
        this.protocol = Protocol.values()[0];
        if (line.hasOption(PROTOCOL)) {
			try {
				this.protocol = Protocol.valueOf(
						line.getOptionValue(PROTOCOL).toUpperCase());
			} catch (IllegalArgumentException e) {
				this.logger.error("Invalid protocol: "
						+ line.getOptionValue(PROTOCOL) + "; using "
						+ this.protocol.toString());
			}
		}

        try {
            this.numHosts = Integer.parseInt(
                    line.getOptionValue(NUM_HOSTS, "1"));
        } catch (NumberFormatException e) {
            throw new ParseException(
                    "Number of hosts is not a number");
        }
//        if (this.numHosts > Math.pow((double) this.numPorts, (double) 3)/4 || this.numHosts < 1) {
//            throw new Exception("Number of hosts is above the max ((numPorts^3)/4) or below the min (1)");
//        }

        try {
            this.numACLs = Integer.parseInt(
                    line.getOptionValue(NUM_ACLS, "0"));
        } catch (NumberFormatException e) {
            throw new ParseException(
                    "Number of ACLs is not a number");
        }
//        if (this.numACLs > (this.numHosts*(this.numHosts-1)) - this.numHosts || this.numACLs < 0) {
//            throw new Exception("Number of ACLs is above the max (numPorts*(numPorts-1) - numPorts) or below the min (0)");
//        }
        this.breakCoreSwtiches = line.hasOption(BREAK_CORE_SWTICHES);
        
        try {
            this.numCoresWithWaypoints = Integer.parseInt(
                    line.getOptionValue(WAYPOINTS, "0"));
        } catch (NumberFormatException e) {
            throw new ParseException(
                    "Number of waypoints is not a number");
        }
        if (this.numCoresWithWaypoints < 0) {
            this.numCoresWithWaypoints = 0;
        }
        if (this.numCoresWithWaypoints > this.numPorts) {
            this.numCoresWithWaypoints = this.numPorts;
        }
        
        this.podacls = line.hasOption(POD_ACLS);
        
        try {
            this.preferredCore = Integer.parseInt(
                    line.getOptionValue(PREFER_CORE, "-1"));
        } catch (NumberFormatException e) {
            throw new ParseException(
                    "Number of hosts is not a number");
        }
    }

    private Options getOptions() {
        Options options = new Options();
        options.addOption(HELP, false,
                "Print usage information");

        Option option = new Option(CONFIGS_DIRECTORY, true,
                "Directory configuration files will be written to");
        option.setArgName("DIR");
        option.setRequired(true);
        options.addOption(option);

        option = new Option(NUM_PORTS, true,
                "Number of ports on a switch device");
        option.setArgName("PORTS");
        option.setRequired(true);
        options.addOption(option);

        option = new Option(PROTOCOL, true,
                "Routing protocol each switch/router runs");
        option.setArgName("PROTOCOL");
        options.addOption(option);

        option = new Option(NUM_HOSTS, true,
                "Number of hosts in the topology");
        option.setArgName("HOSTS");
        options.addOption(option);

        option = new Option(NUM_ACLS, true,
                "Number of Access Control Lists in the topology");
        option.setArgName("ACLS");
        options.addOption(option);

        options.addOption(BREAK_CORE_SWTICHES, false,
                "Should break core switches?");
        
        option = new Option(WAYPOINTS, true,
                "Number of core switches with waypoints in the topology");
        option.setArgName("WAYPOINTS");
        options.addOption(option);
        
        options.addOption(POD_ACLS, false,
                "Include ACLs on agg switches to block traffic from the core "
        		+ "that is destined for another pod");
        
        option = new Option(PREFER_CORE, true,
                "Prefer to send along agg-core links for a particular core");
        option.setArgName("CORE");
        options.addOption(option);
        
        return options;
    }
    
    /**
	 * Get the logger.
	 * @return logger for producing output
	 */
	public Logger getLogger() {
		return this.logger;
	}

    public String getConfigsDirectory() {
        return this.configsDirectory;
    }

    public int getNumPorts() {
        return this.numPorts;
    }

    public Driver.Protocol getProtocol() {
        return this.protocol;
    }

    public int getNumHosts() {
        return this.numHosts;
    }

    public int getNumACLs() {
        return this.numACLs;
    }

    public boolean shouldBreakCoreSwitches() {
        return this.breakCoreSwtiches;
    }
    
    public boolean hasWaypoints() {
        return (this.numCoresWithWaypoints > 0);
    }
    
    public int getNumCoresWithWaypoints() {
        return this.numCoresWithWaypoints;
    }
    
    public boolean shouldIncludePodAcls() {
        return this.podacls;
    }
    
    public int getPreferredCore() {
        return this.preferredCore;
    }
       
    public boolean shouldPreferCore() {
        return (this.preferredCore >= 0);
    }
    
    @Override
    public String toString() {
    	String result = "";
    	result += "Configs directory: " + this.configsDirectory;
    	result += "\nPorts: " + this.numPorts;
    	result += "\nHosts: " + this.numHosts;
    	result += "\nACLs: " + this.numACLs;
    	result += "\nProtocol: " + this.protocol;
    	result += "\nCore switches with waypoints: "+this.numCoresWithWaypoints;
    	result += "\nPod ACLs: " + this.podacls;
    	result += "\nDisable routing on core switches: "+this.breakCoreSwtiches;
    	result += "\nPreferred core: "+this.preferredCore;
    	return result;
    }

}
