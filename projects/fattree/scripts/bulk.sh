#!/bin/bash

# Fetch settings
if [[ $(uname) == "Darwin" ]]; then
    SCRIPTPATH=`greadlink -f $0`
else
    SCRIPTPATH=`readlink -f $0`
fi
BASEDIR="`dirname $SCRIPTPATH`/.."

CONFIGSDIR="$BASEDIR/../../configs/fattree"

RUN="$BASEDIR/scripts/run.sh"

VARY="policies"
PORTS=( 6 ) #( 4 6 8 10 12 14 16 )
HOSTS=( 6 ) #( 6 12 18 24 30 36 42 48 ) 
ACLS="" #56 #192
PROTOCOLS=( "ospf" )
POLICIES=( "primary" ) #( "blocked" "reachable" "waypoint" )

VARYDIR=$CONFIGSDIR/vary-$VARY

for PROTOCOL in "${PROTOCOLS[@]}"; do
    for PORT in "${PORTS[@]}"; do
        for HOST in "${HOSTS[@]}"; do
            for POLICY in "${POLICIES[@]}"; do
                NAME="${PROTOCOL}_ports-${PORT}_hosts-${HOST}_${POLICY}"
                BASE_OPTS="-protocol $PROTOCOL -numports $PORT -numhosts $HOST"

                ACL=$ACLS
                if [ -z "$ACL" ]; then
                    MINHPP=`echo "($HOST/$PORT)" | bc`
                    MAXHPP=`echo "$MINHPP + 1" | bc`
                    PWMAXH=`echo "$HOST % $PORT" | bc`
                    PWMINH=`echo "$PORT - $PWMAXH" | bc`
                    echo "$HOST $PORT $MINHPP $MAXHPP $PWMAXH $PWMINH"
                    INTERPOD=`echo "$PWMAXH * $MAXHPP * ($MAXHPP * ($PWMAXH - 1) + $MINHPP * $PWMINH) + $PWMINH * $MINHPP * ($MINHPP * ($PWMINH - 1) + $MAXHPP * $PWMAXH)" | bc`
                    ACL=$INTERPOD
                fi

                CASEDIR="$VARYDIR/$NAME/1-unbroken"
                mkdir -p $CASEDIR
                case "$POLICY" in
                    blocked)
                        POLICY_OPTS="-numacls $ACL"
                        ;;
                    reachable)
                        POLICY_OPTS="-numacls 0"
                        ;;
                    waypoint)
                        POLICY_OPTS="-numacls $ACL -waypoints $((PORT/2))"
                        ;;
                    primary)
                        POLICY_OPTS="-prefercore 0"
                        ;;
                esac
                $RUN -a "-configs $CASEDIR $BASE_OPTS $POLICY_OPTS"

                CASEDIR="$VARYDIR/$NAME/2-broken"
                mkdir -p $CASEDIR
                case "$POLICY" in
                    blocked)
                        POLICY_OPTS="-numacls 0"
                        ;;
                    reachable)
                        POLICY_OPTS="-numacls $ACL"
                        ;;
                    waypoint)
                        POLICY_OPTS="-numacls 0 -waypoints $((PORT/2))"
                        ;;
                    primary)
                        POLICY_OPTS="-prefercore 1"
                        ;;
                esac
                $RUN -a "-configs $CASEDIR $BASE_OPTS $POLICY_OPTS"
            done
        done
    done
done
