#!/bin/bash

FATTREE_JVM_ARGS=""
FATTREE_CLASSPATH="$BASEDIR/target/*:$BASEDIR/target/lib/*"
FATTREE_MAIN_CLASS="edu.colgate.cs.fattree.Driver"
FATTREE_COMMON_ARGS="$FATTREE_JVM_ARGS -cp $FATTREE_CLASSPATH $FATTREE_MAIN_CLASS"

LOGDIR="$BASEDIR/../../output"

# Options
EXTRA_ARGS=""
while getopts a:h OPT; do
    case $OPT in
        a) # Extra arguments
            EXTRA_ARGS=$OPTARG
            ;;
        h | \?)
    echo "Usage: `basename $0` [-a EXTRA_ARGS]"
            exit 1
            ;;
    esac
done

FATTREE_ARGS="$FATTREE_COMMON_ARGS $EXTRA_ARGS"
