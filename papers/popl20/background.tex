%!TEX root = paper.tex
\section{Motivation}
We provide an overview of distributed network control planes, control
plane abstractions and their shortcomings for reasoning about network
load violations.

\subsection{Network Control Planes}
In a distributed control plane, routers run routing protocols to
compute the paths taken by traffic to the corresponding destination.
Routers may run  routing protocols in conjunction (e.g.,
OSPF~\cite{ospf}, BGP~\cite{bgp}, RIP~\cite{rip}).
% Each protocol's path
% computations are based on different protocol-specific configuration
% parameters and routing advertisements. 
The outcome of the control
plane's computation is a set of forwarding rules computed at each
router, which encode the next-hop for different traffic classes, i.e.,
source-destination subnets. Operators typically decide which protocols
to use and their detailed configuration based on network design and
operational policies that they wish to enforce~\cite{maltz04} like reachability,
waypointing etc. 
% The
% former correspond to hierarchy (e.g., how many logical domains, or
% departments, in the network) and the latter to qualitative path
% properties such as reachability between sets of subnets, waypointing
% for certain traffic, isolation for sensitive flows, etc.
Distributed control planes are programmed at a low-level by
configuring routers using vendor-specific languages (e.g., Cisco
IOS~\cite{ciscoios}), either manually or using an automatic
tool~\cite{robotron, propane, synet, zeppelin}.  
% The configuration
% files encode which protocol(s) to use, what destination addresses to
% advertise, and how to choose one path between multiple possible
% paths. Configurations can also specify static routes (which have
% highest priority over OSPF/BGP routes) and access-control lists (ACL),
% which selectively allow/block certain traffic classes.

Another mechanism commonly used in distributed control planes is
load-balancing traffic between endpoints. ECMP (Equal Cost Multi-Path
routing)~\cite{ecmp}, or its weighted variant (WCMP~\cite{wcmp}), 
are commonly used to ensure traffic between
endpoints is distributed across 
multiple paths in the
network. 
In an ECMP-enabled router, traffic is split \emph{equally} among the
multiple ``best paths'' based on routing metrics.
ECMP/WCMP affords traffic
engineering capabilities to the network without centralized
control mechanisms. 

\subsection{Control Plane Abstractions}

Analyzing whether a network satisfies certain properties over paths,
qualitative or quantitative, requires knowing the network forwarding
state. Understandably, inferring this based on low-level router
configurations is difficult.  Thus, to support management of networks,
many frameworks~\cite{batfish, arc, minesweeper, bagpipe, era} have
been developed to model the distributed network control logic such
that different inferences can be made about network forwarding---under
failures---without actually deploying the network configurations.
These models are used widely in verifying qualitative properties,
e.g., reachability, middlebox traversals and isolation.

However, verification and repair of network overload, a quantitative
property, is challenging in aspects which the state-of-art models are
not equipped to handle. Firstly, this property requires the notion of
how much traffic volume a traffic class sends on a path (or on
multiple paths, if load-balancing schemes are equipped) in order to compute
load on links. Qualitative verification does not care about how much
traffic is sent on the path.
%%  but figuring out the load of a link requires
%% modelling of traffic quantities on links
Moreover, we need to reason
about the collective load imposed by {\em all} traffic classes in the
network.  This differs significantly from verifying qualitative
properties which depend on a small subset of traffic flows, often just
individual traffic classes.
% For instance,
% some of the common qualitative properties operators care about are
% reachability (certain endpoints are reachable from each other),
% waypointing (traffic between certain endpoints passes through
% waypoints, e.g., firewalls, intrusion detection systems (IDS) etc.),
% access control (traffic between certain endpoints is blocked), and 
% isolation (traffic classes don't affect one another).
% Thus, the model used for
% verifying/repairing network overload needs to model traffic quantities 
% and must be succinct and scalable
% in order to deal with a large number of traffic classes in conjunction.

Secondly, operators want to understand if the network can handle load
under different failure scenarios.  While in the worst-case scenarios,
i.e., when large number of devices fail together--- the network is
bound to be unable to handle input traffic, the probability of such
scenarios occurring is very small.  Thus, the operator may constrain
the set of requisite failure scenarios---e.g., only 1 and 2-link
failures. The number of failure scenarios grows exponentially with
number of links. Thus,
% on either the number of
% device (link/switch) failures or the probability of the failure
% scenario (assuming a failure probability distribution for the links
% and that different link failures are independent).  Notice,
% that the number of failure scenarios grows exponentially with respect to the chosen
% threshold---
% ---e.g.,
% for a 45-node fat-tree~\cite{fattree}, the number of 1-, 2- and 3-link failure
% scenarios are 108, 5,778, and 204,156 respectively! 
the approach
adopted by some control plane models, e.g., Batfish~\cite{batfish}, of
enumerating all failure scenarios to compute the routing state 
and detect network overload for each scenario can be
\emph{prohibitively} expensive. Therefore, we require our network model to
efficiently encode network routing state even under failures and
prevent path enumeration.

Two networks models are amenable for verifying network overload
under failures: ARC~\cite{arc} and Minesweeper~\cite{minesweeper}.

\begin{figure}
	\centering
	\subfloat[N1]{\includegraphics[width=0.3\columnwidth]{figures/minesweeper_Plano9.eps}}
    \subfloat[N2]{\includegraphics[width=0.32\columnwidth]{figures/minesweeper_Alpharetta21.eps}}	
	\caption{\label{fig:minesweepertimes}
	Network load verification times for \arc and Minesweeper for networks N1 and N2 and 
	varying number of traffic classes.}
\end{figure}


\subsection{\arc vs. Minesweeper}

The \arc network model leverages the fact that most
routing protocols use a \emph{cost-based path selection 
algorithm}. E.g., OSPF uses Dijkstra's algorithm to compute
minimum cost paths from a source to all destinations, where each link
has an associated cost; and
BGP selects paths based on numeric metrics, most importantly, the AS
path length.
\arc abstracts the different routing protocols, static routes, 
and ACLs based on the above principle, and models the
network's collective forwarding behavior under failures using a
set of weighted directed graphs satisfying the 
\emph{path-equivalence} property: under \emph{any arbitrary failure
scenario}, the path taken by the traffic will
correspond to the \emph{shortest weighted path} in the graph.


\arc cannot model every set of router 
configurations as certain routing constructs like 
BGP local preferences do not adhere to the cost-based 
path selection principle.
%% \arc is able to achieve this property for
%% a vast majority of configurations due to the fact that most routing
%% protocols in use today employ a \emph{cost-based path selection
%%   algorithm}.
Empirically, \arc has been shown to 
produce path-equivalent abstractions
for more
than 95\% of the network configurations
of 314 datacenter networks operated by a large Online Service
Provider~\cite{arc}. Thus, \arc has
sufficiently high feature 
coverage to be useful in practice.

Another state-of-art abstraction
Minesweeper~\cite{minesweeper} provides a general-purpose
symbolic model of the network control and data planes.  Minesweeper
encodes the converged routing state as SMT (Satisfiability Modulo
Theories) formulas~\cite{z3} and has vast feature coverage; for
instance, it can support iBGP and BGP local preferences, which \arc's
cost-based model cannot handle.  Minesweeper %% achieves this coverage
%% using complex SMT constructs to model diverse routing technologies and
%% features and
is primarily used to verify qualitative properties dealing with a
single or small subset of traffic classes under 1-link or no failure.

Let us consider \arc v/s Minesweeper for detecting link overload under
no failures.  Using \arc, we can compute the exact network paths taken
by the traffic classes using the polynomial-time Dijkstra's algorithm
and thus compute individual link utilizations.  On the other hand, for
Minesweeper, due to the symbolic nature of the network abstraction, we
would need to use the SMT solver to find the network paths and compute
utilizations---this can be time-consuming in practice. The presence of
network failures further slows down verification.  We
compare network load verification performance by using the
Minesweeper and \arc abstractions for $k=1$ link failure scenarios in
\Cref{fig:minesweepertimes}.  We can observe that using \arc is
significantly faster than Minesweeper, achieving $5\times$ speedup for
N1 and $20-800\times$ for the larger N2 network.

Moreover, the \arc abstraction offers certain key benefits for 
network transformations~\cite{symmetry} which can help reduce the search space 
efficiently and enables even faster quantitative
verification than Minesweeper (\secref{sec:optimizations}).
For example, in \arc, we know a certain path will not be traversed if
a shorter path exists in the network (by simply comparing the path
cost). This property can be used to reduce the search space---both by
limiting which links to consider as candidates for overload, and by
ignoring the contributions of some source-destination pairs' traffic
volumes---in scenarios when a small number of links can fail
together. In Minesweeper, there is no simple mechanism to compare two
symbolic paths, making such search space reduction much harder.

Thus, given that \arc is a more natural fit for quantitative analysis,
\arc's natural attributes support effective speedup of verification,
and \arc has near-universal control plane coverage, we adopt it as the
basis for our framework. However, \arc needs changes to model (1) paths 
under failures without enumeration, (2) distribution of a traffic class's
load on multiple available equal cost paths (due to ECMP or WCMP), and 
(3) variation of traffic
volumes and the effect on link loads.

% Since, we bound the number of failure scenarios, the network region which a 
% certain traffic class can potentially traverse under those scenarios would be 
% smaller than the entire network. In \arc, the network paths can be computed 
% using the graph weights to efficiently reduce the search space, Minesweeper's 
% symbolic network paths 

% In \arc, the network paths are the shortest 
% distance paths; we leverage this property to efficiently reduce the search space 
% for each traffic class (\secref{sec:optimizations}). Empirically, we find 
% that these optimizations can provide $x-y$ factors of speedup due to the 
% search space reduction.
% Thus, we build our 
% abstraction on top on \arc, 
% sacrificing the generality afforded by Minesweeper 
% for tractability. \kausik{ETG minimization is 
% hard to explain in short.}

