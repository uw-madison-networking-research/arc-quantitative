%!TEX root = paper.tex
\section{\name Encoding} \label{sec:qarc-ilp}
We now present the mixed-integer linear program (MIP) encoding of
\name. Our encoding makes new technical contributions. First, it
shows how to extend \arc ETGs with flow quantities while 
permitting bounded variation of traffic characteristics, while also
accounting for the effect of failures 
and the use of shortest path
forwarding on how flow is routed. This is crucial to determining
link overload. Second, it further shows how to \emph{model flow being split
among multiple shortest paths}, i.e., modeling ECMP, which is a key
construct used in most networks; again, we must account for failures.
Third, our encoding  does not require disjunctions and only uses 
integer variables to represent failure and link overload. 
Table~\ref{tab:qarcvariables}(a) and Table~\ref{tab:qarcvariables}(b) describe the
\name variables and constants, respectively. For each section, the
sentences within boxes state the logical constraints encoded using the
presented constraints.

\begin{figure}
	\centering
	\small
	\subfloat[\qarc variables]{
			\begin{tabular}{m{4em} | m{12em} | P{1.6em}} 
			{\bf Name}& {\bf Description} & {\bf Range}  \\ 
				\hline
				$Flow(e, tc)$ & Fraction of traffic for class $tc$ flowing
				on edge $e$ & [0,1] \\
				\hline 
				$\Delta(tc)$ & Fraction of traffic variation for class $tc$ & [0,1] \\
				\hline
				$\Delta(e, tc)$ & Fraction of traffic variation for class $tc$ flowing
				on edge $e$ & [0,1] \\
				\hline
				$Dist(n, tc)$ & Shortest path distance of node $n$ to destination 
				in ETG &  $\mathbb{Q}$ \\
				\hline
				$Fail(e)$ & Link $e$ (for all ETGs) failure status (1 $\equiv$ failed) & $\{0, 1\}$ \\
				\hline
				$Load(e)$ & Link $e$ load status (1 $\equiv$ overloaded) & $\{0, 1\}$
			\end{tabular}}
			\hfill
\subfloat[\qarc constants] {
			\begin{tabular}{m{4em} | m{15em}}
			{\bf Name}& { \bf Description}  \\ 
				\hline 
				$TC$ & Set of all traffic classes \\
				$\overline{FL}$ & Set of all failure scenarios \\
				%\hline
				$T(tc)$ & Traffic sent by class $tc$ ($T(tc) \in \mathbb{Q}$)  \\
				%\hline
				$Src(tc)$ & Source node of traffic class $tc$ \\
				%\hline
				$Dst(tc)$ & Destination node of traffic class $tc$ \\
				%\hline
				$In(n)$ & Set of in-edges of node $n$ in ETG \\
				%\hline
				$Out(n)$ & Set of out-edges of node $n$ in ETG \\
				%\hline
				$OEdges(tc)$ & Set of all outgoing edges in $tc$ ETG  \\
				%\hline
				$Links$ & Set of all physical links in the network \\
				%\hline
				$Cap(e)$ & Capacity of link $e$ ($Cap(e) \in \mathbb{Q}$) \\
				%\hline
				$\Pi$ & Threshold of total traffic variation 
			\end{tabular}}
			\caption{\name encoding variables and constants \label{tab:qarcvariables}}
\end{figure}

\subsection{Flow Constraints} \label{sec:flow}

\noindent\fbox{%
    \parbox{.97\columnwidth}{%  
	Traffic flows from source to destination along \emph{active} links 
	and flow is \emph{conserved} at every node in the ETG.
    }%
}

	The $Flow$ and $\Delta$ variables represent respectively the fraction of 
	normal and variation of traffic flowing along the network links. 
	For class $tc$ and edge $e$, the actual
	traffic on the edge is $(Flow(e,tc) + \Delta(e, tc)) \times T(tc)$.
	\footnote{The distinction of $Flow$ and $\Delta$ variables
	ensures the verification constraints (\ref{eq:load}) are linear while 
	providing variation in traffic characteristics.}
	For every ETG and 
	corresponding traffic class $tc$, the outgoing flow at $tc$
	source and the incoming flow at $tc$ destination should equal 
	$1 + \Delta(tc)$ which are constrained separately:
\begin{equation} \label{eq:flowend}
\sum_{e \in Out(Src(tc))} Flow(e, tc) = 1 ~~~~
\sum_{e \in ~In(Dst(tc))} Flow(e, tc) = 1 
\end{equation}

\begin{equation}
\sum_{e \in Out(Src(tc))} \Delta(e, tc) = \Delta(tc) ~~~~
\sum_{e \in ~In(Dst(tc))} \Delta(e, tc) = \Delta(tc)
\end{equation}

For all other nodes $n \not\in \{Src(tc), Dst(tc)\}$ 
in the ETG, flow is conserved, i.e., incoming
flow is equal to outgoing flow.
\begin{align}
\sum_{e_{in} \in In(n)} Flow(e_{in}, tc) ~~~& = \sum_{e_{out} \in Out(n)} Flow(e_{out}, tc) \nonumber \\
 \sum_{e_{in} \in In(n)} \Delta(e_{in}, tc) ~~~& = \sum_{e_{out} \in Out(n)} \Delta(e_{out}, tc)
\label{eq:flowconservation}
\end{align}

The above constraints are standard, but we need to now accommodate failures. 
When a link fails, no traffic must flow on it. For this, we use this constraint:
\begin{equation} \label{eq:flowfail}
Flow(e,tc) + Fail(e) \leq 1 
~~~\bigwedge~~~~~\Delta(e,tc) + Fail(e) \leq 1
\end{equation}
Thus, if $Fail(e) = 1$, then $Flow(e,tc)$ and $\Delta(e, tc)$ 
have to be strictly zero.

Operators can bound the individual variation for each traffic class and/or
bound the total extra variation of traffic in the network by a threshold $\Pi$.
\begin{equation} 
	\sum_{tc \in TC} \Delta(tc) * T(tc) \leq \Pi
\end{equation}

Given a solution to the above constraints, 
we construct the flow graph of $tc$
by picking all edges $e$ where $Flow(e,tc) > 0$, which 
indicate all the links traffic flows on. 

\begin{lemma}  ~\label{thm:flowconservation}
For every traffic class $tc \in TC$ every
failure scenario $FL \in \overline{FL}$, 
Constraints~\ref{eq:flowend}-\ref{eq:flowfail} 
ensure that (1) the 
Flow Graph $FG(tc, FL)$ is a directed acyclic graph 
and (2) flow is preserved from $Src(tc)$ to $Dst(tc)$.
\end{lemma}

\begin{proof}
We proceed by contradiction. First, 
let us assume that a router $r$ drops some 
amount of traffic, i.e., the incoming flow 
into $r$ is not equal to outgoing flow of $r$. 
However, this contradicts 
Constraint~\ref{eq:flowconservation}, thus, (2) 
flow is conserved at every router. 

Second, let us assume that the flow graph contains 
a cycle containing router $r$ and flow amount 
$f_c$ which keeps flowing in the cycle. 
The flow $f_c$ will not reach $Dst(tc)$, and since,
flow is conserved at every router, the flow reached
at destination will not contain $f_c$. However,
this contradicts Constraint~\ref{eq:flowend} that
all flow from source is reached at the destination.
Thus, proved that (1) the flow graph will not contain 
any cycles.
\end{proof}


\subsection{Distance Constraints} \label{sec:distance}

By definition of an ETG, the path in the real network between the corresponding 
source and destination matches the shortest path in the ETG. Thus:

\noindent\fbox{%
    \parbox{.97\columnwidth}{%  
	Traffic must only flow on shortest-distance ETG paths.
	}% 
}

We formulate shortest path distances to the destination node of the
ETG in an inductive fashion - the shortest path from a node must
traverse through one of its neighbors, and thus, distance of node $n$
can be defined inductively as the shortest among distances from the
neighbors. Again, failures introduce complications, because distances
can change under different link failures.
To this end, we need to two sets of constraints.
First, we use  the following constraints to ensure that for every node $n$ and traffic class $tc$, the value of
the variable $Dist(n, tc)$ is \emph{at most} the distance of the shortest path that traverses only active links:
\begin{equation} \label{eq:distance}
\forall e = n \rightarrow n' \in Out(n).
~~Dist(n, tc) \leq W(e) + Dist(n', tc) + \infty \times Fail(e)
\end{equation}
The intuition is that when edge $n \rightarrow n'$  has failed 
and $Fail(n \rightarrow n')=1$, 
the shortest distance from $n$ will not depend on the path through $n'$
as the equation will be satisfied trivially due to the large
constant in front of $Fail(n \rightarrow n')$. 
The above constraints
% ensures
%$Dist(n,tc)$ is smaller than distances from neighbors connected by active links.
%Note that Constraints (\ref{eq:distance}) 
provide an upper-bound on 
$Dist$ variables, but do not ensure that the variable values are exactly 
equal to actual shortest distances in the ETG. E.g.,
setting all $Dist$ variables to 0 trivially satisfies
Constraints (\ref{eq:distance}).
%\begin{wrapfigure}{r}{9em}
%	\includegraphics[width=0.4\columnwidth]{figures/flowdistex.pdf}
%	\caption{For the  $S$-$T$ ETG in Figure~\ref{fig:controlplane}(b), 
%	we illustrate the property that sum of 
%	Flow in the network weighted with edge weights is the distance 
%	of the path taken by the flow.}
%	\label{fig:flowdistex}
%\end{wrapfigure}

Second, we use another set of constraints to ensure that the ETG
traffic flow only uses the shortest paths in the network. (We will
consider load-balancing in \secref{sec:loadbalancing} and ignore it
here). We illustrate the idea of our encoding using the example ETG in
\Cref{fig:ecmpex}. For the $Flow$ quantities, the cost
of the path taken by the flow can be computed by the sum of $Flow$ on
all ETG edges multiplied by the edge weights -- i.e., $\sum_{e} W(e)
\times Flow(e,tc)$ (regardless of whether the flow is sent on the
shortest path or not). The following constraint ensures that the flow is sent on the
shortest distance path from source to destination of the ETG of 
$tc$:
\begin{equation} \label{eq:srcdist}
Dist(Src(tc)) = \sum_{e \in OEdges(tc)} Flow(e, tc) * W(e)
\end{equation}
Since Constraint (\ref{eq:distance}) guarantees upper bounds on the shortest distance,
the above constraint  will ensure the traffic will not be sent 
on a longer path. Also, by virtue of Constraints 
(\ref{eq:distance}) and (\ref{eq:srcdist}),
the $Dist$ variables for all nodes in the ETG will be exactly
equal to the shortest distances to the destination. 
Notice, that \emph{the constraint will guarantee this property even if traffic flows across multiple shortest paths}.

To ensure that the traffic variation only flows on shortest paths, we add 
the following constraint:
\begin{equation} \label{eq:flowdelta}
	\forall e \in OEdges(tc). ~\Delta(e, tc) \leq Flow(e, tc) 
\end{equation}
By virtue of Constraint (\ref{eq:srcdist}), $Flow(e,tc)$ is 0 on 
non shortest path links, and consequently, $\Delta(e,tc)$ will be 0.


\begin{figure}
	\centering
	\includegraphics[width=0.6\columnwidth]{figures/ecmpex.pdf}
	\caption{ECMP behavior of node $C.1_{O}$ under
		  two different scenarios for the partial $R-T$ ETG from 
		  \Cref{fig:controlplane}(b). Note that Sum of Flow in 
		  the network weighted with edge weights is the distance 
		  of the path taken by the flow for both scenarios.}
	\label{fig:ecmpex}
\end{figure}

% \begin{figure}
%     \begin{minipage}{0.6\linewidth}
%          \includegraphics[scale=0.37]{figures/ecmpex.pdf}
% 		\compactcaption{\footnotesize 
% 		Illustrating ECMP behavior of node $C.1_{O}$ under
% 		two different scenarios for the partial $R-T$ ETG from 
% 		\Cref{fig:controlplane}(b)}
%         \label{fig:ecmpex}
% \end{minipage}%
% \hfill
% \begin{minipage}{0.35\linewidth}
%         \includegraphics[scale=0.38, right]{figures/flowdistex.pdf}
%         \compactcaption{\footnotesize
% 			Sum of Flow in the network weighted with edge weights is the distance 
% 		 	of the path taken by the flow for the $S$-$T$ ETG from Figure~\ref{fig:controlplane}(b)}
%         \label{fig:flowdistex}
% 	\end{minipage}
% \end{figure}

\begin{lemma} ~\label{thm:shortest}
For every traffic class $tc \in TC$ and failure scenario $FL \in \overline{FL}$, 
Constraints~\ref{eq:flowend}-\ref{eq:flowdelta} 
ensure that each path 
from Src(tc) to Dst(tc) in the Flow Graph 
$FG(tc, FL)$ is a shortest weighted path in $ETG(tc)$ under 
the failure scenario $FL$.
\end{lemma}

\begin{proof}
We proceed by contradiction. 
Let us assume there is a path $p$ in Flow Graph 
$FG(tc, FL)$ which is not a shortest path in $ETG(tc)$,
i.e., there is a shortest path $p'$ and 
$W(p') < W(p)$ (where $W(p)$ denotes the sum of 
weights of edges in path $p$). We denote the 
flow on path $p$ as $f_p$ and flow on path $p'$
as $f_{p'}$. 

By virtue of Constraint~\ref{eq:srcdist}, we can 
write $Dst(src(tc)) = W(p)*f_p + W(p')*f_{p'} + ...$
Using ~\Cref{thm:flowconservation}, 
the sum of flows on all paths must be equal to 
1 and $p'$ is a shortest path, 
therefore, $Dst(src(tc)) > W(p')$.

Consider Constraint~(\ref{eq:distance}) for routers on the 
path $p' = Src(tc) \rightarrow r_1 \ldots Dst(tc)$. $p'$ 
is in the flow graph, so no edge will be failed and Fail 
variables on p' will be set to 0.

\begin{align}
	Dist(Src(tc), tc) &\leq W((Src(tc), r_1)) + Dist(r_1, tc) \nonumber \\
	&\leq W((Src(tc), r_1)) + W((r_1, r_2) + Dist(r_2, tc) \ldots \nonumber \\
	&\leq W(p') + Dist(Dst(tc), tc) \nonumber
\end{align}
Since, $Dist(Dst(tc), tc) = 0$, $Dist(Src(tc), tc) \leq W(p')$.
This contradicts our previous assertion that 
$Dst(src(tc)) > W(p')$. Hence, proved.
\end{proof}

\subsection{Load Balancing Constraints} \label{sec:loadbalancing}
\noindent\fbox{%
    \parbox{.97\columnwidth}{%  
	The outgoing $Flow$ and $\Delta$ 
	of each node are \emph{split} equally among the 
	shortest neighbors connected by active links.
	}% 
}

Routers employ distributed load-balancing techniques to manage load on
network links (e.g., using vanilla ECMP with OSPF, or using BGP
multipath~\cite{bgpmultipath}).  \Cref{fig:ecmpex} demonstrates how
ECMP operates under no failures, and the differences that arise when a
link failure occurs; our model should account for the workings
of ECMP with and without failures.

At any router, by virtue of the flow and distance constraints above,
traffic will never be sent along longer paths. However, we have not constrained 
how traffic is split across the multiple
paths.  The constraints presented in this section ensure traffic is split equally
along the active links at a router which are on the currently
available shortest paths to the destination.
First, we show a simple disjunctive constraint that 
encodes the desired behavior
and we then show how the same behavior 
can be captured without disjunction.

For node $n$ and
all outgoing edge pairs $e_1=n \rightarrow n_1$ and 
$e_2= n \rightarrow n_2$ 
in $Out(n)$, ECMP for $Flow$ (similarly $\Delta$) is:
\begin{equation*} %\label{eq:ecmp}
[W(e_1) + Dist(n_1, tc) = W(e_2) + Dist(n_2, tc)]
~\wedge \neg Fail(e_1) ~\wedge~ \neg Fail(e_2) 
 \implies Flow(e_1, tc) = Flow(e_2, tc)
\end{equation*}
The above constraints ensure that if the distance to the destination
along two active outgoing edges is equal, then the flow on them
will also be equal (if these edges do not lie on the shortest path,
then flow will be 0 on both edges). However, these constraints use
implications (i.e., disjunctions) and cannot be provided in this form to 
an ILP solver. 

Next, we show how by
introducing new \emph{rational} variables we can write modified constraints that
express the ECMP load balancing constraints using only linear
inequalities. This forms a \emph{key innovation of \name encoding}.
Specifically, we define sets of variables $minFlow(n, tc) \in
[0,1]$ and $maxFlow(n, tc) \in [0,1]$ to capture the minimum and
maximum non-zero $Flow$, respectively, out of node $n$ for traffic class
$tc$ (and similarly for $\Delta$ variables).  
The non-zero flow restriction holds for flows along the
shortest paths (flow along non-shortest paths will be 0), and thus we
can impose constraints on $minFlow$ and $maxFlow$ to model ECMP
routing. We elaborate below. We also add similar constraints for 
the traffic variation $\Delta$ variables so that the total traffic adheres 
to ECMP load-balancing.

Adding constraints for $maxFlow(n,tc)$ to be the maximum non-zero flow
value is trivial, as the zero flow values will not affect the
$maxFlow$ variable:
\begin{align} \label{eq:maxflow}
\forall e=n \rightarrow n' \in Out(n). ~Flow(e, tc) &\leq maxFlow(n, tc) \nonumber \\
\forall e=n \rightarrow n' \in Out(n). ~\Delta(e, tc) &\leq max\Delta(n, tc)
\end{align}

Adding constraints for $minFlow(n,tc)$ is trickier:
to ensure that $minFlow(n,tc)$ is the minimum non-zero flow value, we would need
to know the flow values {\em for all possible failure scenarios}. 
To bypass this problem, we use the distance variables $Dist$ to identify the next-hops which lie on the
shortest paths, and thus, will have non-zero flows. Based on this insight, we impose the following
constraints for all $ e=n \rightarrow n' \in Out(n)$ to ensure that $minFlow$ variables have the correct values:
\begin{align} \label{eq:minflow}
\forall e=n &\rightarrow n' \in Out(n). ~minFlow(n, tc) - Flow(e, tc) \leq \nonumber \\
&\infty \times( W(e) + Dist(n',tc) - Dist(n, tc) + Fail(e)) \nonumber \\ 
\forall e=n &\rightarrow n' \in Out(n). ~min\Delta(n, tc) - \Delta(e, tc) \leq \nonumber \\ 
&\infty \times( W(e) + Dist(n',tc) - Dist(n, tc) + Fail(e)) 
\end{align}

First, notice that the quantities $W(e) + Dist(n',tc) - Dist(n, tc)$
and
$W(e) + Dist(n',tc)  - Dist(n, tc)+ Fail(e)$ are always greater or equal than $0$.
Hence, there are three scenarios for each edge which are all encoded by the 
above constraint: (1) the edge $e$ has failed, in which case $Fail(e)=1$, the RHS of
the constraint is infinity, and the constraint is trivially satisfied,
(2) the edge $e$ is not on the shortest path, thus $W(e) + Dist(n',tc)$ is
greater than $Dist(n,tc)$ and thus, the RHS is a large positive constant, and
(3) the edge $e$ is active and on the shortest path, so the RHS of the 
constraint is 0---i.e., $minFlow(n,tc) \leq Flow(e,tc)$. Therefore,
$minFlow(n,tc)$ is smaller than all the non-zero edge flows 
(which can only flow on the shortest paths).

Thus, for a node $n$, we have two variables for the lower bound and upper bound
of all the non-zero edge flows out of the node. For ECMP, we require all non-zero
edge flows on the shortest paths out of a node to be equal, which can be 
enforced by ensuring the lower bound $minFlow(n,tc)$ 
and upper bound $maxFlow(n,tc)$ are equal (similarly for $\Delta$): 
\begin{align} \label{eq:maxminequality}
minFlow(n,tc) &= maxFlow(n,tc) \nonumber \\
min\Delta(n,tc) &= max\Delta(n,tc)
\end{align}

These constraints ensure
the total flow to neighbors on the shortest paths 
are equal to one another, adhering to the ECMP load-balancing model.
Thus, we are able to express the ECMP constraints 
without requiring any disjunctions. The above constraints can be modified 
by multiplying constant weight 
factors to $Flow$ variables to 
model Weighted Cost Multipathing (WCMP)~\cite{wcmp}.

\subsection{Failure Constraints} \label{sec:failure}
\noindent\fbox{%
    \parbox{.97\columnwidth}{%  
	Failure scenarios considered can be restricted by the operator, 
	e.g., by number of links or 
	likelihood of failures.
	}% 
}

Operators may want to restrict the failure
scenarios of interest ($\overline{FL}$) 
to make useful observations about how the
network control plane reacts under failures. We
exemplify how \name can support two kinds of failure scenarios--- 
(1) under $k$-link failure scenarios, and (2) link failure scenarios with
probability greater than threshold $\omega$. Extending to other operator-specified
constraints on scenarios of interest (such as core link failures only) 
is easy and omitted for
brevity. To restrict the number of
link failures to $k$:
\begin{equation} \label{eq:failbound}
\sum_{e \in Links} Fail(e) \leq k
\end{equation}
Operators can also assign failure probabilities 
to individual links and restrict the search 
to scenarios that have probability above a threshold 
(assuming link failures are independent).

\begin{theorem} \label{thm:shortestequal}
For every traffic class $tc \in TC$ and failure scenario $FL \in \overline{FL}$, 
Constraints~\ref{eq:flowend}-\ref{eq:failbound} ensure 
that the flow Graph $FG(tc, FL)$ is a directed acyclic
graph such that each path from $Src(tc)$ to $Dst(tc)$ is the 
shortest path in $ETG(tc)$, and for every router $n$ in flow graph $FG(tc, FL)$, 
the flows on outgoing edges which lie on shortest paths from 
$n$ to $Dst(tc)$ are equal., i.e.,
$\forall n_1, n_2 \in next(n). 
~~F_{tc}((n, n_1)) = F_{tc}((n, n_2))$.
\end{theorem}

\begin{proof}
~\Cref{thm:flowconservation} and \Cref{thm:shortest} 
prove that the flow graph $FG(tc,FL)$ does not contain 
any cycles and only contains the shortest paths in $ETG(tc)$.
To prove that for any router $n$, 
flow on outgoing edges which lie on shortest paths 
are equal, we proceed by contradiction. Let us assume that 
for traffic class $tc$, router $n$ does not split traffic 
equally, i.e., 
$\exists n_1, n_2. F_{tc}((n, n_1)) \not= F_{tc}((n, n_2))$.
In terms of the \name variables, the assumption
translates to $Flow((n, n_1), tc) \not= 
Flow((n, n_2), tc)$. Without loss of generality, 
let $Flow((n, n_1), tc) < Flow((n, n_2), tc)$.

By virtue of Constraints~\ref{eq:maxflow}, the following 
holds:
\begin{equation} \label{eq:ecmpproofmax}
	Flow((n, n_1), tc) < Flow((n, n_2), tc) \leq maxFlow(n, tc) 
\end{equation}

By Lemma~\ref{thm:shortest}, both $n_1$ and $n_2$ 
lie on shortest paths from $n$ to the destination. 
Therefore, the expression $W((n, n_1)) + Dist(n_1,tc) 
- Dist(n, tc) + Fail((n, n_1))$ is equal to 0 (similarly
for $n_2$). Plugging in Constraints~(\ref{eq:minflow}), we get:
\begin{equation} \label{eq:ecmpproofmin}
	minFlow(n, tc) \leq Flow((n, n_1), tc) < Flow((n, n_2), tc) 
\end{equation}

From (~\ref{eq:ecmpproofmax}) and ~(\ref{eq:ecmpproofmin}), we get 
\begin{equation*}
	minFlow(n, tc) < maxFlow(n, tc)
\end{equation*}
However, the above assertion contradicts Constraint~(\ref{eq:maxminequality}). 
Hence, proved. 
\end{proof}
\Cref{thm:shortestequal} and \Cref{thm:pathequivalence} together prove that 
the flow graphs constructed by the \qarc constraints faithfully represent 
the actual routing paths and flow distributions in the actual network.

% Suppose, the operator has probabilities 
% assigned to individual link failures ($P_{fail}(l)$)
% and is only interested in link failure scenarios 
% that have joint probability above a
% threshold $\omega$. 
% The failure probabilities can be derived 
% from telemetry data in real-world networks
% \cite{linkfailureprob}. We assume that
% link failures are independent, thus, the probability of a certain link
% failure scenario is the product of individual link failure
% probabilities. Then, we would want to enforce the following constraint:
% \begin{equation*}
% 	\underset{l \in Links. Fail(l) = 1}{\Pi} P_{fail}(l) \geq \omega 
% \end{equation*}
% We use the logarithm of probabilities to generate the equivalent linear constraint.
 %% , we can enforce the restriction of searching only 
%% for failure scenarios with probability greater than $\omega$:
% \begin{equation} \label{eq:failureprob}
% \sum_{l \in Links} log(P_{fail}(l)) \times Fail(l) \geq log(\omega)
% \end{equation}

\section{Verification using \name} \label{sec:verification}

Finally, verifying for network load violations using \name is done by adding 
load-related constraints.

\noindent\fbox{%
    \parbox{.97\columnwidth}{%  
	$Load(e)$ is set to 1 if link $e$ utilization exceeds  capacity.
	}% 
}

%% We show how we can add additional constraints to the \name encoding 
%% to verify if there are provisioning violations under the failure 
%% scenarios. 
For every link $e$,
$Load(e) \in \{0, 1\}$ is 1 iff link $e$
is overloaded, i.e., total traffic exceeds the capacity of $e$.
We then add the following constraint to model overload on $e$: 

\begin{equation} \label{eq:load}
\frac{\sum_{tc \in TC} [Flow(e, tc) + \Delta(e, tc)] \times T(tc)}{Cap(e)} - Load(e) \geq 0 
\end{equation}

The numerator of the fraction captures the total traffic flow on link $e$,
while the denominator, $Cap(e)$ (a constant), captures the capacity of $e$.
$Load(e)$ can only be $1$ when the traffic on link $e$ exceeds its 
capacity.
To find if there exists a failure scenario where at least one of the links is 
overloaded, we can constrain the sum of
load variables to be at least 1:
\begin{equation} \label{eq:netcong}
\sum_{e \in Links} Load(e) \geq 1
\end{equation}

When we feed the constraints to the ILP solver, there are two
outcomes.  First, the solver returns a satisfiable model from which we
can extract the link failure scenario under which one or more links
are overloaded.  Otherwise, the solver returns unsatisfiable, which
means there is no $k$-link failure scenario which can cause link
overload.  There are two explanations for the latter: first, the
network has sufficient capacity to handle rerouted input traffic under
failures.  Second, it is possible a subset of traffic classes are
disconnected due to the failures (these classes do not have high path
redundancy), and the remaining active traffic classes do not have
sufficient traffic to cause link overloads.  Note that, even when the
solver finds a failure scenario where link overload occurs, certain
traffic classes could be disconnected in the satisfying solution, with
the remaining active traffic classes still able to overload the
link(s).
%%  Operators can use qualitative verifiers~\cite{arc,
%%   minesweeper} to check for reachability properties of traffic
%% classes,
In sum, \qarc, as presented, will try to find potential for link
overload even in the face of disconnections. \qarc can be 
extended to verify potential for overload under failure scenarios that
don't induce any disconnection. However, in networks where many
traffic classes have limited redundancy, there may be no failure
scenario that experiences link overload without any disconnections.

\begin{theorem}[Correctness]
A failure scenario $FL$ satisfies Constraints~\ref{eq:flowend}-\ref{eq:failbound}, 
\ref{eq:load}-\ref{eq:netcong} if and only if
there exists a link $l \in Links$ such that 
$Util_{FL}(l) \geq Cap(l)$. 
\end{theorem}

\begin{proof}
First, we prove that if $FL$ satisfies Constraints~\ref{eq:flowend}-\ref{eq:failbound}, 
\ref{eq:load}-\ref{eq:netcong}, then one of the links' utilization 
will exceed capacity.
We proceed by contradiction. Let us assume that there exists $FL$
such that for all $l \in Links$, 
$Util_{FL}(l) < Cap(l)$, i.e., no link's 
utilization exceeds capacity. In Constraint~\ref{eq:load},
the numerator in the fractional term in the 
LHS represents the amount of 
traffic on the link: $Util_{FL}(l)$. Therefore, for all links:
\begin{align}	
	Load(l) - Util_{FL}(l) > Cap(l) \leq 0
	Load(l) < 1	
\end{align}
However, this contradicts with Constraint~(\ref{eq:netcong}). 
Hence, proved. 

We now prove if there exists a $FL$ such that 
there is a link $l \in Links$ where $Util_{FL}(l) \geq Cap(l)$, then 
there is a satisfying assignment for Constraints~\ref{eq:flowend}-\ref{eq:failbound}
\ref{eq:load}-\ref{eq:netcong}. Using \Cref{thm:shortestequal} and \Cref{thm:pathequivalence},
the flow graphs in the actual network 
will be a satisfying assignment for Constraints~\ref{eq:flowend}-\ref{eq:failbound}.

For link $l$, we set $Load(l) = 1$ and for all other links $l' \in Links \setminus {l}$, we set $Load(l') = 0$.
Since $Util_{FL}(l) \geq Cap(l)$:
\begin{equation*}
	\frac{Util_{FL}(l)}{Cap(l)} - Load(l) =  \frac{Util_{FL}(l)}{Cap(l)} - 1 \geq 0
\end{equation*} 
Thus, Constraint~\ref{eq:load} is satisfied for link $l$. 
For other links $l' \in Links \setminus {l}$, $\frac{Util_{FL}(l)}{Cap(l)} > 0$ as both 
quantities are positive, thus Constraint~\ref{eq:load} is satisfied for all links.
Finally, since $Load(l) = 1$, Constraint~\ref{eq:netcong} is satisfied as well. Hence, proved.
\end{proof}


\qarc can also be used to verify other quantitative properties
dealing with link utilization---for instance, it can verify 
if the average link utilization of 
the network remains below a certain threshold 
for every $k$-link failure 
scenario. 


