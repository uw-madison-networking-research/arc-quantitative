%!TEX root = paper.tex
\section{QARC} \label{sec:qarc}

To address the above challenges, we improve ARC to produce a new
abstraction called \name (\arc with Quantities). 

\vspace*{2mm}
\minisection{ARC}
\arc abstracts the set of router 
configurations using a set of weighted directed graphs
called \emph{extended topology
  graphs} (ETGs). For each traffic class, ARC constructs one ETG which
models the behavior of the network routing protocols and interactions
among the routers for the traffic class. In the ETG, each vertex
corresponds to a routing process; there is one incoming (I) and one
outgoing (O) vertex for each process. Directed edges represent the
possible path for traffic, enabled by the exchange of routing
advertisements between connected processes (intra- or inter-router
advertisement exchanges). 
The computed weights of the edges of the ETG satisfy the 
\emph{path-equivalence property}: under any failure scenario, the actual 
network path(s) are the shortest weighted path(s) in the ETG.
The path-equivalence property of \arc is useful to model ECMP-style 
load-balancing which uses routing cost to decide the multiple 
paths.

\begin{figure}
	\centering
	\subfloat[]{\includegraphics[width=0.3\columnwidth]{figures/ControlPlane.pdf}}
	\subfloat[]{\includegraphics[width=0.6\columnwidth]{figures/arc-etgs.pdf}}
	\caption{ 
	(a) Example  control plane with OSPF and BGP.  
	The boxes denote the routing protocols running on the routers, the 
	numbers on links are OSPF  weights, and the redistribution
	cost of OSPF to BGP is 1. There is an ACL installed at router B for
	traffic class $S-T$. (b) \arc ETGs for (a).}
	\label{fig:controlplane}
\end{figure}


\Cref{fig:controlplane}(a) shows a toy distributed control plane
spanning four routers. Two routers run OSPF, one runs BGP, and one
runs both and is configured to redistribute routes from OSPF to BGP.
The OSPF edge weights are configured to support load balancing for
$R-T$ traffic, while an ACL is configured at router
$B$ for $S-T$ traffic for a security policy.
\Cref{fig:controlplane}(b) illustrates the ETGs 
for the two traffic classes (R-T and S-T).

Originally, \arc was intended for verifying qualitative
properties under {\em any} arbitrary failure scenario, which
translates to applying different graph algorithms on the extended
topology graphs (ETGs)---e.g., determining if a traffic class is
connected under any $k$-link failure translates to
checking if the min-cut for the traffic class's ETG is $\ge k$. 


For verifying/repairing network load properties, we built \name  
which uses a mixed-integer linear program (MIP) 
encoding to add symbolic traffic quantities to
the \arc ETGs which can be used to verify load properties
under different failure scenarios. 
Before getting into the details, %%  of 
%% the \qarc encoding (\secref{sec:qarc-ilp}), 
we first describe how we expect \name to 
be used.

\vspace*{2mm} \minisection{Verification} Network operators have
frameworks to periodically update routing
configurations~\cite{robotron,propane,zeppelin} and measure input
traffic characteristics~\cite{bwe,swan, vl2}.  We envision \qarc to be
used for verification whenever traffic characteristics change
significantly, or the control plane is reconfigured. Taking as input
the new control plane and traffic matrix (between endpoints) with bounds
on variation, our
verification detects if 
there exists a failure scenario leading to
overload (utilization exceeding capacity) on any
link.


Consider the network control plane 
in \Cref{fig:controlplane}(a) and the corresponding \arc ETGs 
in \Cref{fig:controlplane}(b). In this scenario, the operator sees a change in the input traffic: 
the current traffic for classes $R \rightarrow T$ and $S \rightarrow T$ are 80 Gbps 
and 30 Gbps, respectively. All links in the network have a capacity of 100 Gbps.
When no links have failed, the traffic from $S \rightarrow T$
flows through the path $B \rightarrow A$, while the traffic from $R \rightarrow T$ 
is load-balanced by ECMP at $C$; 40Gbps traffic is sent through the two paths:
$D \rightarrow C \rightarrow A$ and $D \rightarrow C \rightarrow B \rightarrow A$ 
(these are the shortest networks paths in the ETGs). As we can observe, all links'
utilizations are below capacity ($D\rightarrow C: 80$, $C\rightarrow B: 40$, 
$C\rightarrow A: 40$, $B\rightarrow A: 40+30=70$). 

Suppose, the operator wants to inspect 
if the network for the given traffic matrix and a
single link failure can experience some link becoming overloaded. 
Using our tool \name, the operator 
can discover that when $C\rightarrow A$ fails, 
the traffic on $B \rightarrow A$ will be 
110 Gbps, exceeding the link's capacity.

% \kausik{talk about using models to stress-test network and check for
%   violations} \aditya{here is one possibility:} Instead of measuring
% and verifying specific traffic matrix, operators can also use \name to
% ``stress-test'' a network, by verifying for a class of traffic
% matrices that all share a property. For example, suppose the operator
% measures that the current traffic matrix M fits a ``gravity
% model''~\cite{}, where the average demand across traffic classes is
% $\rho$. The operator can randomly generate an arbitrary large number
% of traffic matrices $\{M_1,M_2\ldots\}$, where traffic in each $M_i$
% adheres to the gravity model, and the average demand in all $M_i$s is
% $\rho$, and run verification against each $M_i$ independently.
    
Going further, the operator can discover other single link failures by
asking our verification tool to find 1-link failures {\em other than}
$C \rightarrow A$. 
Likewise, the operator can discover sets of $k$-link
failures that cause links to overload. 
%% We discuss verification in
%% \secref{sec:verification}.

\vspace*{2mm}
\minisection{Repair}
When verification detects possible load violation, the operator can invoke our
repair framework. Taking as input the network configurations and the
traffic matrix, this computes a minimal set of links whose capacities
need to be changed to avoid overload  
under failures. Continuing with
our example, for the network in \Cref{fig:controlplane}, \qarc
computes that the operator should increase the $B\rightarrow A$ 
 capacity by 10Gbps to ensure no load violations occur under any 1-link
 failures.

%% We discuss \name's repair in detail in \secref{sec:repair}.

\subsection{Problem Definition}
In this section, we define the verification and repair 
problems presented in the paper. 

We represent the set of traffic classes as $TC$, 
the network topology $N = (Routers, Links)$ 
and the failure scenario is subset of links 
which have failed $FL \subseteq Links$.
For each traffic class $tc \in TC$, the routing 
protocols will ensure traffic will flow from 
source to destination in the 
network as a directed acyclic graph (DAG) under
the current network topology. 
We represent $tc$'s flow graph as 
$FG(tc,FL) = (V_{tc}, L_{tc})$. Traffic 
will not flow on failed links, thus 
$L_{tc} \cap FL = \emptyset$.
\arc constructs a weighted directed 
graph $ETG(tc)$ for each traffic 
class $tc$ with the following property:

\begin{theorem} [Path-equivalence~\cite{arc-tr}] \label{thm:pathequivalence}
	For every traffic class $tc \in TC$, 
	if the destination is reachable from the source router
	in the actual network, then, after removing 
	edges corresponding to failed links 
	from $ETG(tc)$, the shortest path in the ETG from the
	source router to destination router is equivalent
	to the path computed by the actual network.
\end{theorem}

Thus, the flow graph $FG(tc)$ will 
be a sub-graph of $ETG(tc)$ and every 
path from source to destination in $FG(tc)$ will be 
the shortest weighted path based on $ETG(tc)$ weights.

Since, there are multiple paths in the flow graph, the 
traffic will be split across the different paths based 
on the load-balancing scheme deployed in the network.
We define the flow 
function $F_{tc} : L_{tc} \rightarrow \mathbb{Q}$ as the 
amount of flow of $tc$ on a link in $FG(tc, FL)$. 
In ECMP, each router divides the total incoming 
flow equally amongst the outgoing links. For a 
node $r$, let us define the $prev(r)$ to be set 
of nodes which have an incoming edge into $r$ :
$prev(r) = \{r' | (r', r) \in L_{tc}\}$. Similarly,
$next(r)$ is the set of nodes connected by an outgoing edge 
from $r$: $prev(r) = \{r' | (r, r') \in L_{tc}\}$.
Thus, we can define ECMP behavior in terms of the flow 
function as follows:
\begin{equation*}
	\forall r. \forall r' \in next(r). F_{tc}((r, r')) = 
	\frac{\sum_{r'' \in prev(r)} F_{tc}((r'', r))}{|next(r)|}
\end{equation*}

We define the link utilization $Util_{FL}(l)$ 
of each link $l \in Links$ in the network
under the failure scenario as follows: 
$Util_{FL}(l) = \sum_{tc \in TC} F_{tc}(l)$. 
We represent the link capacity of link $l$ as $Cap(l)$.
We define a network load violation occurs when $\exists l 
\in Links$ such that the utilization of the link 
exceeds the link's capacity.

\begin{definition}[Verification]
Given a set of 
failure scenarios $\overline{FL}$, the verification problem is to find 
a failure scenario $FL \in 
\overline{FL}$ where a network load violation occurs
i.e., $\exists l \in Links. Util_{FL}(l) > Cap(l)$.
\end{definition}

\begin{definition}[Repair]
Given a set of 
failure scenarios $\overline{FL}$, we define the \qarc repair problem 
of finding new capacities $Cap'$ such that 
$\forall FL \in \overline{FL}. \forall l \in Links. 
Util_{FL}(l) \leq Cap'(l)$.
\end{definition}