%!TEX root = paper.tex

\section{Evaluation} \label{sec:evaluation}
\name is implemented in Java using the open-source
\arc~\cite{arccode} and Batfish~\cite{batfishcode} frameworks.
\name uses the Gurobi ILP solver~\cite{gurobi} for solving the
verification and repair constraints.
Our evaluation answers the following questions:

\noindent\textbf{Q1}: Do real datacenter and ISP networks suffer from
link overload under failures? (\secref{sec:realeval}) \\
\noindent\textbf{Q2}: How quickly can \qarc detect load violations or report the
absence of violations? (\secref{sec:verificationeval}) \\
\noindent\textbf{Q3}: Do our optimizations speed up verification? (\secref{sec:optimizationeval}) \\
\noindent\textbf{Q4}: How quickly can \name compute links to upgrade?
(\secref{sec:repaireval})

Experiments were conducted on a 5-node
40-core Intel-Xeon 2.40GHz CPU machine with 128GB of RAM.

\minisection{Networks} We study 112 datacenter networks
operated by Microsoft, and 86 ISP
networks obtained from the Topology Zoo~\cite{zoo} dataset.  The
network configurations use OSPF, BGP, static routing constructs, and
ECMP load-balancing
which are all modeled by \arc.
We refer to the datacenter networks as
DC$i$, and the ISP topologies by their names from Topology Zoo. We
also study two synthetic fat-tree~\cite{fattree} datacenter topologies:
Fat6 (45 routers) and Fat8 (80 routers).

\minisection{Link Capacities}
We vary the link capacities to be either 40Gbps or 100Gbps
picked randomly or dependent on topology structure (e.g., for fat-tree: core-aggregate links are 100Gbps
while edge-aggregate links are 40Gbps).

\minisection{Traffic Matrices (TM)} To verify the datacenter networks
on realistic input traffic characteristics, we sample the datacenter
packet traces from the Fbflow dataset~\cite{fbflow}. We use the
gravity~\cite{gravity} model to generate traffic matrices for our
datacenter and ISP networks.  The traffic matrices describe the
traffic between all edge-edge router pairs for the datacenter networks
or all endpoint pairs for the ISP networks.  We denote the maximum
link utilization of our network with 0 failures as $MLU_{0}$. We run
our experiments using the traffic matrices obtained from Fbflow or
gravity which have the property that $MLU_{0} \in [0.65, 0.8]$
\footnote{We extract matrices using gravity/fbflow and multiply
each field of the matrix with a constant factor to obtain the desired $MLU_{0}$};
% thus, there is no
% link overload when no link has failed. 
Under no failures, the max link utilization in the network is less than 80\%.
Our traffic matrices model scenarios when the network is not overloaded when all
links are active, but certain 1 or 2 link failures could potentially 
lead to some links getting overloaded.
Unless otherwise mentioned, we
bound the variation of an individual traffic class so that it cannot
increase more than 50\%, and we bound the total traffic variation
summed over all traffic classes to 10\% of the total network traffic.

\subsection{Verifying Real Networks} \label{sec:realeval}
We use \name to detect if the datacenter (DC) and ISP networks
experience link overload under $k=\{1, 2\}$ link failures.
We generate 20 random traffic matrices (Fbflow for datacenter
and gravity for ISP) and run \qarc's verification. We
report our findings in \Cref{tab:realworld}.

\name finds link overload events for $1$-link failures for 139 of the
200 networks. Moreover, 87 networks show link overload violations for
more than 10 out of 20 TMs, indicating that these networks are quite
susceptible to link overload events even under a single-link failure
scenario. \qarc likewise finds overload for over 90\% of the ISP
networks, compared to 50\% for the datacenter networks,
indicating that datacenter networks are less susceptible to link
overload due to higher path diversity than ISP networks.
%% , highlighting the importance of load
%% verification (and consequently repair) for network operators
Notice
that, the number of networks experiencing violations decreases for
2-link compared to 1-link failures.  This is because 2-link failures
disconnect more traffic classes; the number of active traffic classes
reduces, and thus, links' utilizations do not exceed capacities.


\minisection{Min Traffic Variation} \name can be used to find the
minimum total variation of traffic
(each traffic class cannot increase by more than 50\%) which
can cause overload under 1-link and 2-link failure scenarios.
Minimum variation
can be used to judge how the
network handles unexpected spikes in load
under failures. For this experiment, we use traffic matrices such that
$MLU_{0} = 0.7$ (high utilization) and find the average minimum
traffic variation.
For our ISP and datacenter networks in
\Cref{fig:delta}, we report the variation as a percentage of total
network volume.
We can observe that the ISP networks require lower
variations (2-12\%) to cause link overload, i.e., if the total traffic
increases by 12\%, the network is likely to be overloaded under
failures.  Meanwhile, datacenter networks require more traffic
variation for links to get overloaded (2-40\%), highlighting intrinsic
robustness pertaining to meeting traffic demands and higher
path diversity. Also,
$k=2$ requires lower variation than $k=1$.


% \begin{table}
%     \begin{minipage}{\columnwidth}
%         \centering
%         \caption{
%         Percentage overload violations for 1-link failures for datacenter networks with
%         Fbflow and Gravity traffic matrices. Order of links is shown in brackets for each network. \label{tab:trafficmatrix}}
%         \begin{tabular}{c c c c c c }
%         {\bf Matrix}& {\bf DC1 (<100)} & {\bf DC2 (<100)} & {\bf DC3 (<100)} & {\bf Fat6 (216)} & {\bf DC4 (>100)}  \\
%         \hline
%         Fbflow & 74\% & 72\% & 28\% & 100\% & 100\% \\
%         Gravity & 83\% & 96\% & 38\% & 100\% & 100\% \\
%         \end{tabular}
%     \end{minipage}
%   \end{table}

\minisection{Effect of Traffic Matrices} We also study the effect of
different types of traffic matrices causing link overloads.
We consider 5 datacenter networks and use \qarc to find link overload
for $k=1$ failure scenarios. We generate 100 TMs using both Fbflow and
gravity models such that $MLU_{0} = [0.65, 0.8]$ and bounded variation
is 10\%; in \Cref{tab:trafficmatrix} we report the percentage of
violations found in these networks for the different matrices. We
observe that Fbflow matrices lead to fewer load violations than
gravity matrices for some of the datacenter networks.
% \aditya{would be
%   good to explain. Is it because Fbflow is more, or less, uniform
%   somehow?}
This shows the impact of using \qarc to verify if
significant changes to traffic patterns can affect a network's load
properties under failures.

\textbf{Q1:} \textbf{\qarc finds network load violations under
  failures} in 70\% of real datacenter and ISP networks, and
  ISP networks are more susceptible to link overload than datacenter
  networks. To the best
of our knowledge, this is the first study of finding potential
violations for control planes.



\subsection{Verification Performance} \label{sec:verificationeval}
We now evaluate the performance of \name's algorithm for detecting
link overload for the datacenter and ISP networks using 20 different
traffic matrices with $MLU_{0} \in [0.65, 0.8]$ for 1 and 2-link
failure scenarios.  The datacenter networks have on the order of
10 to order of 100 links (exact numbers hidden for confidentiality)
and order of 100 traffic classes, and the ISP networks have 8-125
links and 10-2000 traffic classes. We use 5 nodes to run parallel
verification and report the average time to successfully find
violations (Yes) or verify no violations (No) for the networks.

\begin{figure}
    \vspace*{-4mm}
    \centering
	\subfloat{\includegraphics[width=0.5\columnwidth]{figures/dc_delta.eps}}
    \subfloat{\includegraphics[width=0.5\columnwidth]{figures/isp_delta.eps}}
    \caption{\label{fig:delta}
        Variation \% for different ISP and datacenter networks for 1 and 2
        link failure scenarios.
        }
\end{figure}

\begin{figure*}
    \centering
	\subfloat[k=1]{\includegraphics[width=0.5\columnwidth]{figures/dc_verification_times1.eps}}
    \subfloat[k=2]{\includegraphics[width=0.5\columnwidth]{figures/dc_verification_times2.eps}}
    \subfloat[k=1]{\includegraphics[width=0.5\columnwidth]{figures/isp_verification_times1.eps}}
    \subfloat[k=2]{\includegraphics[width=0.5\columnwidth]{figures/isp_verification_times2.eps}}
    \caption{\label{fig:verificationtime}
        Verification time (log scale) sorted by network links
        for different WAN and datacenter networks for 1 and 2
        link failure scenarios.}
\end{figure*}


\Cref{fig:verificationtime} shows the results. We observe that verification time
for $k=2$ is greater than $k=1$ due to the larger search space. We sort the
networks by number of network links and observe that network time in general
increases with size of the network. Also, the ISP topologies have higher
verification times compared to the datacenter networks (due to larger number of
traffic classes). For most networks, \name is able to verify if the network will
experience link overload (Yes) or not (No) in under an hour (6.7\% of the runs
timed out due to the large number of links or traffic classes). With our choice
of traffic matrices with $MLU_{0} \in [0.65,0.8]$, the times taken to
find a violation or proving the absence of violations follow
similar trends, indicating similar difficulty.

We also look at \name's performance with increasing number of link failures. We
consider 3 datacenter networks (DC2, DC4, Fat6) and one ISP network (Abilene)
and use 50 random gravity matrices with $MLU_{0} \in [0.65, 0.8]$ and 10\%
traffic variation, and verify for $k=[1,4]$ failure scenarios. We present the
median verification times in \Cref{tab:scalability}.
As $k$ increases, verification times
generally increase (Fat-6 and DC4 experience a drastic increase at $k=3$ and
$k=4$, respectively). For $k \leq 2$, \qarc terminates within an hour for
most instances (<1\% instances timed out). \qarc experiences more timeouts for
$k=3$ (7\%) and $k=4$ (25\%) due to the exponential complexity of verification.

We now consider how \qarc performs compared to naive enumeration. We fix the
input traffic matrix, find the time taken to verify one failure and extrapolate
by the number of failures. We consider a 5-node parallelism, so enumeration is
spread across 5 machines equally. For a network with >100 links, the naive enumeration time versus
median QARC time for different failure scenarios are: (a) $k=1$: 12.8s vs 9.6s,
(b) $k=2$: 1,017s vs 22s, (c) $k=3$: 14.8hrs vs 81s, and (d) $k=4$: 584hrs vs
0.4hrs . While for 1-link failures, the naive enumeration is comparable, QARC
has significant speedup for $k \geq 2$. Moreover, verification with non-zero
traffic matrix variation cannot be performed by enumeration, as there are
infinitely many rational-value traffic matrices to consider.

\textbf{Q2:} \textbf{\name can verify network overload for medium-sized
datacenter and ISP networks} for different failure scenarios in under an
hour.

\begin{table}
    \centering
    \caption{
    Verification times (s) for $k=[1,4]$ link failures. \label{tab:scalability}}
    \vspace*{-2mm}
    \small
    \begin{tabular}{m{6.5em} C{3.5em} C{3.5em} C{3.5em} C{3.5em}}
    {\bf Network(Links)} & {\bf k=1} & {\bf k=2} & {\bf k=3} & {\bf k=4} \\
    \hline
    Abilene(28) & 0.7 & 7.9 & 9.1 & 10.7 \\
    DC2 (<100) & 1.4 & 17.5 & 17.1 & 20.1 \\
    Fat6 (216) & 3.3 & 2.8 & 1,550.6 & 1,357.7 \\
    DC4 (<1000) & 9.6 & 22.1 & 81.2 & 1,496.0 \\
\end{tabular}
\end{table}

% \begin{figure}
%     \begin{minipage}{0.45\linewidth}
%         \includegraphics[scale=0.35, left]{figures/probability_fattree-6.eps}
%         \vspace*{-7.5mm}
%         \caption{
%         \footnotesize
% 		Time for verifying Fat6 for varying failure thresholds}
%         \label{fig:failureprob}
% \end{minipage}%
% \hfill
% \begin{minipage}{0.45\linewidth}
%         \includegraphics[scale=0.355, right]{figures/repair.eps}
%         \vspace*{-8mm}
%         \caption{\footnotesize
%         Time for repair for different networks and $k=2$ failures.}
%         \label{fig:repairtimes}
% 	\end{minipage}
% \end{figure}
% "68\%", "50\%", "42\%", "36\%", "51\%"


\subsection{\name Optimizations} \label{sec:optimizationeval}

We now evaluate how the optimizations presented in
\secref{sec:optimizations} improve \name's performance.
We consider 3 datacenter (DC2, DC4, Fat6) and one ISP
network (Abilene) and use 50 random gravity matrices with $MLU_{0} \in
[0.65, 0.8]$ and no traffic variation, and verify for $k=1$ failures.

\minisection{ETG Minimization}
\Cref{tab:optimizations} reports the speedup obtained by ETG
minimization (speedup = time without minimization/ time with
minimization) and edge reductions for the networks.  For the bigger
networks, the speedup in verification is significantly larger, aiding
in making \name's verification more tractable.  When verifying the
Fat6 and DC4 network with $k=1$, we are able to reduce 86\% and
95\% of the ETG edges respectively, achieving significant
speedups of 69$\times$ and 18$\times$ for Fat6 and DC4.
The ETG minimization phase is quick,
taking under 5 seconds for all the networks.

\minisection{Parallelization}
We now consider the speedup achieved by parallelizing the verification (with
optimal partitions) over verification run on a single node. We run verification
on 5 nodes and compare the performance with 1-node verification. In the parallel
scenario, we terminate verification if any one node finds a link load violation,
otherwise we wait till all nodes report no violations (with a timeout of 1
hour). \Cref{tab:optimizations} shows the verification speedup achieved due to
parallelization. For the bigger networks (Fat6 and DC4), we achieve average
speedup of over 5$\times$ and 10$\times$ over the single node case; thus, \name
parallelization further improves the tractability of verification.

\begin{table}
    \centering
    \caption{
    Speedups due to optimizations for $k=1$ failures.\label{tab:optimizations}}
    \vspace*{-2mm}
    \small
    \begin{tabular}{m{8em} C{3.5em} C{3.5em} C{3.5em} C{3.5em}}
    {\bf Optimizations} & {\bf Abil- ene(28)} &
    {\bf DC2  (<100)} & {\bf Fat6  (216)}
    & {\bf DC4 (<1000)} \\
    \hline
    Edges Removed & 54\% & 67\% & 86\% & 95\% \\
    \hline
    Minimize Speedup  & 2.3 & 4.5 & 69 & 18 \\
    \hline
    Parallel Speedup & 1.3 & 1.3 & 5.0 & 9.5  \\
    \hline
    Partition Classes & 68\% & 50\% & 42\% & 36\%\\
    \hline
    Partition Speedup & 1.00 & 1.19 & 1.38 & 1.52 \\
\end{tabular}
\end{table}

\minisection{Partitioning} Finally, we evaluate the speedup due to the optimal
partitioning scheme which minimizes the number of traffic classes in each of the
5 partitions. We compare the performance of  
5-node optimal partition verification to 
to a 5-node random partitioning scheme which randomly divides the
links in equal sized partitions. For this experiment, we pre-compute the optimal
partitioning (not included in verification time) and compare verification using
the optimal and random naive partitions on 5 nodes.  \Cref{tab:optimizations}
shows the verification speedup achieved by the optimal versus naive partitions.
The speedup achieved for DC2, DC4 and Fat6 is about 20-50\%.  We report the
percentage of traffic classes in the optimal partitions (Partition Classes) for
each network in \Cref{tab:optimizations}; we are able to reduce more than half
of the traffic classes in each partition for DC2, DC4 and Fat6.

\textbf{Q3:} \textbf{\name optimizations speed up verification}
significantly for different networks and failure
scenarios, with the most benefits coming from
ETG minimization and parallel verification for the
bigger Fat6 and DC4 networks.

\begin{figure}
    \centering
    \includegraphics[width=0.55\columnwidth]{figures/repair.eps}
 \vspace*{-5mm}
    \caption{\label{fig:repairtimes}
        Capacity upgrade computation time}
\end{figure}

\subsection{Upgrade Performance} \label{sec:repaireval}
We use \name to generate the minimal link capacity additions required to prevent
link overload under 1-link and 2-link failures. For this experiment, we consider
traffic matrices with $MLU_{0} \in [0.8, 0.95]$ and zero variation. Similar to
verification, we parallelize the phase of computing the new link capacities to 5
nodes. We report the median upgrade time for 5 different networks in
\Cref{fig:repairtimes}.
\qarc is able to compute new link capacities
for the networks in under 5 minutes using the 5 nodes.
In our runs, \qarc requires changing capacities of order of 10 links
(Fat6 repair has the highest number
of links whose capacity has to be increased).
Finding the repair for all 2-link failure scenarios
takes more time than 1-link scenarios
due to the larger search space.

\textbf{Q3:} \textbf{\name can upgrade link capacities to
prevent network overload under failures}
for different networks in under 400 seconds.
