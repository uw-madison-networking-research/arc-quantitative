%!TEX root = paper.tex
\section{QARC (ARC with Quantities)} \label{sec:qarc}

\vspace{1mm}
\minisection{ARC}
\arc abstracts the set of router 
configurations using a set of weighted directed graphs
called \emph{extended topology
  graphs} (ETGs). For each traffic class, ARC constructs one ETG to
model the behavior of the network routing protocols and interactions
among the routers for the traffic class (shown in 
\Cref{fig:controlplane}(b)). In the ETG, each vertex
corresponds to a routing process; there is one incoming (I) and one
outgoing (O) vertex for each process. Directed edges represent
possible paths enabled by exchange of routing
advertisements between connected processes. 
The weights of the edges of the ETG satisfy the 
\emph{path-equivalence property}: under any failure scenario, the actual 
network path(s) are the shortest weighted path(s) in the ETG.
Thus, the ARC abstraction can be used to compute the 
forwarding state of the network under any failure 
scenario, abstracting away the need to model 
route advertisements and path computations at each router.
Note that the path-equivalence property of \arc can model ECMP-style 
load-balancing.


\arc was designed to use
graph algorithms on the extended
topology graphs (ETGs)
to verify qualitative
properties under {\em any} arbitrary failure scenario---e.g., 
a traffic class is
connected under any $k$-link failure if the min-cut for the traffic class's ETG is ${\ge}k$. 


\minisection{QARC}
Our model, QARC, extends ARC 
and it uses a mixed-integer linear program (MIP) 
encoding to add symbolic traffic quantities to
the \arc ETGs to verify load properties
under different failure scenarios. 
%Before getting into the details, %%  of 
%%% the \qarc encoding (\secref{sec:qarc-ilp}), 
%we first describe how we expect \name to 
%be used.

\minisection{Verification} 
Provisioning bandwidth is expensive, especially in wide area networks
where adding new links is not an easy endeavor. Operators therefore want to keep
link utilization high and, at the same time, tolerate link failures. \name helps
operators predict if the network will encounter load violations under failures. 
Operators have
frameworks to periodically update routing
configurations~\cite{robotron,propane,zeppelin} and measure input
traffic characteristics~\cite{bwe,swan, vl2}.
We envision \qarc to be
used for verification whenever traffic characteristics change
significantly, or the control plane is reconfigured. Taking as input
the new control plane and traffic matrix (between endpoints) with bounds
on traffic variation, our
verification detects if 
there exists a failure scenario leading to
utilization exceeding capacity on any
link.


Consider the network control plane 
in \Cref{fig:controlplane}(a) and the corresponding \arc ETGs 
in \Cref{fig:controlplane}(b). In this scenario, the operator sees a change in the input traffic: 
the current traffic for classes $R \rightarrow T$ and $S \rightarrow T$ are 80 Gbps 
and 30 Gbps, respectively. All links in the network have a capacity of 100 Gbps.
When no links have failed, the traffic from $S \rightarrow T$
flows through the path $B \rightarrow A$, while the traffic from $R \rightarrow T$ 
is load-balanced by ECMP at $C$; 40Gbps traffic is sent through the two paths:
$D \rightarrow C \rightarrow A$ and $D \rightarrow C \rightarrow B \rightarrow A$ 
(these are the shortest network paths in the ETGs). As we can observe, all links'
utilizations are below capacity ($D\rightarrow C: 80$, $C\rightarrow B: 40$, 
$C\rightarrow A: 40$, $B\rightarrow A: 40+30=70$). 

Suppose, the operator wants to inspect 
if the network, for the given traffic matrix,
can experience some link becoming overloaded under a single link failure. 
Using our tool \name, the operator 
can discover that when $C\rightarrow A$ fails, 
the traffic on $B \rightarrow A$ will be 
110 Gbps, exceeding the link's capacity.

% \kausik{talk about using models to stress-test network and check for
%   violations} \aditya{here is one possibility:} Instead of measuring
% and verifying specific traffic matrix, operators can also use \name to
% ``stress-test'' a network, by verifying for a class of traffic
% matrices that all share a property. For example, suppose the operator
% measures that the current traffic matrix M fits a ``gravity
% model''~\cite{}, where the average demand across traffic classes is
% $\rho$. The operator can randomly generate an arbitrary large number
% of traffic matrices $\{M_1,M_2\ldots\}$, where traffic in each $M_i$
% adheres to the gravity model, and the average demand in all $M_i$s is
% $\rho$, and run verification against each $M_i$ independently.
    
Going further, the operator can discover other single link failures by
asking our verification tool to find 1-link failures {\em other than}
$C \rightarrow A$. 
Likewise, the operator can discover sets of $k$-link
failures that cause links to overload. 
%% We discuss verification in
%% \secref{sec:verification}.

\minisection{Upgrade}
When verification detects a possible load violation, the operator can invoke our
upgrade framework. Taking as input the network configurations and the
traffic matrix, \qarc computes a minimal set of links whose capacities
need to be upgraded to avoid overload  
under failures. For the network in \Cref{fig:controlplane}, \qarc
suggests to increase the $B\rightarrow A$ 
 capacity by 10Gbps to ensure no load violations occur under any 1-link
 failures.

%% We discuss \name's repair in detail in \secref{sec:repair}.

\subsection{Problem Definition}
Let $TC$ be a set of traffic classes, 
$N = (Routers, Links)$ be the network topology
and $FL \subseteq Links$ be the failure scenario---i.e., 
the set of links that failed.
For each traffic class $tc \in TC$, the routing 
processes compute paths from 
source to destination; 
the paths form a directed acyclic graph (DAG),
which we call the \textit{flow graph}. 
We represent $tc$'s flow graph as 
$FG(tc,FL) = (V_{tc}, L_{tc})$. Traffic 
will not flow on failed links, thus 
$L_{tc} \cap FL = \emptyset$.
\arc constructs a weighted directed 
graph $ETG(tc)$ for each traffic 
class $tc$ with the following property:

\begin{theorem} [Path-equivalence~\cite{arc-tr}] \label{thm:pathequivalence}
	For every traffic class $tc \in TC$, 
	if the destination is reachable from the source router
	in the actual network, then, after removing 
	edges corresponding to failed links 
	from $ETG(tc)$, the shortest path in the ETG from the
	source router to destination router is equivalent
	to the path computed by the actual network.
\end{theorem}

Thus, the flow graph $FG(tc, FL)$ will 
be a sub-graph of $ETG(tc)$ and every 
path from source to destination in $FG(tc, FL)$ will be 
the shortest weighted path based on $ETG(tc)$ weights.

Since there are multiple paths in the flow graph, the 
traffic is split across different paths based 
on the load-balancing scheme deployed in the network.
We define the flow 
function $F(l, tc, FL)$ as the 
amount of flow of $tc$ on a link $l$ in $FG(tc, FL)$. 
In ECMP, each router divides the total incoming 
flow equally among the outgoing links leading to shortest paths. For a 
node $r$, we define the set 
of nodes which have an incoming edge into $r$ as
$prev(r) = \{r' | (r', r) \in L_{tc}\}$,
and the set of nodes connected by an outgoing edge 
from $r$ as $next(r) = \{r' | (r, r') \in L_{tc}\}$.
Thus, we can define ECMP behavior in terms of the flow 
function as:
\begin{equation*}
	\forall r. \forall r' \in next(r).\ F((r, r'),tc,FL) = 
	\frac{\sum_{r'' \in prev(r)} F((r'', r),tc,FL)}{|next(r)|}
\end{equation*}

For each link $l \in Links$ and failure scenario
$FL$,
we define the utilization $l$
under the failure scenario $FL$
as
$Util(l,FL) = \sum_{tc \in TC} F(l,tc,FL)$. 
We represent the link capacity of link $l$ as $Cap(l)$.
A network load violation occurs when 
a link's utilization 
exceeds its capacity.

\begin{definition}[Verification] Given a set of failure scenarios
$\overline{FL}$, the \textit{verification problem} is to find a failure scenario $FL \in
\overline{FL}$ that leads to a network load violation---i.e., 
$\exists FL \in \overline{FL}.\ \exists l \in Links.\ 
Util(l,FL) \geq Cap(l)$.
\end{definition}

% \begin{definition}[Upgrade] Given a set of failure scenarios $\overline{FL}$, we
% define the \qarc upgrade problem of finding new capacities $Cap'$ such that
% $\forall FL \in \overline{FL}. \forall l \in Links. Util_{FL}(l) \leq Cap'(l)$.
% \end{definition}