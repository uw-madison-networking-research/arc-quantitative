from __future__ import division
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
import numpy as np
import statistics

def read_verification() :
    total = 0
    timeout = 0
    timeout_stats = dict()
    verification_data = open("scalability-data.txt")
    netlist = ["Abilene", "fattree-6", "CityCenter73", "Alpharetta21"]
    networks = dict()
    for net in netlist:
        networks[net] = dict()
        timeout_stats[net] = dict()
        for k in range(1, 5):
            networks[net][k] = []
            timeout_stats[net][k] = [0,0]

    for line in verification_data.readlines() :
        fields = line.rstrip().split("\t")
        if len(fields) < 7 : 
            continue
        net = fields[0]
        if net not in netlist:
            continue
        fail = int(fields[3])
        time = int(fields[6])
        status = int(fields[7])
        total += 1
        timeout_stats[net][fail][1] += 1
        if time == 0 or status > 3:
            timeout += 1
            timeout_stats[net][fail][0] += 1
        
        
        networks[net][fail].append(time)
    
    
    for net in netlist:
        print(net)
        for k in range(1, 5):
            print(np.average(networks[net][k])/1000)

    print(total, timeout, float(timeout)/total)
    
    for k in range(1, 5):
        tot = 0
        tim = 0
        for net in netlist:
            tim += timeout_stats[net][k][0]
            tot += timeout_stats[net][k][1]
        print(k, tim*100/tot)

read_verification()


