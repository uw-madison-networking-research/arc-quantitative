%!TEX root = paper.tex
\section{Optimizations} \label{sec:optimizations}
We now describe two techniques that take advantage of 
the properties of \name abstraction and constraints
to speed up verification performance. 
First, we 
use \arc's shortest-distance path property to efficiently minimize the traffic
class ETGs (\secref{sec:etg-minimization}). 
Second, we show how to use minimized ETGs to partition the set of
network links and perform verification in a parallel fashion (\secref{sec:etg-parallel}).
%% \kausik{We also leverage the
  %% minimized ETGs for partitionizing such that the number of traffic
  %% classes in each partition is minimized to further speedup
  %% verification.}

\subsection{ETG Minimization}
\label{sec:etg-minimization}
Due to Theorem~\ref{thm:pathequivalence}, in an ETG, the traffic
will not traverse a path with a higher cost 
if one with a lower 
cost exists. 
Therefore, 
for each traffic class, we can prune
the ETG and remove 
edges and nodes that will not be traversed 
under the targeted number of failures. 
This pruning strategy reduces the size of the 
generated constraints and can result in faster
verification.

To illustrate this property, 
consider the network in \Cref{fig:repairex}. For 
any 1-link failure scenario, the traffic from S to T will never 
traverse router $C$ on links $B \rightarrow C$ and 
$C\rightarrow A$ (because 2 shorter paths exist in the network). 
Thus, in the ETG, we can prune all nodes and edges corresponding 
to router $C$, forming a minimized ETG.


Identifying the redundant nodes and edges for general $k$ link
failures can be challenging---the
naive approach of enumerating all $k$-link failures will be expensive.  
Instead, we modify
\name's encoding to perform this task.  The core insight of our
approach is to find the maximum shortest distance between the source
and the destination under any $k$-link failure scenario---all nodes
and edges that are farther than such a distance will never be traversed
and can be pruned from the ETG.  
The same
minimization cannot be performed efficiently when using Minesweeper's
symbolic SMT-based abstraction: one would need to check each node
and edge to detect if the node/edge is reachable under $k$ failures. 
In \arc, since the edge weights are not symbolic, we can eliminate a 
path if "shorter paths" exist.

\noindent\minisection{Minimization Constraints}
Unlike verification which deals with all traffic classes, 
minimization only involves constraints for a single 
traffic class. The minimization of multiple 
ETGs 
can be done in 
parallel since one ETG's 
routing does not depend on
other ETGs. 
For an ETG of traffic class $tc$, 
we use Constraints (\ref{eq:distance}) to 
specify upper bounds on distances
and Constraints (\ref{eq:failbound})
to bound the number of failures.
The maximum shortest distance between the source and 
destination of the ETG can be found using 
the objective:
\begin{equation} \label{eq:distobj}
\texttt{maximize} ~~Dist(Src(tc), tc)
\end{equation}
Let us denote the objective value provided by the ILP solver as $MaxDist$. 
Given an edge $e : n_1 \rightarrow n_2$, if the shortest path from source
to destination using the edge $n_1 \rightarrow n_2$ is greater than $MaxDist$, 
the edge will never be traversed under any $k$ link failures and can be pruned 
from the ETG. We check for such edges using the condition:
\begin{equation} \label{eq:mincondition}
SP(Src(tc), n_1) + W(e, tc) + SP(n_2, Dst(tc)) > MaxDist
\end{equation}
where $SP(n, n')$ denotes the length of the shortest 
path between $n$ and $n'$ in the graph
(computed using Dijkstra's shortest path algorithm).
After pruning  the edges that are never traversed, 
we prune  the nodes that do not have 
incoming or outgoing edges to obtain a minimized ETG 
$ETG_{min}(tc)$. ETG minimization is sound and complete,
i.e., it will remove edges where traffic will never flow
under $k$ failures and will not remove edges where traffic could flow
under some $k$ failure scenario.

% \begin{theorem}[Soundness] \label{thm:optsoundness}
%   If edge $e$ in $ETG(tc)$ has been removed in the minimized
%   $ETG_{min}(tc)$, then for all failure scenarios $FL 
%   \in \overline{FL}$, $e$ will not in be the flow graph 
%   $FG(tc, FL)$.
% \end{theorem}

% \begin{theorem} [Completeness] \label{thm:optcompleteness}
%   If edge $e$ in $ETG(tc)$ has not been removed in the minimized
%   $ETG_{min}(tc)$, then $\exists FL 
%   \in \overline{FL}$ such that $e$ will be 
%   in the flow graph $FG(tc, FL)$.
% \end{theorem}

\subsection{Parallel Verification} \label{sec:parallel}
\label{sec:etg-parallel}
A major bottleneck of QARC verification is solving 
a Mixed Integer
Linear Program (MIP)
that has size linear in the number of network links and traffic classes. 
Consider the network-wide constraint (\ref{eq:netcong})
used to find load violations where at least one 
link's utilization exceeds its capacity:
\begin{equation*} 
    \sum_{e \in Links} Load(e) \geq 1
\end{equation*}
If we restrict the above constraint to only consider a subset of links,
\name 
will find scenarios where one of these links' utilization 
exceeds its capacity. Thus, if we partition the set of links 
into $N$ partitions, we can run $N$ instances of 
verification on separate machines. 
By splitting the verification $N$-way, each 
instance will effectively have a smaller search space
of links to find load violations, thus, speeding up
verification. 

There are numerous ways to partition the set of links.  Instead of
simply splitting the set of links into $N$ equal-sized partitions, we
leverage ETG minimization to generate a partitioning scheme that
also minimizes the number of the traffic classes that contribute to
load on links in each partition.  The insight of our partitioning
scheme is as follows: a minimized ETG prunes all links where traffic
for the particular class does not flow under any $k$-link failure
scenario. To detect if link $e$ can be overloaded, we can
prune the constraints for traffic classes that do not contain $e$ in
their minimized ETGs. Thus, for each partition, we only need to
consider a subset of traffic classes, reducing
verification time further. 

% \input{parallelopt}
We formulate our partitioning scheme as an Integer Linear Program (ILP).
Our partitioning scheme is tied inherently to the ETG minimization, 
which we can perform efficiently due to the ARC abstraction.
Without ETG minimization, all traffic classes would contain 
all links in their ETG (barring 
ACLs and filters), thus, we would not 
be able to further 
reduce the constraints in each partition. 


