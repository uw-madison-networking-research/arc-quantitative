%!TEX root = paper.tex
\section{Motivation}
We provide an overview of distributed network control planes, control
plane abstractions and their shortcomings for reasoning about network
load violations.

\subsection{Network Control Planes}
A distributed network has two components: the control and the data plane. The
control plane exchanges information to routers and decides the best path for
every packet, these best paths are installed in the data plane. In a distributed
control plane, each router runs one or more routing processes, each process
implementing a path selection algorithm, defined by a standard routing
protocol---e.g., OSPF~\cite{ospf}, BGP~\cite{bgp}, and RIP~\cite{rip}. A routing
process will receive a set of routing advertisements from different routing
processes: could be from the same router or neighbouring routers, and could be
of the same or different routing protocol. Each routing process's path
computations are based on different protocol-specific configuration parameters
and advertisements. Each routing process decides the best path(s) and could
advertise the best path(s) to other routing processes. The steady-state outcome
of the control plane's computation is a set of forwarding rules computed at each
router, which encode the next-hop for different traffic classes, i.e.,
source-destination subnets. Operators typically decide which protocols and
configuration to use based on network design and operational policies they wish
to enforce~\cite{maltz04} like reachability, waypointing etc. 
% The
% former correspond to hierarchy (e.g., how many logical domains, or
% departments, in the network) and the latter to qualitative path
% properties such as reachability between sets of subnets, waypointing
% for certain traffic, isolation for sensitive flows, etc.
\begin{figure}
	\vspace*{-4mm}
	\centering
	\subfloat[]{\includegraphics[width=0.35\columnwidth]{figures/ControlPlane.pdf}}
	\subfloat[]{\includegraphics[width=0.65\columnwidth]{figures/arc-etgs.pdf}}
	\caption{
	(a) Example  control plane with OSPF and BGP.  
	The boxes denote the routing processes, the 
	numbers on links are OSPF  weights, and the redistribution
	cost of OSPF to BGP is 1. There is an ACL installed at router B for
	traffic class $S-T$. (b) \arc ETGs for (a).}
	\label{fig:controlplane}
\end{figure}


\Cref{fig:controlplane}(a) shows a toy distributed control plane
spanning four routers. Two routers run OSPF, one runs BGP, and one
runs both and is configured to redistribute routes from OSPF to BGP.
The OSPF edge weights are configured to support load balancing for
$R-T$ traffic, while an Access Control List (ACL) is configured at router
$B$ to block $S-T$ traffic for a security policy. 
Distributed control planes are programmed at a low-level by
configuring routers using vendor-specific languages (e.g., Cisco
IOS~\cite{ciscoios}), either manually or using automated
tools~\cite{robotron, propane, synet, zeppelin}.  
% The configuration
% files encode which protocol(s) to use, what destination addresses to
% advertise, and how to choose one path between multiple possible
% paths. Configurations can also specify static routes (which have
% highest priority over OSPF/BGP routes) and access-control lists (ACL),
% which selectively allow/block certain traffic classes.

Another mechanism commonly used in distributed control planes is
load-balancing traffic between endpoints. ECMP (Equal Cost Multi-Path
routing)~\cite{ecmp}, or its weighted variant (WCMP~\cite{wcmp}), 
are used to distribute traffic across multiple paths between
endpoints in the
network. 
In an ECMP  router, traffic is split \emph{equally} among the
 ``best paths'' based on routing metrics.
%ECMP/WCMP affords traffic
%engineering capabilities to the network without centralized
%control mechanisms. 

\subsection{Control Plane Abstractions}

Analyzing whether a network satisfies certain properties over paths,
qualitative or quantitative, requires knowing the network forwarding
state. Understandably, inferring this based on low-level router
configurations is difficult as the forwarding state is 
the result of complex distributed computations. 
Thus, 
many frameworks~\cite{batfish, arc, tiramisu, minesweeper, bagpipe, era} have
been developed to model the distributed network control logic, so
that different inferences can be made about network forwarding---under
failures---without actually deploying the network configurations.
These models are used widely in verifying qualitative properties,
e.g., middlebox traversals.

However, verification of network overload, a quantitative
property, is challenging in aspects that the state-of-art models 
cannot handle. Firstly, verifying this property requires 
a model that can reason about
how much traffic a traffic class sends on a path (or
multiple paths for ECMP/WCMP).
% Qualitative verification does not care about how much
%traffic is sent on the path.
%%  but figuring out the load of a link requires
%% modelling of traffic quantities on links
Moreover, the model needs to reason
about the collective load imposed by {\em all} traffic classes in the
network.  Contrarily, verifying qualitative
properties only requires reasoning about small subsets of traffic classes.
% For instance,
% some of the common qualitative properties operators care about are
% reachability (certain endpoints are reachable from each other),
% waypointing (traffic between certain endpoints passes through
% waypoints, e.g., firewalls, intrusion detection systems (IDS) etc.),
% access control (traffic between certain endpoints is blocked), and 
% isolation (traffic classes don't affect one another).
% Thus, the model used for
% verifying/repairing network overload needs to model traffic quantities 
% and must be succinct and scalable
% in order to deal with a large number of traffic classes in conjunction.

Secondly, operators want to understand if the network can handle load
under different failure scenarios.  While in the worst-case scenario where
all devices fail together the network will be
overloaded, the probability of such
scenario occurring is very small.  Thus, the operator may constrain
the set of requisite failure scenarios---e.g., only 1 and 2-link
failures. The number of failure scenarios grows exponentially with
number of failed links. Thus,
the approach
adopted by some control plane models, e.g., Batfish~\cite{batfish}, of
enumerating all failure scenarios can be
\emph{prohibitively} expensive. Therefore, we require our network model to
symbolically encode network routing state even under failures.



\begin{figure}
	\vspace*{-2mm}
	\centering
	\subfloat[N1]{\includegraphics[width=0.45\columnwidth]{figures/minesweeper_Plano9.eps}}
    \subfloat[N2]{\includegraphics[width=0.475\columnwidth]{figures/minesweeper_Alpharetta21.eps}}	
	\caption{\label{fig:minesweepertimes}
	Network load verification times for \arc and Minesweeper for networks N1 and N2 and 
	varying number of traffic classes.}
\end{figure}


\subsection{\arc vs. Minesweeper}
Two networks models are amenable for verifying network overload
under failures: ARC~\cite{arc} and Minesweeper~\cite{minesweeper}.
Our work builds on ARC and in this section we motivate this choice.

The \arc network model uses the fact that most routing protocols use a
\emph{cost-based path selection algorithm}. E.g., OSPF uses Dijkstra's algorithm
to compute minimum cost paths from a source to all destinations, where each link
has a cost; and BGP selects paths based on numeric metrics, most importantly,
the AS path length. \arc abstracts the different routing protocols, static
routes, and ACLs based on the above principle, and models the network's
collective forwarding behavior under failures using a set of weighted directed
graphs (called ETGs) satisfying the \emph{path-equivalence} property: under \emph{any
arbitrary failure scenario}, the path taken by the traffic will correspond to
the \emph{shortest weighted path} in the graph. 
~\Cref{fig:controlplane}(b) illustrates the ETGs for the two traffic
classes (R-T and S-T) for the network shown in ~\Cref{fig:controlplane}. 
\arc cannot model every set of router 
configurations as certain routing constructs like 
BGP local preferences do not adhere to the cost-based 
path selection principle.
%% \arc is able to achieve this property for
%% a vast majority of configurations due to the fact that most routing
%% protocols in use today employ a \emph{cost-based path selection
%%   algorithm}.


Minesweeper~\cite{minesweeper} 
encodes the converged routing state of the control plan using SMT (Satisfiability Modulo
Theories) formulas~\cite{z3} and 
supports iBGP and BGP local preferences, which \arc
cannot handle.  Minesweeper %% achieves this coverage
%% using complex SMT constructs to model diverse routing technologies and
%% features and
is primarily used to verify qualitative properties dealing with a
single or small subset of traffic classes under 1-link or no failure.

Let us consider \arc v/s Minesweeper for detecting link overload under
no failures.  Using \arc, we can compute the exact network paths taken
by the traffic classes using the polynomial-time Dijkstra's algorithm
and thus compute individual link utilizations.  On the other hand, for
Minesweeper, due to the symbolic nature of the network abstraction, we
need to use the SMT solver to find the network paths and compute
utilizations. 
Handling
network failures further slows down verification.  We
compare verification performance by using the
Minesweeper and \arc abstractions for $k=1$ link failure scenarios in
\Cref{fig:minesweepertimes}.  We can observe that \arc is
significantly faster than Minesweeper, achieving $5\times$ speedup for
N1 and $20-800\times$ for the larger N2 network.

Moreover, the \arc abstraction offers certain key benefits for network
transformations~\cite{symmetry} which can help reduce the search space
efficiently and enables even faster quantitative verification than Minesweeper
(\secref{sec:optimizations}). For example, in \arc, we know a certain path will
not be traversed if a shorter path exists in the network (by simply comparing
the path cost). This property can be used to reduce the search space---both by
limiting which links to consider as candidates for overload, and by ignoring the
contributions of some source-destination pairs' traffic volumes---in scenarios
when a small number of links can fail together. 
In Minesweeper, the paths are
symbolic (represented by SMT constraints), and thus, we cannot a priori tell
which path would be preferred.

Since \arc is a more natural fit for quantitative analysis,
\arc's natural attributes support effective speedup of verification,
and \arc has near-universal control plane coverage, we adopt it as the
basis for our framework. 
In \S~\ref{sec:qarc}, we extend \arc to model 
(1) paths under failures without enumeration, 
(2) distribution of load on equal cost paths (due to ECMP or WCMP), and 
(3) variation of traffic volumes.

% Since, we bound the number of failure scenarios, the network region which a 
% certain traffic class can potentially traverse under those scenarios would be 
% smaller than the entire network. In \arc, the network paths can be computed 
% using the graph weights to efficiently reduce the search space, Minesweeper's 
% symbolic network paths 

% In \arc, the network paths are the shortest 
% distance paths; we leverage this property to efficiently reduce the search space 
% for each traffic class (\secref{sec:optimizations}). Empirically, we find 
% that these optimizations can provide $x-y$ factors of speedup due to the 
% search space reduction.
% Thus, we build our 
% abstraction on top on \arc, 
% sacrificing the generality afforded by Minesweeper 
% for tractability. \kausik{ETG minimization is 
% hard to explain in short.}

