%!TEX root = paper.tex

\begin{table}
	\small
		\begin{minipage}{\columnwidth}
			\caption{\name variables \label{tab:qarcvariables}}
			\vspace*{-2mm}
			\centering
			\begin{tabular}{m{4em}  m{17em}  P{2em}} 
			{\bf Name}& {\bf Description} & {\bf Range}  \\ 
				\hline
				$Flow(e, tc)$ & Fraction of traffic for class $tc$ flowing
				on edge $e$ & [0,1] \\
				\hline 
				$\Delta(tc)$ & Fraction of traffic variation for class $tc$ & [0,1] \\
				\hline
				$\Delta(e, tc)$ & Fraction of traffic variation for class $tc$ flowing
				on edge $e$ & [0,1] \\
				\hline
				$Dist(n, tc)$ & Shortest path distance of node $n$ to destination 
				in ETG &  $\mathbb{Q}$ \\
				\hline
				$Fail(e)$ & Link $e$ ($\forall tc$) failure status (1 $\equiv$ failed) & $\{0, 1\}$ \\
				\hline
				$Load(e)$ & Link $e$ load status (1 $\equiv$ overloaded) & $\{0, 1\}$
			\end{tabular}
		\end{minipage}
%		\vspace*{-6mm}
\end{table}

\section{\name Encoding} \label{sec:qarc-ilp}
We now present the mixed-integer linear program (MIP) encoding 
the semantics of
\name. Our encoding makes new technical contributions. First, it
extends \arc shortest-path-forwarding ETGs with flow quantities while accounting
for failures and 
permitting bounded variation of traffic characteristics. 
This is crucial to determining
link overload. 
Second, it \emph{models flow being split
among multiple shortest paths} even under failures, i.e., QARC models ECMP, which is a key
construct used in most networks.
Third, it does not require disjunctions and 
only uses 
integer variables to represent failures and link overload,
therefore enabling fast verification.
 
Table~\ref{tab:qarcvariables} and Table~\ref{tab:qarcconstants} describe
\name's variables and constants, respectively. For each section, the
sentences within boxes state what property the
presented constraints encode.


\setlength{\tabcolsep}{4pt}
\begin{table}
	\small
	%	
		\begin{minipage}{\columnwidth}
			\caption{\name constants \label{tab:qarcconstants}}
			\vspace*{-2mm}
			\centering
			\begin{tabular}{m{6em}  m{16em}} 
			{\bf Name}& { \bf Description}  \\ 
				\hline 
				$TC$ & Set of all traffic classes \\
				$\overline{FL}$ & Set of all failure scenarios \\
				%\hline
				$T(tc)$ & Traffic sent by class $tc$ ($T(tc) \in \mathbb{Q}$)  \\
				%\hline
				$W(e,tc)$ & Weight of edge $e$ in $tc$ ETG \\

				$Src(tc)$ & Source node of traffic class $tc$ \\
				%\hline
				$Dst(tc)$ & Destination node of traffic class $tc$ \\
				%\hline
				$In(n)$ & Set of in-edges of node $n$ in ETG \\
				%\hline
				$Out(n)$ & Set of out-edges of node $n$ in ETG \\
				%\hline
				$OEdges(tc)$ & Set of all outgoing edges in $tc$ ETG  \\
				%\hline
				$Links$ & Set of all physical links in the network \\
				%\hline
				$Cap(e)$ & Capacity of link $e$ ($Cap(e) \in \mathbb{Q}$) \\
				%\hline
				$\Pi$ & Threshold of total traffic variation 
			\end{tabular}
		\end{minipage}
	%	\vspace*{-4mm}
\end{table}

\subsection{Flow Constraints} \label{sec:flow}

\noindent\fbox{%
    \parbox{.97\columnwidth}{%  
	Traffic flows from source to destination along \emph{active} links 
	and flow is \emph{conserved} at every node in the ETG.
    }%
}

	The $Flow$ and $\Delta$ variables represent respectively the fraction of 
	normal and variation of traffic flowing along the network links. 
	For class $tc$ and edge $e$, the actual
	traffic on the edge is $(Flow(e,tc) + \Delta(e, tc)) \times T(tc)$.
	The distinction of $Flow$ and $\Delta$ variables
	ensures the verification constraints (\ref{eq:load}) are linear while 
	providing variation in traffic characteristics.
	For every ETG and 
	corresponding traffic class $tc$, the outgoing flow at $tc$
	source and the incoming flow at $tc$ destination should equal 
	$1 + \Delta(tc)$ which are constrained separately:
\begin{align} \label{eq:flowend}
	\smashoperator{\sum_{e \in Out(Src(tc))}} Flow(e, tc) = 1 \hspace*{1cm}
	\smashoperator{\sum_{e \in In(Dst(tc))}} Flow(e, tc) = 1 \nonumber \\ 
	\smashoperator{\sum_{e \in Out(Src(tc))}} \Delta(e, tc) = \Delta(tc) \hspace*{1cm}
	\smashoperator{\sum_{e \in ~In(Dst(tc))}} \Delta(e, tc) = \Delta(tc)
\end{align}

For all other nodes $n \not\in \{Src(tc), Dst(tc)\}$ 
in the ETG, flow is conserved, i.e., incoming
flow is equal to outgoing flow.
\begin{align}
\smashoperator{\sum_{e_{in} \in In(n)}} Flow(e_{in}, tc) ~~~~~& = \smashoperator{\sum_{e_{out} \in Out(n)}} Flow(e_{out}, tc) \nonumber \\
\smashoperator{\sum_{e_{in} \in In(n)}} \Delta(e_{in}, tc) ~~~~~& = \smashoperator{\sum_{e_{out} \in Out(n)}} \Delta(e_{out}, tc)
\label{eq:flowconservation}
\end{align}

Next, we need to accommodate failures. 
When a link fails, no traffic must flow on it:
\begin{equation} \label{eq:flowfail}
Flow(e,tc) + Fail(e) \leq 1 
~~~\bigwedge~~~~~\Delta(e,tc) + Fail(e) \leq 1
\end{equation}
Thus, if $Fail(e) = 1$, then $Flow(e,tc)=0$ and $\Delta(e, tc)=0$.

Operators can bound the individual variation for each traffic class and/or
bound the total extra variation of traffic in the network by a threshold $\Pi$:
\begin{equation} 
	\sum_{tc \in TC} \Delta(tc) * T(tc) \leq \Pi
\end{equation}

Given a solution to the above constraints, 
we construct the flow graph of $tc$
by picking all edges $e$ where $Flow(e,tc) > 0$, which 
indicate all the links traffic flows on. 

\subsection{Distance Constraints} \label{sec:distance}

A computed path in the real network  matches the shortest path in the ETG 
between the corresponding 
source and destination (Theorem~\ref{thm:pathequivalence}). 
Thus:

\vspace*{1mm}
\noindent\fbox{%
    \parbox{.97\columnwidth}{%  
	Traffic must only flow on shortest-distance ETG paths.
	}% 
}

We formulate shortest path distances to the destination node of the ETG in an
inductive fashion - the shortest path from a node must traverse through one of
its neighbors, and thus, distance of node $n$ can be defined inductively as the
shortest among distances from the neighbors. Again, failures introduce
complications, because distances can change under different link failures. To
this end, we need to two sets of constraints. First, we use the following
constraints to ensure that for every node $n$ and traffic class $tc$, the value
of the variable $Dist(n, tc)$ is \emph{at most} the distance of the shortest
path that traverses only active links. For all $e = n \rightarrow n' \in Out(n)$:
\begin{equation} \label{eq:distance}
Dist(n, tc) \leq W(e, tc) + Dist(n', tc) + \infty \times Fail(e)
\end{equation}
The intuition is that when edge $n \rightarrow n'$  has failed 
and $Fail(n \rightarrow n')=1$, 
the shortest distance from $n$ will not depend on the path through $n'$
as the equation will be satisfied trivially due to the large
constant in front of $Fail(n \rightarrow n')$. 
The above constraints
% ensures
%$Dist(n,tc)$ is smaller than distances from neighbors connected by active links.
%Note that Constraints (\ref{eq:distance}) 
provide an upper-bound on 
$Dist$ variables, but do not ensure that the variable values are exactly 
equal to actual shortest distances in the ETG. E.g.,
setting all $Dist$ variables to 0 trivially satisfies
Constraints (\ref{eq:distance}).
%\begin{wrapfigure}{r}{9em}
%	\includegraphics[width=0.4\columnwidth]{figures/flowdistex.pdf}
%	\caption{For the  $S$-$T$ ETG in Figure~\ref{fig:controlplane}(b), 
%	we illustrate the property that sum of 
%	Flow in the network weighted with edge weights is the distance 
%	of the path taken by the flow.}
%	\label{fig:flowdistex}
%\end{wrapfigure}

Second, we use another set of constraints to ensure that the ETG
traffic flow only uses the shortest paths in the network. (We will
consider load-balancing in \secref{sec:loadbalancing} and ignore it
here). We illustrate the idea of our encoding using the example ETG in
\Cref{fig:ecmpex}. For the $Flow$ quantities, the cost
of the path taken by the flow can be computed by the sum of $Flow$ on
all ETG edges multiplied by the edge weights -- i.e., $\sum_{e} W(e, tc)
\times Flow(e,tc)$ (regardless of whether the flow is sent on the
shortest path or not). The following constraint ensures that the flow is sent on the
shortest distance path from source to destination of the ETG of 
$tc$:
\begin{equation} \label{eq:srcdist}
Dist(Src(tc)) = \smashoperator{\sum_{e \in OEdges(tc)}} Flow(e, tc) * W(e, tc)
\end{equation}
Since Constraint (\ref{eq:distance}) guarantees upper bounds on the shortest distance,
the above constraint  will ensure the traffic will not be sent 
on a longer path. Also, by virtue of Constraints 
(\ref{eq:distance}) and (\ref{eq:srcdist}),
the $Dist$ variables for all nodes in the ETG will be exactly
equal to the shortest distances to the destination. 
Notice, that \emph{the constraint will guarantee this property even if traffic flows across multiple shortest paths}.

To ensure that the traffic variation only flows on shortest paths, we add 
the following constraint:
\begin{equation} \label{eq:flowdelta}
	\forall e \in OEdges(tc). ~\Delta(e, tc) \leq Flow(e, tc) 
\end{equation}
By virtue of Constraint (\ref{eq:srcdist}), $Flow(e,tc)$ is 0 on 
non shortest path links, and consequently, $\Delta(e,tc)$ will be 0.


\begin{figure}
	\centering
	\includegraphics[width=0.8\columnwidth]{figures/ecmpex.pdf}
	\caption{ECMP behavior of node $C.1_{O}$ under
		  two different scenarios for the partial $R-T$ ETG from 
		  \Cref{fig:controlplane}(b). Note that Sum of Flow in 
		  the network weighted with edge weights is the distance 
		  of the path taken by the flow for both scenarios.}
	\label{fig:ecmpex}
\end{figure}

% \begin{figure}
%     \begin{minipage}{0.6\linewidth}
%          \includegraphics[scale=0.37]{figures/ecmpex.pdf}
% 		\compactcaption{\footnotesize 
% 		Illustrating ECMP behavior of node $C.1_{O}$ under
% 		two different scenarios for the partial $R-T$ ETG from 
% 		\Cref{fig:controlplane}(b)}
%         \label{fig:ecmpex}
% \end{minipage}%
% \hfill
% \begin{minipage}{0.35\linewidth}
%         \includegraphics[scale=0.38, right]{figures/flowdistex.pdf}
%         \compactcaption{\footnotesize
% 			Sum of Flow in the network weighted with edge weights is the distance 
% 		 	of the path taken by the flow for the $S$-$T$ ETG from Figure~\ref{fig:controlplane}(b)}
%         \label{fig:flowdistex}
% 	\end{minipage}
% \end{figure}

\subsection{Load Balancing Constraints} \label{sec:loadbalancing}
\noindent\fbox{%
    \parbox{.97\columnwidth}{%  
	The outgoing $Flow$ and $\Delta$ 
	of each node are \emph{split} equally among the 
	shortest neighbors connected by active links.
	}% 
}


\Cref{fig:ecmpex} demonstrates how
ECMP operates under no failures, and the differences that arise when a
link failure occurs.

At any router, by virtue of Constraints~\ref{eq:flowend}-\ref{eq:flowdelta},
traffic will never be sent along longer paths.
The constraints presented in this section ensure traffic is split equally
among the active links at a router which are on the currently
available shortest paths to the destination.
First, we show a simple disjunctive constraint that 
encodes the desired behavior
and we then show how the same behavior 
can be captured without disjunction.

For node $n$ and
all outgoing edge pairs $e_1=n \rightarrow n_1$ and 
$e_2= n \rightarrow n_2$ 
in $Out(n)$, ECMP for $Flow$ (similarly $\Delta$) is:
\begin{multline*} %\label{eq:ecmp}
[W(e_1, tc) + Dist(n_1, tc) = W(e_2, tc) + Dist(n_2, tc)] \\
~\wedge \neg Fail(e_1) ~\wedge~ \neg Fail(e_2) 
 \implies Flow(e_1, tc) = Flow(e_2, tc)
\end{multline*}
The above constraints ensure that if the distance to the destination
along two active outgoing edges is equal, then the flow on them
will also be equal (if these edges do not lie on the shortest path,
then flow will be 0 on both edges). However, these constraints use
implications (i.e., disjunctions) and cannot be provided in this form to 
an ILP solver. 

By introducing new \emph{rational} variables we can write constraints that
express the ECMP load balancing constraints using only linear
inequalities. This forms a \emph{key innovation of \name encoding}.
Specifically, we define sets of variables $minFlow(n, tc) \in
[0,1]$ and $maxFlow(n, tc) \in [0,1]$ to capture the minimum and
maximum non-zero $Flow$, respectively, out of node $n$ for traffic class
$tc$.  
The non-zero flow restriction holds for flows along the
shortest paths (flow along non-shortest paths will be 0), and thus we
can impose constraints on $minFlow$ and $maxFlow$ to model ECMP
routing. We also add similar constraints for 
the traffic variation $\Delta$ variables so that the total traffic adheres 
to ECMP.

Adding constraints for $maxFlow(n,tc)$ to be the maximum non-zero flow
value is trivial, as the zero flow values will not affect the
$maxFlow$ variable:
\begin{align} \label{eq:maxflow}
\forall e=n \rightarrow n' \in Out(n). ~Flow(e, tc) &\leq maxFlow(n, tc) \nonumber \\
\forall e=n \rightarrow n' \in Out(n). ~\Delta(e, tc) &\leq max\Delta(n, tc)
\end{align}

Adding constraints for $minFlow(n,tc)$ is trickier:
to ensure that $minFlow(n,tc)$ is the minimum non-zero flow value, we need
to know the flow values {\em for all possible failure scenarios}. 
To bypass this problem, we use the distance variables $Dist$ to identify the next-hops that lie on the
shortest paths, and thus, have non-zero flows. Based on this insight, we impose the following
constraints for all $ e=n \rightarrow n' \in Out(n)$ to ensure that $minFlow$ variables have the correct values:
\begin{align} \label{eq:minflow}
\forall e=n &\rightarrow n' \in Out(n). ~minFlow(n, tc) - Flow(e, tc) \leq \nonumber \\
&\infty \times( W(e,tc) + Dist(n',tc) - Dist(n, tc) + Fail(e)) \nonumber \\ 
\forall e=n &\rightarrow n' \in Out(n). ~min\Delta(n, tc) - \Delta(e, tc) \leq \nonumber \\ 
&\infty \times( W(e,tc) + Dist(n',tc) - Dist(n, tc) + Fail(e)) 
\end{align}

First, notice that the quantities $W(e,tc) + Dist(n',tc) - Dist(n, tc)$
and
$W(e,tc) + Dist(n',tc)  - Dist(n, tc)+ Fail(e)$ are always greater or equal than $0$.
Hence, there are three scenarios for each edge which are all encoded by the 
above constraint: (1) the edge $e$ has failed, in which case $Fail(e)=1$, the RHS of
the constraint is infinity, and the constraint is trivially satisfied,
(2) the edge $e$ is not on the shortest path, thus $W(e,tc) + Dist(n',tc)$ is
greater than $Dist(n,tc)$ and thus, the RHS is a large positive constant, and
(3) the edge $e$ is active and on the shortest path, so the RHS of the 
constraint is 0---i.e., $minFlow(n,tc) \leq Flow(e,tc)$. Therefore,
$minFlow(n,tc)$ is smaller than all the non-zero edge flows 
(which can only flow on the shortest paths).

Thus, for a node $n$, we have two variables for the lower bound and upper bound
of all the non-zero edge flows out of the node. For ECMP, we require all non-zero
edge flows on the shortest paths out of a node to be equal, which can be 
enforced by ensuring the lower bound $minFlow(n,tc)$ 
and upper bound $maxFlow(n,tc)$ are equal (similarly for $\Delta$): 
\begin{align} \label{eq:maxminequality}
minFlow(n,tc) &= maxFlow(n,tc) \nonumber \\
min\Delta(n,tc) &= max\Delta(n,tc)
\end{align}

Constraints~\ref{eq:flowend}-\ref{eq:maxminequality} ensure
the total flow to neighbors on the shortest paths 
are equal to one another, adhering to the ECMP load-balancing model.
The above constraints can be modified 
by multiplying constant weight 
factors to $Flow$ variables to 
model Weighted Cost Multipathing (WCMP)~\cite{wcmp}.

\subsection{Failure Constraints} \label{sec:failure}
\noindent\fbox{%
    \parbox{.97\columnwidth}{%  
	Failure scenarios can be restricted by the operator, 
	e.g., by number of links or 
	likelihood of failures.
	}% 
}

Operators may want to restrict the failure
scenarios of interest ($\overline{FL}$) 
to make useful observations about how the
network control plane reacts under failures. 
%We
%exemplify how \name can support two kinds of failure scenarios--- 
%(1) under $k$-link failure scenarios, and (2) link failure scenarios with
%probability greater than threshold $\omega$. Extending to other operator-specified
%constraints on scenarios of interest (such as core link failures only) 
%is easy and omitted for
%brevity. 
The following constraint restricts the number of
link failures to $k$:
\begin{equation} \label{eq:failbound}
\sum_{e \in Links} Fail(e) \leq k
\end{equation}

\begin{theorem} \label{thm:shortestequal}
	For every traffic class $tc \in TC$ and failure scenario $FL \in \overline{FL}$, 
	Constraints~\ref{eq:flowend}-\ref{eq:failbound} ensure 
	that the flow Graph $FG(tc, FL)$ is a directed acyclic
	graph such that each path from $Src(tc)$ to $Dst(tc)$ is the 
	shortest path in $ETG(tc)$, and for every router $n$ in flow graph $FG(tc, FL)$, 
	the flows on outgoing edges which lie on shortest paths from 
	$n$ to $Dst(tc)$ are equal., i.e.,
	$\forall n_1, n_2 \in next(n). 
	~~F((n, n_1),tc,FL) = F((n, n_2),tc,FL)$.
\end{theorem}
	

\Cref{thm:shortestequal} and \Cref{thm:pathequivalence} together prove that 
the flow graphs constructed by the \qarc constraints faithfully represent 
the routing paths and flow distributions in the actual network.


Operators can also assign failure probabilities 
to individual links and restrict the search 
to scenarios that have probability above a threshold.
Suppose, the operator has probabilities 
assigned to individual link failures ($P_{fail}(l)$)
and is only interested in link failure scenarios 
that have joint probability above a
threshold $\omega$. 
The failure probabilities can be derived 
from telemetry data in real-world networks
\cite{linkfailureprob}. We assume that
link failures are independent, thus, the probability of a certain link
failure scenario is the product of individual link failure
probabilities. Then, we would want to enforce the following constraint 
to search for failure scenarios with probability greater than $\omega$:
\begin{equation*}
	\prod_{l \in Links. Fail(l) = 1} P_{fail}(l) \geq \omega 
\end{equation*}
We use the logarithm of probabilities to generate the equivalent linear constraint.
\begin{equation} \label{eq:failureprob}
\sum_{l \in Links} log(P_{fail}(l)) \times Fail(l) \geq log(\omega)
\end{equation}
We can have a theorem similar to \Cref{thm:shortestequal} for link 
failure probabilities.


\section{Verification using \name} \label{sec:verification}

Finally, verifying for network load violations using \name is done by adding 
load-related constraints.

\noindent\fbox{%
    \parbox{.97\columnwidth}{%  
	$Load(e)=1$ iff the utilization of link $e$ exceeds  capacity.
	}% 
}

%% We show how we can add additional constraints to the \name encoding 
%% to verify if there are provisioning violations under the failure 
%% scenarios. 
%For every link $e$,
%$Load(e) \in \{0, 1\}$ is 1 iff link $e$
%is overloaded, i.e., total traffic exceeds the capacity of $e$.
The following constraint  correctly 
sets the value of the variable $Load(e) \in \{0, 1\}$:

\begin{equation} \label{eq:load}
\frac{\sum_{tc \in TC} [Flow(e, tc) + \Delta(e, tc)] \times T(tc)}{Cap(e)} - Load(e) \geq 0 
\end{equation}

The numerator of the fraction captures the total traffic flow on link $e$,
while the denominator, $Cap(e)$ (a constant), captures the capacity of $e$.
$Load(e)$ can only be $1$ when the traffic on link $e$ exceeds its 
capacity.
To find if there exists a failure scenario where at least one of the links is 
overloaded, we can constrain the sum of
load variables to be at least 1:
\begin{equation} \label{eq:netcong}
\sum_{e \in Links} Load(e) \geq 1
\end{equation}

When feeding the constraints to the ILP solver, there can be two
outcomes.  First, the solver returns a satisfiable model from which we
can extract the link failure scenario under which one or more links
are overloaded.  
Second, the solver returns unsatisfiable, which
means there is no $k$-link failure scenario that  can cause link
overload.  There are two explanations for this outcome: 
(a) the
network has sufficient capacity to handle rerouted input traffic under
failures, or
(b) a subset of traffic classes that
do not have high path
redundancy are
disconnected due to the failures, and the remaining active traffic classes do not have
sufficient traffic to cause overloads.  Note that, even when the
solver finds a failure scenario where link overload occurs, certain
traffic classes could be disconnected in the satisfying solution, with
the remaining active traffic classes still able to overload the
link(s).
%%  Operators can use qualitative verifiers~\cite{arc,
%%   minesweeper} to check for reachability properties of traffic
%% classes,
To summarize, \qarc, as presented, will try to find potential for link
overload even in the face of disconnections. 

% \qarc can be 
% extended to verify potential for overload under failure scenarios that
% don't induce any disconnection. However, in networks where many
% traffic classes have limited redundancy, there may be no failure
% scenario that experiences link overload without any disconnections.


\begin{theorem}[Correctness] \label{thm:correctness}
    A failure scenario $FL$ satisfies Constraints~\ref{eq:flowend}-\ref{eq:netcong} if and only if
    there exists a link $l \in Links$ such that 
    $Util(l,FL) \geq Cap(l)$. 
\end{theorem}

% \qarc can also be used to verify other quantitative properties
% dealing with link utilization---for instance, it can verify 
% if the average link utilization of 
% the network remains below a certain threshold 
% for every $k$-link failure 
% scenario. 


