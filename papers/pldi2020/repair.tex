%!TEX root = paper.tex

\begin{figure}
	\centering
	\includegraphics[width=0.55\columnwidth]{figures/repairex.pdf}
  \caption{Example OSPF network where a link gets overloaded under 1 and 2 link 
    failure scenarios. The capacities in green are the minimum link 
    capacity additions computed by \qarc to ensure no violations occur under $\leq 2$ 
    failures.}
	\label{fig:repairex}
\end{figure}


\section{Upgrading Link Capacities in \name} \label{sec:repair}
Once a load violation is found, the operator might want  to 
modify the network to ensure that {all link utilizations do not exceed capacity under
all considered failure scenarios}. One way to achieve this property is to modify
the control plane. However, modifying the control plane may cause violations of
{\em qualitative} properties that the current configuration satisfies (access
control, waypointing for middleboxes, etc.).  Finding policy-compliant
control-plane modifications is already a computationally hard problem~\cite{cpr},
and accounting for network load on all links for all traffic classes complicates the problem further. 

% \loris{this para can go}
% Let's revisit the root cause of network load violations under
% failures: the link does not have adequate capacity to handle traffic
% routed through the link under a particular failure scenario.
% Datacenter traffic patterns~\cite{swan} and link failure
% characteristics~\cite{} are evolving over time; thus,
% static link capacities which was sufficient to handle the input traffic 
% may not suffice at a later point of time. 
% At the same time,
% repairing by adding/removing cables/fibres 
% (to add/remove capacity) is
% arduous and time-consuming.

%\loris{as well as this first sentence (modulo some rewriting)}
% We adopt a different approach. Fiber-optic links are increasingly
% prevalent in datacenter networks to meet bandwidth
% demands~\cite{facebook-fabric, jupiter-rising, microsoft-optical}.  

Instead, we propose to use the \qarc abstraction to \emph{find
new link capacities guaranteeing all link utilizations do not exceed capacity under
all considered failure scenarios}. There are multiple mechanisms for realizing the
new capacities---physically adding more cables to increase capacity, or
dynamically changing capacity in the case of optical links~\cite{radwan}.
Increasing bandwidth is expensive, thus, we need to ensure we do not increase
capacities beyond what is necessary.

% Second, deploying new capacities to network
% links is non-trivial as these links need to be powered down for the
% capacity change; thus, we need to schedule the capacity changes
% without causing links to be overloaded during the
% repair (\secref{sec:repairschedule}).



% In a recent work~\cite{radwan}, Singh et. al make a case for dynamically
% changing optical link capacities and show that by doing so operators
% can extract 75-100\% capacity gains from their WAN optical links. Recent 
% studies~\cite{facebook-fabric, jupiter-rising, microsoft-optical} 
% have shown the prevalence of fiber-optic links in 
% datacenter networks as well. 
% We propose to repair the network by {\em changing
% optical link capacities dynamically} to avoid link 
% overload.  This approach poses virtually no
% additional cost and no manual effort.

% We address key practical issues in this repair strategy. First, the
% trade-off of dynamically changing capacity is that packet loss
% probability on the link increases with increasing capacity; thus, we
% need to ensure we do not increase capacities beyond what is necessary
% (\secref{sec:optrep}). 


% \aditya{This belongs in related work} Repairing network configurations to satisfy
% certain properties is a difficult task, and state-of-art works
% ~\cite{cpr, netcomplete} attempt to 
% repair configurations to satisfy various
% qualitative properties with additional management objectives, 
% for instance, minimizing the
% number of configuration changes in terms of lines of code or routers. 

% However, a key aspect of repair for network provisioning compared 
% to the state-of-art network configuration repair is that these works 
% deal with repair for different qualitative intents which are dependent 
% on a single (or small) set of traffic classes. On the other hand,
% network provisioning depends on all traffic classes; our repair 
% must be able to consider the effect of a particular configuration 
% change on all these traffic classes.




%%  and may require assistance from external entities---thus,
%% complicating the network operator's responsibilities.

\minisection{Upgrade Formulation} 
Concretely, the upgrade problem is as follows:
\emph{
    Given a set of network configurations and input characteristics, find 
    the minimal set of links and the minimal capacity additions to these links
    such that the network is not overloaded under the given failure scenarios.
}

\Cref{fig:repairex} illustrates the upgraded link capacities computed by
\name.  The traffic sent by class $S\rightarrow T$ is 90 Gbps. As we
can see, under 2-link failure scenarios (e.g., $D-A$ and $B-A$), some
links' utilization exceeds capacity (e.g., $B-C$). \name computes the
minimum capacity additions to the links (shown in green---e.g.,
\textcolor{green}{+10}); by increasing the capacities, no
1- or 2-link failure scenario can cause violations.

We use \name to compute new link capacities such that
link utilization never exceeds the ``new'' capacity under the provided
failure scenarios. We use the \qarc constraints
(\secref{sec:flow}-\secref{sec:failure}) and  compute the minimum link
capacity that link $l$ should have using the following objective:
\begin{equation} 
  \texttt{maximize} \sum_{tc \in TC} [Flow(l, tc) + \Delta(l, tc)] \times T(tc)
  \label{eq:maxutil}
\end{equation}
\qarc computes the maximum utilization for the link under the different
failure scenarios given the input traffic characteristics with bounded variation, 
which is the {\em minimum new capacity} required on the link
to ensure that the particular link is not overloaded. 
Note that, this capacity is not dependent on the capacity
of other links, as the distributed control plane chooses the ``best''
active path(s) under failures based on configuration parameters such
as static link weights and path lengths. Thus, similar to verification, we can 
parallelize the phase of computing the upgraded link capacities.

\begin{table*}
  \makebox[0pt][c]{\parbox{\linewidth}{%
  \begin{minipage}{0.47\linewidth}
    \caption{
  Networks which experience link overload for one or more TMs (>0\%) 
  and more than 10 of 20 TMs (>50\%) 
  at $k=1$ and $k=2$. \label{tab:realworld}}%
  \vspace*{-4mm}
      \centering
      \begin{tabular}{C{3em} C{3em} C{2.5em} C{2.5em} C{2.5em} C{2.5em}} 
      {\bf Class} & {\bf Total} &{\bf k=1 \newline (>0\%)} & {\bf k=1 \newline (>50\%)} & {\bf k=2 \newline (>0\%)} & {\bf k=2 \newline (>50\%)} \\
      \hline
      DC & 114 & 56 & 20 & 52 & 23 \\
      ISP & 86 & 83 & 67 & 82 & 63 \\
      \end{tabular}
  \end{minipage}
  \hfill
  \begin{minipage}{0.47\linewidth}
    \caption{
      Percentage overload violations for 1-link failures for datacenter networks with 
      Fbflow and Gravity traffic matrices \label{tab:trafficmatrix}}
      \vspace*{-4mm}
      \centering
      \begin{tabular}{C{3.5em} C{3em} C{3em} C{3em} C{3em} C{3em}}
      {\bf Matrix}& {\bf DC1 \newline (<100)} & {\bf DC2 \newline (<100)} & {\bf DC3 \newline (<100)} & {\bf Fat6 \newline (216)} & {\bf DC4 \newline (<1000)}  \\
      \hline
      Fbflow & 74\% & 72\% & 28\% & 100\% & 100\% \\
      Gravity & 83\% & 96\% & 38\% & 100\% & 100\% \\
      \end{tabular}       
  \end{minipage}
  }}
\end{table*}

The set of links whose capacities need an upgrade
is \emph{minimal}---i.e., if any of these links
have capacities less than the computed ones, 
there exists a failure scenario under which 
a link will be overloaded.

% \begin{theorem} \label{thm:upgrade}
% If there is link $l$ such that $Cap(l)$ is strictly less than computed 
% capacity $Cap'(l)$, then there exists a failure scenario $FL 
% \in \overline{FL}$ such that $Util_{FL}(l) > Cap(l)$.
% \end{theorem}

% \begin{proof}
% We proceed by contradiction. Let us assume there is no 
% failure scenario $FL \in \overline{FL}$ such that $Util_{FL}(l) > 
% Cap(l)$. Therefore, the following holds.
% \begin{align} \label{eq:repairproof}
%   \max\limits_{FL \in \overline{FL}} Util_{FL}(l) \leq Cap(l)
% \end{align}
% Objective~\ref{eq:maxutil} is used to compute the new capacity $Cap'(l)$:
% \begin{align}
%   Cap'(l) &= \max\limits_{FL \in \overline{FL}} \sum_{tc \in TC} [Flow(l, tc) + \Delta(l, tc)] \times T(tc) \nonumber \\
%   Cap'(l) &= \max\limits_{FL \in \overline{FL}} Util_{FL}(l) \nonumber \\
%   Cap'(l) &\leq Cap(l) \hspace*{0.5cm}\text{[From (\ref{eq:repairproof})]} \nonumber
% \end{align}
% However, this contradicts our assertion that $Cap(l) < Cap'(l)$. Hence, proved.
% \end{proof}


% \subsection{Repair Scheduling} \label{sec:repairschedule}
% A deployment challenge hindering the usage of optical links with
% dynamic link capacities is the design of the Bandwidth Variable
% Transceivers (BVTs) in optical switches; BVTs are not optimized for
% changing the modulation of the optical link (which is required to
% change capacity).  State-of-the-art BVTs require the link to be
% brought to a lower power state to change the modulation.  This
% translates to the link being viewed as ``down'' from network
% protocols' viewpoint when the BVT changes the link modulation. Singh
% et.  al~\cite{radwan} report an average downtime of 68 seconds for a
% link undergoing capacity change.  Thus, we could cause link
% overloads if we changed the link capacities in an
% indiscriminate manner.

% Consider the network shown in \Cref{fig:repairex} where \Name computes
% the new link capacities such that no links are overloaded under $k\leq 2$
% link failure scenarios. If we changed the capacity of all 3 links
% together, the network becomes disconnected.  Suppose, we change the
% capacity of link $B \rightarrow A$ first. When the change is happening, since the
% link is ``down'', all traffic will go via the shortest path through
% $D$, and thus $D \rightarrow A$ (which is still at 80Gbps) is overloaded. The
% correct schedule computed by \qarc will be: first change $B \rightarrow C$, then
% $B \rightarrow A$ and $D \rightarrow A$ together.  
% This two stage update ensures no links are overloaded 
% within any stage, assuming no failures occur during the stages.

% \input{greedyrepair}

% We use the \name
% encoding to extract a repair schedule 
% which does not result in any
% link overload during the process 
% of changing the link
% capacities. 
% We compute a greedy 
% schedule in ``stages'', where in each
% stage, we change the capacities of a maximal subset 
% of links.  
% We ensure that every stage is overload-free---i.e., 
% the utilization of active links 
% do not exceed their capacities, thus, ensuring 
% a smooth transition from the old to the new capacities. 

% \Cref{alg:greedyrepair} proceeds as follows.
% At every stage, we have a set of links $UpgradeSet$ whose capacities
% we need to change. At the beginning
% ($stage=1$) $UpgradeSet$ is the set of links computed using
% Constraints~\ref{eq:maxutil}. 
% Given $UpgradeSet$, we compute a maximal subset of links $MaxL$ 
% (Line~\ref{alg:maxl}) whose capacities can be safely upgraded in this
% stage by bringing the links ``down''. We add the \name flow 
% (~\secref{sec:flow}), distance (~\secref{sec:distance}) and 
% load-balancing constraints (~\secref{sec:loadbalancing}).
% To obtain a schedule without link overload, we add constraints 
% ensuring that no link's utilization exceeds its capacity:
% \begin{equation}
% Cap(l) - \sum_{tc \in TC} [Flow(l, tc) + \Delta(l, tc)] \times T(tc) \geq 0
% \end{equation}
% To find the maximal set of links $MaxL$ we can schedule for upgrade in the stage, we  
% use the following objective,
% since a link appears as failed (low-power state) when undergoing upgrade:
% \begin{equation}
%   \texttt{maximize} \sum_{l \in UpgradeSet} Fail(l)
% \end{equation}
% Once, we compute the subset of links $maxL$, we change the capacities
% of those links, and repeat the procedure for the links remaining in
% $UpgradeSet$. Note that we assume that no other link failures 
% happen during the
% period when we are deploying the repair.
% Our  algorithm finds a schedule of repair
% stages or will return unsatisfiable 
% at some stage---i.e., we could not find a 
% repair schedule for all links 
% whose capacity needed to be changed 
% without causing link overload.



% We assume that no other link failures happen during the
% period when we are deploying the repair.

% Ideally, we
% would want to compute a repair schedule with a small number
% of stages (as each stage can take around 60 seconds on average).
% To this end, we propose a \emph{greedy} algorithm 
% (\Cref{alg:greedyrepair}) where in each stage, we maximize the number of links whose
% capacities we can change without causing links to be overloaded 
% during that stage.
%We present our greedy recursive algorithm in
%\Cref{alg:greedyrepair}.




