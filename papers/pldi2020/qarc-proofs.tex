%!TEX root = paper.tex

\section{\name Proofs}

\subsection{Verification}

\begin{lemma} \label{thm:flowconservation}
    For every traffic class $tc \in TC$ every
    failure scenario $FL \in \overline{FL}$, 
    Constraints~\ref{eq:flowend}-\ref{eq:flowfail} 
    ensure that (1) the 
    Flow Graph $FG(tc, FL)$ is a directed acyclic graph 
    and (2) flow is preserved from $Src(tc)$ to $Dst(tc)$.
\end{lemma}

    
\begin{proof}
    We proceed by contradiction. First, 
    let us assume that a router $r$ drops some 
    amount of traffic, i.e., the incoming flow 
    into $r$ is not equal to outgoing flow of $r$. 
    However, this contradicts 
    Constraint~\ref{eq:flowconservation}, thus, (2) 
    flow is conserved at every router. 
    
    Second, let us assume that the flow graph contains 
    a cycle containing router $r$ and flow amount 
    $f_c$ which keeps flowing in the cycle. 
    The flow $f_c$ will not reach $Dst(tc)$, and since,
    flow is conserved at every router, the flow reached
    at destination will not contain $f_c$. However,
    this contradicts Constraint~\ref{eq:flowend} that
    all flow from source is reached at the destination.
    Thus, proved that (1) the flow graph will not contain 
    any cycles.
\end{proof}



\begin{lemma} \label{thm:shortest}
    For every traffic class $tc \in TC$ and failure scenario $FL \in \overline{FL}$, 
    Constraints~\ref{eq:flowend}-\ref{eq:flowdelta} 
    ensure that each path 
    from Src(tc) to Dst(tc) in the Flow Graph 
    $FG(tc, FL)$ is a shortest weighted path in $ETG(tc)$ under 
    the failure scenario $FL$.
\end{lemma}


\begin{proof}
    We proceed by contradiction. 
    Let us assume there is a path $p$ in Flow Graph 
    $FG(tc, FL)$ which is not a shortest path in $ETG(tc)$,
    i.e., there is a shortest path $p'$ and 
    $W(p') < W(p)$ (where $W(p)$ denotes the sum of 
    weights of edges in path $p$). We denote the 
    flow on path $p$ as $f_p$ and flow on path $p'$
    as $f_{p'}$. 
    
    By virtue of Constraint~\ref{eq:srcdist}, we can 
    write $Dst(src(tc)) = W(p)*f_p + W(p')*f_{p'} + ...$
    Using ~\Cref{thm:flowconservation}, 
    the sum of flows on all paths must be equal to 
    1 and $p'$ is a shortest path, 
    therefore, $Dst(src(tc)) > W(p')$.
    
    Consider Constraint~(\ref{eq:distance}) for routers on the 
    path $p' = Src(tc) \rightarrow r_1 \ldots Dst(tc)$. $p'$ 
    is in the flow graph, so no edge will be failed and Fail 
    variables on p' will be set to 0.
    
    \begin{align}
        Dist(Src(tc), tc) &\leq W((Src(tc), r_1)) + Dist(r_1, tc) \nonumber \\
        &\leq W((Src(tc), r_1)) + W((r_1, r_2) + Dist(r_2, tc) \ldots \nonumber \\
        &\leq W(p') + Dist(Dst(tc), tc) \nonumber
    \end{align}
    Since, $Dist(Dst(tc), tc) = 0$, $Dist(Src(tc), tc) \leq W(p')$.
    This contradicts our previous assertion that 
    $Dst(src(tc)) > W(p')$. Hence, proved.
\end{proof}

\begingroup
\def\thetheorem{\ref{thm:shortestequal}}
\begin{theorem}
For every traffic class $tc \in TC$ and failure scenario $FL \in \overline{FL}$, 
Constraints~\ref{eq:flowend}-\ref{eq:failbound} ensure 
that the flow Graph $FG(tc, FL)$ is a directed acyclic
graph such that each path from $Src(tc)$ to $Dst(tc)$ is the 
shortest path in $ETG(tc)$, and for every router $n$ in flow graph $FG(tc, FL)$, 
the flows on outgoing edges which lie on shortest paths from 
$n$ to $Dst(tc)$ are equal., i.e.,
$\forall n_1, n_2 \in next(n). 
~~F_{tc}((n, n_1)) = F_{tc}((n, n_2))$.
\end{theorem}
\addtocounter{theorem}{-1}
\endgroup

\begin{proof}
~\Cref{thm:flowconservation} and \Cref{thm:shortest} 
prove that the flow graph $FG(tc,FL)$ does not contain 
any cycles and only contains the shortest paths in $ETG(tc)$.
To prove that for any router $n$, 
flow on outgoing edges which lie on shortest paths 
are equal, we proceed by contradiction. Let us assume that 
for traffic class $tc$, router $n$ does not split traffic 
equally, i.e., 
$\exists n_1, n_2. F_{tc}((n, n_1)) \not= F_{tc}((n, n_2))$.
In terms of the \name variables, the assumption
translates to $Flow((n, n_1), tc) \not= 
Flow((n, n_2), tc)$. Without loss of generality, 
let $Flow((n, n_1), tc) < Flow((n, n_2), tc)$.

By virtue of Constraints~\ref{eq:maxflow}, the following 
holds:
\begin{equation} \label{eq:ecmpproofmax}
    Flow((n, n_1), tc) < Flow((n, n_2), tc) \leq maxFlow(n, tc) 
\end{equation}

By Lemma~\ref{thm:shortest}, both $n_1$ and $n_2$ 
lie on shortest paths from $n$ to the destination. 
Therefore, the expression $W((n, n_1)) + Dist(n_1,tc) 
- Dist(n, tc) + Fail((n, n_1))$ is equal to 0 (similarly
for $n_2$). Plugging in Constraints~(\ref{eq:minflow}), we get:
\begin{equation} \label{eq:ecmpproofmin}
    minFlow(n, tc) \leq Flow((n, n_1), tc) < Flow((n, n_2), tc) 
\end{equation}

From (~\ref{eq:ecmpproofmax}) and ~(\ref{eq:ecmpproofmin}), we get 
\begin{equation*}
    minFlow(n, tc) < maxFlow(n, tc)
\end{equation*}
However, the above assertion contradicts Constraint~(\ref{eq:maxminequality}). 
Hence, proved. 
\end{proof}

\begingroup
\def\thetheorem{\ref{thm:correctness}}
\begin{theorem}[Correctness]
A failure scenario $FL$ satisfies Constraints~\ref{eq:flowend}-\ref{eq:failbound}, 
\ref{eq:load}-\ref{eq:netcong} if and only if
there exists a link $l \in Links$ such that 
$Util_{FL}(l) \geq Cap(l)$. 
\end{theorem}
\addtocounter{theorem}{-1}
\endgroup

\begin{proof}
First, we prove that if $FL$ satisfies Constraints~\ref{eq:flowend}-\ref{eq:failbound}, 
\ref{eq:load}-\ref{eq:netcong}, then one of the links' utilization 
will exceed capacity.
We proceed by contradiction. Let us assume that there exists $FL$
such that for all $l \in Links$, 
$Util_{FL}(l) < Cap(l)$, i.e., no link's 
utilization exceeds capacity. In Constraint~\ref{eq:load},
the numerator in the fractional term in the 
LHS represents the amount of 
traffic on the link: $Util_{FL}(l)$. Therefore, for all links:
\begin{align}	
    Load(l) - Util_{FL}(l) > Cap(l) \leq 0
    Load(l) < 1	
\end{align}
However, this contradicts with Constraint~(\ref{eq:netcong}). 
Hence, proved. 

We now prove if there exists a $FL$ such that 
there is a link $l \in Links$ where $Util_{FL}(l) \geq Cap(l)$, then 
there is a satisfying assignment for Constraints~\ref{eq:flowend}-\ref{eq:failbound}
\ref{eq:load}-\ref{eq:netcong}. Using \Cref{thm:shortestequal} and \Cref{thm:pathequivalence},
the flow graphs in the actual network 
will be a satisfying assignment for Constraints~\ref{eq:flowend}-\ref{eq:failbound}.

For link $l$, we set $Load(l) = 1$ and for all other links $l' \in Links \setminus {l}$, we set $Load(l') = 0$.
Since $Util_{FL}(l) \geq Cap(l)$:
\begin{equation*}
    \frac{Util_{FL}(l)}{Cap(l)} - Load(l) =  \frac{Util_{FL}(l)}{Cap(l)} - 1 \geq 0
\end{equation*} 
Thus, Constraint~\ref{eq:load} is satisfied for link $l$. 
For other links $l' \in Links \setminus {l}$, $\frac{Util_{FL}(l)}{Cap(l)} > 0$ as both 
quantities are positive, thus Constraint~\ref{eq:load} is satisfied for all links.
Finally, since $Load(l) = 1$, Constraint~\ref{eq:netcong} is satisfied as well. Hence, proved.
\end{proof}

\subsection{Minimization Proofs}
\begin{theorem}[Soundness]\label{thm:optsoundness}
If edge $e$ in $ETG(tc)$ has been removed in the minimized
$ETG_{min}(tc)$, then for all failure scenarios $FL 
\in \overline{FL}$, $e$ will not in be the flow graph 
$FG(tc, FL)$.
\end{theorem}

\begin{proof}
We proceed by contradiction. Let us assume there 
exists a failure scenario $FL \in \overline{FL}$ 
such that $e = (n_1, n_2)$ is not in $ETG_{min}(tc)$ 
but is present in $FG(tc, FL)$.

Using~\Cref{thm:shortest}, since $e$ 
is present in $FG(tc, FL)$, it is on a shortest 
path from $Src(tc)$ to $Dst(tc)$ in $ETG(tc)$.
Therefore, 
$SP(Src(tc), n_1) + W(n_1, n_2) + SP(n_2, Dst(tc)) = 
SP(Src(tc), Dst(tc))$. 

Since $e$ is not in the minimized ETG, 
Condition (\ref{eq:mincondition}) holds, therefore:
\begin{align} \label{eq:minproof}
SP(Src(tc), n_1) + W(n_1, n_2) + SP(n_2, Dst(tc)) &> MaxDist \nonumber \\
SP(Src(tc), Dst(tc)) &> MaxDist 
\end{align}

However, $maxDist = MAX_{FL \in \overline{FL}} Dist(Src(tc), tc)$, 
thus, $maxDist \geq SP(Src(tc), Dst(tc))$. This contradicts 
our assertion (\ref{eq:minproof}). Hence, proved. 
\end{proof}

\begin{theorem} [Completeness] \label{thm:optcompleteness}
If edge $e$ in $ETG(tc)$ has not been removed in the minimized
$ETG_{min}(tc)$, then $\exists FL 
\in \overline{FL}$ such that $e$ will be 
in the flow graph $FG(tc, FL)$.
\end{theorem}

\begin{proof}
We proceed by contradiction. Assume that 
is $\forall FL \in \overline{FL}$, $e = (n_1, n_2)$ is 
not in $FG(tc, FL)$. 

Since $e$ was not removed by ETG minimization, using 
Condition (\ref{eq:mincondition}):
\begin{equation*}
SP(Src(tc), n_1) + W(n_1, n_2) + SP(n_2, Dst(tc)) \leq MaxDist
\end{equation*}

From Objective (\ref{eq:distobj}), $\exists FL$ such that 
$SP(Src(tc), n_1) + W(n_1, n_2) + SP(n_2, Dst(tc)) \leq Dist(Src(tc))$, 
which implies $e$ lies on a shortest path from source to destination.
Using ~\Cref{thm:shortest},
if $e$ is on a shortest path from $Src(tc)$ to $Dst(tc)$, 
then $e$ is in $FG(tc, FL)$. This contradicts our assumption that $e$ 
in not in the flow graphs under all failure scenarios in $\overline{FL}$.
Hence, proved.
\end{proof}

\subsection{Upgrade Proofs}

\begin{theorem}\label{thm:upgrade}
If there is link $l$ such that $Cap(l)$ is strictly less than computed 
capacity $Cap'(l)$, then there exists a failure scenario $FL 
\in \overline{FL}$ such that $Util_{FL}(l) > Cap(l)$.
\end{theorem}

\begin{proof}
We proceed by contradiction. Let us assume there is no 
failure scenario $FL \in \overline{FL}$ such that $Util_{FL}(l) > 
Cap(l)$. Therefore, the following holds.
\begin{align} \label{eq:repairproof}
  \max\limits_{FL \in \overline{FL}} Util_{FL}(l) \leq Cap(l)
\end{align}
Objective~\ref{eq:maxutil} is used to compute the new capacity $Cap'(l)$:
\begin{align}
  Cap'(l) &= \max\limits_{FL \in \overline{FL}} \sum_{tc \in TC} [Flow(l, tc) + \Delta(l, tc)] \times T(tc) \nonumber \\
  Cap'(l) &= \max\limits_{FL \in \overline{FL}} Util_{FL}(l) \nonumber \\
  Cap'(l) &\leq Cap(l) \hspace*{0.5cm}\text{[From (\ref{eq:repairproof})]} \nonumber
\end{align}
However, this contradicts our assertion that $Cap(l) < Cap'(l)$. Hence, proved.
\end{proof}
    