%!TEX root = paper.tex
\section{Introduction}

Managing modern data center and ISP networks is an incredibly
difficult task. Many of these networks serve a diverse set of
customers who demand varying stringent guarantees from the network,
such as high path availability, and a variety of path-based properties
(e.g., service chaining, and path isolation), and these requirements
evolve over time. Thus, operators must constantly ensure that the
network is configured to meet various requirements.

A number of recent tools aim to simplify this task. These tools
validate if the network satisfies important properties, and
automatically take corrective action otherwise.
%% Data-plane
%% verifiers~\cite{hsa,anteater}, for example, periodically check if the
%% data plane satisfies properties such as reachability/waypointing
%% constraints, and freedom from blackholes and loops. More recent
For example, network verifiers~\cite{batfish,arc,era,minesweeper}
check if the network's control and data planes satisfy
$k$-connectivity, service chaining, and path-level
isolation~\cite{arc,batfish}, and repair/synthesis
tools~\cite{netcomplete,propane,zeppelin,cpr} generate
property-compliant control and data planes.

While these tools are invaluable, they focus on {\em qualitative}
properties---informally, path properties that are all
variants of reachability. In contrast, {\em quantitative} properties,
which are also central to network management, have received little
systematic treatment. One such property, which our paper focuses on,
pertains to meeting demand, i.e., {\em can a given network (topology
  and control plane) accommodate an input traffic demand matrix
  without any link becoming overloaded?}

%% \loris{The following para seems broken. The first and second sup-para are essentially repetitive} \aditya{seems fine? double-check?}

Our community's growing focus on qualitative properties is well
justified, because violation of such properties can lead to serious
correctness problems or policy violations, e.g., broken isolation,
circumventing firewalls, or network partitions. But violation of
quantitative properties can have equally serious implications. For
instance, when network links become overloaded, many customers'
applications may suffer from increased losses and latencies, violating
service-level agreements and impacting operator revenues.
%% However, despite its importance, the problem of meeting demand has
%% received little systematic treatment.

%% The goal of our work is to automatically verify if demand can be
%% met, and to automatically repair networks' provisioning when it is
%% violated. Traditionally, provisioning was done at very coarse
%% timescales (order of weeks/months), however, with the rise of new
%% technologies like optical links with dynamic
%% capacities~\cite{radwan}, provisioning will be performed at finer
%% timescales and more frequently to account for evolving network
%% conditions.

{\bf \noindent Verification.} Verifying link overload is non-trivial. First,
link failures are common in networks~\cite{datacenterfailures}. Thus,
even if link loads are low now, significant overload may occur when
one or more links fail. In datacenter networks, packet losses due to 
high load (i.e., congestion) are common~\cite{corropt}.
%% When one or more links fail together, the control plane's design
%% can ensure that the new paths installed continue to satisfy
%% qualitative properties~\cite{zeppelin,cpr}. But the network may
%% experience sudden overload due to the reduced overall network
%% capacity.
Thus, ideally, we must {\em proactively verify for potential
  overload}: does there exists a failure where some link is
overloaded?  This is difficult because of the exponentially many
failure scenarios.

%% When building networks, ISP and datacenter
%% operators carefully select link capacities that balance cost and
%% expected performance, given some assumptions about traffic patterns
%% their network may carry. Subsequent to design-time, simple load
%% balancing techniques, such as ECMP~\cite{ecmp}, are employed to manage
%% link load. This simplistic set of approaches is not enough to avoid
%% links becoming overloaded.

%% One primary reason is evolution---in the network's control plane and
%% in its long-term traffic volumes. The former occurs because cloud and
%% ISP network customers' reachability, access control, and service
%% chaining requirements change
%% frequently~\cite{cloudcitesomething,theo:sigcomm11isp}, and these are
%% enforced by modifying existing control planes~\cite{propane, zeppelin,
%%   cpr}, inducing new network paths. Long-term traffic evolution occurs
%% because customers' workloads and service-level agreements change over
%% time~\cite{vl2}. While the network may be able to meet the demands
%% expected at design time, links may become overloaded under evolution
%% because there isn't enough capacity to carry the newly-induced traffic
%% patterns.  Thus, there is a need to constantly verify if link overload
%% can occur.

%% it is important to ensure there is sufficient provisioning as the
%% network evolves. \aditya{Need a pointer to empirical result here}

%% , which may in
%% turn induce application-level failures. This often compounds the
%% impact of a failure, making it difficult to understand the reason for,
%% and react to, application failures.


Second, checking for overload needs an estimated model of traffic
volumes, but actual traffic volumes may vary substantially around such
estimated values~\cite{fbflow}.  The network's control plane may
not be programmed to accommodate such variations, leading to overload
when traffic spikes unexpectedly along certain paths. Thus,
verification must account for \emph{variations} in measured traffic
volumes.
%% in
%% the estimated traffic volumes.

Third, the control planes in most networks are
\emph{distributed} in nature and use a mix of many different routing
protocols~\cite{mpa,theo:sigcomm11isp}, such as, Open Shortest Path
First (OSPF) and Border Gateway Protocol (BGP), configured in
low-level languages.  The complexity of the control plane
designs~\cite{mpa,complexity,theo:sigcomm11isp} and the interactions
among the constituent routing protocols make it difficult to reason
about the paths induced in a network under failures, and hence, about
potential link overload.


%% Provisioning is an expensive
%% endeavor and requires careful analysis--- over-provisioning link
%% capacities is a waste of money and resources.  Moreover, it is an
%% ardous and physical process; adding or removing link capacities can
%% potentially require manual effort of adding new fibers at physical
%% locations in the datacenter.




%% Another aspect to consider in datacenter settings is that  
%% {\bf Verification.} 
%% \loris{Garbled}
%% On the whole, it is hard to understand the joint impact of the control
%% plane, failures and traffic on provisioning.

Our first contribution is a verification framework that, given a
network and its set of distributed router configurations, an input
traffic matrix, and a bound on the variation of traffic volumes,
identifies failure scenarios that can cause traffic overload on some
network link. Not all failure scenarios are equally crucial. Thus, our
framework allows operators to focus on failure events that occur with
a certain minimum likelihood or involve $k$ or fewer links.


 %%  the
%% following question:

%% \noindent\fbox{%
%%     \parbox{\columnwidth}{%    \textbf{Q}.


%%     }%
%% }


%% Recent research works have tried to
%% redress some of the operators' concerns with regards to 
%% managing networks. Some of these works deal with 
%% reasoning about quantitative properties like network provisioning
%% under failures for SDNs~\cite{probnetkat,probnetkatpopl17} 
%% and other global routing adaptation strategies~\cite{robustvalidation}.
%% The other major line of research has been in modelling and verifying 
%% various qualitative properties of distributed 
%% network configurations~\cite{batfish,minesweeper,arc}. 

%% \begin{figure}
%% 	\centering
%% 	\includegraphics[width=0.8\columnwidth]{figures/qarc-venn.pdf}
%%     \caption{Landscape of research in quantitative network analysis and 
%%     verification and repair of network configurations.
%%     \todo{fill reference numbers, batfish}}
%% 	\label{fig:controlplane}
%% \end{figure}

%% Our first contribution is to bridge the gap between these two areas
%% by developing a framework

To conduct verification, we need a model of how the distributed
control plane reacts to failures and recomputes paths, and how traffic
load is spread across recomputed paths. Furthermore, analyzing the
model to determine potential load violations should be fast to enable
quick corrective action. To this end, we develop \name, a control plane model
for quantitative analysis, with a focus on analyzing link
overload. \name builds on a weighted digraph-based control plane
abstraction, and couples it with flow quantities and a novel mixed
integer linear program encoding. \name computes if links can become
overloaded under some failure without enumerating failures or 
traffic matrices that adhere to a given bounded variation; without
materializing paths under each failure; and without having to analyze
all links and considering the load contributed by all source-destination
pairs. These attributes help it achieve substantial speedup of $5-800\times$ 
over competitors~\cite{minesweeper}.

{\bf \noindent Repair.} Once verification has identified a potential load
violation, it is crucial to understand how to avoid overload to
mitigate impact on production traffic as much as possible. We call
this {\em repairing} the network.
One possible repair strategy is to synthesize new router
configurations to jointly ensure that both qualitative policies and
link-load goals hold under a class of failures. Aside from the
daunting theoretical complexity of this joint problem\footnote{Even
  the simpler problem of synthesizing a control plane that adheres to
  qualitative properties is NP-hard~\cite{zeppelin}.}, this approach
does not account for the fact that overload may be occurring because
the network's capacity is fundamentally constrained under failures. 
Alternatively, the approach of physically provisioning
new links to improve network capacity is time-consuming and
expensive.

Our second contribution is a repair strategy for proactively adding
capacity to avoid potential violations. We leverage the fact that 
production networks use optical links, and that recent advances~\cite{radwan}
allow us to dynamically change link capacities, with the caveat that
increased capacities can lead to increased packet losses. Our repair
strategy computes a minimal repair, where a set of links have ``new''
capacities such that packet loss rates are minimally affected, and no
network link is overloaded under certain failure scenarios and traffic
variation margins.  We develop a greedy capacity-update scheduling
algorithm that minimizes network disruptions during repair.

{\bf \noindent Analysis of real networks.} Today, very little is known about how
robust real-world network designs and control plane configurations are
in avoiding overload under observed and expected traffic patterns. To
this end, we conduct a detailed study of potential link overload in
production  networks.

We apply \name to 112 data center networks of a large service
provider, and 86 ISP networks from the Topology zoo. We use a mix of
real and synthetic control plane configurations. We apply a variety of
realistic traffic matrices and traffic volume variations.
We find that \emph{70\%} of the networks experience link overload
under 1 or 2 link failures with different traffic characteristics and
variation. We also find that the ISP networks in our study are
susceptible to link overload under failures if overall traffic
increases by 2-12\%, while datacenter networks require 5-35\% to
experience link overload.  We also show \name is practical: \name can
verify and repair all real-world networks we study in under an hour
(\secref{sec:evaluation}).
% We find that all networks are provisioned under all 
% 1-link failure scenarios, and 2 networks encounter 
% violations for 2- and 3-link failure scenarios. 



%\aditya{don't need a list of contributions}
% \minisection{Contributions}
% \vspace*{-1mm}
% \begin{itemize}
% \item A new abstraction \name (\arc with Quantities) used to verify
% network provisioning under failure scenarios (\secref{sec:qarc}).

% \item A repair strategy using dynamic capacity 
% optical links to prevent provisioning violations under failures
% (\secref{sec:repair}).

% \item A tractable parallelizable implementation of the verification and repair
% algorithms which leverages the \arc abstraction to minimize the search space.
% (\secref{sec:optimizations}).


% \end{itemize}
%\vspace{-2mm}
