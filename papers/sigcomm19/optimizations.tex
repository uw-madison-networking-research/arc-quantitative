%!TEX root = paper.tex
\section{Optimizations} \label{sec:optimizations}
We now describe different avenues for speeding up verification
performance which leverages the properties of \name abstraction and
constraints. Firstly, we can use \arc's shortest-distance path
property to efficiently minimize the traffic class ETGs. Then, we show
how to leverage minimized ETGs together \name's encoding of the
overload problem to partition the set of network links to consider for
overload and avoid considering the contribution of certain traffic
classes' load, to perform verification in an even faster,
parallel fashion.
%% \kausik{We also leverage the
  %% minimized ETGs for partitionizing such that the number of traffic
  %% classes in each partition is minimized to further speedup
  %% verification.}

\subsection{ETG Minimization}
By construction of an ETG, the traffic
will not traverse a path with a higher cost 
if one with a lower 
cost exists. Using this observation, 
for each traffic class, we can prune
the ETG and remove 
edges and nodes that will not be traversed 
under the targeted number of failures. 
Such a pruning reduces the size of the 
generated constraints and can result in faster
verification.
To illustrate this property, 
consider the network in \Cref{fig:repairex}. For 
any 1-link failure scenario, the traffic from S to T will never 
traverse through router $C$ on links $B \rightarrow C$ and 
$C\rightarrow A$ (because 2 shorter paths exist in the network). 
Thus, in the ETG, we can prune all nodes and edges corresponding 
to router $C$, forming a minimized ETG.


Identifying the redundant nodes and edges for general $k$ link
failures (or failure probability thresholds) can be challenging---the
naive approach of enumerating all $k$ link failures will be expensive.  
Instead, we modify
\name's encoding to perform ETG minimization.  The core insight of our
approach is to find the maximum shortest distance between the source
and the destination under any $k$ link failure scenario---all nodes
and edges which are farther than such distance will never be traversed
and can be pruned from the ETG.  Such equivalent
minimization cannot be performed efficiently when using a Minesweeper's
symbolic SMT-based abstraction: we would need to potentially check each node
and edge to detect if the node/edge is reachable under $k$ failures. 
In \arc, since the edge weights are not symbolic, we can eliminate a 
path if we know a "shorter path" exists.

\noindent\minisection{Minimization Constraints}
Unlike verification which deals with all traffic classes, 
minimization only involves constraints for a single 
traffic class. The minimization of multiple 
ETGs 
can be done in 
parallel since one ETG's 
routing does not depend on
other ETGs. 
For an ETG of traffic class $tc$, 
we use Constraints (\ref{eq:distance}) to 
specify upper bounds on distances
and Constraints (\ref{eq:failbound})
to bound the total number of failures.
The maximum shortest distance between the source and 
destination of the ETG can be found using 
the objective:
\begin{equation}
\texttt{maximize} ~~Dist(Src(tc), tc)
\end{equation}
Let us denote the objective value provided by the LP solver as $MaxDist$. 
Given an edge $n_1 \rightarrow n_2$, if the shortest path from source
to destination using the edge $n_1 \rightarrow n_2$ is greater than $MaxDist$, 
the edge will never be traversed under any $k$ link failures and can be pruned 
from the ETG. We check for such edges using the condition:
\begin{equation*}
SP(Src(tc), n_1) + W(n_1, n_2) + SP(n_2, Dst(tc)) > MaxDist
\end{equation*}
where $SP(n, n')$ denotes the length of shortest 
path between $n$ and $n'$ in the graph
(compute using Dijkstra's shortest path algorithm).
After pruning out the edges which are never traversed, 
we prune out the nodes which do not have 
incoming or outgoing edges to obtain a minimized ETG. 


\input{parallel}
