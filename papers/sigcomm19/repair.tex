%!TEX root = paper.tex

\section{Repair for Network Overload} \label{sec:repair}
The high-level goal of repair is to modify the network and its configurations so
that, given the traffic characteristics, the 
network is not overloaded
i.e., \emph{all link utilizations do not exceed capacity under 
all considered failure scenarios}.
One way to achieve this property is to repair the control plane.
However, modifying the control plane may cause violations of 
{\em qualitative} properties that the current
configuration satisfies (access control, waypointing for middleboxes,
etc.).  Finding policy compliant control-plane modifications is already
a computationally hard problem~\cite{cpr} and accounting for
network load which depends on all traffic classes in the network,
complicates the problem further.  Thus, we instead ask the following
question: \emph{can we ensure the network is not overloaded
  under failures without changing the control plane?}

% \loris{this para can go}
% Let's revisit the root cause of network load violations under
% failures: the link does not have adequate capacity to handle traffic
% routed through the link under a particular failure scenario.
% Datacenter traffic patterns~\cite{swan} and link failure
% characteristics~\cite{opticalfailures} are evolving over time; thus,
% static link capacities which was sufficient to handle the input traffic 
% may not suffice at a later point of time. 
% At the same time,
% repairing by adding/removing cables/fibres 
% (to add/remove capacity) is
% arduous and time-consuming.

%\loris{as well as this first sentence (modulo some rewriting)}
% We adopt a different approach. Fiber-optic links are increasingly
% prevalent in datacenter networks to meet bandwidth
% demands~\cite{facebook-fabric, jupiter-rising, microsoft-optical}.  
In a recent work~\cite{radwan}, Singh et. al make a case for dynamically
changing optical link capacities and show that by doing so operators
can extract 75-100\% capacity gains from their WAN optical links. Recent 
studies~\cite{facebook-fabric, jupiter-rising, microsoft-optical} 
have shown the prevalence of fiber-optic links in 
datacenter networks as well. 
We propose to repair the network by {\em changing
optical link capacities dynamically} to avoid link 
overload.  This approach poses virtually no
additional cost and no manual effort.

We address key practical issues in this repair strategy. First, the
trade-off of dynamically changing capacity is that packet loss
probability on the link increases with increasing capacity; thus, we
need to ensure we do not increase capacities beyond what is necessary
(\secref{sec:optrep}). Second, deploying new capacities to network
links is non-trivial as these links need to be powered down for the
capacity change; thus, we need to schedule the capacity changes
without causing links to be overloaded during the
repair (\secref{sec:repairschedule}).


% \aditya{This belongs in related work} Repairing network configurations to satisfy
% certain properties is a difficult task, and state-of-art works
% ~\cite{cpr, netcomplete} attempt to 
% repair configurations to satisfy various
% qualitative properties with additional management objectives, 
% for instance, minimizing the
% number of configuration changes in terms of lines of code or routers. 

% However, a key aspect of repair for network provisioning compared 
% to the state-of-art network configuration repair is that these works 
% deal with repair for different qualitative intents which are dependent 
% on a single (or small) set of traffic classes. On the other hand,
% network provisioning depends on all traffic classes; our repair 
% must be able to consider the effect of a particular configuration 
% change on all these traffic classes.



%%  and may require assistance from external entities---thus,
%% complicating the network operator's responsibilities.

\subsection{Computing Minimal Link Capacities} \label{sec:optrep}
Concretely, the first part of the repair problem is as follows:
\emph{
    Given a set of network configurations and input characteristics, find 
    the minimal set of links and the minimal capacity additions to these links
    such that the network is not overloaded under the given failure scenarios.
}

\Cref{fig:repairex} illustrates the optical link repair computed by
\name.  The traffic sent by class $S\rightarrow T$ is 90 Gbps. As we
can see, under 2-link failure scenarios (e.g., $D-A$ and $B-A$), some
links' utilization exceeds capacity (e.g., $B-C$). \name computes the
minimum capacity additions to the links (shown in green---e.g.,
\textcolor{green}{+10}); by increasing the capacities proactively, no
1- or 2-link failure scenario can cause violations.

We use \name to compute new capacities for the optical links such that
link utilization never exceeds the ``new'' capacity under the provided
failure scenarios. We use the \qarc constraints
(\secref{sec:flow}-\secref{sec:failure}) and  compute the minimum link
capacity that link $l$ should have by the following objective:
\begin{equation}
  \texttt{maximize} \sum_{tc \in TC} [Flow(l, tc) + \Delta(l, tc)] \times T(tc)
  \label{eq:maxutil}
\end{equation}
\qarc computes the maximum utilization for the link under the different
failure scenarios given the input traffic characteristics with bounded variation, 
which is the {\em minimum new capacity} required on the link
to ensure that the particular link is not overloaded. 
Note that, this capacity is not dependent on the capacity
of other links, as the distributed control plane chooses the ``best''
active path(s) under failures based on configuration parameters such
as static link weights and path lengths (and not the dynamic link
capacities, for instance). Thus, similar to verification, we can 
parallelize the phase of computing the dynamic link capacities.

Our repair approach identifies all links whose new capacity
exceeds the current one and schedules them for capacity
increase; all other capacities are kept at their current
levels\footnote{
Note that, we can also use \qarc to identify the links whose capacities can be 
reduced without causing violations under failures.
}. Note that the set of links whose capacities need an upgrade
form a \emph{minimal} set in the sense that if any of these links
have a capacity less than the computed capacity, the network is 
vulnerable, i.e., there exists a failure scenario under which the link in
question will be get overloaded.

\subsection{Repair Scheduling} \label{sec:repairschedule}
A deployment challenge hindering the usage of optical links with
dynamic link capacities is the design of the Bandwidth Variable
Transceivers (BVTs) in optical switches; BVTs are not optimized for
changing the modulation of the optical link (which is required to
change capacity).  State-of-the-art BVTs require the link to be
brought to a lower power state to change the modulation.  This
translates to the link being viewed as ``down'' from network
protocols' viewpoint when the BVT changes the link modulation. Singh
et.  al~\cite{radwan} report an average downtime of 68 seconds for a
link undergoing capacity change.  Thus, we could cause link
overloads if we changed the link capacities in an
indiscriminate manner.


We use the \name
encoding to extract a schedule which does not result in any
link overload during the process of changing the link
capacities. 
We compute a greedy 
schedule in ``stages'', such that in each
stage, we change the capacities of a maximal subset 
of links.  This can be done by adding a maximization 
objective for the $Fail(e)$ variables of \name---
as the links whose capacities are changed will be 
in a low-power or "failed" state.
We also ensure that every stage is overload-free---i.e., 
the utilization of active links 
do not exceed their capacities. We present the 
algorithm in \secref{sec:repairschedalg}.
Our  algorithm finds a schedule of repair
stages or will return unsatisfiable 
at some stage---i.e., we could not find a 
repair schedule for all links 
whose capacity needed to be changed 
without causing link overload.

\begin{figure}
	\centering
	\includegraphics[width=0.6\columnwidth]{figures/repairex.pdf}
  \vspace*{-4mm}  
  \caption{Example OSPF network where a link gets overloaded under 1 and 2 link 
    failure scenarios. The capacities in green are the minimum optical link 
    capacity additions computed by \qarc to ensure no violations occur under $\leq 2$ 
    failures.}
	\label{fig:repairex}
\end{figure}

% We assume that no other link failures happen during the
% period when we are deploying the repair.

% Ideally, we
% would want to compute a repair schedule with a small number
% of stages (as each stage can take around 60 seconds on average).
% To this end, we propose a \emph{greedy} algorithm 
% (\Cref{alg:greedyrepair}) where in each stage, we maximize the number of links whose
% capacities we can change without causing links to be overloaded 
% during that stage.
%We present our greedy recursive algorithm in
%\Cref{alg:greedyrepair}.




