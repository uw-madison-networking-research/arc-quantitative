def analyse() :
    arc_data = open("qarc-arc.txt")
    robust = dict()
    data = []
    for line in arc_data.readlines() :
        fields = line.rstrip().split("\t")
        if len(fields) < 7 : continue
        if fields[4].isdigit() and fields[5].isdigit() and fields[6].isdigit():
            data.append(int(fields[4]) + int(fields[5]) + int(fields[6]))
        
        if fields[2] != "1" :  continue
        name = fields[0].split("/")[0]
        if name not in robust : 
            robust[name] = []
        if len(fields) > 7 : 
            if "/" in fields[0] :
                if [fields[3], fields[7], fields[0].split("/")[1], fields[2]] not in robust[name] : 
                    robust[name].append([fields[3], fields[7], fields[0].split("/")[1], fields[2]])

    count = 0
    for name in robust : 
        for [a, b, c, d] in robust[name] :
            if a == "1" :
                if ["1.5", "2", c, d] in robust[name] :
                    print(name, c, d)
                    count += 1
                    break
    
    print(count, len(robust))
    print(len(data))
    print(sum(data) / float(len(data)))
    data = sorted(data)
    print(data[len(data)/2], data[len(data) - 1])


def analyse_verification() : 
    verification_data = open("verification.txt")
    robust = dict()
    for line in verification_data.readlines() :
        fields = line.rstrip().split("\t")
        if len(fields) < 2 : continue
        network = fields[0]
        if network not in robust : 
            robust[network] = dict()
        
        fails = int(fields[2])
        if fails not in robust[network] :
            robust[network][fails] = [0,0]

        if int(fields[6]) == 2 :
            robust[network][fails][0] += 1
        elif int(fields[6]) == 3 :
            robust[network][fails][1] += 1
    
    for net in robust : 
        print(net, robust[net])
    


analyse()