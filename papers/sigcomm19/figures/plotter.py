# Color Scheme
#e41a1c
#377eb8
#4daf4a
#984ea3
#ff7f00
#ffff33
#f781bf
from __future__ import division
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
import numpy as np

matplotlib.rcParams['text.usetex'] = True
label_size = 12
matplotlib.rcParams['xtick.labelsize'] = label_size 
matplotlib.rcParams['ytick.labelsize'] = label_size
markersize = 11

def adjustFigAspect(fig,aspect=1):
    '''
    Adjust the subplot parameters so that the figure has the correct
    aspect ratio.
    '''
    xsize,ysize = fig.get_size_inches()
    minsize = min(xsize,ysize)
    xlim = .4*minsize/xsize
    ylim = .4*minsize/ysize
    if aspect < 1:
        xlim *= aspect
    else:
        ylim /= aspect
    fig.subplots_adjust(left=.5-xlim,
                        right=.5+xlim,
                        bottom=.5-ylim,
                        top=.5+ylim)


def read_verification(network, figno) :
    verification_data = open("verification.txt")
    minimizetime = dict()
    addtime = dict()
    solvetime = dict()

    timeout = 0
    for line in verification_data.readlines() :
        fields = line.rstrip().split("\t")
        k = int(fields[2])
        if (fields[0] == network) :
            if k not in solvetime :
                solvetime[k] = []
            gs_t = (float(fields[5]) + float(fields[4]) + float(fields[3]))/1000.0
            if gs_t < 3600 : 
                solvetime[k].append(gs_t)
            else : 
                timeout += 1
                
    data = []
    for k in range(1, 4) :
        print(network, k, "median", sorted(solvetime[k])[len(solvetime[k])/2])
        data.append(solvetime[k])

    indices = range(1, 4)
    width = 0.3
    
    verificationPlot, axis = plt.subplots(figsize=(4.5,4.5))
    #adjustFigAspect(verificationPlot, 0.75)
    plt.boxplot(data, notch=True)
    
    plt.ylabel('Time (s)', fontsize=22)
    #plt.title('QARC Verification Times for ' + network)
    plt.tick_params(axis='both', labelsize=24)
    plt.ylim(ymin=0)
    if network == "CityCenter73" :
        plt.ylim(ymax=3000)
    plt.xlabel('\#Failures', fontsize=22)
    plt.xticks(indices, ["1", "2", "3"])
    axis.yaxis.grid(True)

    plt.savefig('verification_' + network + '.eps', format='eps', dpi=1000, bbox_inches='tight')

def read_minimization(figno, linkfails, netlist) :
    minimization_data = open("minimization.txt")

    speedup_data = dict()

    for line in minimization_data.readlines() :
        fields = line.rstrip().split("\t")
        k = int(fields[2])
        network = fields[0]

        if k == linkfails :
            nt = (float(fields[3]) + float(fields[4]))/1000.0
            ot = (float(fields[6]) + float(fields[7]) + float(fields[8]))/1000.0
            speedup = nt / ot
            if network not in speedup_data :
                speedup_data[network] = []
            speedup_data[network].append(speedup)

    savg = []
    sstd = []
    for net in netlist :
        savg.append(np.average(speedup_data[net]))
        sstd.append(np.std(speedup_data[net]))
        print(net, "minimize speedup", np.average(speedup_data[net]))


    indices = np.arange(len(netlist)) 
    names = ["DC1\n(22)", "DC2\n(60)", "DC3\n(70)", "Fat6\n(216)", "DC4\n(320)"]
    minimizeFig, axis = plt.subplots(figsize=(4.5,4.5))
    axis.bar(indices, savg, width=0.25, color='#377eb8', yerr=sstd)
    plt.ylabel('Speedup', fontsize=22)
    plt.xlabel('Networks', fontsize=22)
    plt.ylim(ymin=0)
    #plt.title('Verification times w/o Optimization for ' + network)
    plt.xticks(indices, names)
    plt.tick_params(axis='both', labelsize=20)
    plt.savefig('minimization' + str(linkfails) + '.eps', format='eps', dpi=1000, bbox_inches='tight')

    # plt.ylim(ymin=0)
    # if network == "Alpharetta21" :
    #     plt.ylim(ymax=25)
    # if network == "fattree-6" : 
    #     plt.ylim(ymax=2800)

    # axis.axvline([2.5], linestyle='--', color='black')
    # axis.axvline([4.5], linestyle='--', color='black')

    # ind = 1
    # for patch in bplot['boxes']:
    #     if ind % 2 == 0 :
    #         # optimized
    #         patch.set_facecolor('#4daf4a')
    #     else :
    #         # unoptimized
    #         patch.set_facecolor('#377eb8')
    #     ind += 1

    # legend_elements = [Patch(facecolor='#377eb8', label='no ETG-M'), 
    #                     Patch(facecolor='#4daf4a', label='ETG-M')]
    # axis.legend(handles=legend_elements, loc='upper center', ncol=2, 
    # fontsize=22, bbox_to_anchor=[0.5, 1.25], columnspacing=1.0)
    # 
    # for k in range(1, 4) :
    #     sumtimes = 0.0
    #     for i in range(len(minimize[k])) : 
    #         sumtimes += minimize[k][i]
    #     print("Minimize", network, k, sumtimes/len(minimize[k]))

def read_probability(network, figno) :
    probability_data = open("probability.txt")
    minimizetime = dict()
    addtime = dict()
    solvetime = dict()

    timeout = 0
    for line in probability_data.readlines() :
        fields = line.rstrip().split("\t")
        p = float(fields[2])
        if p == 0.01 : 
            k = 1
        if p == 0.0001 : 
            k = 2
        if p == 0.000001 : 
            k = 3
        if (fields[0] == network) :
            if k not in solvetime :
                solvetime[k] = []
            gs_t = (float(fields[5]) + float(fields[4]) + float(fields[3]))/1000.0
            if gs_t < 3600 : 
                solvetime[k].append(gs_t)
            else : 
                timeout += 1
                
    data = []
    for k in solvetime :
        data.append(solvetime[k])

    indices = range(1, 4)
    width = 0.3
    
    probabilityPlot, axis = plt.subplots(figsize=(4.5,4.5))
    #adjustFigAspect(probabilityPlot, 0.75)
    plt.boxplot(data, notch=True)
    plt.ylabel('Time (s)', fontsize=20)
    plt.xlabel('Probability Threshold', fontsize=20)
    #plt.title('QARC probability Times for ' + network)
    plt.tick_params(axis='both', labelsize=20)
    plt.ylim(ymin=0)
    plt.ylim(ymax=170)
    plt.xticks(indices, ["$10^{-2}$", "$10^{-4}$", "$10^{-6}$"])
    axis.yaxis.grid(True)

    plt.savefig('probability_' + network + '.eps', format='eps', dpi=1000, bbox_inches='tight')
    

def read_repair_plot(figno) : 
    repair_data = open("repair.txt")
    opt = 0
    sched = 0
    links = [100000, 0]
    stages = [10000, 0]
    repairtime = dict()
    for line in repair_data.readlines() :
        fields = line.rstrip().split("\t")
        k = int(fields[2])
        if k == 2 :
            network = fields[0]
            if network not in repairtime :
                repairtime[network] = []
            
            if fields[3] == "TIMEOUT" : 
                rt = 3700
            else :
                rt = (float(fields[3]) + float(fields[4]))/1000.0
                opt += float(fields[3])/1000.0
                sched += float(fields[4])/1000.0

                if int(fields[6]) > links[1] :
                    links[1] = int(fields[6])
                if int(fields[6]) != 0 and int(fields[6]) < links[0] :
                    links[0] = int(fields[6])

                if fields[5] == "true" : 
                    if int(fields[7]) > stages[1] :
                        stages[1] = int(fields[7])
                    if int(fields[7]) != 0 and int(fields[7]) < stages[0] :
                        stages[0] = int(fields[7])
                

            if rt < 3600 : 
                repairtime[network].append(rt)
    
    print("OPT", "SCHED", opt, sched, opt/(opt+sched))
    print("Links", links)
    print("Stages", stages)
    data = []
    data.append(repairtime["Alpharetta21"])
    data.append(repairtime["Tulsa21"])
    data.append(repairtime["CityCenter73"])
    data.append(repairtime["fattree-6"])
    indices = [1,2,3,4]

    repairPlot, axis = plt.subplots(figsize=(4.5,4.5))
    #adjustFigAspect(repairPlot, 1.5)
    plt.boxplot(data, notch=True)
    plt.ylim(ymin=0)
    plt.ylim(ymax=1500)
    plt.xticks(indices, ["N2", "N3", "N4", "Fat6"])
    plt.ylabel('Time (s)', fontsize=20)
    plt.xlabel('Networks', fontsize=20)
    #plt.title('QARC probability Times for ' + network)
    plt.tick_params(axis='both', labelsize=20)
    axis.axvline([1.5], linestyle='--', color='black')
    axis.axvline([2.5], linestyle='--', color='black')
    axis.axvline([3.5], linestyle='--', color='black')
    
    plt.savefig('repair.eps', format='eps', dpi=1000, bbox_inches='tight')



def read_minesweeper(figno) :
    minesweeper_data = open("minesweeper.txt")
    qarc_time = dict()
    minesweeper_time = dict()
    for network in ["Plano9", "Alpharetta21"] :
        qarc_time[network] = dict()
        minesweeper_time[network] = dict()

    for line in minesweeper_data.readlines() :
        fields = line.rstrip().split("\t")
        if len(fields) < 2 : continue
        network = fields[0]
        etgs = int(fields[2])

        while etgs not in qarc_time[network] : 
            qarc_time[network][etgs] = []
            minesweeper_time[network][etgs] = []
        
        qarc_time[network][etgs].append(float(fields[3]))
        minesweeper_time[network][etgs].append(float(fields[4]))
    
    for network in ["Plano9", "Alpharetta21"] :
        minesweeper_plot, (ax) = plt.subplots(1, 1, figsize=(5, 4.5))

        x = []
        q_averages = []
        q_stdevs = []
        m_averages = []
        m_stdevs = []
        
        for key in range(5, 100, 5) :
            if key not in qarc_time[network] : continue
            x.append(key)
            q_averages.append(np.average(qarc_time[network][key]))
            q_stdevs.append(np.std(qarc_time[network][key]))
            m_averages.append(np.average(minesweeper_time[network][key]))
            m_stdevs.append(np.std(minesweeper_time[network][key]))
            print(key, m_averages[len(x)-1]/q_averages[len(x)-1])

        ax.plot(x, q_averages, '#377eb8',marker="D", markersize=markersize, label="ARC")
        ax.errorbar(x, q_averages, color='#377eb8', yerr=q_stdevs, linestyle="None")
        ax.plot(x, m_averages, '#d95f02',marker="o", markersize=markersize, label="Minesweeper")
        ax.errorbar(x, m_averages, color='#d95f02', yerr=m_stdevs, linestyle="None")
        plt.ylabel('Time (s)', fontsize = 22)
        plt.xlabel('\#Classes', fontsize = 22)
        #plt.title('QARC Repair Times for ' + network)
        if network == "Alpharetta21" : 
            plt.ylim(ymax=3200)
        plt.ylim(ymin=0)
        plt.xlim(xmin=0, xmax=x[len(x) - 1]+3)
        plt.tick_params(axis='both', labelsize=24)
        #plt.xticks(indices, ["k=2", "k=3"])

        
        ax.legend(['ARC','Minesweeper'], loc='upper center', ncol=2, 
            fontsize=20, bbox_to_anchor=[0.5, 1.2], columnspacing=1.0)

        plt.savefig('minesweeper_' + network + '.eps', format='eps', dpi=1000, bbox_inches='tight')

        figno += 1

def verification_parallel(figno, linkfails, netclass, ntype) :
    verificationTimes = dict()
    verification_data = open("verification-parallel.txt")
    timeouts = 0

    for line in verification_data.readlines() :
        fields = line.rstrip().split("\t")
        if len(fields) < 8 : continue
        network = fields[0]
        vtime = int(fields[6])/1000
        fail = int(fields[3])
        if vtime >= 3600 or int(fields[7]) == 9 :
            timeouts += 1
        if fail != linkfails : continue
        if network not in netclass : continue
        if network not in networkSizes : continue
        if network not in verificationTimes:
            verificationTimes[network] = [[], []]
        if int(fields[7]) == 2 :
            verificationTimes[network][0].append(vtime)
            networkresilience[network][0] += 1
        
        elif int(fields[7]) == 3:
            verificationTimes[network][1].append(vtime)
            networkresilience[network][1] += 1

        
    
    print("Timeouts", timeouts)
    sortedNetworks = []
    
    for net in verificationTimes :
        sortedNetworks.append([net, networkSizes[net][0]])
    
    print("Networks in graph", len(verificationTimes), len(netclass), len(sortedNetworks))

    sortedNetworks.sort(key=lambda x: x[1]) # Sort network by sizes

    networkindex = dict()
    for ind in range(len(sortedNetworks)) :
        networkindex[sortedNetworks[ind][0]] = ind

    satTimes = []
    satNetworks = []
    unsatTimes = []
    unsatNetworks = []
    for net in verificationTimes :
        if len(verificationTimes[net][0]) > 0 :
            satNetworks.append(networkindex[net])
            satTimes.append(sum(verificationTimes[net][0])/len(verificationTimes[net][0]))

        if len(verificationTimes[net][1]) > 0 : 
            unsatNetworks.append(networkindex[net])
            unsatTimes.append(sum(verificationTimes[net][1])/len(verificationTimes[net][1]))
    
    print(satNetworks, unsatNetworks)
    verificationPlot, axis = plt.subplots(figsize=(4.5,4))
    plt.plot(satNetworks, satTimes, 'o', color='blue', label="Yes")
    plt.plot(unsatNetworks, unsatTimes, 'x', color='red', label="No")
    plt.ylabel('Time (s)', fontsize = 22)
    if nettype == "dc" :
        plt.xlabel('DC Networks', fontsize = 22)
    elif nettype == "isp" :
        plt.xlabel('ISP Networks', fontsize = 22)
    axis.set_yscale("log")
    plt.ylim(ymin=0.001, ymax=3600)
    plt.xlim(xmin=0)
    plt.tick_params(axis='both', labelsize=24)
    axis.legend(['Yes','No'], loc='upper center', ncol=2, 
        fontsize=20, bbox_to_anchor=[0.5, 1.2], columnspacing=0.5)
    plt.savefig(ntype + '_verification_times' + str(linkfails) + '.eps', format='eps', dpi=1000, bbox_inches='tight')
        
# figno = 1
# for network in ["Tulsa21", "CityCenter73", "fattree-6", "Plano9", "Alpharetta21"]:
#     read_verification(network, figno)
#     figno += 1
#     read_minimization(network, figno)
#     figno += 1

# figno += 1
# read_repair_plot(figno)
# figno += 1
# read_probability("fattree-6", figno)
# figno += 1
# read_minesweeper(figno)
isp_networks = ["Epoch", "Forthnet","Garr199901","Gblnet","Geant2001","Geant2009","Geant2012","Getnet","Globalcenter","Goodnet","Gridnet","GtsCzechRepublic","GtsPoland","HostwayInternational","HurricaneElectric","Ibm","Ilan","Integra","Itnet","Janetbackbone","JanetExternal","Jgn2Plus","KentmanAug2005","Kreonet","LambdaNet","Layer42","Litnet","Marnet","Mren","Napnet","Navigata","Netrail","NetworkUsa","Niif","Noel","Nordu1989","Nordu1997","Nsfnet","Packetexchange","Padi","Peer1","Psinet","Quest","Renam","Renater1999","Renater2001","Renater2004","Renater2006","Renater2008","Renater2010","Restena","Rhnet","Rnp","Sago","Sanren","Savvis","Singaren","Spiralight","Sprint","Telcove","Telecomserbia","Twaren","Uran","VisionNet","Xeex","Xspedius","York", "Aarnet","Abilene","Abvt", "Aconet","Agis","Ai3","Amres","Ans","Arn","Arpanet196912","Arpanet19706","Atmnet","Bandcon","Basnet","Bbnplanet","Bics","Biznet","Cesnet1993","Cesnet1999","Cesnet2001","Cesnet200304","Cesnet200511","Cesnet201006","Claranet","CrlNetworkServices","Cynet","Darkstrand","Dataxchange","DeutscheTelekom"]

dc_networks = [ 
"microsoft/microsoft/cys01-x3sb-xcgd-4-1a_1b/2014-10-24-22-16-53","microsoft/microsoft/cys01-x3sb-xcgd-4-1a_1b/2015-02-09-18-29-07","microsoft/microsoft/ams-6nx-1a_1b/2015-06-10-04-50-14","microsoft/microsoft/ams-6nx-1a_1b/2014-10-17-11-16-37","microsoft/microsoft/co1-x3sb-xcgd-3-1a_1b/2014-10-21-08-37-35","microsoft/microsoft/co1-x3sb-xcgd-3-1a_1b/2014-11-11-09-57-00","microsoft/microsoft/bn3-x3sb-xcgd-2-1a_1b/2014-10-21-08-36-20","microsoft/microsoft/bn3-x3sb-xcgd-2-1a_1b/2014-10-24-19-35-25","microsoft/microsoft/by2-x3sb-xcg-4-1a_1b/2015-01-24-05-53-42","microsoft/microsoft/by2-x3sb-xcg-4-1a_1b/2014-06-16-12-01-32","microsoft/microsoft/cys01-x3sb-xcg-5-1a_1b/2014-08-19-07-06-12","microsoft/microsoft/cys01-x3sb-xcg-5-1a_1b/2014-09-02-08-34-59","microsoft/microsoft/ch1-x3sb-xcg-3-1a_1b/2015-01-24-05-28-12","microsoft/microsoft/ch1-x3sb-xcg-3-1a_1b/2014-06-17-11-11-28","microsoft/microsoft/sin-6nx-cis-1a_1b/2014-01-10-12-33-49","microsoft/microsoft/sin-6nx-cis-1a_1b/2014-10-24-11-01-11","microsoft/microsoft/dm2-x7ax-mpgsc-1a_1b/2013-09-18-12-13-25","microsoft/microsoft/dm2-x7ax-mpgsc-1a_1b/2013-08-08-16-31-57","microsoft/microsoft/co2-x3sb-xcgd-3-1a_1b/2014-11-11-15-37-37","microsoft/microsoft/co2-x3sb-xcgd-3-1a_1b/2014-10-21-08-36-28","microsoft/microsoft/bay-6nx-hot-1a_1b/2013-10-18-13-35-03","microsoft/microsoft/bay-6nx-hot-1a_1b/2013-09-27-16-44-38","microsoft/microsoft/am2-6nx-cis-1a_1b/2014-10-24-09-19-54","microsoft/microsoft/am2-6nx-cis-1a_1b/2014-01-31-14-03-46","microsoft/microsoft/dm2-x3sb-xcgd-2-1a_1b/2014-02-05-11-09-03","microsoft/microsoft/dm2-x3sb-xcgd-2-1a_1b/2013-11-11-18-14-04","microsoft/microsoft/bay-x3sb-xcgd-2-1a_1b/2014-10-24-18-02-35","microsoft/microsoft/bay-x3sb-xcgd-2-1a_1b/2014-10-21-08-37-12","microsoft/microsoft/cys01-x3sb-xcgd-3-1a_1b/2014-10-24-22-10-32","microsoft/microsoft/cys01-x3sb-xcgd-3-1a_1b/2014-10-21-08-37-05","microsoft/microsoft/sn1-x3sb-xcgd-1-1a_1b/2014-10-09-17-17-45","microsoft/microsoft/sn1-x3sb-xcgd-1-1a_1b/2014-10-15-16-58-20","microsoft/microsoft/bn3-x3sb-xcgd-3-1a_1b/2014-10-21-08-36-56","microsoft/microsoft/bn3-x3sb-xcgd-3-1a_1b/2014-10-24-19-43-45","microsoft/microsoft/hk2-x3sb-xcg-2-1a_1b/2015-02-24-19-46-05","microsoft/microsoft/hk2-x3sb-xcg-2-1a_1b/2015-05-06-08-24-07","microsoft/microsoft/dm2-x3sb-xcg-8-1a_1b/2014-08-14-06-49-10","microsoft/microsoft/dm2-x3sb-xcg-8-1a_1b/2014-08-13-06-55-58","microsoft/microsoft/bl2-x3sb-xcg-3-1a_1b/2014-12-16-02-43-10","microsoft/microsoft/bl2-x3sb-xcg-3-1a_1b/2014-06-17-14-38-38","microsoft/microsoft/bl2-x7x-ppt-1a_1b/2013-09-18-12-35-57","microsoft/microsoft/bl2-x7x-ppt-1a_1b/2013-09-06-10-18-41","microsoft/microsoft/am2-x3sb-xcg-5-1a_1b/2014-12-11-02-04-30","microsoft/microsoft/am2-x3sb-xcg-5-1a_1b/2014-12-10-23-29-01","microsoft/microsoft/am2-x3sb-xcg-6-1a_1b/2014-12-11-00-07-10","microsoft/microsoft/am2-x3sb-xcg-6-1a_1b/2014-12-11-11-41-52","microsoft/microsoft/hkn-x3sb-xcg-8-1a_1b/2014-09-12-07-38-59","microsoft/microsoft/hkn-x3sb-xcg-8-1a_1b/2014-05-07-02-27-29","microsoft/microsoft/bn1-x7x-1a_1b/2013-11-12-17-28-18","microsoft/microsoft/bn1-x7x-1a_1b/2014-06-04-11-59-01","microsoft/microsoft/blu-x3sb-xcg-13-1a_1b/2014-08-13-18-55-13","microsoft/microsoft/blu-x3sb-xcg-13-1a_1b/2014-08-13-07-02-15","microsoft/microsoft/blu-x3sb-xcgd-8-1a_1b/2014-11-11-15-20-01","microsoft/microsoft/blu-x3sb-xcgd-8-1a_1b/2014-10-21-08-34-24","microsoft/microsoft/am3-x3sb-xcgd-2-1a_1b/2013-11-11-21-16-43","microsoft/microsoft/am3-x3sb-xcgd-2-1a_1b/2014-05-31-05-39-13","microsoft/microsoft/by2-x3sb-xcg-7-1a_1b/2014-08-26-07-02-42","microsoft/microsoft/by2-x3sb-xcg-7-1a_1b/2014-05-12-14-49-15","microsoft/microsoft/bl2-6nb-1a_1b/2013-08-28-11-29-23","microsoft/microsoft/bl2-6nb-1a_1b/2014-05-16-00-56-10","microsoft/microsoft/db3-x3sb-xcg-9-1a_1b/2014-07-31-06-34-32","microsoft/microsoft/db3-x3sb-xcg-9-1a_1b/2014-07-19-06-15-51","microsoft/microsoft/dfw04-x3sb-xcg-1-1a_1b/2014-11-20-23-48-11","microsoft/microsoft/dfw04-x3sb-xcg-1-1a_1b/2014-11-20-17-56-41","microsoft/microsoft/cys01-x3sb-xcgd-2-1a_1b/2014-10-24-22-01-55","microsoft/microsoft/cys01-x3sb-xcgd-2-1a_1b/2014-10-21-08-37-10","microsoft/microsoft/dm2-x3sb-xcgd-8-1a_1b/2014-11-11-17-03-38","microsoft/microsoft/dm2-x3sb-xcgd-8-1a_1b/2014-10-21-08-37-19","microsoft/microsoft/ch1-x3sb-xcg-4-1a_1b/2014-06-17-11-26-37","microsoft/microsoft/ch1-x3sb-xcg-4-1a_1b/2015-01-24-05-35-16","microsoft/microsoft/ch1-6nx-cis-2a_2b/2014-10-24-10-01-26","microsoft/microsoft/ch1-6nx-cis-2a_2b/2014-01-31-14-20-35","microsoft/microsoft/sin05-96gmr-mms-1a_1b/2014-12-18-18-07-47","microsoft/microsoft/sin05-96gmr-mms-1a_1b/2013-08-22-14-22-28","microsoft/microsoft/dm2-x3sb-xcgd-9-1a_1b/2014-11-11-17-37-30","microsoft/microsoft/dm2-x3sb-xcgd-9-1a_1b/2014-10-21-08-37-12","microsoft/microsoft/wst-76g-mms-1a_1b/2014-08-08-19-14-51","microsoft/microsoft/wst-76g-mms-1a_1b/2014-08-08-20-45-04","microsoft/microsoft/ch1-6nx-cis-3a_3b/2014-01-31-14-37-31","microsoft/microsoft/ch1-6nx-cis-3a_3b/2014-10-24-10-03-30","microsoft/microsoft/db4-x3sb-xcgd-2-1a_1b/2013-12-04-18-18-32","microsoft/microsoft/db4-x3sb-xcgd-2-1a_1b/2014-01-03-13-31-30","microsoft/microsoft/dm2-x3sb-xcgd-7-1a_1b/2014-11-18-17-23-51","microsoft/microsoft/dm2-x3sb-xcgd-7-1a_1b/2014-11-13-17-33-00","microsoft/microsoft/hkn-x3sb-xcg-102-1a_1b/2015-06-25-20-32-12","microsoft/microsoft/hkn-x3sb-xcg-102-1a_1b/2015-06-26-10-33-00","microsoft/microsoft/ams-x3sb-xcgd-4-1a_1b/2014-12-16-14-14-12","microsoft/microsoft/ams-x3sb-xcgd-4-1a_1b/2014-10-24-14-54-49","microsoft/microsoft/db3-6nx-cis-1a_1b/2014-10-24-10-46-21","microsoft/microsoft/db3-6nx-cis-1a_1b/2014-01-31-14-08-04","microsoft/microsoft/sn1-6nx-cis-3a_3b/2014-02-24-13-22-50","microsoft/microsoft/sn1-6nx-cis-3a_3b/2014-02-11-13-25-17","microsoft/microsoft/sn2-x3sb-xcgd-2-1a_1b/2014-10-21-08-36-26","microsoft/microsoft/sn2-x3sb-xcgd-2-1a_1b/2014-11-11-14-47-09","microsoft/microsoft/am3-x3sb-xcgd-3-1a_1b/2013-12-04-18-16-43","microsoft/microsoft/am3-x3sb-xcgd-3-1a_1b/2015-06-21-02-22-48","microsoft/microsoft/by2-x7x-hot-1a_1b/2014-11-24-09-15-56","microsoft/microsoft/by2-x7x-hot-1a_1b/2015-02-13-00-34-50","microsoft/microsoft/blu-x3sb-xcgd-7-1a_1b/2014-10-21-08-36-37","microsoft/microsoft/blu-x3sb-xcgd-7-1a_1b/2014-11-11-14-44-49","microsoft/microsoft/co2-x3sb-xcg-2-1a_1b/2014-05-02-17-45-57","microsoft/microsoft/co2-x3sb-xcg-2-1a_1b/2014-05-07-16-26-50","microsoft/microsoft/bay-x7x-hot-1a_1b/2014-06-05-00-11-34","microsoft/microsoft/bay-x7x-hot-1a_1b/2013-10-04-11-09-18","microsoft/microsoft/ams-x3sb-xcgd-2-1a_1b/2013-11-11-21-00-25","microsoft/microsoft/ams-x3sb-xcgd-2-1a_1b/2014-05-31-06-00-03","microsoft/microsoft/ch1-6nx-cis-1a_1b/2014-01-31-14-13-48","microsoft/microsoft/ch1-6nx-cis-1a_1b/2014-10-24-09-54-38","microsoft/microsoft/db3-x7x-hot-1a_1b/2013-10-14-10-27-16","microsoft/microsoft/db3-x7x-hot-1a_1b/2013-10-01-11-08-33","microsoft/microsoft/db4-x3sb-xcgd-3-1a_1b/2014-12-16-14-31-51","microsoft/microsoft/db4-x3sb-xcgd-3-1a_1b/2014-11-18-17-38-33","microsoft/microsoft/ams-6gmr-mms-1a_1b/2013-11-06-18-29-54","microsoft/microsoft/ams-6gmr-mms-1a_1b/2013-10-10-21-02-38","microsoft/microsoft/by2-x3sb-xcgd-3-1a_1b/2015-06-21-02-15-41","microsoft/microsoft/by2-x3sb-xcgd-3-1a_1b/2015-06-25-18-17-35","microsoft/microsoft/sn1-x3ax-frb-4a_4b/2015-05-08-01-05-48","microsoft/microsoft/sn1-x3ax-frb-4a_4b/2015-03-18-17-00-39","microsoft/microsoft/sin-6nmr-mms-2a_2b/2013-11-04-14-22-44","microsoft/microsoft/sin-6nmr-mms-2a_2b/2013-10-11-01-46-57","microsoft/microsoft/sn1-6nx-hot-1a_1b/2013-11-21-13-26-08","microsoft/microsoft/sn1-6nx-hot-1a_1b/2013-12-03-13-35-02","microsoft/microsoft/bl2-x3sb-xcg-4-1a_1b/2014-06-17-14-38-41","microsoft/microsoft/bl2-x3sb-xcg-4-1a_1b/2014-12-16-02-48-03","microsoft/microsoft/bn1-x3sb-xcgd-3-1a_1b/2014-11-07-13-43-10","microsoft/microsoft/bn1-x3sb-xcgd-3-1a_1b/2014-10-21-08-37-02","microsoft/microsoft/blu-x3sb-xcgd-6-1a_1b/2014-10-17-06-23-31","microsoft/microsoft/blu-x3sb-xcgd-6-1a_1b/2014-10-17-09-39-11","microsoft/microsoft/bl2-6gmr-mms-1a_1b/2013-10-11-01-02-56","microsoft/microsoft/bl2-6gmr-mms-1a_1b/2013-11-01-18-43-12","microsoft/microsoft/cys01-x7ax-1a_1b/2014-06-04-11-56-07","microsoft/microsoft/cys01-x7ax-1a_1b/2015-04-02-13-20-18","microsoft/microsoft/bn3-x3sb-xcgd-1-1a_1b/2014-10-24-19-25-45","microsoft/microsoft/bn3-x3sb-xcgd-1-1a_1b/2014-10-21-08-36-38","microsoft/microsoft/ams-x3sb-xcgd-3-1a_1b/2014-07-01-06-48-19","microsoft/microsoft/ams-x3sb-xcgd-3-1a_1b/2013-12-04-18-15-45","microsoft/microsoft/bn3-x3sb-xcg-1-1a_1b/2014-08-13-07-03-26","microsoft/microsoft/bn3-x3sb-xcg-1-1a_1b/2014-09-13-07-47-42","microsoft/microsoft/db3-6nx-cis-2a_2b/2013-11-08-10-03-32","microsoft/microsoft/db3-6nx-cis-2a_2b/2014-01-31-14-12-05","microsoft/microsoft/dm2-x3sb-xcgd-6-1a_1b/2014-10-21-08-36-34","microsoft/microsoft/dm2-x3sb-xcgd-6-1a_1b/2014-11-11-15-35-51","microsoft/microsoft/cys01-x3sb-xcg-2-1a_1b/2014-08-13-06-58-43","microsoft/microsoft/cys01-x3sb-xcg-2-1a_1b/2014-08-14-07-03-21","microsoft/microsoft/blu-x3sb-xcg-2-1a_1b/2015-02-07-04-22-48","microsoft/microsoft/blu-x3sb-xcg-2-1a_1b/2014-06-17-16-04-33","microsoft/microsoft/db5-x3sb-xcgd-1-1a_1b/2015-04-09-11-41-34","microsoft/microsoft/am3-x3sb-xcgd-4-1a_1b/2014-10-16-05-15-46","microsoft/microsoft/chg-96gmr-mms-1a_1b/2013-08-21-19-17-29","microsoft/microsoft/chg-96gmr-mms-1a_1b/2013-09-13-10-19-53","microsoft/microsoft/hel01-x3sb-xcgd-1-1a_1b/2015-06-21-03-02-39","microsoft/microsoft/hel01-x3sb-xcgd-1-1a_1b/2015-04-09-11-41-51","microsoft/microsoft/bl2-x7x-hot-1a_1b/2013-09-06-09-27-24","microsoft/microsoft/bl2-x7x-hot-1a_1b/2013-09-10-13-31-30","microsoft/microsoft/cys01-x3sb-xcgd-1-1a_1b/2014-10-24-21-52-58","microsoft/microsoft/cys01-x3sb-xcgd-1-1a_1b/2014-10-21-08-36-43","microsoft/microsoft/cos01-x3sb-xcg-1-1a_1b/2014-11-20-17-34-37","microsoft/microsoft/cos01-x3sb-xcg-1-1a_1b/2014-11-20-23-58-31","microsoft/microsoft/ams-6nx-cis-1a_1b/2014-10-24-09-44-37","microsoft/microsoft/ams-6nx-cis-1a_1b/2014-01-31-14-46-51","microsoft/microsoft/co1-x3sb-xcgd-2-1a_1b/2014-02-05-11-08-41","microsoft/microsoft/co1-x3sb-xcgd-2-1a_1b/2013-11-11-18-12-04","microsoft/microsoft/bay-x7x-ppt-1a_1b/2013-09-23-11-10-56","microsoft/microsoft/bay-x7x-ppt-1a_1b/2014-01-07-10-45-27","microsoft/microsoft/cys01-x3sb-xcg-1-1a_1b/2014-05-14-05-25-59","microsoft/microsoft/cys01-x3sb-xcg-1-1a_1b/2014-05-19-12-41-20","microsoft/microsoft/blu-x3sb-xcg-1-1a_1b/2014-06-17-15-51-14","microsoft/microsoft/blu-x3sb-xcg-1-1a_1b/2015-01-24-03-54-20","microsoft/microsoft/SaoPaolo","microsoft/microsoft/db3-x3sb-xcgd-4-1a_1b/2013-12-04-18-17-24","microsoft/microsoft/db3-x3sb-xcgd-4-1a_1b/2015-06-21-02-24-35","microsoft/microsoft/sin-x3sb-xcg-4-1a_1b/2014-07-23-03-07-46","microsoft/microsoft/sin-x3sb-xcg-4-1a_1b/2014-07-19-02-58-29","microsoft/microsoft/co2-6nx-cis-1a_1b/2014-05-16-01-01-01","microsoft/microsoft/co2-6nx-cis-1a_1b/2014-10-24-10-21-46","microsoft/microsoft/sn2-x3sb-xcg-102-1a_1b/2014-10-09-18-05-25","microsoft/microsoft/sn2-x3sb-xcg-102-1a_1b/2014-10-14-11-27-48","microsoft/microsoft/blu-x3sb-xcgd-3-1a_1b/2013-11-11-18-08-08","microsoft/microsoft/blu-x3sb-xcgd-3-1a_1b/2014-02-05-11-05-36","microsoft/microsoft/tul04-x3sb-xcg-1-1a_1b/2014-11-20-17-28-20","microsoft/microsoft/tul04-x3sb-xcg-1-1a_1b/2014-11-20-23-30-53","microsoft/microsoft/by2-x3sb-xcg-3-1a_1b/2015-01-24-05-29-09","microsoft/microsoft/by2-x3sb-xcg-3-1a_1b/2014-06-16-12-01-58","microsoft/microsoft/bn1-x3sb-xcg-2-1a_1b/2014-07-19-04-24-09","microsoft/microsoft/bn1-x3sb-xcg-2-1a_1b/2014-06-23-14-28-08","microsoft/microsoft/co2-6nx-hot-2a_2b/2013-12-17-17-35-54","microsoft/microsoft/co2-6nx-hot-2a_2b/2013-12-11-21-26-58","microsoft/microsoft/hkn-6nx-cis-1a_1b/2014-01-31-13-53-02","microsoft/microsoft/hkn-6nx-cis-1a_1b/2014-10-24-10-55-06","microsoft/microsoft/bay-x3sb-xcgd-1-1a_1b/2014-10-24-18-01-55","microsoft/microsoft/bay-x3sb-xcgd-1-1a_1b/2015-02-09-18-45-11","microsoft/microsoft/atl05-x3sb-xcg-1-1a_1b/2014-11-20-17-21-52","microsoft/microsoft/atl05-x3sb-xcg-1-1a_1b/2014-11-20-23-37-06","microsoft/microsoft/Colorado","microsoft/microsoft/Beijing", 
"Plano9", "CityCenter73", "Tulsa21","Alpharetta21"]

netsk1 = dict()
netsk2 = dict()
for line in open("verification-parallel.txt").readlines() :
    fields = line.rstrip().split("\t")
    if len(fields) < 8 : continue
    network = fields[0]
    fail = int(fields[3])
    if int(fields[7]) <= 3 :
        if fail == 1 :
            netsk1[network] = True
        if fail == 2 :
            netsk2[network] = True


networkSizes = dict()
network_data = open("QARC-Networks")
for line in network_data.readlines() :
    fields = line.rstrip().split("\t")
    networkname = fields[1]
    if int(fields[2]) <= 4 or int(fields[3]) < 10 :
        continue
    if networkname not in netsk1 or networkname not in netsk2 : 
        continue
    networkSizes[networkname] = (int(fields[2]), int(fields[3]))

# networks1cut = []
# for line in open("problem1.txt").readlines() :
#     fields = line.rstrip().split("\t")
#     if len(fields) < 8 : continue
#     network = fields[0]
#     if int(fields[7]) == 3: 
#         if network not in networks1cut :
#             networks1cut.append(network)

# networks2cut = []
# for line in open("problem2.txt").readlines() :
#     fields = line.rstrip().split("\t")
#     if len(fields) < 8 : continue
#     network = fields[0]
#     if int(fields[7]) == 3: 
#         if network not in networks2cut :
#             networks2cut.append(network)

# print(len(networks1cut), len(networks2cut))
# for net in networks1cut :
#     if net not in networks2cut :
#         print(net)




networkClass = dc_networks
nettype = "dc"
linkfails = 2


networkresilience = dict()
for net in networkClass :
    networkresilience[net] = [0,0]

#verification_parallel(1, linkfails, networkClass, nettype)
faulty = 0
faulty50 = 0
totnets = 0
for net in networkresilience :
    if sum(networkresilience[net]) == 0 :
        continue
    
    totnets += 1
    if networkresilience[net][0] > 0 :
        faulty += 1
        
    if networkresilience[net][0] > networkresilience[net][1] :
        faulty50 +=1
            
print("Resilience", nettype, linkfails, totnets, faulty, faulty50)

def verification_parallel_speedup(figno, linkfails, netlist):
    verificationTimes = dict()
    parallel_speedup_data = open("verification-parallel-speedup.txt")

    prev = []
    for line in parallel_speedup_data.readlines() :
        fields = line.rstrip().split("\t")
        if len(fields) < 7 : continue
        network = fields[0]
        if linkfails != int(fields[3]) : continue
        vtime = int(fields[6])/1000
        if int(fields[7]) == 9 :
            vtime = 3600
        if int(fields[2]) == 5: # Parallel
            prev = [network, vtime]
        if prev[0] == network and int(fields[2]) == 1: # Single
            if network not in verificationTimes :
                verificationTimes[network] = []
            verificationTimes[network].append(vtime/prev[1])
            prev = []
        
    savg = []
    sstd = []   
    for net in netlist :
        savg.append(np.average(verificationTimes[net]))
        sstd.append(np.std(verificationTimes[net]))
        print(net, "parallel speedup", np.median(verificationTimes[net]))

    indices = np.arange(len(netlist)) 
    names = ["DC1\n(20)","Abilene\n(28)", "DC2\n(70)", "Fat6\n(216)", "DC3\n(320)"]
    parspeedupFig, axis = plt.subplots(figsize=(4.5,4.5))
    rects = axis.bar(indices, savg, width=0.25, color='#377eb8', yerr=sstd)
    plt.ylim(ymin=0, ymax=15)
    plt.xlabel('Networks', fontsize=22)
    plt.ylabel('Speedup', fontsize=22)
    plt.xticks(indices, names)
    plt.tick_params(axis='both', labelsize=20)
    plt.savefig('parallel_speedup' + str(linkfails) + '.eps', format='eps', dpi=1000, bbox_inches='tight')

def verification_partitioning_speedup(figno, netlist):
    verificationTimes = dict()
    partitioning_data = open("partitioning.txt")

    prev = []
    verificationTimes = dict()
    for line in partitioning_data.readlines() :
        fields = line.rstrip().split("\t")
        if len(fields) < 7 : continue
        network = fields[0]
        vtime = int(fields[6])/1000
        if int(fields[7]) == 9 :
            vtime = 3600
        if prev == []: # Parallel
            prev = [network, vtime]
        elif prev[0] == network: # Single
            if network not in verificationTimes : 
                verificationTimes[network] = []
            verificationTimes[network].append(vtime/prev[1])
            prev = []

    savg = []
    sstd = []   
    for net in netlist :
        savg.append(np.average(verificationTimes[net]))
        sstd.append(np.std(verificationTimes[net]))
        print(net, "partition speedup", np.average(verificationTimes[net]))


    indices = np.arange(len(netlist)) 
    names = ["Abilene\n(28)", "DC1\n(70)", "Fat6\n(216)", "DC2\n(320)", "Fat8\n(512)"]
    parspeedupFig, axis = plt.subplots(figsize=(4.5,4.5))
    rects = axis.bar(indices, savg, width=0.25, color='#377eb8', yerr=sstd)
    plt.xlabel('Networks', fontsize=22)
    plt.ylabel('Speedup', fontsize=22)
    plt.xticks(indices, names)
    plt.ylim(ymin=0, ymax=2.5)
    autolabel(rects, axis, ["68\%", "50\%", "42\%", "36\%", "51\%"])
    plt.tick_params(axis='both', labelsize=20)
    plt.savefig('partition_speedup.eps', format='eps', dpi=1000, bbox_inches='tight')


def autolabel(rects, axis, vals):
    """
    Attach a text label above each bar displaying its height
    """
    index = 0
    for rect in rects:
        height = rect.get_height()
        axis.text(rect.get_x() + rect.get_width()*1.2, 1.05*height,
                '%s' % str(vals[index]),
                ha='center', va='bottom')
        index += 1



# print("k=1")
#verification_parallel_speedup(3, 1, ["Plano9","Abilene", "Alpharetta21", "fattree-6", "CityCenter73"])
# print("k=2")
# verification_parallel_speedup(4, 2, ["Plano9","Abilene", "Alpharetta21", "fattree-6", "CityCenter73"])

#verification_partitioning_speedup(5, ["Abilene", "Plano9", "fattree-6", "CityCenter73", "Tulsa21"])

def read_minimization(figno, linkfails, netlist) :
    minimization_data = open("minimization.txt")

    speedup_data = dict()

    for line in minimization_data.readlines() :
        fields = line.rstrip().split("\t")
        k = int(fields[2])
        network = fields[0]

        if k == linkfails :
            nt = (float(fields[3]) + float(fields[4]))/1000.0
            ot = (float(fields[6]) + float(fields[7]) + float(fields[8]))/1000.0
            speedup = nt / ot
            if network not in speedup_data :
                speedup_data[network] = []
            speedup_data[network].append(speedup)

    savg = []
    sstd = []
    for net in netlist :
        savg.append(np.average(speedup_data[net]))
        sstd.append(np.std(speedup_data[net]))
        print(net, "minimize speedup", np.average(speedup_data[net]))


    indices = np.arange(len(netlist)) 
    names = ["DC1\n(22)", "DC2\n(60)", "DC3\n(70)", "Fat6\n(216)", "DC4\n(320)"]
    minimizeFig, axis = plt.subplots(figsize=(4.5,4.5))
    axis.bar(indices, savg, width=0.25, color='#377eb8', yerr=sstd)
    plt.ylabel('Speedup', fontsize=22)
    plt.xlabel('Networks', fontsize=22)
    plt.ylim(ymin=0)
    #plt.title('Verification times w/o Optimization for ' + network)
    plt.xticks(indices, names)
    plt.tick_params(axis='both', labelsize=20)
    plt.savefig('minimization' + str(linkfails) + '.eps', format='eps', dpi=1000, bbox_inches='tight')


#read_minimization(6, 1, ["Plano9",  "Tulsa21", "Alpharetta21", "fattree-6", "CityCenter73"])

# read_minesweeper(7)


def traffic_matrix(figno, netlist) :
    gravityData = dict()
    fbflowData = dict()
    for net in netlist :
        gravityData[net] = [0,0]
        fbflowData[net] = [0,0]
    
    for line in open("dc-matrix.txt").readlines() :
        fields = line.rstrip().split("\t")
        if len(fields) < 7 : continue
        net = fields[0]
        if fields[1] == "gravity" :
            if int(fields[7]) == 2: 
                gravityData[net][0] += 1
            if int(fields[7]) == 3:
                gravityData[net][1] += 1
        
        if fields[1] == "fbflow" :
            if int(fields[7]) == 2: 
                fbflowData[net][0] += 1
            if int(fields[7]) == 3:
                fbflowData[net][1] += 1
    
    
    for net in netlist :
        print(net, 100*(gravityData[net][0])/sum(gravityData[net]), 100*(fbflowData[net][0])/sum(fbflowData[net]))


#traffic_matrix(8, ["Plano9", "CityCenter73", "Tulsa21", "Alpharetta21", "fattree-6", "fattree-8", "microsoft/microsoft/Colorado", "microsoft/microsoft/Beijing"])

def read_repair(figno, netlist) : 
    repair_data = open("repair-parallel.txt")
    repairTimes1 = dict()
    repairTimes2 = dict()
    timeouts = 0

    for net in netlist :
        repairTimes1[net] = []
        repairTimes2[net] = []

    for line in repair_data.readlines() :
        fields = line.rstrip().split("\t")
        network = fields[0]
        capTime = int(fields[6])/1000
        schedTime = int(fields[7])/1000
        if capTime <= 0 or capTime >= 3600 :
            timeouts += 1
            continue
        if schedTime <= 0 or schedTime >= 3600 :
            timeouts += 1
            continue

        if network not in netlist : continue
        
        repairTime = capTime + schedTime
        linkfails = int(fields[3])
        if linkfails == 1: 
            repairTimes1[network].append(repairTime)
        if linkfails == 2:
            repairTimes2[network].append(repairTime)
    
    r1avg = []
    r1std = []
    r2avg = []
    r2std = []
    for net in netlist :
        r1avg.append(np.average(repairTimes1[net]))
        r1std.append(np.std(repairTimes1[net]))
        r2avg.append(np.average(repairTimes2[net]))
        r2std.append(np.std(repairTimes2[net]))
        print(net, np.std(repairTimes2[net]))


    indices = np.arange(len(netlist))
    repairPlot, axis = plt.subplots(figsize=(5,4))
    rects1 = axis.bar(indices, r1avg, width=0.25, color='#377eb8', yerr=r1std, label='k=1')
    rects1 = axis.bar(indices+0.25, r2avg, width=0.25, color='#4daf4a', yerr=r2std, label='k=2')
    plt.ylim(ymin=0)
    plt.xticks(indices + 0.25, ["DC2\n(O(10))", "DC3\n(O(10))", "Geant\n(76)", "Fat6\n(216)", "DC4\n(O(100))"])
    plt.ylabel('Time (s)', fontsize=20)
    plt.xlabel('Networks', fontsize=20)
    # #plt.title('QARC probability Times for ' + network)
    axis.legend(['k=1','k=2'], loc='upper center', ncol=2, 
            fontsize=20, bbox_to_anchor=[0.5, 1.3], columnspacing=0.5)
    plt.tick_params(axis='both', labelsize=20)

    plt.savefig('repair.eps', format='eps', dpi=1000, bbox_inches='tight')


read_repair(9, ["Alpharetta21", "microsoft/microsoft/Colorado", "Geant2001", "fattree-6", "CityCenter73"])


def delta_plot(figno, netclass, ntype) :
    deltas = dict()
    verification_data = open("delta.txt")

    for line in verification_data.readlines() :
        fields = line.rstrip().split("\t")
        if len(fields) < 8 : continue
        network = fields[0]
        fail = int(fields[3])
        delta = 100*float(fields[4])
        if network not in netclass : continue
        if network not in networkSizes : continue
        if delta > 100 or int(fields[7]) == 3: continue
        if network not in deltas:
            deltas[network] = [[], []]
        if int(fields[3]) == 1 :
            deltas[network][0].append(delta)
        
        elif int(fields[3]) == 2:
            deltas[network][1].append(delta)
            
    sortedNetworks = []
    
    for net in deltas :
        if net not in networkSizes : continue
        sortedNetworks.append([net, networkSizes[net][0]])
    
    sortedNetworks.sort(key=lambda x: x[1]) # Sort network by sizes

    networkindex = dict()
    for ind in range(len(sortedNetworks)) :
        networkindex[sortedNetworks[ind][0]] = ind

    delta1 = []
    nets1 = []
    delta2 = []
    nets2 = []
    for net in deltas :
        if len(deltas[net][0]) > 0 :
            nets1.append(networkindex[net])
            delta1.append(np.average(deltas[net][0]))

        if len(deltas[net][1]) > 0 : 
            nets2.append(networkindex[net])
            delta2.append(np.average(deltas[net][1]))
    
    deltaPlot, axis = plt.subplots(figsize=(4.5,4))
    plt.plot(nets1, delta1, 'o', color='#377eb8', label='k=1')
    plt.plot(nets2, delta2, '+', color='#4daf4a', markersize=10, label='k=2')
    plt.ylabel('Variation (\%)', fontsize = 22)
    if ntype == "dc" :
        plt.xlabel('DC Networks', fontsize = 22)
    else :
        plt.xlabel('ISP Networks', fontsize = 22)
    # axis.set_yscale("log")
    # plt.ylim(ymin=0.001, ymax=3600)
    plt.xlim(xmin=0)
    plt.tick_params(axis='both', labelsize=24)
    axis.legend(['k=1','k=2'], loc='upper center', ncol=2, 
            fontsize=20, bbox_to_anchor=[0.5, 1.25], columnspacing=1.0)
    plt.savefig(ntype + '_delta.eps', format='eps', dpi=1000, bbox_inches='tight')


# networkClass = dc_networks
# nettype = "dc"
# delta_plot(9, networkClass, nettype)


# networkClass = isp_networks
# nettype = "isp"
# delta_plot(10, networkClass, nettype)